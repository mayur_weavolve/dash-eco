function subscribePopup() {
    var subscribe = $('#subscribe'),
        time = subscribe.data('time');
    setTimeout(function() {
        if (subscribe.length > 0) {
            subscribe.addClass('active');
            $('body').css('overflow', 'hidden');
        }
    }, time);
    $('.ps-popup__close').on('click', function(e) {
        e.preventDefault();
        $(this)
            .closest('.ps-popup')
            .removeClass('active');
        $('body').css('overflow', 'auto');
    });
    // $('#subscribe').on('click', function(event) {
    //     if (!$(event.target).closest('.ps-popup__content').length) {
    //         subscribe.removeClass('active');
    //         $('body').css('overflow-y', 'auto');
    //     }
    // });
}

function createCookie(name, value, days) {
    var expires;

    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 1 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0)
            return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

$(window).on('DOMContentLoaded', function() {
     var userPincode = localStorage['pincode'];
    pincode = userPincode;
    $("#pincode-input").val(pincode);
    $(".pincode").text(userPincode);
    if(pincode === undefined){
        subscribePopup();
        $(".ps-popup__close").hide();
    }else{
        if( readCookie( 'loadedAlready' ) != 'YES' ) {
            $("#pincode-input").val(pincode);
            // $(".setpincode").click();
            createCookie( 'loadedAlready', 'YES', 1);
        }
    }
    $('body').addClass('loaded');
});

function setPincode(){
    var pincodeVal = $("#pincode-input").val();
    if(!isNaN(pincodeVal) && pincodeVal.length == 6 ){
        var data = {};
        data['pincode'] = pincodeVal;
        $(".loader").show();
        $.ajax({
            url: '/setpincode',
            type: 'post',
            data: data,
            success: function(res){
                res = JSON.parse(res);
                if(res.status === 'success'){
                    localStorage['pincode'] = pincodeVal;
                    localStorage['latitude'] = res.latitude;
                    localStorage['longitude'] = res.longitude;
                    $(".pincode").text(pincodeVal);
                    $('#subscribe').removeClass('active');
                    $('body').css('overflow-y', 'auto');
                    window.location.href = window.location.href;
                }else{
                    $(".error-pin").text(res.message);
                }
                $(".loader").hide();
            },
            error: function(res){
                $(".loader").hide();
                $(".error-pin").text('Please enter valid pincode.');
            }
        });
    }else{
        $(".error-pin").text('Please enter valid pincode.');
    }
}

 //This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
    function calcDistance(lat1, lon1, lat2, lon2)
    {
        var R = 6371; // km
        var dLat = toRad(lat2-lat1);
        var dLon = toRad(lon2-lon1);
        var lat1 = toRad(lat1);
        var lat2 = toRad(lat2);

        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c;
        return d;
    }

    // Converts numeric degrees to radians
 function toRad(Value)
 {
     return Value * Math.PI / 180;
 }


 function deleteCartPopup(pval) {
     $("#deleteProductValue").val(pval);
    var cartDelete = $('#cartDelete'),
        time = cartDelete.data('time');
    setTimeout(function() {
        if (cartDelete.length > 0) {
            cartDelete.addClass('active');
            $('body').css('overflow', 'hidden');
        }
    }, time);
    $('.ps-popup__close').on('click', function(e) {
        e.preventDefault();
        $(this)
            .closest('.ps-popup')
            .removeClass('active');
        $('body').css('overflow', 'auto');
    });
    $('.delete-cart-popup-close').on('click', function(e) {
        e.preventDefault();
        $(this)
            .closest('.ps-popup')
            .removeClass('active');
        $('body').css('overflow', 'auto');
    });
}

$("body").on("click",".setpincode",function(e){
    setPincode();
});

$("body").on("click",".product-quickview",function(e){
   var productData = JSON.parse($(this).attr('data-value'));
   var productImage = '';
   if(productData.productImage  != null){
       productImage = '/product_photo/'+productData.productImage;
   }
   var productLink = '/'+productData.product_name.toLowerCase().replace(/[^a-zA-Z0-9]/g,"-")+'/'+productData.product_id+'_'+productData.vendorID;
   var html = '';
   html+= '<div class="ps-product__header"><div class="ps-product__thumbnail" data-vertical="false">';
   html+= '<div class="ps-product__images" data-arrow="true">';
   html+= '<div class="item"><img src="/product_photo/'+productData.productImage+'" alt=""></div>';
   html+= '</div></div><div class="ps-product__info"><h1>'+productData.product_name+'</h1><div class="ps-product__meta">';
   html+= '<p>Brand:<a href="javascript:void(0);"> '+productData.manufacturer+' </a></p><div class="ps-product__rating"><select class="ps-rating" data-read-only="true"><option value="1">1</option><option value="1">2</option><option value="1">3</option><option value="1">4</option><option value="2">5</option></select><span>(1 review)</span></div></div>';
   html+= '<h4 class="ps-product__price sale">₹'+productData.final_price+' <del>₹'+productData.mrp+'</del></h4><div class="ps-product__desc"><p>Sold By:<a href="javascript:void(0);"><strong> '+productData.nickname+' </strong></a></p><ul class="ps-list--dot">'+productData.description+'</ul></div>';
   html+= '<div class="ps-product__shopping"><a class="ps-btn ps-btn--black" href="'+productLink+'">Explore Product</a>';
    //    html+= '<a class="ps-btn" href="#">Buy Now</a></div></div></div>';
    $(".quick-product-data").html(html);
    $('select.ps-rating').each(function() {
        var readOnly;
        if ($(this).attr('data-read-only') == 'true') {
            readOnly = true;
        } else {
            readOnly = false;
        }
        $(this).barrating({
            theme: 'fontawesome-stars',
            readonly: readOnly,
            emptyValue: '0',
        });
    });
  $("#product-quickview").modal('show');
  $(".ps-product--quickview ").css('display','block');

});

$("body").on("click",".code-btn",function(e){
    var id = $(this).attr('data-sid');
    $.ajax({
            url: '/user/live-order/getsecuritycode/'+id,
            type: 'get',
            data : {
                    '_token': '{{csrf_token()}}'
                    },
            success: function(data){
              data = JSON.parse(data);
                    if(data.status == 'success'){
                        $('.code-btn[data-sid="'+id+'"]').html(data.code);
                    }else{
                        $('.code-btn[data-sid="'+id+'"]').html('code');
                    }
            }
    });
});


