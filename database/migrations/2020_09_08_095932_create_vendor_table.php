<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor', function (Blueprint $table) {
            $table->id();
            $table->string('user_id')->nullable();
            $table->string('name')->nullable();
            $table->string('business_name')->nullable();
            $table->string('nickname')->nullable();
            $table->string('whatsapp_no')->nullable();
            $table->string('mobileno')->nullable();
            $table->string('address')->nullable();
            $table->string('gst_no')->nullable();
            $table->string('postcode')->nullable();
            $table->string('photo')->nullable();
            $table->string('business_category')->nullable();
            $table->string('business_established')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor');
    }
}
