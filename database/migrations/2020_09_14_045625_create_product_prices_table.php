<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_prices', function (Blueprint $table) {
            $table->id();
            $table->string('vender_id')->nullable();
            $table->string('product_id')->nullable();
            $table->float('mrp')->nullable();
            $table->float('discount')->nullable();
            $table->float('final_price')->nullable();
            $table->string('warranty')->nullable();
            $table->string('installation_included')->nullable();
            $table->string('exchange')->nullable();
            $table->string('free_delivery')->nullable();
            $table->string('mode')->nullable();
            $table->string('size')->nullable();
            $table->string('colour')->nullable();
            $table->string('stock')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_prices');
    }
}
