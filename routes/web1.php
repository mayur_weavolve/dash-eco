<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('layouts.user');
// });



//frontend
Auth::routes();

// open pages for all users

Route::get('/', 'FrontUserController@comingsoon')->name('comingsoon');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/comingsoon', 'FrontUserController@comingsoon')->name('comingsoon');
Route::get('/become-a-vendor', 'FrontUserController@become_a_vendor')->name('become_a_vendor');
Route::get('/signup', 'FrontUserController@signup')->name('signup');
Route::get('/signup/check', 'FrontUserController@check_signup')->name('check_signup');
Route::get('/delivery-or-pickup', 'FrontUserController@deliveryOrPickup')->name('deliveryOrPickup');
Route::get('/checkmobile/{mobile}','FrontUserController@checkUser')->name('check_user');
Route::post('/add/review','FrontUserController@addReview')->name('add_to_review');
Route::post('/productlist','FrontUserController@productlist')->name('productlist');
Route::get('/productlist','FrontUserController@productlist')->name('productlist');
Route::get('/productlisthome','FrontUserController@productlisthome')->name('productlisthome');
Route::get('/my-account','FrontUserController@myaccount')->name('my_account');
Route::post('/my-account/add','FrontUserController@userInfoAdd')->name('user_info_add');
Route::get('/address','FrontUserController@userAddress')->name('user_address');
Route::post('/address/add','FrontUserController@userAddressAdd')->name('user_address_add');
Route::get('/verification','FrontUserController@verification')->name('verification');
Route::get('/verify/user','FrontUserController@verify_user')->name('verify_user');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/front', 'FrontController@front')->name('front');
Route::get('/about-us', 'FrontController@aboutus')->name('aboutus');
Route::get('/career','FrontController@career')->name('career');
Route::get('/contact-us','FrontController@contactUs')->name('contactUs');
Route::get('/faq','FrontController@faq')->name('faq');
Route::get('/how-to-order','FrontController@how_to_order')->name('how_to_order');
Route::get('/return-refund-policy','FrontController@returnRefundPolicy')->name('returnRefundPolicy');
Route::get('/terms-and-conditions','FrontController@termCondition')->name('termCondition');
Route::get('/why-dash','FrontController@why_dash')->name('why_dash');
Route::post('/get-in-touch','FrontController@sendMail')->name('send_email');
Route::post('/subscribe','FrontController@subscribe')->name('subscribe');
Route::post('/setpincode','FrontUserController@setpincode')->name('setpincode');
Route::post('/add/cart','FrontMyCartController@addToCart')->name('add_to_cart');
Route::get('/my-cart','FrontMyCartController@my_cart')->name('my_cart');
Route::get('/delete/cart/{id}','FrontMyCartController@delete_cart')->name('delete_cart');
Route::get('/cart/delete/{id}','FrontMyCartController@cart_delete')->name('cart_delete');
Route::post('/update/cart','FrontMyCartController@updateCart')->name('update_cart');
Route::get('/checkout','FrontCheckOutController@checkout')->name('checkout');
Route::post('/add/checkout','FrontCheckOutController@add_to_checkout')->name('add_to_checkout');


Route::get('/vendor/home','FrontVendorProductController@vendorHome')->name('vendor_home');
Route::get('vendor/product','FrontVendorProductController@VendorProductList')->name('vendor_product_list');
Route::get('vendor/product/add','FrontVendorProductController@vendor_add_product')->name('vendor_add_product');
Route::post('vendor/product/add','FrontVendorProductController@vendor_save_product_add')->name('vendor_save_product');
Route::get('vendor/product/price','FrontVendorProductController@product_price')->name('vendor_product_price');
Route::get('vendor/product/price/add/{id}','FrontVendorProductController@vendor_product_price_add')->name('vendor_product_price_add');
Route::post('vendor/product/price/add/{id}','FrontVendorProductController@vendor_save_product_price')->name('vendor_save_product_price');
Route::post('/vendor/getproductattribute','FrontVendorProductController@get_product_attribute')->name('vendor_get_product_attribute');
Route::get('vendor/product/edit/{id}/{vid}','FrontVendorProductController@productDetailEditView')->name('product_detail_edit_view');
Route::post('vendor/product/edit/{id}/{vid}','FrontVendorProductController@edit_save')->name('product_detail_edit_save');
Route::post('/vendor/editproductattributevalue','FrontVendorProductController@edit_vendor_product_attributeValue')->name('edit_vendor_product_attributeValue');
Route::get('vendor/recent/order','FrontVendorOrderController@VendorRecentOrderList')->name('vendor_recent_order_list');
Route::get('vendor/completed/order','FrontVendorOrderController@VendorCompletedOrderList')->name('vendor_completed_order_list');


Route::get('live/order/list','FrontVendorOrderController@LiveOrder')->name('live_order');
Route::get('live/order/getsecuritycode/{id}','FrontVendorOrderController@getsecuritycode')->name('getsecuritycode');
Route::get('completed/order/list','FrontVendorOrderController@CompletedOrder')->name('completed_order');
Route::get('/order/detail/{id}','FrontVendorOrderController@orderDetail')->name('vendor_order_detail');
Route::get('track/{id}','FrontVendorOrderController@trackInfo')->name('track');
Route::get('orderAccept/{id}','FrontVendorOrderController@orderAccept')->name('orderAccept');
Route::get('orderReject/{id}','FrontVendorOrderController@orderReject')->name('orderReject');

Route::get('track','FrontVendorOrderController@tracking_view')->name('track_view');
Route::post('track/info','FrontVendorOrderController@tracking_info')->name('tracking_info');
Route::post('/update/status','FrontVendorOrderController@updateStatus')->name('update_status');
Route::post('/update/image','FrontVendorOrderController@updateImage')->name('update_image');

Route::post('/payment/{orderId}','PaymentController@payment_check');
Route::get('/payment/{orderId}','PaymentController@payment')->name('payment');
Route::get('/payment-success','PaymentController@payment_success')->name('payment_success');
Route::get('/payment-fail','PaymentController@payment_fail')->name('payment_fail');


//Admin panel
Route::group(['middleware' => 'auth'], function(){
    Route::get('welcome','HomeController@index')->name('welcome');

    Route::get('/backend/admin/vendor/list','VendorController@vendor')->name('vendor');
    Route::get('/backend/admin/vendor/add','VendorController@add')->name('vendor_add');
    Route::post('/backend/admin/vendor/add','VendorController@save_vendor');
    Route::get('/backend/admin/vendor/edit/{id}','VendorController@edit')->name('vendor_edit');
    Route::post('/backend/admin/vendor/edit/{id}','VendorController@edit_save');
    Route::get('/backend/admin/vendor/delete/{id}','VendorController@delete')->name('vendor_delete');

    Route::get('/backend/admin/product/list','ProductController@product')->name('product');
    Route::get('/backend/admin/product/upload','ProductController@add')->name('product_add');
    Route::post('/backend/admin/product/upload','ProductController@save_product');
    Route::get('/backend/admin/product/add','ProductController@add_product')->name('add_product');
    Route::post('/backend/admin/product/add','ProductController@save_product_add');
    Route::get('/backend/admin/product/edit/{id}','ProductController@edit')->name('product_edit');
    Route::post('/backend/admin/product/edit/{id}','ProductController@edit_save')->name('product_edit_save');
    Route::get('/backend/admin/product/delete/{id}','ProductController@delete')->name('product_delete');

    Route::post('/getproductattribute','ProductController@get_product_attribute')->name('get_product_attribute');
    Route::post('/editproductattributevalue','ProductController@edit_product_attributeValue')->name('edit_product_attributeValue');

    Route::get('/backend/admin/product/price/{id}','ProductPriceController@product_price')->name('product_price');
    Route::get('/backend/admin/product/price/{id}/add','ProductPriceController@add')->name('product_price_add');
    Route::post('/backend/admin/product/price/{id}/add','ProductPriceController@save_product_price');
    Route::get('/backend/admin/product/price/edit/{id}/{vid}','ProductPriceController@edit')->name('product_price_edit');
    Route::post('/backend/admin/product/price/edit/{id}/{vid}','ProductPriceController@edit_save');
    Route::get('/backend/admin/product/price/delete/{id}/{vid}','ProductPriceController@delete')->name('delete_price');

    Route::get('/backend/admin/category/list','CategoryController@category')->name('category');
    Route::get('/backend/admin/category/add','CategoryController@add')->name('category_add');
    Route::post('/backend/admin/category/add','CategoryController@save_add');
    Route::get('/backend/admin/category/edit/{id}','CategoryController@edit')->name('category_edit');
    Route::post('/backend/admin/category/edit/{id}','CategoryController@edit_save');
    Route::get('/backend/admin/category/delete/{id}','CategoryController@category_delete')->name('category_delete');

    Route::get('/backend/admin/subcategory/list','SubcategoryController@list')->name('subcategory');
    Route::get('/backend/admin/subcategory/add','SubcategoryController@add')->name('subcategory_add');
    Route::post('/backend/admin/subcategory/add','SubcategoryController@save_add');
    Route::get('/backend/admin/subcategory/edit/{id}','SubcategoryController@edit')->name('subcategory_edit');
    Route::post('/backend/admin/subcategory/edit/{id}','SubcategoryController@edit_save');
    Route::get('/backend/admin/subcategory/delete/{id}','SubcategoryController@subcategory_delete')->name('subcategory_delete');

    Route::get('/backend/admin/user/list','UserController@user')->name('user');
    Route::get('/backend/admin/user/add','UserController@add')->name('user_add');
    Route::post('/backend/admin/user/add','UserController@save_add')->name('user_save');
    Route::get('/backend/admin/user/edit/{id}','UserController@edit')->name('user_edit_view');
    Route::post('/backend/admin/user/edit','UserController@user_edit')->name('user_edit');
    Route::get('/backend/admin/user/delete/{id}','UserController@user_delete')->name('user_delete');

    Route::get('/backend/admin/order/list','OrderController@order')->name('order_list');
    Route::get('/backend/admin/order/detail/{id}','OrderController@detail')->name('order_detail');
    Route::post('/backend/update/status','OrderController@updateStatus')->name('backend_update_status');
    
    Route::get('/backend/admin/review/list','ReviewController@review')->name('review_list');
    Route::get('/backend/admin/review/edit/{id}','ReviewController@review_edit_view')->name('review_edit_view');
    Route::post('/backend/admin/review/edit','ReviewController@review_edit')->name('review_edit');


});


// please set this 3 route in last, do not change without my permission and not add any line below this comment
Route::get('/category/{category}/{id}','FrontUserController@productListForCategory')->name('product_list');
Route::get('/{category}/{subcategory}/{id}','FrontUserController@productListForSubcategory')->name('product_list_subcategory');
Route::get('/{product}/{id}','FrontUserController@productDetail')->name('product_detail');
Route::get('/search/list','FrontUserController@searchList')->name('search_list');


