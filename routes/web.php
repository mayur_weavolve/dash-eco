<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('layouts.user');
// });



//frontend
Auth::routes();

// open pages for all users
Route::get('/', 'FrontController@front')->name('front');
// Route::get('/', 'FrontUserController@comingsoon')->name('comingsoon');
Route::get('/comingsoon', 'FrontUserController@comingsoon')->name('comingsoon');
Route::get('/become-a-vendor', 'FrontUserController@become_a_vendor')->name('become_a_vendor');
Route::get('/signup', 'FrontUserController@signup')->name('signup');
Route::get('/signup/check', 'FrontUserController@check_signup')->name('check_signup');
Route::get('/delivery-or-pickup', 'FrontUserController@deliveryOrPickup')->name('deliveryOrPickup');
Route::get('/checkmobile/{mobile}','FrontUserController@checkUser')->name('check_user');
Route::post('/add/review','FrontUserController@addReview')->name('add_to_review');
Route::post('/productlist','FrontUserController@productlist')->name('productlist');
Route::get('/productlist','FrontUserController@productlist')->name('productlist');
Route::get('/productlisthome','FrontUserController@productlisthome')->name('productlisthome');
Route::get('/verification','FrontUserController@verification')->name('verification');
Route::get('/verify/user','FrontUserController@verify_user')->name('verify_user');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/about-us', 'FrontController@aboutus')->name('aboutus');
Route::get('/career','FrontController@career')->name('career');
Route::get('/contact-us','FrontController@contactUs')->name('contactUs');
Route::get('/faq','FrontController@faq')->name('faq');
Route::get('/how-to-order','FrontController@how_to_order')->name('how_to_order');
Route::get('/return-refund-policy','FrontController@returnRefundPolicy')->name('returnRefundPolicy');
Route::get('/terms-and-conditions','FrontController@termCondition')->name('termCondition');
Route::get('/why-dash','FrontController@why_dash')->name('why_dash');
Route::post('/get-in-touch','FrontController@sendMail')->name('send_email');
Route::post('/subscribe','FrontController@subscribe')->name('subscribe');
// user open access pages
Route::post('/setpincode','FrontUserController@setpincode')->name('setpincode');
Route::post('/add/cart','FrontMyCartController@addToCart')->name('add_to_cart');
Route::get('/user/my-cart','FrontMyCartController@my_cart')->name('my_cart');
Route::get('/delete/cart/{id}','FrontMyCartController@delete_cart')->name('delete_cart');
Route::get('/cart/delete/{id}','FrontMyCartController@cart_delete')->name('cart_delete');
Route::post('/update/cart','FrontMyCartController@updateCart')->name('update_cart');
// vendor order accept - reject
Route::get('/orderAccept/{id}','FrontVendorOrderController@orderAccept')->name('orderAccept');
Route::get('/orderReject/{id}','FrontVendorOrderController@orderReject')->name('orderReject');

// tracking order - vendor
Route::get('/track','FrontVendorOrderController@tracking_view')->name('track_view');
Route::post('/track/info','FrontVendorOrderController@tracking_info')->name('tracking_info');
Route::post('/update/status','FrontVendorOrderController@updateStatus')->name('update_status');
Route::post('/update/image','FrontVendorOrderController@updateImage')->name('update_image');

// payment
Route::post('/payment/{orderId}','PaymentController@payment_check');
Route::get('/payment/{orderId}','PaymentController@payment')->name('payment');
Route::get('/payment-success','PaymentController@payment_success')->name('payment_success');
Route::get('/payment-fail','PaymentController@payment_fail')->name('payment_fail');
Route::get('/invoice','PaymentController@invoice')->name('invoice');

//user panel
Route::get('/user/search/list','FrontUserController@searchList')->name('search_list');
Route::get('/user/search/ajax','FrontUserController@searchAjax')->name('search_ajax');
Route::get('/user/checkout','FrontCheckOutController@checkout')->name('checkout');
Route::group(['prefix' => 'user', 'middleware' => ['auth','user']], function () {
    Route::get('/my-account','FrontUserController@myaccount')->name('my_account');
    Route::post('/my-account/add','FrontUserController@userInfoAdd')->name('user_info_add');
    Route::get('/address','FrontUserController@userAddress')->name('user_address');
    Route::post('/address/add','FrontUserController@userAddressAdd')->name('user_address_add');
    Route::get('/live-order/list','FrontVendorOrderController@LiveOrder')->name('live_order');
    Route::get('/live-order/getsecuritycode/{id}','FrontVendorOrderController@getsecuritycode')->name('getsecuritycode');
    Route::get('/completed-order/list','FrontVendorOrderController@CompletedOrder')->name('completed_order');
    Route::get('/order/detail/{id}','FrontVendorOrderController@orderDetail')->name('vendor_order_detail');
    Route::get('/order/track/{id}','FrontVendorOrderController@trackInfo')->name('track');
    Route::post('/add/checkout','FrontCheckOutController@add_to_checkout')->name('add_to_checkout');
});
//vendor panel
Route::group(['prefix' => 'vendor', 'middleware' => ['auth','vendor']], function () {
    Route::get('/home','FrontVendorProductController@vendorHome')->name('vendor_home');
    Route::get('/product','FrontVendorProductController@VendorProductList')->name('vendor_product_list');
    Route::get('/product/add','FrontVendorProductController@vendor_add_product')->name('vendor_add_product');
    Route::post('/product/add','FrontVendorProductController@vendor_save_product_add')->name('vendor_save_product');
    Route::get('/product/price','FrontVendorProductController@product_price')->name('vendor_product_price');
    Route::get('/product/price/add/{id}','FrontVendorProductController@vendor_product_price_add')->name('vendor_product_price_add');
    Route::post('/product/price/add/{id}','FrontVendorProductController@vendor_save_product_price')->name('vendor_save_product_price');
    Route::post('/getproductattribute','FrontVendorProductController@get_product_attribute')->name('vendor_get_product_attribute');
    Route::get('/product/edit/{id}/{vid}','FrontVendorProductController@productDetailEditView')->name('product_detail_edit_view');
    Route::post('/product/edit/{id}/{vid}','FrontVendorProductController@edit_save')->name('product_detail_edit_save');
    Route::post('/editproductattributevalue','FrontVendorProductController@edit_vendor_product_attributeValue')->name('edit_vendor_product_attributeValue');
    Route::get('/recent/order','FrontVendorOrderController@VendorRecentOrderList')->name('vendor_recent_order_list');
    Route::get('/completed/order','FrontVendorOrderController@VendorCompletedOrderList')->name('vendor_completed_order_list');
    Route::post('/update/status','FrontVendorOrderController@vendorUpdateStatus')->name('vendor_update_status');
    Route::get('/track/info/{id}','FrontVendorOrderController@vendorTrack')->name('vendor_track');
    Route::get('/payment','FrontVendorPaymentController@payment_view')->name('vendor_payment_view');
    Route::post('/payment','FrontVendorPaymentController@payment_add')->name('vendor_payment_add');

});
//Admin panel
Route::get('/welcome','HomeController@index')->name('welcome');
Route::group(['middleware' => ['auth','admin']], function(){
    Route::get('/backend/admin/welcome','HomeController@index')->name('welcome');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/backend/admin/vendor/list','VendorController@vendor')->name('vendor');
    Route::get('/backend/admin/vendor/add','VendorController@add')->name('vendor_add');
    Route::post('/backend/admin/vendor/add','VendorController@save_vendor');
    Route::get('/backend/admin/vendor/edit/{id}','VendorController@edit')->name('vendor_edit');
    Route::post('/backend/admin/vendor/edit/{id}','VendorController@edit_save');
    Route::get('/backend/admin/vendor/delete/{id}','VendorController@delete')->name('vendor_delete');

    Route::get('/backend/admin/product/list','ProductController@product')->name('product');
    Route::get('/backend/admin/product/upload','ProductController@add')->name('product_add');
    Route::post('/backend/admin/product/upload','ProductController@save_product');
    Route::get('/backend/admin/product/add','ProductController@add_product')->name('add_product');
    Route::post('/backend/admin/product/add','ProductController@save_product_add');
    Route::get('/backend/admin/product/edit/{id}','ProductController@edit')->name('product_edit');
    Route::post('/backend/admin/product/edit/{id}','ProductController@edit_save')->name('product_edit_save');
    Route::get('/backend/admin/product/delete/{id}','ProductController@delete')->name('product_delete');

    Route::post('/getproductattribute','ProductController@get_product_attribute')->name('get_product_attribute');
    Route::post('/editproductattributevalue','ProductController@edit_product_attributeValue')->name('edit_product_attributeValue');
    Route::get('/backend/admin/product/detail/{product}/{id}','ProductController@adminproductDetail')->name('backend_product_detail');

    Route::get('/backend/admin/product/price/{id}','ProductPriceController@product_price')->name('product_price');
    Route::get('/backend/admin/product/price/{id}/add','ProductPriceController@add')->name('product_price_add');
    Route::post('/backend/admin/product/price/{id}/add','ProductPriceController@save_product_price');
    Route::get('/backend/admin/product/price/edit/{id}/{vid}','ProductPriceController@edit')->name('product_price_edit');
    Route::post('/backend/admin/product/price/edit/{id}/{vid}','ProductPriceController@edit_save');
    Route::get('/backend/admin/product/price/delete/{id}/{vid}','ProductPriceController@delete')->name('delete_price');



    Route::get('/backend/admin/category/list','CategoryController@category')->name('category');
    Route::get('/backend/admin/category/add','CategoryController@add')->name('category_add');
    Route::post('/backend/admin/category/add','CategoryController@save_add');
    Route::get('/backend/admin/category/edit/{id}','CategoryController@edit')->name('category_edit');
    Route::post('/backend/admin/category/edit/{id}','CategoryController@edit_save');
    Route::get('/backend/admin/category/delete/{id}','CategoryController@category_delete')->name('category_delete');

    Route::get('/backend/admin/subcategory/list','SubcategoryController@list')->name('subcategory');
    Route::get('/backend/admin/subcategory/add','SubcategoryController@add')->name('subcategory_add');
    Route::post('/backend/admin/subcategory/add','SubcategoryController@save_add');
    Route::get('/backend/admin/subcategory/edit/{id}','SubcategoryController@edit')->name('subcategory_edit');
    Route::post('/backend/admin/subcategory/edit/{id}','SubcategoryController@edit_save');
    Route::get('/backend/admin/subcategory/delete/{id}','SubcategoryController@subcategory_delete')->name('subcategory_delete');

    Route::get('/backend/admin/user/list','UserController@user')->name('user');
    Route::get('/backend/admin/user/add','UserController@add')->name('user_add');
    Route::post('/backend/admin/user/add','UserController@save_add')->name('user_save');
    Route::get('/backend/admin/user/edit/{id}','UserController@edit')->name('user_edit_view');
    Route::post('/backend/admin/user/edit','UserController@user_edit')->name('user_edit');
    Route::get('/backend/admin/user/delete/{id}','UserController@user_delete')->name('user_delete');

    Route::get('/backend/admin/order/list','OrderController@order')->name('order_list');
    Route::get('/backend/admin/order/detail/{id}','OrderController@detail')->name('order_detail');
    Route::post('/backend/update/status','OrderController@updateStatus')->name('backend_update_status');
    Route::get('/cancel/order/{id}','OrderController@cancelByVendor')->name('cancel_by_vendor');
    Route::post('/add/sub/order','OrderController@addSubOrder')->name('add_sub_order');
    Route::get('/backend/admin/review/list','ReviewController@review')->name('review_list');
    Route::get('/backend/admin/review/edit/{id}','ReviewController@review_edit_view')->name('review_edit_view');
    Route::post('/backend/admin/review/edit','ReviewController@review_edit')->name('review_edit');

    Route::get('/backend/admin/payment/list','PaymentController@payment_list')->name('payment_list');
    Route::get('/backend/admin/payment/refund/list','PaymentController@payment_refund_list')->name('payment_refund_list');
    Route::get('/backend/admin/payment/payout/list','PaymentController@payment_payout_list')->name('payment_payout_list');

    Route::get('/backend/admin/refund/list','OrderController@refundList')->name('refund_list');
    Route::get('/backend/admin/refund/pay/{id}','OrderController@refundPayView')->name('refund_pay_view');
    Route::post('/backend/admin/refund/pay','OrderController@refundPayAdd')->name('refund_pay_add');
    Route::get('/backend/admin/payout/list','OrderController@payoutList')->name('payout_list');

    Route::get('/backend/admin/payout/status/{id}','OrderController@payoutStatusView')->name('payout_status_view');
    Route::post('/backend/admin/payout/status','OrderController@payoutStatusAdd')->name('payout_status_add');

    Route::get('/backend/admin/payout/pay/{id}','OrderController@payoutPayView')->name('payout_pay_view');
    Route::post('/backend/admin/payout/pay','OrderController@payoutPayAdd')->name('payout_pay_add');

    Route::get('/backend/admin/city/list','CityController@city')->name('city');
    Route::get('/backend/admin/city/add','CityController@add')->name('city_add');
    Route::post('/backend/admin/city/add','CityController@save_add')->name('city_save');
    Route::get('/backend/admin/city/edit/{id}','CityController@edit')->name('city_edit_view');
    Route::post('/backend/admin/city/edit','CityController@city_edit')->name('city_edit');
    Route::get('/backend/admin/city/delete/{id}','CityController@city_delete')->name('city_delete');

    Route::get('/backend/admin/user/info/{id}','UserController@adminInfo')->name('admin_info');
    Route::get('/backend/admin/vendor/info/{id}','UserController@adminVendorInfo')->name('admin_vendor_info');
    Route::get('/backend/admin/product/info/{id}','UserController@adminProductInfo')->name('admin_product_info');


});


// please set this 3 route in last, do not change without my permission and not add any line below this comment
Route::get('/category/{category}/{id}','FrontUserController@productListForCategory')->name('product_list');
Route::get('/{category}/{subcategory}/{id}','FrontUserController@productListForSubcategory')->name('product_list_subcategory');
Route::get('/{product}/{id}','FrontUserController@productDetail')->name('product_detail');


