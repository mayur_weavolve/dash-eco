@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						City List
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('city_add') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
						    <span>Add City</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item">

			</ul>
	    </div>
	</div>
	<div class="m-portlet__body">
			<div class = "row">
			
				<div class = "col-md-10">
					<form action="{{route('city')}}" method ="get">
					<input type="search" name="search"class="form-control" placeholder="Search City Name"><br>
				</div>
				<div class="col-md-2 text-right">
					<div class="row">
						<div class="col-md-6">
							<button type = "submit" class ="btn btn-primary">Search</button><br>
						</div>
						<div class="col-md-6">
								<a href="{{route('city')}}" class="btn btn-primary">Clear</a>
						</div>
					</div>
					</form>
				</div>
			
				
			</div>
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
				    <th>Name</th>
                    <th>Country Code</th>
                    <th>State Code</th>
					<th>Zipcode</th>
					<th>Latitude</th>
					<th>Longitude</th>
                   	<th>Action</th>
				</tr>
			</thead>
			<tbody>
			@if(count($cities) == 0)
			<tr><td colspan="8"  style="text-align:center;">No record found</td>
			@endif
			<?php foreach ($cities as $city) { ?>
			<tr>
				<td>{{ $city->city_name}}</td>
                <td>{{ $city->country_code}}</td>
                <td>{{ $city->state_code}}</td>
				<td>{{ $city->zipcode}}</td>
				<td>{{ $city->latitude}}</td>
				<td>{{ $city->longitude}}</td>
				<td>
						<a href="{{route('city_edit_view',[$city->id])}}"  title="Edit"><i class="m-menu__link-icon fa fa-edit"></i></a>  &nbsp;&nbsp;
						<a href="{{route('city_delete',[$city->id])}}" onclick="return confirm('Are you sure you want to delete this City?');"  title="Delete"><i class="m-menu__link-icon fa fa-trash-alt"></i></a> &nbsp;&nbsp;
						
				</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
		{{ $cities->links('paginator.default') }}
	</div>
</div>
</div>
</div>
</div>

@endsection
