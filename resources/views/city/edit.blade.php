@extends('layouts.admin')

@section('content')

<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Edit City
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			    <ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('city') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-reply"></i>
						    <span>Back</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item"></li>
			    </ul>
	        </div>
	    </div>



	<form class="m-form m-form--label-align-left- m-form--state-" id="m_form" enctype="multipart/form-data" method="post" action="{{route('city_edit')}}">
	{{csrf_field() }}
		<div class="m-portlet__body">
			<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
				<div class="row">
					<div class="col-xl-8 offset-xl-2">
						<div class="m-form__section">
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* City Name :</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="hidden" name="id" class="form-control m-input" value="{{ $edit->id}}">
                                    <input type="text" name="city_name" class="form-control m-input" value="{{ $edit->city_name}}" required>
                                    @error('city_name')
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('city_name') }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                    <label for="country_code" class="col-xl-3 col-lg-3 col-form-label">* Country Code :</label>
                                    <div class="col-xl-8 col-lg-8">
                                        <input type="text" name="country_code" class="form-control m-input" value="{{ $edit->country_code }}" required>
                                        @error('country_code')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('country_code') }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="state_code" class="col-xl-3 col-lg-3 col-form-label">* State Code :</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="text" name="state_code" class="form-control m-input" value="{{ $edit->state_code }}" required>
                                    @error('state_code')
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('state_code') }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                    <label for="zipcode" class="col-xl-3 col-lg-3 col-form-label">* Zipcode :</label>
                                    <div class="col-xl-8 col-lg-8">
                                        <input type="text" name="zipcode" class="form-control m-input" value="{{ $edit->zipcode }}" required>
                                        @error('zipcode')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('zipcode') }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                            </div>
                            <div class="form-group m-form__group row">
                                    <label for="latitude" class="col-xl-3 col-lg-3 col-form-label">* Latitude :</label>
                                    <div class="col-xl-8 col-lg-8">
                                        <input type="text" name="latitude" class="form-control m-input" value="{{ $edit->latitude }}" required>
                                        @error('latitude')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('latitude') }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                            </div>
                            <div class="form-group m-form__group row">
                                    <label for="longitude" class="col-xl-3 col-lg-3 col-form-label">* Longitude :</label>
                                    <div class="col-xl-8 col-lg-8">
                                        <input type="text" name="longitude" class="form-control m-input" value="{{ $edit->longitude }}" required>
                                        @error('longitude')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('longitude') }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                            </div>
							<div class="col-lg-8 m--align-right">
								<button class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit" >
									<span>
										<i class="la la-check"></i>&nbsp;&nbsp;
											<span>Submit</span>
									</span>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
</div>
</div>
</div>

@endsection
