@extends('layouts.admin')

@section('content')

<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Edit Subcategory
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			    <ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('subcategory') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-reply"></i>
						    <span>Back</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item"></li>
			    </ul>
	        </div>
	    </div>



	<form class="m-form m-form--label-align-left- m-form--state-" id="m_form" enctype="multipart/form-data" method="post" action="">
	{{csrf_field() }}
		<div class="m-portlet__body">
			<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
				<div class="row">
					<div class="col-xl-8 offset-xl-2">
						<div class="m-form__section">
                            <div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Category Name :</label>
                                <div class="col-xl-8 col-lg-8">
                                    <select name="category_name" class="form-control m-input" value="" required>
                                        <option value="">Select Category</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}" @if($edit->category_id == $category->id) selected @endif>{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('category_name')
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('category_name') }}</strong>
                                    </span>
                                    @enderror
                                </div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Subcategory Name :</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="text" name="name" class="form-control m-input" value="{{$edit->name}}" required>
                                    @error('name')
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @enderror
                                </div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Status:</label>
								<div class="col-xl-8 col-lg-8">
									<select name="status" class="form-control m-input" required>
										<option value="">Select Status</option>
										<option value="active"<?php if($edit->status == "active"){ echo 'selected'; }?>>Active</option>
										<option value="inactive"<?php if($edit->status == "inactive"){ echo 'selected'; }?>>In-Active</option>
									</select>
									@error('status')
                       					<span class="help-block" role="alert">
                           					<strong>{{ $errors->first('status') }}</strong>
                        				</span>
                    				@enderror
								</div>
							</div>
							<div class="col-lg-8 m--align-right">
								<button class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit" >
									<span>
										<i class="la la-check"></i>&nbsp;&nbsp;
											<span>Submit</span>
									</span>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
</div>
</div>
</div>

@endsection
