<?php
    use App\Category;
    use App\Cart;
    $hashtags = productHashtag();
    // checkRole();
    $userscartProducts = usercart();
    $catData = getAvailableCategory();
    $categories = $catData['categories'];
    $subcategories = $catData['subcategories'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="author" content="DASH shop - free same day home delivery from local shop">
    <meta name="keywords" content="online shop, hardware store near me, mobile shop near me,buy mobile, free same day home delivery, new smartphone, hardware items, new machine, makeup, home decor, material, shops near me">
    <meta name="description" content="Buy from local shops online and get same day home delivery or pickup. Shops ranges from Hardware, computers, smart phones mobile, grocery, clothes, shoes, stationary, home decor, construction products and lot more">


    <meta property="og:google_product_category" content="<?php echo $products->productsubcategory->subcategory->category->name; ?>">
    <meta property="og:title" content="{{ $products->product_name }} | Dash Shop">
    <meta property="og:description" content="Buy from local shops online and get same day home delivery or pickup. Shops ranges from Hardware, computers, smart phones mobile, grocery, clothes, shoes, stationary, home decor, construction products and lot more">
    <meta property="og:url" content="{{ url()->current() }}">
    <meta property="og:image" content="<?php echo URL::to('/'); ?>/product_photo/{{$products->productImage->name}}">
    <meta property="og:image:alt" content="{{$products->product_name}}">
    <meta property="product:brand" content="{{$products->manufacturer}}">
    <meta property="product:availability" content="in stock">
    <meta property="product:condition" content="new">
    <meta property="product:price:amount" content="{{$productPrice->final_price}}">
    <meta property="product:price:currency" content="INR">
    <meta property="product:retailer_item_id" content="<?php echo $productBrand->product_id.'_'.$productBrand->vendorID; ?>">
    <meta property="product:item_group_id" content="<?php echo $products->productsubcategory->subcategory->category->name; ?>">

    <title>{{ $products->product_name }} | Dash Shop</title>
    <link rel="icon" href="<?php echo URL('/'); ?>/user/img/coming-soon-logo_withoutname.png" type="image/png" />
    <link rel="shortcut icon" href="<?php echo URL('/'); ?>/user/img/coming-soon-logo_withoutname.png" />
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700&amp;amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/fonts/Linearicons/Linearicons/Font/demo-files/demo.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/owl-carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/owl-carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/slick/slick/slick.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/nouislider/nouislider.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/lightGallery-master/dist/css/lightgallery.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/jquery-bar-rating/dist/themes/fontawesome-stars.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/select2/dist/css/select2.min.css">
    <link href="<?php echo URL('/'); ?>/admin/assets/css/tokenize2.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/css/style.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/css/market-place-4.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/css/custom.css">
    <link href="<?php echo URL('/'); ?>/admin/assets/vendors/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
    <script src="<?php echo URL('/'); ?>/user/plugins/jquery.min.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/assets/js/tokenize2.min.js" type="text/javascript"></script>
    <script src="<?php echo URL('/'); ?>/admin/assets/vendors/ckeditor/ckeditor.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/assets/vendors/ckeditor/adapters/jquery.js"></script>

    <div id="loader" class="loader">
        <img src="<?php echo URL('/'); ?>/user/img/loaders.gif">
    </div>

<!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '738117736753766');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=738117736753766&ev=PageView&noscript=1" />
    </noscript>
<!-- End Facebook Pixel Code -->
</head>

<body>
    <header class="header header--product" data-sticky="true">
        <nav class="navigation">
            <div class="container">
                <article class="ps-product--header-sticky">
                    <div class="ps-product__thumbnail"><img src="/product_photo/{{$products->productImage->name}}" alt="" /></div>
                    <div class="ps-product__wrapper">
                        <div class="ps-product__content"><a class="ps-product__title" href="javascript:void(0);">{{ $products->product_name }}</a>
                                    <span class="km-button @if($productPrice->free_delivery == 0 || $products->getDistance($productPrice->vendor) > $productPrice->free_delivery) pickup-span @endif">  @if($productPrice->free_delivery == 0)
                                                            Pickup Only
                                                        @else
                                                            @if(getDistance($productPrice->vendor) > $productPrice->free_delivery)
                                                                Pickup Only
                                                            @else
                                                                Delivery @if($productPrice->mode == 1)
                                                                    @if(date('H') < 14)
                                                                        today
                                                                    @else
                                                                        tomorrow
                                                                    @endif
                                                                @else
                                                                    tomorrow
                                                                @endif
                                                            @endif

                                                        @endif | {{getDistance($productPrice->vendor)}} km  away </span>
                        </div>
                        <div class="ps-product__shopping"><span class="ps-product__price"><span>₹ {{ number_format($productPrice->final_price)}}</span>
                            <del>₹ {{ number_format($productPrice->mrp)}}</del></span><a class="ps-btn addToCart" href="javascript:void(0);" id="addToCart" data-name="checkout"> Add to Cart</a></div>
                    </div>
                </article>
            </div>
        </nav>
    </header>
    <header class="header header--1" data-sticky="false">
        <div class="header__top">
            <div class="ps-container">
                <div class="header__left">
                    <div class="menu--product-categories">
                        <div class="menu__toggle"><i class="icon-menu"></i><span> Shop by Department</span></div>
                        <div class="menu__content">
                            <ul class="menu--dropdown">
                                @foreach( $categories as $categorylist)
                                    <li class="menu-item-has-children has-mega-menu" ><a href="{{route('product_list', ['category' => strtolower(str_replace(" ","-",$categorylist['name'])), 'id' => $categorylist['id']])}}"><i class="{{$categorylist['icon']}}"></i> {{$categorylist['name']}}</a>
                                    <div class="mega-menu">
                                            <div class="mega-menu__column">
                                                <h4>{{$categorylist['name']}}</h4>
                                                <ul class="mega-menu__list">
                                                    @foreach($subcategories[$categorylist['id']] as $subcat)
                                                        <li><a href="{{route('product_list_subcategory', ['category' => strtolower(str_replace(" ","-",$categorylist['name'])),'subcategory' => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$subcat['name'])), 'id' => $subcat['id']])}}">{{$subcat['name']}}</a></li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                    </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div><a class="ps-logo" href="/"><img src="<?php echo URL('/'); ?>/user/img/coming-soon-logo.png"  width="130px" alt=""></a>
                </div>
                <div class="header__center">
                    <form class="ps-form--quick-search" action="{{route('search_list')}}" method="get">
                        <input class="form-control" type="search" name="search" value="@if(isset($search)) {{ $search }} @endif" placeholder="I'm shopping for...">
                        <button type = "submit">Search</button>
                    </form>
                </div>
                <div class="header__right">
                    <div class="header__actions">
                        <!-- <a class="header__extra" href="javascript:void(0);"><i class="icon-chart-bars"></i><span><i>@if(sizeof($userscartProducts) > 0) {{ sizeof($userscartProducts) }} @else 0 @endif</i></span></a>
                        <a class="header__extra" href="javascript:void(0);"><i class="icon-heart"></i><span><i>@if(sizeof($userscartProducts) > 0) {{ sizeof($userscartProducts) }} @else 0 @endif</i></span></a> -->
                        <div class="ps-cart--mini"><a class="header__extra" href="#cart-mobile"><i class="icon-bag2"></i><span><i>@if(sizeof($userscartProducts) > 0) {{ sizeof($userscartProducts) }} @else 0 @endif</i></span></a>
                            <div class="ps-cart__content">
                            @if(sizeof($userscartProducts) > 0)
                                <div class="ps-cart__items">
                                    @foreach($userscartProducts as $cartProduct)
                                        <div class="ps-product--cart-mobile">
                                            <?php $pUrl = $cartProduct->product->id.'_'.$cartProduct->vendor_id; ?>
                                            <div class="ps-product__thumbnail"><a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$cartProduct->product->product_name)), 'id' => $pUrl ])}}"><img src="<?php echo URL::to('/'); ?>/product_photo/{{$cartProduct->product->productImage->name}}" alt=""></a></div>
                                            <div class="ps-product__content"><a class="ps-product__remove" href="javascript:void(0);" onclick="deleteCartPopup({{$cartProduct->cartId}})"   title="Delete"><i class="icon-cross"></i></a><a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$cartProduct->product->product_name)), 'id' => $cartProduct->product->id.'_'.$cartProduct->vendor_id ])}}">{{ $cartProduct->product->product_name}}</a>
                                                <p><strong>Sold by:</strong> {{ $cartProduct->vendor['nickname'] }}</p><small>{{ $cartProduct->cartQauntity }} x ₹ {{number_format($cartProduct->final_price)}} </small>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="ps-cart__footer">
                                    <h3>Sub Total:<strong>₹{{  subtotal($userscartProducts) }}</strong></h3>
                                    <figure><a class="ps-btn" href="{{ route('my_cart') }}">View Cart</a><a class="ps-btn" href="{{ route('checkout') }}">Checkout</a></figure>
                                </div>
                                @else
                                    <div class="ps-cart__items empty-cart">
                                        <div class="ps-product--cart-mobile">
                                            <span>Your cart is Empty</span>
                                        </div>
                                        <div class="ps-cart__footer">
                                            <figure><a class="ps-btn" href="{{ route('front') }}">Shop Now</a></figure>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="ps-block--user-header">
                        <div class="ps-block__left"><a href="@if(Auth::user())@if( Auth::user()->type == 'vendor') {{ route('vendor_home') }} @else {{ route('my_account') }}  @endif @else  {{ route('signup') }} @endif"><i class="icon-user"></i></a></div>
                            @if(Auth::user())
                                @if(Auth::user()->name != null)
                                    <div class="ps-block__right"><a href="@if( Auth::user()->type == 'vendor') {{ route('vendor_home') }} @else {{ route('my_account') }}  @endif">{{ Auth::user()->name }}</a></div>
                                @else
                                    <div class="ps-block__right"><a href="@if( Auth::user()->type == 'vendor') {{ route('vendor_home') }} @else {{ route('my_account') }}  @endif">**{{ substr (Auth::user()->mobileno, -4) }}</a></div>
                                @endif
                            @else
                                <div class="ps-block__right"><a href="/signup">Login</a><a href="/signup">Register</a></div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="navigation">
            <div class="ps-container">
                <div class="navigation__left">
                    <div class="menu--product-categories">
                        <div class="menu__toggle"><i class="icon-menu"></i><span> Shop by Department</span></div>
                        <div class="menu__content">
                            <ul class="menu--dropdown">
                                @foreach( $categories as $categorylist)
                                    <li class="menu-item-has-children has-mega-menu" ><a href="{{route('product_list', ['category' => strtolower(str_replace(" ","-",$categorylist['name'])), 'id' => $categorylist['id']])}}"><i class="{{$categorylist['icon']}}"></i> {{$categorylist['name']}}</a>
                                    <div class="mega-menu">
                                            <div class="mega-menu__column">
                                                <h4>{{$categorylist['name']}}</h4>
                                                <ul class="mega-menu__list">
                                                    @foreach($subcategories[$categorylist['id']] as $subcat)
                                                        <li><a href="{{route('product_list_subcategory', ['category' => strtolower(str_replace(" ","-",$categorylist['name'])),'subcategory' => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$subcat['name'])), 'id' => $subcat['id']])}}">{{$subcat['name']}}</a></li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                    </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="navigation__right right-nav">
                    <ul class="navigation__extra full-pin-bottom-menu">
                         <li id="pin-li" onClick="subscribePopup()" ><span id="change-pin">Change Your Pincode</span><div class="pincode-box"><span class="pincode"></span><i class="icon-pencil" ></i></div></li>
                    <!--
                        <li><a href="javascript:void(0);">Sell on DASH</a></li>
                        <li><a href="javascript:void(0);">Tract your order</a></li>
                        <li>
                            <div class="ps-dropdown"><a href="javascript:void(0);">US Dollar</a>
                                <ul class="ps-dropdown-menu">
                                    <li><a href="javascript:void(0);">Us Dollar</a></li>
                                    <li><a href="javascript:void(0);">Euro</a></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div class="ps-dropdown language"><a href="javascript:void(0);"><img src="<?php echo URL('/'); ?>/img/flag/en.png" alt="">English</a>
                                <ul class="ps-dropdown-menu">
                                    <li><a href="javascript:void(0);"><img src="<?php echo URL('/'); ?>/img/flag/germany.png" alt=""> Germany</a></li>
                                    <li><a href="javascript:void(0);"><img src="<?php echo URL('/'); ?>/img/flag/fr.png" alt=""> France</a></li>
                                </ul>
                            </div>
                        </li>-->
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <header class="header header--mobile header--mobile-product" data-sticky="true">
        <div class="navigation--mobile">
            <div class="navigation__left"><a class="header__back" href="javascript: window.history.go(-1)"><i class="icon-chevron-left"></i><strong>Back</strong></a></div>
            <div class="navigation__right">
                <div class="header__actions">
                    <div class="ps-cart--mini"><a class="header__extra" href="javascript:void(0);"><i class="icon-bag2"></i><span><i>@if(sizeof($userscartProducts) > 0) {{ sizeof($userscartProducts) }} @else 0 @endif</i></span></a>
                        <div class="ps-cart__content">
                            @if(sizeof($userscartProducts) > 0)
                                <div class="ps-cart__items">
                                    @foreach($userscartProducts as $cartProduct)
                                        <div class="ps-product--cart-mobile">
                                            <div class="ps-product__thumbnail"><a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$cartProduct->product->product_name)), 'id' => $cartProduct->product->id])}}"><img src="/product_photo/{{$cartProduct->product->productImage->name}}" alt=""></a></div>
                                            <div class="ps-product__content"><a class="ps-product__remove" href="javascript:void(0);" onclick="deleteCartPopup({{$cartProduct->cartId}})" ><i class="icon-cross"></i></a><a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$cartProduct->product->product_name)), 'id' => $cartProduct->product->id])}}">{{ $cartProduct->product->product_name}}</a>
                                                <p><strong>Sold by:</strong> {{ $cartProduct->product->productPrice[0]->vendor['nickname'] }}</p><small>{{ $cartProduct->cartQauntity }} x ₹ {{ number_format($cartProduct->product->productPrice[0]->final_price) }} </small>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="ps-cart__footer">
                                    <h3>Sub Total:<strong>₹{{  subtotal($userscartProducts) }}</strong></h3>
                                    <figure><a class="ps-btn" href="{{ route('my_cart') }}">View Cart</a><a class="ps-btn" href="{{ route('checkout') }}">Checkout</a></figure>
                                </div>
                            @else
                                <div class="ps-cart__items empty-cart">
                                    <div class="ps-product--cart-mobile">
                                        <span>Your cart is Empty</span>
                                    </div>
                                    <div class="ps-cart__footer">
                                        <figure><a class="ps-btn" href="{{ route('front') }}">Shop Now</a></figure>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="ps-block--user-header">
                    <div class="ps-block__left"><a href="@if(Auth::user())@if( Auth::user()->type == 'vendor') {{ route('vendor_home') }} @else {{ route('my_account') }}  @endif @else  {{ route('signup') }} @endif"><i class="icon-user"></i></a></div>
                            @if(Auth::user())
                                @if(Auth::user()->name != null)
                                    <div class="ps-block__right"><a href="@if( Auth::user()->type == 'vendor') {{ route('vendor_home') }} @else {{ route('my_account') }}  @endif">{{ Auth::user()->name }}</a></div>
                                @else
                                    <div class="ps-block__right"><a href="@if( Auth::user()->type == 'vendor') {{ route('vendor_home') }} @else {{ route('my_account') }}  @endif">**{{ substr (Auth::user()->mobileno, -4) }}</a></div>
                                @endif
                            @else
                                <div class="ps-block__right"><a href="/signup">Login</a><a href="/signup">Register</a></div>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </header>
    <nav class="navigation--mobile-product">
            <a class="ps-btn ps-btn--black addToCart" href="javascript:void(0);" id="addToCart"  data-name="cart">Add to cart</a>
            <a class="ps-btn addToCart" href="javascript:void(0);" id="addToCart"  data-name="checkout" >Buy Now</a>
    </nav>

    @yield("content")

    <footer class="ps-footer ps-footer--3">
        <div class="container">
            <div class="ps-block--site-features ps-block--site-features-2">
                <div class="ps-block__item">
                    <div class="ps-block__left"><i class="icon-rocket"></i></div>
                    <div class="ps-block__right">
                        <h4>Same day free home delivery</h4>
                        <p>order before 2pm</p>
                    </div>
                </div>
                <div class="ps-block__item">
                    <div class="ps-block__left"><i class="icon-sync"></i></div>
                    <div class="ps-block__right">
                        <h4>Product <br/>exchange</h4>
                        <p>If goods are damaged</p>
                    </div>
                </div>
                <div class="ps-block__item">
                    <div class="ps-block__left"><i class="icon-credit-card"></i></div>
                    <div class="ps-block__right">
                        <h4>100% Secure Payment</h4>
                        <p>UPI, Paytm, Bank transfer and other</p>
                    </div>
                </div>
                <div class="ps-block__item">
                    <div class="ps-block__left"><i class="icon-bubbles"></i></div>
                    <div class="ps-block__right">
                        <h4>Instant chat support</h4>
                        <p>100% customer satisfaction</p>
                    </div>
                </div>
            </div>
            <div class="ps-footer__widgets">
                <aside class="widget widget_footer widget_contact-us">
                    <h4 class="widget-title">Contact us</h4>
                    <div class="widget_content">
                    <a class="km-span-list" id="submit" href="https://tawk.to/chat/5f6b07f1f0e7167d0012eea0/default" target="popup"
  onclick="window.open('https://tawk.to/chat/5f6b07f1f0e7167d0012eea0/default','popup','width=600,height=600'); return false;" >Chat</a>
                        <p>Call us 10 am to 5 pm (Mon to Sat)</p>
                        <h3>+91 99987 63163</h3>
                        <p>Maruti Sharnam, Anand, 388001 <br><a href="mailto:hi@dashshop.in">hi@dashshop.in</a></p>
                        <ul class="ps-list--social">
                            <li><a class="facebook" href="https://www.facebook.com/dashservicesindia/"><i class="fa fa-facebook"></i></a></li>
                            <!-- <li><a class="twitter" href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                            <li><a class="google-plus" href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li> -->
                            <li><a class="instagram" href="https://www.instagram.com/dashservicesindia/"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </aside>
                <aside class="widget widget_footer">
                    <h4 class="widget-title">Quick links</h4>
                    <ul class="ps-list--link">
                        <li><a href="/return-refund-policy">Return & Refund Policy</a></li>
                        <li><a href="/terms-and-conditions">Term & Condition</a></li>
                        <li><a href="/delivery-or-pickup">Same day Delivery or Pickup</a></li>
                        <!-- <li><a href="javascript:void(0);">Return</a></li> -->
                        <li><a href="/faq">FAQs</a></li>
                    </ul>
                </aside>
                <aside class="widget widget_footer">
                    <h4 class="widget-title">Company</h4>
                    <ul class="ps-list--link">
                        <li><a href="/about-us">About Us</a></li>
                        <!-- <li><a href="/affilate">Affilate</a></li> -->
                        <li><a href="/career">Career</a></li>
                        <li><a href="/contact-us">Contact</a></li>
                    </ul>
                </aside>
                <aside class="widget widget_footer">
                    <h4 class="widget-title">DASH shop</h4>
                    <ul class="ps-list--link">
                        <!-- <li><a href="javascript:void(0);">Our Press</a></li> -->
                        <li><a href="/user/my-cart">My Cart</a></li>
                        <li><a href="/user/my-account">My Account</a></li>
                        <li><a href="/">Shop</a></li>
                    </ul>
                </aside>
            </div>
            <div class="ps-footer__links">
                @foreach($categories as $categorylist)
                    <p><strong>{{$categorylist['name']}}:</strong>
                        @foreach($subcategories[$categorylist['id']] as $subcat)
                            <a href="{{route('product_list_subcategory', ['category' => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$categorylist['name'])),'subcategory' => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$subcat['name'])), 'id' => $subcat['id'] ] )}}">{{$subcat['name']}}</a>
                        @endforeach
                    </p>
                @endforeach
            </div>
            <div class="ps-footer__copyright">
                <p>© 2020 DASH. All Rights Reserved</p>
                <p><span>Our Payment Partners:</span><a href="javascript:void(0);"><img src="/user/img/payment-method/visa.jpg" alt="visa"></a><a href="javascript:void(0);"><img src="/user/img/payment-method/mastercard.jpg" alt="mastercard"></a><a href="javascript:void(0);"><img src="/user/img/payment-method/paytm.svg" alt="paytm"></a><a href="javascript:void(0);"><img src="/user/img/payment-method/upi.svg" alt="upi"></a></p>
            </div>
        </div>
    </footer>
    <div id="back2top"><i class="pe-7s-angle-up"></i></div>
     <!--pincode popup -->
     <div class="ps-popup" id="subscribe" data-time="10">
        <div class="ps-popup__content bg--cover" data-background-color="#fff" style="background-color:#fff">
        <a class="ps-popup__close" href="javascript:void(0);"><i class="icon-cross"></i></a>
            <div class="ps-form--subscribe-popup" >
                <div class="ps-form__content">
                    <h4>Get <strong>Same Day</strong> Delivery*</h4>
                    <p>Enter Your Pincode <br /> Shop in your area <br /> Support local</p>
                    @if(sizeof($userscartProducts) > 0)<p class="error">**Changing the pincode will empty your cart.</p>@endif
                    <p class="error-pin"></p>
                    <div class="form-group pininput-div">
                        <input class="form-control" type="tel" placeholder="Pincode e.g. 388001" id="pincode-input" name="pincode-input"  maxlength="6" required>
                        <button class="ps-btn setpincode" >Set Pincode</button>
                    </div>
                    <!-- <div class="ps-checkbox">
                        <input class="form-control" type="checkbox" id="not-show" name="not-show">
                        <label for="not-show">Don't show this popup again</label>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
      <!-- end pincode popup -->
        <!-- cart popup -->
  @if(count($userscartProducts) > 0)
        <div class="ps-popup" id="cartDelete" data-time="10">
            <div class="ps-popup__content bg--cover" data-background-color="#fff" style="background-color:#fff">
            <!-- <a class="ps-popup__close" href="javascript:void(0);"><i class="icon-cross"></i></a> -->
                <div class="ps-form--subscribe-popup" >
                    <div class="ps-form__content">
                        <input type="hidden" name="deleteProductValue" id="deleteProductValue" value="0">
                        <h4><strong>Delete Cart Item</strong></h4>
                        <p>Are you sure you want to delete cart item?</p>
                        <div class="form-group pininput-div">
                                <span style="display: inline;"><a class="ps-btn delete-cart-popup-close" href="javascript:void(0);" >Cancel</a></span>
                                <span style="display: inline;"><a class="ps-btn delete-btn" onclick="deleteCartProduct();">Delete</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div id="back2top"><i class="pe-7s-angle-up"></i></div>
    <div class="ps-site-overlay"></div>
    <div id="loader-wrapper">
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <div class="ps-search" id="site-search"><a class="ps-btn--close" href="javascript:void(0);"></a>
        <div class="ps-search__content">
            <form class="ps-form--primary-search" action="do_action" method="post">
                <input class="form-control" type="text" placeholder="Search for...">
                <button><i class="aroma-magnifying-glass"></i></button>
            </form>
        </div>
    </div>

    <div class="modal fade" id="product-quickview" tabindex="-1" role="dialog" aria-labelledby="product-quickview" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content"><span class="modal-close" data-dismiss="modal"><i class="icon-cross2"></i></span>
                <article class="ps-product--detail ps-product--fullwidth ps-product--quickview quick-product-data">
                        <!-- data load from the javascript -->
                </article>
            </div>
        </div>
    </div>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-151417642-5"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-151417642-5');
</script>

   <!-- Global site tag (gtag.js) - Google Ads: 603777454 -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=AW-603777454"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-603777454');
</script> -->

<!-- <script>
function gtag_report_conversion(url) {
  var callback = function () {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  };
  gtag('event', 'conversion', {
      'send_to': 'AW-603777454/AcHgCOin09sBEK7T858C',
      'event_callback': callback
  });
  return false;
}
</script> -->
<!--Start of Tawk.to Script-->
<!-- <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5f6b07f1f0e7167d0012eea0/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
    Tawk_API.onLoad = function () {
        $('#tawkchat-container iframe').css('margin-bottom', '125px');
        $('#tawkchat-minified-box').css('margin-bottom', '125px');
    };
</script> -->
<script>

var LOADER_CONTAINER = $("#loader");
var LOADER_INTERVAL = 1600;
var LOADERS = [
    ["🚰","Fancy tap"],
    ["🆒","Cool as AC"],
    ["💡","Bulb fuse hai?"],
    ["🍳","Frying pan"],
    ["👇💃☝","Sale. Sale. Sale."],
    ["🔖","Get your coupon"],
    ["🎁","Gf ka b’day hai?"],
    ["🎒","School bag = Cool bag"],
    ["💝","Surprise your wife"],
    ["🍙","Cheeni kum hai?"],
    ["☕","Hello frandz chai le lo"],
    ["💄","Shopkeeper lipstick hai?"],
    ["💃","Yahan sab milege"],
    ["🐎","Jaldi order karo"],
    ["📱🔌","Trrring trrring"],
    ["📲","Smart toh phone hota hai"]
    ["👠👞","Ready for a date night?"],
    ["⏰","It’s never too late"],
    ["🚚","Same day delivery"],

]

// $(document).ready(function() {

//   var cycleLoader = function() {
//     var index = Math.floor(Math.random() * LOADERS.length);
//     var selected = LOADERS[index];
//     var selectedEmoji = selected[0];
//     var selectedText = selected[1];

//     // First transition out the old loader
//     setTimeout(function(){
//       LOADER_CONTAINER.children().addClass("animateOut");
//     }, LOADER_INTERVAL - 300); // This negative value should be the same as $animation-duration in the CSS

//     // Then remove the animated out divs
//     LOADER_CONTAINER.children(".emoji").last().remove();
//     LOADER_CONTAINER.children(".text").last().remove();

//     // Then animate in the new one
//     LOADER_CONTAINER.append('<div class="emoji">' + selectedEmoji + '</div>');
//     LOADER_CONTAINER.append('<div class="text">' + selectedText + '</div>');
//   }

//   setInterval(cycleLoader, LOADER_INTERVAL);
//   cycleLoader(); // Run first time without delay

// });

document.onreadystatechange = function() {
    if (document.readyState !== "complete") {
        document.querySelector("#loader").style.visibility = "visible";
        document.querySelector("body").style.visibility = "hidden";
    } else {
        document.querySelector("#loader").style.display = "none";
        document.querySelector("body").style.visibility = "visible";
    }
};

function deleteCartProduct(){
     var cartId = $("#deleteProductValue").val();
     window.location.href = '<?php echo URL::to('/'); ?>/cart/delete/'+cartId;
 }

</script>

<script type="application/ld+json">
            var productDetailLink = 'facebook'+{{$products->product_name}}.toLowerCase().replace(/[^a-zA-Z0-9]/g,"-")+'_'+{{$products->product_id}};
            var product =  {{ preg_replace("/\r|\n/"," ",$products->product_name) }},
            var description = {{ preg_replace("/\r|\n/"," ",strip_tags($products->description)) }},
            var url = <?php echo url()->current();?>;
            var image = <?php echo URL::to('/'); ?>/product_photo/{{$products->productImage->name}};
            var brand = {{$products->manufacturer}};
            var price = {{$products->productPrice[0]->final_price}}
            debugger;
            {
              "@context":"https://schema.org",
              "@type":"Product",
              "productID":product,
              "name":product,
              "description":description,
              "url":url,
              "image":image,
              "brand":brand,
              "offers": [
                {
                  "@type": "Offer",
                  "price": price,
                  "priceCurrency": "INR",
                  "itemCondition": "https://schema.org/NewCondition",
                  "availability": "https://schema.org/InStock"
                }
              ]
            }
    </script>

<!--End of Tawk.to Script-->
    <script src="<?php echo URL('/'); ?>/user/plugins/nouislider/nouislider.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/popper.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/masonry.pkgd.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/isotope.pkgd.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/jquery.matchHeight-min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/slick/slick/slick.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/slick-animation.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/lightGallery-master/dist/js/lightgallery-all.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/sticky-sidebar/dist/sticky-sidebar.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/gmap3.min.js"></script>
    <!-- <script src="<?php echo URL('/'); ?>/user/js/infinite-scroll.pkgd.min.js"></script> -->
    <!-- custom scripts-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/crypto-js.js"></script>
    <script src="<?php echo URL('/'); ?>/user/js/main.js"></script>
    <script src="<?php echo URL('/'); ?>/user/js/custom.js"></script>
</body>

</html>
