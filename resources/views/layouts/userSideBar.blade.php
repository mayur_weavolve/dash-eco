<div class="ps-widget__content">
        <ul>
            <li class="active"><a href="javascript:void(0);"><i class="icon-user"></i> Account Information</a></li>
            <li><a href="javascript:void(0);"><i class="icon-alarm-ringing"></i> Notifications</a></li>
            <li><a href="javascript:void(0);"><i class="icon-papers"></i> Invoices</a></li>
            <li><a href="route('user_address')"><i class="icon-map-marker"></i> Address</a></li>
            <li><a href="javascript:void(0);"><i class="icon-store"></i> Recent Viewed Product</a></li>
            <li><a href="javascript:void(0);"><i class="icon-heart"></i> Wishlist</a></li>
            <li><a href="javascript:void(0);"><i class="icon-power-switch"></i>Logout</a></li>
        </ul>
</div>