<?php
    use App\Category;
    use App\Cart;
    $hashtags = productHashtag();
    // checkRole();
    $userscartProducts = usercart();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="author" content="DASH shop - free same day home delivery from local shop">
    <meta name="keywords" content="online shop, hardware store near me, mobile shop near me,buy mobile, free same day home delivery, new smartphone, hardware items, new machine, makeup, home decor, material, shops near me">
    <meta name="description" content="Buy from local shops online and get same day home delivery or pickup. Shops ranges from Hardware, computers, smart phones mobile, grocery, clothes, shoes, stationary, home decor, construction products and lot more">
    <title>DASH shop - free same day home delivery from local shop</title>
    <link rel="icon" href="<?php echo URL('/'); ?>/user/img/coming-soon-logo_withoutname.png" type="image/png" />
    <link rel="shortcut icon" href="<?php echo URL('/'); ?>/user/img/coming-soon-logo_withoutname.png" />
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700&amp;amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/fonts/Linearicons/Linearicons/Font/demo-files/demo.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/owl-carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/owl-carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/slick/slick/slick.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/nouislider/nouislider.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/lightGallery-master/dist/css/lightgallery.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/jquery-bar-rating/dist/themes/fontawesome-stars.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/select2/dist/css/select2.min.css">
    <link href="<?php echo URL('/'); ?>/admin/assets/css/tokenize2.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/css/style.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/css/market-place-4.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/css/custom.css">
    <script src="<?php echo URL('/'); ?>/user/plugins/jquery.min.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/assets/js/tokenize2.min.js" type="text/javascript"></script>
    <script src="<?php echo URL('/'); ?>/admin/assets/vendors/ckeditor/ckeditor.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/assets/vendors/ckeditor/adapters/jquery.js"></script>

    <div id="loader" class="loader">
        <img src="<?php echo URL('/'); ?>/user/img/loaders.gif">
    </div>
<!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '738117736753766');
        fbq('track', 'PageView');
    </script>
    <noscript>
    <img height="1" width="1" src="https://www.facebook.com/tr?id=738117736753766&ev=PageView&noscript=1" />
    </noscript>
<!-- End Facebook Pixel Code -->
</head>

<body style="visibility: hidden;">

<header class="header header--standard header--market-place-4" data-sticky="true">
        <div class="header__top">
            <div class="container">
                <div class="header__left">
                    <p>Welcome to DASH Online Local Shop !</p>
                </div>
                <div class="header__right">
                    <ul class="header__top-links">
                        <li id="pin-li" onClick="subscribePopup()" ><span id="change-pin">Change Your Pincode</span><div class="pincode-box"><span class="pincode"></span><i class="icon-pencil" ></i></div></li>
                        <!-- <li><a href="javascript:void(0);">Store Location</a></li>
                        <li><a href="javascript:void(0);">Track Your Order</a></li>
                        <li>
                            <div class="ps-dropdown"><a href="javascript:void(0);">US Dollar</a>
                                <ul class="ps-dropdown-menu">
                                    <li><a href="javascript:void(0);">Us Dollar</a></li>
                                    <li><a href="javascript:void(0);">Euro</a></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div class="ps-dropdown language"><a href="javascript:void(0);"><img src="img/flag/en.png" alt="">English</a>
                                <ul class="ps-dropdown-menu">
                                    <li><a href="javascript:void(0);"><img src="img/flag/germany.png" alt=""> Germany</a></li>
                                    <li><a href="javascript:void(0);"><img src="img/flag/fr.png" alt=""> France</a></li>
                                </ul>
                            </div>
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>
        <div class="header__content">
            <div class="container">
                <div class="header__content-left"><a class="ps-logo" href="<?php echo URL::to('/'); ?>"><img src="<?php echo URL('/'); ?>/user/img/coming-soon-logo.png"  width="130px" alt="light-logo"></a>
                    <div class="menu--product-categories">
                        <div class="menu__toggle"><i class="icon-menu"></i><span> Shop by Department</span></div>
                        <div class="menu__content">
                            <ul class="menu--dropdown">
                                @foreach( $categories as $categorylist)
                                    <li class="menu-item-has-children has-mega-menu" ><a href="{{route('product_list', ['category' => strtolower(str_replace(" ","-",$categorylist['name'])), 'id' => $categorylist['id']])}}"><i class="{{$categorylist['icon']}}"></i> {{$categorylist['name']}}</a>
                                    <div class="mega-menu">
                                            <div class="mega-menu__column">
                                                <h4>{{$categorylist['name']}}</h4>
                                                <ul class="mega-menu__list">
                                                    @foreach($subcategories[$categorylist['id']] as $subcat)
                                                        <li><a href="{{route('product_list_subcategory', ['category' => strtolower(str_replace(" ","-",$categorylist['name'])),'subcategory' => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$subcat['name'])), 'id' => $subcat['id']])}}">{{$subcat['name']}}</a></li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                    </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="header__content-center">
                    <form class="ps-form--quick-search" action="{{route('search_list')}}" method="get">
                        <!-- <div class="form-group--icon"><i class="icon-chevron-down"></i>
                            <select class="form-control">
                                <option value="1">All</option>
                                <option value="1">Smartphone</option>
                                <option value="1">Sounds</option>
                                <option value="1">Technology toys</option>
                            </select>
                        </div> -->
                        <input class="form-control" type="search" name="search" value="@if(isset($search)) {{ $search }} @endif" placeholder="I'm shopping for...">
                        <button type = "submit">Search</button>
                    </form>
                    <p>
                        @foreach($hashtags as  $hashtag)
                            <a href="<?php echo URL::to('/')?>/user/search/list?search={{ $hashtag->hashtag }}">{{ $hashtag->hashtag }}</a>
                        @endforeach
                    </p>
                </div>
                <div class="header__content-right">
                    <div class="header__actions">
                        <div class="ps-cart--mini"><a class="header__extra" href="javascript:void(0);"><i class="icon-bag2"></i><span><i>@if(sizeof($userscartProducts) > 0) {{ sizeof($userscartProducts) }} @else 0 @endif</i></span></a>
                            <div class="ps-cart__content">
                                @if(sizeof($userscartProducts) > 0)
                                    <div class="ps-cart__items">
                                        @foreach($userscartProducts as $cartProduct)
                                            <div class="ps-product--cart-mobile">
                                                <div class="ps-product__thumbnail"><a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$cartProduct->product->product_name)), 'id' => $cartProduct->product->id])}}"><img src="/product_photo/{{$cartProduct->product->productImage->name}}" alt=""></a></div>
                                                <div class="ps-product__content"><a class="ps-product__remove" href="javascript:void(0);" onclick="deleteCartPopup({{$cartProduct->cartId}})" ><i class="icon-cross"></i></a><a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$cartProduct->product->product_name)), 'id' => $cartProduct->product->id])}}">{{ $cartProduct->product->product_name}}</a>
                                                    <p><strong>Sold by:</strong> {{ $cartProduct->product->productPrice[0]->vendor['nickname'] }}</p><small>{{ $cartProduct->cartQauntity }} x ₹ {{ number_format($cartProduct->product->productPrice[0]->final_price) }} </small>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="ps-cart__footer">
                                        <h3>Sub Total:<strong>₹{{  subtotal($userscartProducts) }}</strong></h3>
                                        <figure><a class="ps-btn" href="{{ route('my_cart') }}">View Cart</a><a class="ps-btn" href="{{ route('checkout') }}">Checkout</a></figure>
                                    </div>
                                @else
                                    <div class="ps-cart__items empty-cart">
                                        <div class="ps-product--cart-mobile">
                                            <span>Your cart is Empty</span>
                                        </div>
                                        <div class="ps-cart__footer">
                                            <figure><a class="ps-btn" href="{{ route('front') }}">Shop Now</a></figure>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="ps-block--user-header">
                            <div class="ps-block__left"><a href="@if(Auth::user())@if( Auth::user()->type == 'vendor') {{ route('vendor_home') }} @else {{ route('my_account') }}  @endif @else  {{ route('signup') }} @endif"><i class="icon-user"></i></a></div>
                            @if(Auth::user())
                                @if(Auth::user()->name != null)
                                    <div class="ps-block__right"><a href="@if( Auth::user()->type == 'vendor') {{ route('vendor_home') }} @else {{ route('my_account') }}  @endif">{{ Auth::user()->name }}</a></div>
                                @else
                                    <div class="ps-block__right"><a href="@if( Auth::user()->type == 'vendor') {{ route('vendor_home') }} @else {{ route('my_account') }}  @endif">**{{ substr (Auth::user()->mobileno, -4) }}</a></div>
                                @endif
                            @else
                                <div class="ps-block__right"><a href="/signup">Login</a><a href="/signup">Register</a></div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <header class="header header--mobile header--mobile-categories" data-sticky="true">
         <div class="header__top">
            <div class="header__left">
                <p>Welcome to DASH Online Local Shop !</p>
            </div>
            <div class="header__right">
                 <li id="pin-li" onClick="subscribePopup()" ><span id="change-pin">Change Your Pincode</span><div class="pincode-box"><span class="pincode"></span><i class="icon-pencil" ></i></div></li>
                <!-- <ul class="navigation__extra">
                    <li><a href="javascript:void(0);">Sell on DASH</a></li>
                    <li><a href="javascript:void(0);">Tract your order</a></li>
                    <li>
                        <div class="ps-dropdown"><a href="javascript:void(0);">US Dollar</a>
                            <ul class="ps-dropdown-menu">
                                <li><a href="javascript:void(0);">Us Dollar</a></li>
                                <li><a href="javascript:void(0);">Euro</a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="ps-dropdown language"><a href="javascript:void(0);"><img src="img/flag/en.png" alt="">English</a>
                            <ul class="ps-dropdown-menu">
                                <li><a href="javascript:void(0);"><img src="img/flag/germany.png" alt=""> Germany</a></li>
                                <li><a href="javascript:void(0);"><img src="img/flag/fr.png" alt=""> France</a></li>
                            </ul>
                        </div>
                    </li>
                </ul> -->
            </div>
        </div>
        <div class="navigation--mobile">
            <div class="navigation__left"><a class="ps-logo" href="<?php echo URL::to('/'); ?>"><img src="<?php echo URL('/'); ?>/user/img/coming-soon-logo.png"  width="100px" alt=""></a></div>
            <div class="navigation__right">
                <div class="header__actions">
                    <div class="ps-cart--mini"><a class="header__extra" href="javascript:void(0);"><i class="icon-bag2"></i><span><i>@if(sizeof($userscartProducts) > 0) {{ sizeof($userscartProducts) }} @else 0 @endif</i></span></a>
                        <div class="ps-cart__content">
                        @if(sizeof($userscartProducts) > 0)
                                <div class="ps-cart__items">
                                    @foreach($userscartProducts as $cartProduct)
                                        <div class="ps-product--cart-mobile">
                                            <?php $pUrl = $cartProduct->product->id.'_'.$cartProduct->vendor_id; ?>
                                            <div class="ps-product__thumbnail"><a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$cartProduct->product->product_name)), 'id' => $pUrl ])}}"><img src="/product_photo/{{$cartProduct->product->productImage->name}}" alt=""></a></div>
                                            <div class="ps-product__content"><a class="ps-product__remove" href="javascript:void(0);" onclick="deleteCartPopup({{$cartProduct->cartId}})"   title="Delete"><i class="icon-cross"></i></a><a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$cartProduct->product->product_name)), 'id' => $cartProduct->product->id.'_'.$cartProduct->vendor_id ])}}">{{ $cartProduct->product->product_name}}</a>
                                                <p><strong>Sold by:</strong> {{ $cartProduct->vendor['nickname'] }}</p><small>{{ $cartProduct->cartQauntity }} x ₹ {{ number_format($cartProduct->final_price) }} </small>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="ps-cart__footer">
                                    <h3>Sub Total:<strong>₹{{  subtotal($userscartProducts) }}</strong></h3>
                                    <figure><a class="ps-btn" href="{{ route('my_cart') }}">View Cart</a><a class="ps-btn" href="{{ route('checkout') }}">Checkout</a></figure>
                                </div>
                            @else
                                <div class="ps-cart__items empty-cart">
                                    <div class="ps-product--cart-mobile">
                                        <span>Your cart is Empty</span>
                                    </div>
                                    <div class="ps-cart__footer">
                                        <figure><a class="ps-btn" href="{{ route('front') }}">Shop Now</a></figure>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="ps-block--user-header">
                    <div class="ps-block__left"><a href="@if(Auth::user())@if( Auth::user()->type == 'vendor') {{ route('vendor_home') }} @else {{ route('my_account') }}  @endif @else  {{ route('signup') }} @endif"><i class="icon-user"></i></a></div>
                        @if(Auth::user())
                            @if(Auth::user()->name != null)
                                <div class="ps-block__right"><a href="/user/my-account">{{ Auth::user()->name }}</a></div>
                            @else
                                <div class="ps-block__right"><a href="/user/my-account">**{{ substr (Auth::user()->mobileno, -4) }}</a></div>
                            @endif
                        @else
                            <div class="ps-block__right"><a href="/signup">Login</a><a href="/signup">Register</a></div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="ps-search--mobile">
            <form class="ps-form--search-mobile" action="<?php echo URL::to('/'); ?>" method="get">
                <div class="form-group--nest">
                    <input class="form-control" type="text" placeholder="Search something...">
                    <button><i class="icon-magnifier"></i></button>
                </div>
            </form>
        </div> -->
        </nav>
        <div class="header__filter">
            <button class="ps-shop__filter-mb" id="filter-sidebar"><i class="icon-equalizer"></i><span>Filter</span></button>
            <div class="header__sort"><i class="icon-sort-amount-desc"></i>
                <select name="sort" class="ps-select sorting" >
                    <option value="distance" >Sort by distance</option>
                    <option value="id" >Sort by popularity</option>
                    <option value="id" >Sort by average rating</option>
                    <option value="priceasc" >Sort by price: low to high</option>
                    <option value="pricedesc" >Sort by price: high to low</option>
                </select>
            </div>
        </div>
    </header>
    <div class="ps-panel--sidebar" id="cart-mobile">
        <div class="ps-panel__header">
            <h3>Shopping Cart</h3>
        </div>
        <div class="navigation__content">
            <div class="ps-cart--mobile">
                @if(sizeof($userscartProducts) > 0)
                    <div class="ps-cart__items">
                        @foreach($userscartProducts as $cartProduct)
                            <div class="ps-product--cart-mobile">
                                <div class="ps-product__thumbnail"><a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$cartProduct->product->product_name)), 'id' => $cartProduct->product->id])}}"><img src="/product_photo/{{$cartProduct->product->productImage->name}}" alt=""></a></div>
                                <div class="ps-product__content"><a class="ps-product__remove" href="javascript:void(0);" onclick="deleteCartPopup({{$cartProduct->cartId}})" ><i class="icon-cross"></i></a><a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$cartProduct->product->product_name)), 'id' => $cartProduct->product->id])}}">{{ $cartProduct->product->product_name}}</a>
                                    <p><strong>Sold by:</strong> {{ $cartProduct->product->productPrice[0]->vendor['nickname'] }}</p><small>{{ $cartProduct->cartQauntity }} x ₹ {{ number_format($cartProduct->product->productPrice[0]->final_price)}} </small>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="ps-cart__footer">
                        <h3>Sub Total:<strong>₹{{  subtotal($userscartProducts) }}</strong></h3>
                        <figure><a class="ps-btn" href="{{ route('my_cart') }}">View Cart</a><a class="ps-btn" href="{{ route('checkout') }}">Checkout</a></figure>
                    </div>
                @else
                    <div class="ps-cart__items empty-cart">
                        <div class="ps-product--cart-mobile">
                            <span>Your cart is Empty</span>
                        </div>
                        <div class="ps-cart__footer">
                            <figure><a class="ps-btn" href="{{ route('front') }}">Shop Now</a></figure>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="ps-panel--sidebar" id="navigation-mobile">
        <div class="ps-panel__header">
            <h3>Categories</h3>
        </div>
        <div class="ps-panel__content">
            <ul class="menu--mobile">
                @foreach( $categories as $categorylist)
                        <li class="menu-item-has-children has-mega-menu" ><a href="{{route('product_list', ['category' => strtolower(str_replace(" ","-",$categorylist['name'])), 'id' => $categorylist['id']])}}">{{ $categorylist['name'] }}</a><span class="sub-toggle"></span>
                        <div class="mega-menu">
                                <div class="mega-menu__column">
                                        @foreach($subcategories[$categorylist['id']] as $subcat)
                                            <h4><a href="{{route('product_list_subcategory', ['category' => strtolower(str_replace(" ","-",$categorylist['name'])),'subcategory' => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$subcat['name'])), 'id' => $subcat['id']])}}">{{$subcat['name']}}</a></h4>
                                        @endforeach
                                </div>
                        </div>
                        </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="navigation--list">
        <div class="navigation__content"><a class="navigation__item ps-toggle--sidebar" href="#menu-mobile"><i class="icon-menu"></i><span> Menu</span></a><a class="navigation__item ps-toggle--sidebar" href="#navigation-mobile"><i class="icon-list4"></i><span> Categories</span></a><a class="navigation__item ps-toggle--sidebar" href="#search-sidebar"><i class="icon-magnifier"></i><span> Search</span></a><a class="navigation__item ps-toggle--sidebar" href="#cart-mobile"><i class="icon-bag2"></i><span> Cart</span></a></div>
    </div>
    <div class="ps-panel--sidebar" id="search-sidebar">
        <div class="ps-panel__header">
            <form class="ps-form--search-mobile" action="{{route('search_list')}}" method="get">
                <div class="form-group--nest">
                    <input class="form-control" type="text" name="search"  value="@if(isset($search)) {{ $search }} @endif" placeholder="Search something...">
                    <button><i class="icon-magnifier"></i></button>
                </div>
            </form>
        </div>
        <div class="navigation__content"></div>
    </div>
    <div class="ps-panel--sidebar" id="menu-mobile">
        <div class="ps-panel__header">
            <h3>Menu</h3>
        </div>
        <div class="ps-panel__content">
            <ul class="menu--mobile">
                <li class="current-menu-item "><a href="<?php echo URL::to('/'); ?>">Home</a></li>
                <li class="current-menu-item menu-item-has-children"><a href="javascript:void(0);">Quick links</a><span class="sub-toggle"></span>
                    <ul class="sub-menu">
                        <li><a href="/return-refund-policy">Return & Refund Policy</a></li>
                        <li><a href="/terms-and-conditions">Term & Condition</a></li>
                        <li><a href="/delivery-or-pickup">Same day Delivery or Pickup</a></li>
                        <!-- <li><a href="javascript:void(0);">Return</a></li> -->
                        <li><a href="/faq">FAQs</a></li>
                    </ul>
                </li>
                <li class="current-menu-item menu-item-has-children"><a href="javascript:void(0);">Company</a><span class="sub-toggle"></span>
                    <ul class="sub-menu">
                        <li><a href="/about-us">About Us</a></li>
                        <!-- <li><a href="/affilate">Affilate</a></li> -->
                        <li><a href="/career">Career</a></li>
                        <li><a href="/contact-us">Contact</a></li>
                    </ul>
                </li>
                <li class="current-menu-item menu-item-has-children"><a href="javascript:void(0);">DASH shop</a><span class="sub-toggle"></span>
                    <ul class="sub-menu">
                        <!-- <li><a href="javascript:void(0);">Our Press</a></li> -->
                        <li><a href="/user/my-cart">My Cart</a></li>
                        <li><a href="/user/my-account">My Account</a></li>
                        <li><a href="/">Shop</a></li>
                    </ul>
                </li>
                <li class="mobile-menu-pin">
                    <div id="pin-li" class="pin-li" onClick="subscribePopup()" ><span id="change-pin">Change Your Pincode</span><div class="pincode-box"><span class="pincode"></span><i class="icon-pencil" ></i></div></div>
                </li>
            </ul>
        </div>
    </div>

    @yield("content")

    <footer class="ps-footer ps-footer--3">
        <div class="container">
            <div class="ps-block--site-features ps-block--site-features-2">
                <div class="ps-block__item">
                    <div class="ps-block__left"><i class="icon-rocket"></i></div>
                    <div class="ps-block__right">
                        <h4>Same day free home delivery</h4>
                        <p>order before 2pm</p>
                    </div>
                </div>
                <div class="ps-block__item">
                    <div class="ps-block__left"><i class="icon-sync"></i></div>
                    <div class="ps-block__right">
                        <h4>Product <br/>exchange</h4>
                        <p>If goods are damaged</p>
                    </div>
                </div>
                <div class="ps-block__item">
                    <div class="ps-block__left"><i class="icon-credit-card"></i></div>
                    <div class="ps-block__right">
                        <h4>100% Secure Payment</h4>
                        <p>UPI, Paytm, Bank transfer and other</p>
                    </div>
                </div>
                <div class="ps-block__item">
                    <div class="ps-block__left"><i class="icon-bubbles"></i></div>
                    <div class="ps-block__right">
                        <h4>Instant chat support</h4>
                        <p>100% customer satisfaction</p>
                    </div>
                </div>
            </div>
            <div class="ps-footer__widgets">
                <aside class="widget widget_footer widget_contact-us">
                    <h4 class="widget-title">Contact us</h4>
                    <div class="widget_content">
                    <a class="km-span-list" id="submit" href="https://tawk.to/chat/5f6b07f1f0e7167d0012eea0/default" target="popup"
  onclick="window.open('https://tawk.to/chat/5f6b07f1f0e7167d0012eea0/default','popup','width=600,height=600'); return false;" >Chat</a>
                        <p>Call us 10 am to 5 pm (Mon to Sat)</p>
                        <h3>+91 99987 63163</h3>
                        <p>Maruti Sharnam, Anand, 388001 <br><a href="mailto:hi@dashshop.in">hi@dashshop.in</a></p>
                        <ul class="ps-list--social">
                            <li><a class="facebook" href="https://www.facebook.com/dashservicesindia/"><i class="fa fa-facebook"></i></a></li>
                            <!-- <li><a class="twitter" href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                            <li><a class="google-plus" href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li> -->
                            <li><a class="instagram" href="https://www.instagram.com/dashservicesindia/"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </aside>
                <aside class="widget widget_footer">
                    <h4 class="widget-title">Quick links</h4>
                    <ul class="ps-list--link">
                        <li><a href="/return-refund-policy">Return & Refund Policy</a></li>
                        <li><a href="/terms-and-conditions">Term & Condition</a></li>
                        <li><a href="/delivery-or-pickup">Same day Delivery or Pickup</a></li>
                        <!-- <li><a href="javascript:void(0);">Return</a></li> -->
                        <li><a href="/faq">FAQs</a></li>
                    </ul>
                </aside>
                <aside class="widget widget_footer">
                    <h4 class="widget-title">Company</h4>
                    <ul class="ps-list--link">
                        <li><a href="/about-us">About Us</a></li>
                        <!-- <li><a href="/affilate">Affilate</a></li> -->
                        <li><a href="/career">Career</a></li>
                        <li><a href="/contact-us">Contact</a></li>
                    </ul>
                </aside>
                <aside class="widget widget_footer">
                    <h4 class="widget-title">DASH shop</h4>
                    <ul class="ps-list--link">
                        <!-- <li><a href="javascript:void(0);">Our Press</a></li> -->
                        <li><a href="/user/my-cart">My Cart</a></li>
                        <li><a href="/user/my-account">My Account</a></li>
                        <li><a href="/">Shop</a></li>
                    </ul>
                </aside>
            </div>
            <div class="ps-footer__links">
                @foreach($categories as $categorylist)
                    <p><strong>{{$categorylist['name']}}:</strong>
                        @foreach($subcategories[$categorylist['id']] as $subcat)
                            <a href="{{route('product_list_subcategory', ['category' => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$categorylist['name'])),'subcategory' => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$subcat['name'])), 'id' => $subcat['id'] ] )}}">{{$subcat['name']}}</a>
                        @endforeach
                    </p>
                @endforeach
            </div>
            <div class="ps-footer__copyright">
                <p>© 2020 DASH. All Rights Reserved</p>
                <p><span>Our Payment Partners:</span><a href="javascript:void(0);"><img src="/user/img/payment-method/visa.jpg" alt="visa"></a><a href="javascript:void(0);"><img src="/user/img/payment-method/mastercard.jpg" alt="mastercard"></a><a href="javascript:void(0);"><img src="/user/img/payment-method/paytm.svg" alt="paytm"></a><a href="javascript:void(0);"><img src="/user/img/payment-method/upi.svg" alt="upi"></a></p>
            </div>
        </div>
    </footer>
    <!--pincode popup -->
    <div class="ps-popup" id="subscribe" data-time="10">
        <div class="ps-popup__content bg--cover" data-background-color="#fff" style="background-color:#fff">
        <a class="ps-popup__close" href="javascript:void(0);"><i class="icon-cross"></i></a>
            <div class="ps-form--subscribe-popup" >
                <div class="ps-form__content">
                    <h4>Get <strong>Same Day</strong> Delivery*</h4>
                    <p>Enter Your Pincode <br /> Shop in your area <br /> Support local</p>
                    @if(sizeof($userscartProducts) > 0)<p class="error">**Changing the pincode will empty your cart.</p>@endif
                    <p class="error-pin"></p>
                    <div class="form-group pininput-div">
                        <input class="form-control" type="tel" placeholder="Pincode e.g. 388001" id="pincode-input" name="pincode-input"  maxlength="6" required>
                        <button class="ps-btn setpincode" >Set Pincode</button>
                    </div>
                    <!-- <div class="ps-checkbox">
                        <input class="form-control" type="checkbox" id="not-show" name="not-show">
                        <label for="not-show">Don't show this popup again</label>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
      <!-- end pincode popup -->
    <!-- filter sidebar mobile -->
    <div class="ps-filter--sidebar">
        <div class="ps-filter__header">
            <h3>Filter Products</h3><a class="ps-btn--close ps-btn--no-boder" href="javascript:void(0);"></a>
        </div>
        <div class="ps-filter__content">
            <aside class="widget widget_shop">
                <h4 class="widget-title">Categories</h4>
                <ul class="ps-list--categories">
                    @foreach( $categories as $categorylist)
                        <li class="current-menu-item menu-item-has-children"><a href="{{route('product_list', ['category' => strtolower(str_replace(" ","-",$categorylist['name'])), 'id' => $categorylist['id']])}}">{{ $categorylist['name'] }}</a><span class="sub-toggle "><i class="fa fa-angle-down"></i></span>
                            <ul class="sub-menu ">
                                @foreach($subcategories[$categorylist['id']] as $subcat)
                                    <li class="current-menu-item "><a href="{{route('product_list_subcategory', ['category' => strtolower(str_replace(" ","-",$categorylist['name'])),'subcategory' => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$subcat['name'])), 'id' => $subcat['id']])}}">{{$subcat['name']}}</a></li>
                                @endforeach
                            </ul>
                        </li>
                    @endforeach
                </ul>
            </aside>
            <aside class="widget widget_shop">
                <figure>
                    <h4 class="widget-title">By Price</h4>
                    <div id="nonlinearmob"></div>
                    <p class="ps-slider__meta">Price:<span class="ps-slider__value">₹<span class="ps-slidermob__min"></span></span>-<span class="ps-slider__value">₹<span class="ps-slidermob__max"></span></span></p>
                </figure>
                <figure>
                    <h4 class="widget-title">By Distance</h4>
                    <div id="nonlineardistancemob"></div>
                    <p class="ps-slider__meta">Distance:<span class="ps-slider__value"><span class="ps-sliderdistancemob__min"></span>KM</span>-<span class="ps-slider__value"><span class="ps-sliderdistancemob__max"></span>KM</span></p>
                </figure>
                @if(sizeof($data['colours']) > 0)
                <figure>
                    <h4 class="widget-title">By Color</h4>
                    @foreach($data['colours'] as $colour)
                    <div class="ps-checkbox ps-checkbox--color  ps-checkbox--inline">
                        <span class="form-control colourinput colour " type="checkbox" id="{{ $colour->colour }}" value="{{ $colour->colour }}" name="colour" style="display: none;">{{ $colour->colour }}</span>
                        <label for="{{ $colour->colour }}"  class="colourlabel" style="background-color:#{{ $colour->colour }} !important;"></label>
                    </div>
                    @endforeach
                </figure>
                @endif
                @if(sizeof($data['sizes']) > 0)
                <figure class="sizes">
                    <h4 class="widget-title">BY SIZE</h4>
                        @foreach($data['sizes'] as $size)
                            <a href="javascript:void(0);"  class="size" value="{{ $size->size }}">{{ $size->size }}</a>
                        @endforeach
                </figure>
                @endif
                @if(sizeof($data['weights']) > 0)
                <figure class="sizes">
                    <h4 class="widget-title">BY WEIGHT</h4>
                    @foreach($data['weights'] as $weight)
                            <a href="javascript:void(0);" class="weight" value="{{ $weight->weight }}">{{ $weight->weight }}</a>
                    @endforeach
                </figure>
                @endif
                @if(sizeof($data['measurements']) > 0)
                <figure class="sizes">
                    <h4 class="widget-title">BY MEASUREMENT</h4>
                        @foreach($data['measurements'] as $measurement)
                            <a href="javascript:void(0);"  class="measurement" value="{{ $measurement->measurement }}">{{ $measurement->measurement }}</a>
                        @endforeach
                </figure>
                @endif
                @if(sizeof($data['storages']) > 0)
                <figure class="sizes">
                    <h4 class="widget-title">BY STORAGE</h4>
                        @foreach($data['storages'] as $storage)
                            <a href="javascript:void(0);"  class="storage" value="{{ $storage->storage }}">{{ $storage->storage }}</a>
                        @endforeach
                </figure>
                @endif
            </aside>
        </div>
    </div>
 <!-- filter sidebar mobile end -->
  <!-- cart popup -->
  @if(count($userscartProducts) > 0)
        <div class="ps-popup" id="cartDelete" data-time="10">
            <div class="ps-popup__content bg--cover" data-background-color="#fff" style="background-color:#fff">
            <!-- <a class="ps-popup__close" href="javascript:void(0);"><i class="icon-cross"></i></a> -->
                <div class="ps-form--subscribe-popup" >
                    <div class="ps-form__content">
                        <input type="hidden" name="deleteProductValue" id="deleteProductValue" value="0">
                        <h4><strong>Delete Cart Item</strong></h4>
                        <p>Are you sure you want to delete cart item?</p>
                        <div class="form-group pininput-div">
                                <span style="display: inline;"><a class="ps-btn delete-cart-popup-close" href="javascript:void(0);" >Cancel</a></span>
                                <span style="display: inline;"><a class="ps-btn delete-btn" onclick="deleteCartProduct();">Delete</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div id="back2top"><i class="pe-7s-angle-up"></i></div>
    <div class="ps-site-overlay"></div>
    <div id="loader-wrapper">
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <div class="ps-search" id="site-search"><a class="ps-btn--close" href="javascript:void(0);"></a>
        <div class="ps-search__content">
            <form class="ps-form--primary-search" action="do_action" method="post">
                <input class="form-control" type="text" placeholder="Search for...">
                <button><i class="aroma-magnifying-glass"></i></button>
            </form>
        </div>
    </div>
    <div class="modal fade" id="product-quickview" tabindex="-1" role="dialog" aria-labelledby="product-quickview" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content"><span class="modal-close" data-dismiss="modal"><i class="icon-cross2"></i></span>
                <article class="ps-product--detail ps-product--fullwidth ps-product--quickview quick-product-data">
                        <!-- data load from the javascript -->
                </article>
            </div>
        </div>
    </div>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-151417642-5"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-151417642-5');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 603777454 -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=AW-603777454"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-603777454');
</script> -->

<!-- <script>
function gtag_report_conversion(url) {
  var callback = function () {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  };
  gtag('event', 'conversion', {
      'send_to': 'AW-603777454/AcHgCOin09sBEK7T858C',
      'event_callback': callback
  });
  return false;
}
</script> -->
<!--Start of Tawk.to Script-->
<!-- <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5f6b07f1f0e7167d0012eea0/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
    Tawk_API.onLoad = function () {
        $('#tawkchat-container iframe').css('margin-bottom', '125px');
        $('#tawkchat-minified-box').css('margin-bottom', '125px');
    };
</script> -->
<script>

var LOADER_CONTAINER = $("#loader");
var LOADER_INTERVAL = 1600;
var LOADERS = [
    ["🚰","Fancy tap"],
    ["🆒","Cool as AC"],
    ["💡","Bulb fuse hai?"],
    ["🍳","Frying pan"],
    ["👇💃☝","Sale. Sale. Sale."],
    ["🔖","Get your coupon"],
    ["🎁","Gf ka b’day hai?"],
    ["🎒","School bag = Cool bag"],
    ["💝","Surprise your wife"],
    ["🍙","Cheeni kum hai?"],
    ["☕","Hello frandz chai le lo"],
    ["💄","Shopkeeper lipstick hai?"],
    ["💃","Yahan sab milege"],
    ["🐎","Jaldi order karo"],
    ["📱🔌","Trrring trrring"],
    ["📲","Smart toh phone hota hai"]
    ["👠👞","Ready for a date night?"],
    ["⏰","It’s never too late"],
    ["🚚","Same day delivery"],

]

// $(document).ready(function() {

//   var cycleLoader = function() {
//     var index = Math.floor(Math.random() * LOADERS.length);
//     var selected = LOADERS[index];
//     var selectedEmoji = selected[0];
//     var selectedText = selected[1];

//     // First transition out the old loader
//     setTimeout(function(){
//       LOADER_CONTAINER.children().addClass("animateOut");
//     }, LOADER_INTERVAL - 300); // This negative value should be the same as $animation-duration in the CSS

//     // Then remove the animated out divs
//     LOADER_CONTAINER.children(".emoji").last().remove();
//     LOADER_CONTAINER.children(".text").last().remove();

//     // Then animate in the new one
//     LOADER_CONTAINER.append('<div class="emoji">' + selectedEmoji + '</div>');
//     LOADER_CONTAINER.append('<div class="text">' + selectedText + '</div>');
//   }

//   setInterval(cycleLoader, LOADER_INTERVAL);
//   cycleLoader(); // Run first time without delay

// });

document.onreadystatechange = function() {
    if (document.readyState !== "complete") {
        document.querySelector("#loader").style.visibility = "visible";
        document.querySelector("body").style.visibility = "hidden";
    } else {
        document.querySelector("#loader").style.display = "none";
        document.querySelector("body").style.visibility = "visible";
    }
};

function deleteCartProduct(){
     var cartId = $("#deleteProductValue").val();
     window.location.href = '<?php echo URL::to('/'); ?>/cart/delete/'+cartId;
 }

</script>
<!--End of Tawk.to Script-->
    <script src="<?php echo URL('/'); ?>/user/plugins/nouislider/nouislider.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/popper.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/masonry.pkgd.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/isotope.pkgd.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/jquery.matchHeight-min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/slick/slick/slick.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/slick-animation.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/lightGallery-master/dist/js/lightgallery-all.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/sticky-sidebar/dist/sticky-sidebar.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/gmap3.min.js"></script>
    <!-- <script src="<?php echo URL('/'); ?>/user/js/infinite-scroll.pkgd.min.js"></script> -->
    <!-- custom scripts-->
    <script src="<?php echo URL('/'); ?>/user/js/main.js"></script>
    <script src="<?php echo URL('/'); ?>/user/js/custom.js"></script>
</body>

</html>
