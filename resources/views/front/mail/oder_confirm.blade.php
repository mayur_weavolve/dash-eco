<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="author" content="DASH shop - free same day home delivery from local shop">
    <meta name="keywords" content="online shop, hardware store near me, mobile shop near me,buy mobile, free same day home delivery, new smartphone, hardware items, new machine, makeup, home decor, material, shops near me">
    <meta name="description" content="Buy from local shops online and get same day home delivery or pickup. Shops ranges from Hardware, computers, smart phones mobile, grocery, clothes, shoes, stationary, home decor, construction products and lot more">
    <title>DASH shop - free same day home delivery from local shop</title>
    <link rel="icon" href="<?php echo URL('/'); ?>/user/img/coming-soon-logo_withoutname.png" type="image/png" />
    <link rel="shortcut icon" href="<?php echo URL('/'); ?>/user/img/coming-soon-logo_withoutname.png" />
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700&amp;amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/fonts/Linearicons/Linearicons/Font/demo-files/demo.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/owl-carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/owl-carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/slick/slick/slick.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/nouislider/nouislider.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/lightGallery-master/dist/css/lightgallery.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/jquery-bar-rating/dist/themes/fontawesome-stars.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/select2/dist/css/select2.min.css">
    <link href="<?php echo URL('/'); ?>/admin/assets/css/tokenize2.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/css/style.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/css/market-place-4.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/css/custom.css">
    <script src="<?php echo URL('/'); ?>/user/plugins/jquery.min.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/assets/js/tokenize2.min.js" type="text/javascript"></script>
    <script src="<?php echo URL('/'); ?>/admin/assets/vendors/ckeditor/ckeditor.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/assets/vendors/ckeditor/adapters/jquery.js"></script>

    
</head>
<style>
    .tables{
        margin:5% 0%;
    }
    .main{
        margin-top: 5%;
    }
    .tables td, .tables th {
        padding: 0.25rem;
        vertical-align: top;
         border-top: none !important;
    }
    .tables tbody+tbody {
        border-top: none !important;
    }

    .newtd{
        padding: 0.25rem;
        vertical-align: top;
        border-top: none !important; 
    }
</style>
    <section class="ps-section--account">
        <div class="container">
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <div class="ps-section__right">
                        <div class="ps-section--account-setting">
                            <div class="ps-section__header">
                                <h5>Hi <name>, thank you for shopping at DASH Shop.</h5>
                            </div>
                            <div class="ps-section__content">
                                
                                <div class="table-responsive">
                                    <table class="table ps-table">
                                        <thead>
                                            <tr>
                                                <th>Product</th>
                                                <th>Price</th>
                                                <th>Quantity</th>
                                                <th>Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($orders as $order)
                                                @foreach($order->subOrders as $sub)
                                                    <tr>
                                                        <td>
                                                            <div class="ps-product--cart">
                                                                <div class="ps-product__thumbnail"><a href="product-default.html"><img src="/{{$sub->product->productImage->name}}" alt=""></a></div>
                                                                <div class="ps-product__content"><a href="product-default.html">{{$sub->product->product_name}}</a>
                                                                    
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><span><i>₹</i>{{number_format($sub->amount)}}</span></td>
                                                        <td>{{$sub->quantity}}</td>
                                                        <td><span><i>₹</i> {{number_format($sub->quantity * $sub->amount)}}</span></td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                </tbody>
                            </table>
                                <table class="tables">
                                        <tr ><td>Feel free to call us on +91 99987 63163 for any help or support.</td></tr>
                                        
                                </table>    

                                <table class="tables">
                                        <tr ><td>Thank you</td></tr>
                                        <tr ><td>DASH Team</td></tr>
                                        <tr ><td>www.dashshop.in</td></tr>
                                </table>    
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-2"></div>
            </div>
        </div>
    </section>
   
    <script src="<?php echo URL('/'); ?>/user/plugins/nouislider/nouislider.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/popper.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/masonry.pkgd.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/isotope.pkgd.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/jquery.matchHeight-min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/slick/slick/slick.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/slick-animation.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/lightGallery-master/dist/js/lightgallery-all.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/sticky-sidebar/dist/sticky-sidebar.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/gmap3.min.js"></script>
    <!-- <script src="<?php echo URL('/'); ?>/user/js/infinite-scroll.pkgd.min.js"></script> -->
    <!-- custom scripts-->
    <script src="<?php echo URL('/'); ?>/user/js/main.js"></script>
    <script src="<?php echo URL('/'); ?>/user/js/custom.js"></script>

</html>

