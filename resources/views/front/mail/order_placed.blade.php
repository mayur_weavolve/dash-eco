<!DOCTYPE html>
<html lang="en">
<head>
<style>

    /*1. RESET AND TYPHOGRAPHY */
    html {
    font-family: sans-serif;
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%;
    }

    body {
    margin: 0;
    }

    article,
    aside,
    details,
    figcaption,
    figure,
    footer,
    header,
    main,
    menu,
    nav,
    section,
    summary {
    display: block;
    }

    audio,
    canvas,
    progress,
    video {
    display: inline-block;
    }

    audio:not([controls]) {
    display: none;
    height: 0;
    }

    progress {
    vertical-align: baseline;
    }

    template,
    [hidden] {
    display: none;
    }

    a {
    background-color: transparent;
    -webkit-text-decoration-skip: objects;
    }

    a:active,
    a:hover {
    outline-width: 0;
    }

    abbr[title] {
    border-bottom: none;
    text-decoration: underline;
    -webkit-text-decoration: underline dotted;
            text-decoration: underline dotted;
    }

    b,
    strong {
    font-weight: inherit;
    }

    b,
    strong {
    font-weight: bolder;
    }

    dfn {
    font-style: italic;
    }

    h1 {
    font-size: 2em;
    margin: 0.67em 0;
    }

    mark {
    background-color: #ff0;
    color: #000;
    }

    small {
    font-size: 80%;
    }

    sub,
    sup {
    font-size: 75%;
    line-height: 0;
    position: relative;
    vertical-align: baseline;
    }

    sub {
    bottom: -0.25em;
    }

    sup {
    top: -0.5em;
    }

    img {
    border-style: none;
    }

    svg:not(:root) {
    overflow: hidden;
    }

    code,
    kbd,
    pre,
    samp {
    font-family: monospace, monospace;
    font-size: 1em;
    }

    figure {
    margin: 1em 40px;
    }

    hr {
    box-sizing: content-box;
    height: 0;
    overflow: visible;
    }

    optgroup {
    font-weight: bold;
    }

    button,
    input {
    overflow: visible;
    }

    button,
    select {
    text-transform: none;
    }

    fieldset {
    border: 1px solid #c0c0c0;
    margin: 0 2px;
    padding: 0.35em 0.625em 0.75em;
    }

    legend {
    box-sizing: border-box;
    color: inherit;
    display: table;
    max-width: 100%;
    padding: 0;
    /* 3 */
    white-space: normal;
    }

    textarea {
    overflow: auto;
    }

    [type="checkbox"],
    [type="radio"] {
    box-sizing: border-box;
    padding: 0;
    }

    [type="number"]::-webkit-inner-spin-button,
    [type="number"]::-webkit-outer-spin-button {
    height: auto;
    }

    [type="search"] {
    -webkit-appearance: textfield;
    outline-offset: -2px;
    }

    [type="search"]::-webkit-search-cancel-button,
    [type="search"]::-webkit-search-decoration {
    -webkit-appearance: none;
    }

    ::-webkit-input-placeholder {
    color: inherit;
    opacity: 0.54;
    }

    ::-webkit-file-upload-button {
    -webkit-appearance: button;
    font: inherit;
    }

    * {
    font-family: "Work Sans", sans-serif;
    font-weight: 400;
    }

    html {
    font-size: 62.5%;
    }

    body {
    font-size: 14px;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
    position: relative;
    color: #000;
    margin-top: 0;
    margin-bottom: 10px;
    font-weight: 700;
    }

    h1 a,
    h2 a,
    h3 a,
    h4 a,
    h5 a,
    h6 a {
    color: inherit;
    }

    h1 {
    font-size: 48px;
    }

    h2 {
    font-size: 36px;
    }

    h3 {
    font-size: 24px;
    }

    h4 {
    font-size: 18px;
    }

    h5 {
    font-size: 14px;
    }

    h6 {
    font-size: 12px;
    }

    p {
    font-size: 1.4rem;
    line-height: 1.6em;
    color: #666;
    }

    p span {
    font-family: inherit;
    color: inherit;
    font-size: inherit;
    }

    a {
    position: relative;
    color: inherit;
    text-decoration: none;
    transition: all 0.4s ease;
    }

    a:hover {
    color: #ffcc00;
    }

    a,
    input,
    textarea,
    button,
    select {
    outline: none;
    }

    a:hover,
    h1:focus,
    h2:focus,
    h3:focus,
    h4:focus,
    h5:focus,
    h6:focus {
    text-decoration: none;
    }

    a:focus,
    button:focus,
    select:focus,
    input:focus,
    textarea:focus {
    outline: none;
    text-decoration: none;
    }

    img {
    max-width: 100%;
    }

    iframe {
    border: 0;
    max-width: 100%;
    width: 100%;
    }

    figure {
    margin: 0;
    }

    .ps-document ul,
    .ps-document ol {
    margin-bottom: 10px;
    }

    .ps-document ul ul,
    .ps-document ul ol,
    .ps-document ol ul,
    .ps-document ol ol {
    margin-bottom: 0;
    }

    .ps-document ul li,
    .ps-document ol li {
    color: #666;
    font-size: 14px;
    line-height: 1.6em;
    }

    .ps-document h5 {
    font-size: 16px;
    font-weight: 600;
    }

    .ps-document p {
    margin-bottom: 4rem;
    line-height: 1.8em;
    color: #666;
    }



    .ps-product--cart {
    display: flex;
    flex-flow: row nowrap;
    align-items: center;
    }

    .ps-product--cart > * {
    width: 100%;
    }

    .ps-product--cart .ps-product__thumbnail {
    max-width: 100px;
    }

    .ps-product--cart .ps-product__content {
    padding-left: 30px;
    }

    .ps-product--cart .ps-product__content a {
    font-size: 16px;
    color: #0066cc;
    }

    .ps-product--cart .ps-product__content a:hover {
    color: #ffcc00;
    }

    .ps-product--cart .ps-product__content p strong {
    font-weight: 500;
    }

    .ps-product--detail {
    margin-bottom: 5rem;
    }

    .ps-product--detail h1 {
    margin-bottom: 10px;
    font-size: 24px;
    color: #000;
    font-weight: 400;
    line-height: 1.2;
    }

    .ps-product--detail .slick-slide {
    outline: none;
    -ms-box-shadow: none;
    box-shadow: none;
    }

    .ps-product--detail .slick-slide:focus, .ps-product--detail .slick-slide:active, .ps-product--detail .slick-slide:hover {
    outline: none;
    }

    .ps-product--detail .ps-product__variants .item {
    margin-bottom: 10px;
    border: 1px solid #d9d9d9;
    cursor: pointer;
    }

    .ps-product--detail .ps-product__variants .item img {
    opacity: 0.5;
    transition: all 0.4s ease;
    }

    .ps-product--detail .ps-product__variants .item .ps-video {
    position: relative;
    z-index: 10;
    }

    .ps-product--detail .ps-product__variants .item .ps-video:before {
    content: '\f04b';
    font-family: FontAwesome;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    color: white;
    z-index: 20;
    }

    .ps-product--detail .ps-product__variants .item:last-child {
    margin-bottom: 0;
    }

    .ps-product--detail .ps-product__variants .item.slick-current {
    border-color: #ffcc00;
    }

    .ps-product--detail .ps-product__variants .item.slick-current img {
    opacity: 1;
    }

    .ps-product--detail .ps-product__variants .item:hover img {
    width: 100%;
    opacity: 1;
    }

    .ps-product--detail .ps-product__gallery {
    position: relative;
    }

    .ps-product--detail .ps-product__gallery .slick-arrow {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    z-index: 100;
    width: 35px;
    height: 35px;
    color: #000;
    font-size: 18px;
    background-color: rgba(255, 255, 255, 0.5);
    border-radius: 4px;
    visibility: hidden;
    opacity: 0;
    }



    @media (max-width: 767px) {
    .ps-filter--shopping .ps-filter__column-switch {
        display: none;
    }
    }

    #shop-filter-lastest a {
    padding: 10px 20px;
    font-size: 16px;
    }

    @media (min-width: 1200px) {
    .table-responsive {
        overflow-x: initial;
    }
    }

    .ps-table thead > tr > th {
    font-family: "Work Sans", sans-serif;
    font-size: 18px;
    font-weight: 700;
    color: #515356;
    text-transform: uppercase;
    border-bottom: 1px solid #e5e5e5;
    }

    .ps-table tbody > tr > td {
    vertical-align: middle;
    padding: 20px 30px;
    border: 1px solid #ddd;
    color: #666;
    }

    .ps-table--specification tbody tr td:first-child {
    background-color: #f4f4f4;
    font-weight: 500;
    color: #000;
    width: 150px;
    }



    .ps-section--account {
    padding: 60px 0;
    }

    .ps-section--account .ps-section__header {
    padding-bottom: 30px;
    }

    .ps-section--account .ps-section__header h3 {
    font-weight: 600;
    }

    .ps-section--account-setting .ps-table {
    border: 1px solid #eaeaea;
    }

    .ps-section--account-setting .ps-table thead tr th {
    text-transform: none;
    font-weight: 500;
    font-size: 16px;
    border: 1px solid #eaeaea;
    border-bottom: none;
    padding: 10px 15px;
    }

    .ps-section--account-setting .ps-table tr td {
    padding: 10px 15px;
    }




    html .bg--parallax {
    position: relative;
    z-index: 10;
    background-attachment: fixed !important;
    background-repeat: no-repeat !important;
    background-size: cover !important;
    background-position: 50% 50%;
    }

    .bg--cover {
    background-position: 50% 50% !important;
    background-size: cover !important;
    }

    .bg--top {
    background-position: 50% 50% !important;
    background-size: cover !important;
    }

    .ps-block--category-2 ul, .ps-block--products-of-category .ps-block__categories ul, .ps-block--product-box ul, .ps-block--categories-box ul, .ps-block--menu-categories .ps-block__content ul, .ps-block--shopping-total ul, .ps-block--ourteam ul, .ps-block--vendor-filter ul, .ps-block--store ul, .ps-block--payment-method ul, .ps-panel--sidebar ul, .widget_sidebar ul, .widget_shop ul, .widget_category ul, .widget_footer ul, .widget--blog ul, .widget--vendor ul, .ps-widget--account-dashboard .ps-widget__content ul, .ps-carousel--animate ul, .ps-product ul, .ps-product--detail .ps-product__countdown figure ul, .ps-product--hot-deal .ps-product__header ul, .ps-product--photo ul, .ps-filter ul, .ps-filter--sidebar ul, .ps-form--account ul, .ps-product-list ul, .ps-deal-of-day ul, .ps-shopping .ps-shopping__header ul, .ps-best-sale-brands ul, .ps-blog .ps-blog__header ul, .ps-my-account-2 ul, .ps-store-list .ps-store-link ul, .ps-page--comming-soon ul, .header ul, .navigation--sidebar ul, .ps-block--category-2 ol, .ps-block--products-of-category .ps-block__categories ol, .ps-block--product-box ol, .ps-block--categories-box ol, .ps-block--menu-categories .ps-block__content ol, .ps-block--shopping-total ol, .ps-block--ourteam ol, .ps-block--vendor-filter ol, .ps-block--store ol, .ps-block--payment-method ol, .ps-panel--sidebar ol, .widget_sidebar ol, .widget_shop ol, .widget_category ol, .widget_footer ol, .widget--blog ol, .widget--vendor ol, .ps-widget--account-dashboard .ps-widget__content ol, .ps-carousel--animate ol, .ps-product ol, .ps-product--detail .ps-product__countdown figure ol, .ps-product--hot-deal .ps-product__header ol, .ps-product--photo ol, .ps-filter ol, .ps-filter--sidebar ol, .ps-form--account ol, .ps-product-list ol, .ps-deal-of-day ol, .ps-shopping .ps-shopping__header ol, .ps-best-sale-brands ol, .ps-blog .ps-blog__header ol, .ps-my-account-2 ol, .ps-store-list .ps-store-link ol, .ps-page--comming-soon ol, .header ol, .navigation--sidebar ol {
        margin: 0;
        padding: 0;
        list-style: none;
    }

    .tables{
        margin:5% 0%;
    }
    .main{
        margin-top: 5%;
    }
    .tables td, .tables th {
        padding: 0.25rem;
        vertical-align: top;
         border-top: none !important;
    }
    .tables tbody+tbody {
        border-top: none !important;
    }

    .newtd{
        padding: 0.25rem;
        vertical-align: top;
        border-top: none !important;
    }
</style>
</head>
    <section class="ps-section--account">
        <div class="container">
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <div class="ps-section__right">
                        <div class="ps-section--account-setting">
                            <div class="ps-section__header">
                                <h5>Hi {{$user->users->name}}, thank you for shopping at DASH Shop.</h5>
                            </div>
                            <div class="ps-section__content">

                                <div class="table-responsive">
                                    <table class="table ps-table">
                                        <thead>
                                            <tr>
                                                <th>Product</th>
                                                <th>Price</th>
                                                <th>Quantity</th>
                                                <th>Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($orders as $order)
                                                @foreach($order->subOrders as $sub)
                                                    <tr>
                                                        <td>
                                                            <div class="ps-product--cart">
                                                                <?php $pURL = $sub->product->id.'_'. $sub->vendor_id; ?>
                                                                <div class="ps-product__thumbnail"><a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$sub->product->product_name)), 'id' =>  $pURL ])}}"><img src="<?php echo URL::to('/'); ?>/product_photo/{{$sub->product->productImage->name}}" alt=""></a></div>
                                                                <div class="ps-product__content"><a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$sub->product->product_name)), 'id' =>  $pURL ])}}">{{$sub->product->product_name}}</a>

                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><span><i>₹</i>{{number_format($sub->amount)}}</span></td>
                                                        <td>{{$sub->quantity}}</td>
                                                        <td><span><i>₹</i> {{number_format($sub->quantity * $sub->amount)}}</span></td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                </tbody>
                            </table>
                                <table class="tables">
                                        <tr ><td>Feel free to call us on +91 99987 63163 for any help or support.</td></tr>

                                </table>

                                <table class="tables">
                                        <tr ><td>Thank you</td></tr>
                                        <tr ><td>DASH Team</td></tr>
                                        <tr ><td>www.dashshop.in</td></tr>
                                </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-2"></div>
            </div>
        </div>
    </section>

</html>

