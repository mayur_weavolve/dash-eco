@extends('layouts.user')

@section('content')
<div id="homepage-6">
        <div class="ps-home-banner">
            <div class="ps-carousel--nav-inside owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="0" data-owl-nav="true" data-owl-dots="true" data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="1" data-owl-duration="1000" data-owl-mousedrag="on" data-owl-animate-in="fadeIn" data-owl-animate-out="fadeOut">
                <div class="ps-banner--market-4" data-background="front_images/banners/1.png"><img src="front_images/banners/3.png" alt="">
                    <div class="ps-banner__content">
                        <h4>Limit Edition</h4>
                        <h3>ENJOY GREAT <br/>SAVINGS <br/>THIS DIWALI <strong> 40%</strong></h3><a class="ps-btn" href="javascript:void(0);">Shop Now</a>
                    </div>
                </div>
                <div class="ps-banner--market-4" data-background="front_images/banners/4.png"><img src="front_images/banners/2.png" alt="">
                    <div class="ps-banner__content">
                        <h4>Version 2018</h4>
                        <h3>GIFT YOUR <br/>LOVED ONES <br/>FROM A LOCAL STORE <strong>  ₹999</strong></h3><a class="ps-btn" href="javascript:void(0);">Shop Now</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="ps-site-features">
            <div class="container">
                <div class="ps-block--site-features ps-block--site-features-2">
                    <div class="ps-block__item">
                        <div class="ps-block__left"><i class="icon-rocket"></i></div>
                        <div class="ps-block__right">
                            <h4>Same day free home delivery</h4>
                            <p>order before 2pm</p>
                        </div>
                    </div>
                    <div class="ps-block__item">
                        <div class="ps-block__left"><i class="icon-sync"></i></div>
                        <div class="ps-block__right">
                            <h4>Product <br/>exchange</h4>
                            <p>If goods are damaged</p>
                        </div>
                    </div>
                    <div class="ps-block__item">
                        <div class="ps-block__left"><i class="icon-credit-card"></i></div>
                        <div class="ps-block__right">
                            <h4>100% Secure Payment</h4>
                            <p>UPI, Paytm, Bank transfer and other</p>
                        </div>
                    </div>
                    <div class="ps-block__item">
                        <div class="ps-block__left"><i class="icon-bubbles"></i></div>
                        <div class="ps-block__right">
                            <h4>Instant chat support</h4>
                            <p>100% customer satisfaction</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ps-promotions">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 "><a class="ps-collection" href="javascript:void(0);"><img src="front_images/1.png" alt=""></a>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 "><a class="ps-collection" href="javascript:void(0);"><img src="front_images/2.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="ps-deal-hot">
          <!--   <div class="container">
                <div class="row">
                    <div class="col-xl-9 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <div class="ps-block--deal-hot" data-mh="dealhot">
                            <div class="ps-block__header">
                                <h3>Deal hot today</h3>
                                <div class="ps-block__navigation"><a class="ps-carousel__prev" href=".ps-carousel--deal-hot"><i class="icon-chevron-left"></i></a><a class="ps-carousel__next" href=".ps-carousel--deal-hot"><i class="icon-chevron-right"></i></a></div>
                            </div>
                            <div class="ps-product__content">
                                <div class="ps-carousel--deal-hot ps-carousel--deal-hot owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="0" data-owl-nav="false" data-owl-dots="false" data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="1" data-owl-duration="1000" data-owl-mousedrag="on">
                                @foreach($hotdeals as $product)
                                    <div class="ps-product--detail ps-product--hot-deal">
                                        <div class="ps-product__header">
                                            <div class="ps-product__thumbnail" data-vertical="true">
                                                <figure>
                                                    <div class="ps-wrapper">
                                                        <div class="ps-product__gallery" data-arrow="true">
                                                            @foreach($product->productImages as $image)
                                                                 <div class="item"><a href="<?php echo URL::to('/'); ?>/product_photo/{{$image->name}}"><img src="<?php echo URL::to('/'); ?>/product_photo/{{$image->name}}" alt=""></a></div>
                                                            @endforeach
                                                            </div>
                                                        <div class="ps-product__badge"><span>Save <br> $9.000</span></div>
                                                    </div>
                                                </figure>
                                                <div class="ps-product__variants" data-item="4" data-md="3" data-sm="3" data-arrow="false">

                                                    @foreach($product->productImages as $image)
                                                        <div class="item"><img src="<?php echo URL::to('/'); ?>/product_photo/{{ $image->name }}" alt=""></div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="ps-product__info">
                                                <h5>{{ $product->productSubcategory->subcategory->name }}</h5>
                                                <h3 class="ps-product__name">{{ $product->product_name }}</h3>
                                                <div class="ps-product__meta">
                                                    <h4 class="ps-product__price sale">$97.78 <del> $156.99</del></h4>
                                                    <div class="ps-product__rating">
                                                        <select class="ps-rating" data-read-only="true">
                                                            <option value="1">1</option>
                                                            <option value="1">2</option>
                                                            <option value="1">3</option>
                                                            <option value="1">4</option>
                                                            <option value="2">5</option>
                                                        </select><span>(1 review)</span>
                                                    </div>
                                                    <div class="ps-product__specification">
                                                        <p>Status:<strong class="in-stock"> In Stock</strong></p>
                                                    </div>
                                                </div>
                                                <div class="ps-product__expires">
                                                    <p>Expires In</p>
                                                    <ul class="ps-countdown" data-time="Oct 21, 2020 23:00:00">
                                                        <li><span class="days"></span>
                                                            <p>Days</p>
                                                        </li>
                                                        <li><span class="hours"></span>
                                                            <p>Hours</p>
                                                        </li>
                                                        <li><span class="minutes"></span>
                                                            <p>Minutes</p>
                                                        </li>
                                                        <li><span class="seconds"></span>
                                                            <p>Seconds</p>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="ps-product__processs-bar">
                                                    <div class="ps-progress" data-value="60"><span class="ps-progress__value"></span></div>
                                                    <p><strong>30/50</strong> Sold</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <aside class="widget widget_best-sale" data-mh="dealhot">
                            <h3 class="widget-title">Top 20 Best Seller</h3>
                            <div class="widget__content">
                                <div class="owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="0" data-owl-nav="false" data-owl-dots="false" data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="1" data-owl-duration="1000" data-owl-mousedrag="on">
                                    <div class="ps-product-group">
                                        @foreach($bestsellers as $product)
                                            <div class="ps-product--horizontal">
                                                <div class="ps-product__thumbnail"><a href="product-default.html"><img src="@if(isset($product->productImage)) <?php echo URL::to('/'); ?>/product_photo/{{$product->productImage->name}} @endif" alt=""></a></div>
                                                <div class="ps-product__content"><a class="ps-product__title" href="product-default.html"> <?php if(strlen($product->product_name) > 35){ echo substr($product->product_name,0,35)."...";  }else{ echo $product->product_name;  }?> </a>
                                                    <div class="ps-product__rating">
                                                        <select class="ps-rating" data-read-only="true">
                                                            <option value="1">1</option>
                                                            <option value="1">2</option>
                                                            <option value="1">3</option>
                                                            <option value="1">4</option>
                                                            <option value="2">5</option>
                                                        </select><span>02</span>
                                                    </div>
                                                    
                                                    <p class="ps-product__price sale">$990.00 <del>$1250.00 </del></p>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div> -->
        </div>
        <div class="ps-top-categories">
            <div class="container">
                <h3>Top categories of the month</h3>
                <div class="row">
                    @foreach($categories as $category)
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-6 ">
                            <div class="ps-block--category"><a class="ps-block__overlay" href="{{route('product_list', ['category' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$category->name)), 'id' => $category->id])}}"></a><img src="<?php echo URL::to('/'); ?>/category_photo/{{$category->image}}" alt="">
                                <p>{{ $category->name }}</p>
                            </div>
                        </div>
                   @endforeach
                </div>
            </div>
        </div>
    </div>
    @endsection
