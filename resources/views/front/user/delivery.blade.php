@extends('layouts.user')

@section('content')
<div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li>Delivery or Pickup</li>
            </ul>
        </div>
    </div>
    <div class="ps-page--single" id="about-us"><img src="<?php echo URL('/'); ?>/user/img/delivery_slider_image.png" alt="">
        <div class="ps-about-intro">
            <div class="container">
                <div class="ps-section__header">
                    <h4>Delivery / Pickup</h4>
                    <h3>At DASH our goal is to offer you same-day delivery within your city.</h3>
                    <p>The time frame for order delivery is divided into two parts:</p><br>
                    <ul>
                        <li><h5>Processing time:      </h5>Order verification, tailoring and quality check. All orders are sent to our DASH Partner vendors for acceptance after the order is placed. The local vendors process the orders, with an aim of same day delivery or pick-up as per your selected option if order is placed before 2:00 PM.<br><br></li>
                        <li><h5>Delivery or Pick-Up:  </h5>This refers to the time it takes for items to be assembled & packed by our local vendors. Delivery or Pick-Up is usually completed within the same day if order is placed before 2:00 PM. For orders placed after 2:00 PM delivery or pick-up is usually completed the next working day. However, at times due to unforeseen circumstances it can take longer from time to time. Any changes in the situation or timings will be communicated to you by the vendor. </li>
                    </ul>
                    <br><br>
                    <p>*Orders on weekend might take some extra time but we will always communicate with you to confirm timings. </p>
                </div>
            </div>
        </div>
    </div>
    @endsection
