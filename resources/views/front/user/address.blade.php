@extends('layouts.user')

@section('content')
<main class="ps-page--my-account">
    <div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li>User Information</li>
            </ul>
        </div>
    </div>
    <section class="ps-section--account">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="ps-section__left">
                        <aside class="ps-widget--account-dashboard">
                            <div class="ps-widget__header"><img src="img/users/3.png" alt="">
                                <figure>
                                    <figcaption>Welcome,</figcaption>
                                    <p><a href="javascript:void(0);">@if(Auth::user()->name === null ) {{Auth::user()->mobileno}} @else {{ Auth::user()->name }} @endif</a></p>
                                </figure>
                            </div>
                            <div class="ps-widget__content">
                                <ul>
                                    <li><a href="{{route('my_account')}}"><i class="icon-user"></i> Account Information</a></li>
                                    <li><a href="{{route('live_order')}}"><i class="icon-cart"></i> Live Order</a></li>
                                    <li><a href="{{route('completed_order')}}"><i class="icon-papers"></i> My Orders</a></li>
                                    <li class="active"><a href="{{route('user_address')}}"><i class="icon-map-marker"></i> Address</a></li>
                                    <!-- <li><a href="javascript:void(0);"><i class="icon-store"></i> Recent Viewed Product</a></li>
                                    <li><a href="javascript:void(0);"><i class="icon-heart"></i> Wishlist</a></li> -->
                                    <li>
                                        <a href="{{ route('logout') }}"><i class="icon-power-switch"></i>{{ __('Logout') }}</a>
                                    </li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="ps-section__right">
                        <form class="ps-form--account-setting" enctype="multipart/form-data" method="post" action="{{route('user_address_add')}}">
                            {{csrf_field() }}
                            <div class="ps-form__header">
                                <h3> User Information</h3>
                            </div>
                            <div class="ps-form__content">
                                <input class="form-control" type="hidden" name="id" value="{{Auth::user()->id}}">
                                <div class="form-group">
                                    <label>Address Line1</label>
                                    <input class="form-control" type="text" name="addressline1" placeholder="Please enter your Address" value="<?php if(count($user) >0){ echo($user[0]->addressline1); }else{ echo('');} ?>" required>
                                    @error('addressline1')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('addressline1') }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                        <label>Address Line2</label>
                                        <input class="form-control" type="text" name="addressline2" placeholder="Please enter your Address" value="<?php if(count($user) >0){ echo($user[0]->addressline2); }else{ echo('');} ?>">
                                        @error('addressline2')
                                            <span class="help-block" role="alert">
                                                <strong>{{ $errors->first('addressline2') }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>City</label>
                                            <input class="form-control" type="text" name="city" placeholder="Please enter your City" value="<?php if(count($user) >0 ){ echo($user[0]->city); }else{ echo('');} ?>" required>
                                            @error('city')
                                            <span class="help-block" role="alert">
                                                <strong>{{ $errors->first('city') }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>State</label>
                                            <input class="form-control" type="text" name="state" placeholder="Please enter your State" value="<?php if(count($user) >0){ echo($user[0]->state); }else{ echo('');} ?>" required>
                                            @error('state')
                                            <span class="help-block" role="alert">
                                                <strong>{{ $errors->first('state') }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Pincode</label>
                                            <input class="form-control" type="text" name="pincode" placeholder="Please enter your Pincode" value="<?php if(count($user)>0){ echo($user[0]->pincode); }else{ echo('');} ?>" required>
                                            @error('pincode')
                                                <span class="help-block" role="alert">
                                                    <strong>{{ $errors->first('pincode') }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group submit">
                                <button class="ps-btn" type="submit">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
@endsection
