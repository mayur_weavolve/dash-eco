@extends('layouts.user')

@section('content')
    <div class="ps-page--my-account">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li>Verification</li>
                </ul>
            </div>
        </div>
        <div class="ps-my-account">
            <div class="container">
                <form class="ps-form--account ps-tab-root" action="{{route('verify_user')}}" method="get">
                    <ul class="ps-tab-list">
                        <li class="active"><a href="#sign-in">Verification</a></li>
                    </ul>
                    <div class="ps-tabs">
                        <div class="ps-tab active" id="sign-in">
                            <div class="ps-form__content">
                                <h5>Log in your account</h5>
                                <div class="form-group">
                                    <input class="form-control mobile_number" type="tel" placeholder="Enter your mobile number" name="mobile_number" pattern="[789][0-9]{9}" min="10" max="10">
                                </div>
                                <div class="form-group form-forgot otpfield">
                                    <input class="form-control otp" type="tel" placeholder="OTP" name="otp"><a href="javascript:void(0);" class="resendotp" >Resend</a>
                                </div>
                                <div id="employees"></div>
                                <!-- <div class="form-group">
                                    <div class="ps-checkbox">
                                        <input class="form-control" type="checkbox" id="remember-me" name="remember-me">
                                        <label for="remember-me">Rememeber me</label>
                                    </div>
                                </div> -->
                                <div class="form-group sendotp">
                                    <button class="ps-btn ps-btn--fullwidth" id="sign-in-button" type="button" >Login</button>
                                </div>
                                <div class="form-group submit">
                                    <button class="ps-btn ps-btn--fullwidth" type="button" >Verify</button>
                                </div>
                                <div class="success-msg"></div>
                                <div class="error-msg"></div>
                                <div id="recaptcha-container"></div>
                            </div>
                            <div class="ps-form__footer">
                                <!-- <p>Connect with:</p>
                                <ul class="ps-list--social">
                                    <li><a class="facebook" href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
                                    <li><a class="google" href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a class="twitter" href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                                    <li><a class="instagram" href="javascript:void(0);"><i class="fa fa-instagram"></i></a></li>
                                </ul> -->
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
    $(".submit").hide();
    $(".otpfield").hide();
    $(".resendotp").click(function(){
            $(".sendotp").click();
    });
    $(".sendotp").click(function(){
        $(".success-msg").text('');
        $(".error-msg").text('');
        firebase.auth().languageCode = 'en';
        firebase.auth().settings.appVerificationDisabledForTesting = false;
            window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
                'size': 'normal',
                'callback': function(response) {
                    // reCAPTCHA solved, allow signInWithPhoneNumber.
                    onSignInSubmit();
                    // debugger;
                },
                'expired-callback': function() {
                    // Response expired. Ask user to solve reCAPTCHA again.
                    window.recaptchaVerifier.render().then(function(widgetId) {
                        grecaptcha.reset(widgetId);
                    });
                    // debugger;
                }
            });



        var mobNum = $(".mobile_number").val();
        var filter = /^\d*(?:\.\d{1,2})?$/;
        if (filter.test(mobNum)) {
            if(mobNum.length==10){
                // $(".loader").show();
               $.ajax({
                    url: '/checkuser/'+mobNum,
                    type: 'get',
                    success: function(data){
                            if(data.user == 0){
                                $(".login").submit();
                                // $(".sendotp").hide();
                                // $(".otpfield").hide();
                            }else{
                                var phoneNumber = "+91"+$('.mobile_number').val();
                                var appVerifier = window.recaptchaVerifier;
                                firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
                                    .then(function (confirmationResult) {
                                        // debugger;
                                        window.confirmationResult = confirmationResult;
                                        $(".submit").show();
                                        $(".sendotp").hide();
                                        $(".otpfield").show();
                                        // $(".loader").hide();
                                        $(".success-msg").text('OTP sent successfully');
                                    }).catch(function (error) {
                                        // debugger;
                                        // $(".loader").hide();
                                        window.recaptchaVerifier.render().then(function(widgetId) {
                                            grecaptcha.reset(widgetId);
                                        });
                                    });
                            }

                    }
                });
             }else {
                $(".error-msg").text('Please enter 10  digit mobile number');
                return false;
              }
            }
            else {
                $(".error-msg").text('Please enter valid mobile number');
                return false;
           }
    });

    $(".submit").click(function(){
        $(".loader").show();
        $(".success-msg").text('');
        $(".error-msg").text('');
         // var code = getCodeFromUserInput();
         var testVerificationCode = $(".otp").val();
         if(testVerificationCode != ''){
            confirmationResult.confirm(testVerificationCode).then(function (result) {
                var user = result.user;
                debugger;
                $(".login").submit();
            }).catch(function (error) {
                console.log(error);
                debugger;
                $(".loader").hide();
                if(error.code == "auth/invalid-verification-code"){
                    $(".error-msg").text('Invalid OTP, Please enter valid OTP');
                    return false;
                }
            });
         }else{
            $(".loader").hide();
            $(".error-msg").text('Please enter OTP');
            return false;
         }

    });

</script>
<script src="https://www.gstatic.com/firebasejs/7.24.0/firebase-app.js"></script>
<!-- <script src="https://www.gstatic.com/firebasejs/7.24.0/firebase-analytics.js"></script> -->
<script src="https://www.gstatic.com/firebasejs/7.24.0/firebase-auth.js"></script>
<!-- <script src="https://www.gstatic.com/firebasejs/7.24.0/firebase-firestore.js"></script> -->
<script src="<?php echo URL('/'); ?>/user/js/init-firebase.js"></script>
@endsection
