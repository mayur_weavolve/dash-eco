<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="author" content="DASH shop - free same day home delivery from local shop">
    <meta name="keywords" content="online shop, hardware store near me, mobile shop near me,buy mobile, free same day home delivery, new smartphone, hardware items, new machine, makeup, home decor, material, shops near me">
    <meta name="description" content="Buy from local shops online and get same day home delivery or pickup. Shops ranges from Hardware, computers, smart phones mobile, grocery, clothes, shoes, stationary, home decor, construction products and lot more">
    <title>DASH Shop - Sell Local &amp; Buy Local &amp; Support Local</title>
    <link rel="icon" href="<?php echo URL('/'); ?>/user/img/coming-soon-logo_withoutname.png" type="image/x-icon" />
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700&amp;amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/fonts/Linearicons/Linearicons/Font/demo-files/demo.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/owl-carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/owl-carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/slick/slick/slick.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/nouislider/nouislider.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/lightGallery-master/dist/css/lightgallery.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/jquery-bar-rating/dist/themes/fontawesome-stars.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/plugins/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="<?php echo URL('/'); ?>/user/css/style.css">
    <style>
            .ps-page--comming-soon .ps-page__header .ps-logo {
                width: 120px;
                height: 120px;
            }
    </style>
</head>

<body>
    <div class="ps-page--comming-soon">
        <div class="container">
            <div class="ps-page__header"><a class="ps-logo" href="/"><img src="<?php echo URL('/'); ?>/user/img/coming-soon-logo.png" alt=""></a>
                <h3> Coming Soon</h3>
                <h1>Sell local | Buy local | Support local</h1>
            </div>
            <div class="ps-page__content"><img src="<?php echo URL('/'); ?>/user/img/coming-soon.jpg" alt="">
                <figure>
                    <figcaption> LAUNCHING IN:</figcaption>
                    <ul class="ps-countdown" data-time="Octomber 29, 2020 12:00:00">
                        <li><span class="days"></span>
                            <p>Days</p>
                        </li>
                        <li><span class="hours"></span>
                            <p>Hours</p>
                        </li>
                        <li><span class="minutes"></span>
                            <p>Minutes</p>
                        </li>
                        <li><span class="seconds"></span>
                            <p>Second</p>
                        </li>
                    </ul>
                </figure>
            </div>
            <div class="ps-page__content">
                <h2>Are you a shop owner?</h2>
                <a class="ps-btn ps-btn--lg" href="/become-a-vendor">Sell online with us</a>
                <br><br>
            </div>
            <div class="ps-page__footer">
                <ul class="ps-list--social">
                    <li><a href="https://www.instagram.com/dashservicesindia/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="https://www.facebook.com/dashservicesindia/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <!-- <li><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li> -->
                    <li><a href="https://www.youtube.com/channel/UCkewbOhdWAt_6nVAs_FLAxw" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div id="back2top"><i class="pe-7s-angle-up"></i></div>
    <div class="ps-site-overlay"></div>
    <div id="loader-wrapper">
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <div class="ps-search" id="site-search"><a class="ps-btn--close" href="javascript:void(0);"></a>
        <div class="ps-search__content">
            <form class="ps-form--primary-search" action="do_action" method="post">
                <input class="form-control" type="text" placeholder="Search for...">
                <button><i class="aroma-magnifying-glass"></i></button>
            </form>
        </div>
    </div>
    <div class="modal fade" id="product-quickview" tabindex="-1" role="dialog" aria-labelledby="product-quickview" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content"><span class="modal-close" data-dismiss="modal"><i class="icon-cross2"></i></span>
                <article class="ps-product--detail ps-product--fullwidth ps-product--quickview">
                    <div class="ps-product__header">
                        <div class="ps-product__thumbnail" data-vertical="false">
                            <div class="ps-product__images" data-arrow="true">
                                <div class="item"><img src="<?php echo URL('/'); ?>/user/img/products/detail/fullwidth/1.jpg" alt=""></div>
                                <div class="item"><img src="<?php echo URL('/'); ?>/user/img/products/detail/fullwidth/2.jpg" alt=""></div>
                                <div class="item"><img src="<?php echo URL('/'); ?>/user/img/products/detail/fullwidth/3.jpg" alt=""></div>
                            </div>
                        </div>
                        <div class="ps-product__info">
                            <h1>Marshall Kilburn Portable Wireless Speaker</h1>
                            <div class="ps-product__meta">
                                <p>Brand:<a href="shop-default.html">Sony</a></p>
                                <div class="ps-product__rating">
                                    <select class="ps-rating" data-read-only="true">
                                        <option value="1">1</option>
                                        <option value="1">2</option>
                                        <option value="1">3</option>
                                        <option value="1">4</option>
                                        <option value="2">5</option>
                                    </select><span>(1 review)</span>
                                </div>
                            </div>
                            <h4 class="ps-product__price">$36.78 – $56.99</h4>
                            <div class="ps-product__desc">
                                <p>Sold By:<a href="shop-default.html"><strong> Go Pro</strong></a></p>
                                <ul class="ps-list--dot">
                                    <li> Unrestrained and portable active stereo speaker</li>
                                    <li> Free from the confines of wires and chords</li>
                                    <li> 20 hours of portable capabilities</li>
                                    <li> Double-ended Coil Cord with 3.5mm Stereo Plugs Included</li>
                                    <li> 3/4″ Dome Tweeters: 2X and 4″ Woofer: 1X</li>
                                </ul>
                            </div>
                            <div class="ps-product__shopping"><a class="ps-btn ps-btn--black" href="javascript:void(0);">Add to cart</a><a class="ps-btn" href="javascript:void(0);">Buy Now</a>
                                <div class="ps-product__actions"><a href="javascript:void(0);"><i class="icon-heart"></i></a><a href="javascript:void(0);"><i class="icon-chart-bars"></i></a></div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
    <!-- Global site tag (gtag.js) - Google Ads: 603777454 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-603777454"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-603777454');
</script>

<script>
function gtag_report_conversion(url) {
  var callback = function () {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  };
  gtag('event', 'conversion', {
      'send_to': 'AW-603777454/AcHgCOin09sBEK7T858C',
      'event_callback': callback
  });
  return false;
}
</script>
    <script src="<?php echo URL('/'); ?>/user/plugins/jquery.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/nouislider/nouislider.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/popper.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/masonry.pkgd.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/isotope.pkgd.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/jquery.matchHeight-min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/slick/slick/slick.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/slick-animation.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/lightGallery-master/dist/js/lightgallery-all.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/sticky-sidebar/dist/sticky-sidebar.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo URL('/'); ?>/user/plugins/gmap3.min.js"></script>
    <!-- custom scripts-->
    <script src="<?php echo URL('/'); ?>/user/js/main.js"></script>
</body>

</html>
