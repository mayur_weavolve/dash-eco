@extends('layouts.user')

@section('content')
<main class="ps-page--my-account">
    <div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li>User Information</li>
            </ul>
        </div>
    </div>
    <section class="ps-section--account">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="ps-section__left">
                        <aside class="ps-widget--account-dashboard">
                            <div class="ps-widget__header"><img src="img/users/3.png" alt="">
                                <figure>
                                    <figcaption>Welcome,</figcaption>
                                    <p><a href="javascript:void(0);">@if($user->name === null ) {{$user->mobileno}} @else {{ $user->name }} @endif</a></p>
                                </figure>
                            </div>
                            <div class="ps-widget__content">
                                <ul>
                                    <li class="active"><a href="{{route('my_account')}}"><i class="icon-user"></i> Account Information</a></li>
                                    <!-- <li><a href="javascript:void(0);"><i class="icon-alarm-ringing"></i> Notifications</a></li> -->
                                    <li><a href="{{route('live_order')}}"><i class="icon-cart"></i> Live Order</a></li>
                                    <li><a href="{{route('completed_order')}}"><i class="icon-papers"></i> My Orders</a></li>
                                    <li><a href="{{route('user_address')}}"><i class="icon-map-marker"></i> Address</a></li>
                                    <!-- <li><a href="javascript:void(0);"><i class="icon-store"></i> Recent Viewed Product</a></li>
                                    <li><a href="javascript:void(0);"><i class="icon-heart"></i> Wishlist</a></li> -->
                                    <li>
                                        <a href="{{ route('logout') }}" ><i class="icon-power-switch"></i>{{ __('Logout') }}</a>
                                    </li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="ps-section__right">
                        <form class="ps-form--account-setting" enctype="multipart/form-data" method="post" action="{{route('user_info_add')}}">
                            {{csrf_field() }}
                            <div class="ps-form__header">
                                <h3> User Information</h3>
                            </div>
                            <div class="ps-form__content">
                                <input class="form-control" type="hidden" name="id" value="{{$user->id}}">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input class="form-control" type="text" name="name" placeholder="Please enter your name" value="{{$user->name}}" required>
                                    @error('name')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Phone Number</label>
                                            <input class="form-control" type="text" name="mobileno" placeholder="Please enter your Phone Number" value="{{$user->mobileno}}"  pattern="[0-9]{10}" required>
                                            @error('mobileno')
                                            <span class="help-block" role="alert">
                                                <strong>{{ $errors->first('mobileno') }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Whatsapp Number</label>
                                            <input class="form-control" type="number" name="whatsapp_no" placeholder="Please enter your Whatsapp Number" value="{{$user->whatsapp_no}}"    pattern="[0-9]{10}" required>
                                            @error('whatsapp_no')
                                            <span class="help-block" role="alert">
                                                <strong>{{ $errors->first('whatsapp_no') }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" type="email" name="email" placeholder="Please enter your Email" value="{{$user->email}}" required>
                                            @error('email')
                                            <span class="help-block" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Pincode</label>
                                            <input class="form-control" type="number" name="pincode" pattern="[0-9]{6}" placeholder="Please enter your Pincode" value="{{$user->pincode}}" required>
                                            @error('pincode')
                                                <span class="help-block" role="alert">
                                                    <strong>{{ $errors->first('pincode') }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group submit">
                                <button class="ps-btn" type="submit">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
@endsection
