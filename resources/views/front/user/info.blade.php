@extends('layouts.user')

@section('content')
<style>
        .ps-section__right{
            margin-top: 10%;
            padding: 34px;
            border: 1px solid #ccc;
        }
</style>
<main class="ps-page--my-account">
    <div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li>Vendor Information</li>
            </ul>
        </div>
    </div>
    <section class="ps-section--account">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="ps-section__left">
                        <aside class="ps-widget--account-dashboard">
                            <div class="ps-widget__header"><img src="img/users/3.png" alt="">
                                <figure>
                                    <figcaption>Welcome,</figcaption>
                                    <p><a href="javascript:void(0);">@if(Auth::user()->name === null ) {{Auth::user()->mobileno}} @else {{ Auth::user()->name }} @endif</a></p>
                                </figure>
                            </div>
                            <div class="ps-widget__content">
                                <ul>
                                    <li><a href="{{route('my_account')}}"><i class="icon-user"></i> Account Information</a></li>
                                    <li><a href="{{route('live_order')}}"><i class="icon-cart"></i> Live Order</a></li>
                                    <li><a href="{{route('completed_order')}}"><i class="icon-papers"></i> Invoices</a></li>
                                    <li class="active"><a href="{{route('user_address')}}"><i class="icon-map-marker"></i> Address</a></li>
                                    <!-- <li><a href="javascript:void(0);"><i class="icon-store"></i> Recent Viewed Product</a></li>
                                    <li><a href="javascript:void(0);"><i class="icon-heart"></i> Wishlist</a></li> -->
                                    <li>
                                        <a href="{{ route('logout') }}"><i class="icon-power-switch"></i>{{ __('Logout') }}</a>
                                    </li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                </div>
                <div class="col-lg-8">
                        <div class="ps-section__right">
                        <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div style="display:inline"><b>Name : </b></div>&nbsp;
                                        <div style="display:inline">{{$info->name}}</div>
                                    </div>
                                    <div class="row">
                                        <div style="display:inline"><b>Email :</b> </div>&nbsp;
                                        <div style="display:inline">{{$info->email}}</div>
                                    </div>
                                    
                                    <div class="row">
                                        <div style="display:inline"><b>Whatsapp No :</b> </div>&nbsp;
                                        <div style="display:inline">{{$info->whatsapp_no}}</div>
                                    </div>
                    
                                    <div class="row">
                                        <div style="display:inline"><b>Mobile No :</b> </div>&nbsp;
                                        <div style="display:inline">{{$info->mobileno}}</div>
                                    </div>
                    
                                    
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div style="display:inline"><b>Address Line1: </b></div>&nbsp;
                                        <div style="display:inline">{{$info->address->addressline1}}</div>
                                    </div>
                    
                                    <div class="row">
                                        <div style="display:inline"><b>Address Line2: </b></div>&nbsp;
                                        <div style="display:inline">{{$info->address->addressline2}}</div>
                                    </div>
                    
                                    <div class="row">
                                        <div style="display:inline"><b>City :</b> </div>&nbsp;
                                        <div style="display:inline">{{$info->address->city}}</div>
                                    </div>
                                    
                                    <div class="row">
                                        <div style="display:inline"><b>State : </b></div>&nbsp;
                                        <div style="display:inline">{{$info->address->state}} , {{$info->address->pincode}}</div>
                                    </div>
                                    
                                    
                                </div>        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
@endsection
