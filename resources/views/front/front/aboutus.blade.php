@extends('layouts.user')

@section('content')
<div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li>About Us</li>
            </ul>
        </div>
    </div>
    <div class="ps-page--single" id="about-us"><img src="<?php echo URL('/'); ?>/user/img/about_us.png" alt="">
        <div class="ps-about-intro">
            <div class="container">
                <div class="ps-section__header">
                    <h4>Welcome to DASH shop</h4>
                    <h3>Were dedicated to giving you the best local shopping experience with wide a range of products and free same day delivery or pickup.</h3>
                    <p>Our mission is to connect local service providers and shops to nearby customers. With a focus on free same-day delivery, on-point customer service & wide-variety to enhance the shopping experience of our users.</p>
                </div>
                <div class="ps-section__content">
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6 ">
                            <div class="ps-block--icon-box"><i class="icon-cube"></i>
                                <h4>1000</h4>
                                <p>Product for sale</p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6 ">
                            <div class="ps-block--icon-box"><i class="icon-store"></i>
                                <h4>2500</h4>
                                <p>Sellers Active on DASH</p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6 ">
                            <div class="ps-block--icon-box"><i class="icon-bag2"></i>
                                <h4>3000</h4>
                                <p>Buyer active on DASH</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ps-newsletter">
        <div class="container">
            <form class="ps-form--newsletter">
                <div class="row">
                    <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <div class="ps-form__left">
                            <h3>Newsletter</h3>
                            <p>Subscribe to get information about products and coupons</p>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <div class="ps-form__right">
                            <div class="form-group--nest">
                                <input class="form-control" id="email" type="email" placeholder="Email address" name="email" required>
                                <a class="ps-btn" id="subscribe">Subscribe</a>
                            </div>
                            <div class="messageDiv"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        $("#subscribe").click(function(){
            var email = $('#email').val();
            if(email != ''){
            $.ajax({
                url: '<?php echo URL::to('/'); ?>/subscribe',
                type:"post",
                data:{
                    '_token': '{{csrf_token()}}',
                    'email' : email,
                    },
                success: function(response){
                   response = JSON.parse(response);
                        if(response.status === 'success'){
                            var message = response.message;
                             var  html = '<div>'+message+'</div>';
                                $('.messageDiv').html(html);

                        }
                }
            });
            }else{
                var html = '<div>Please Enter Email Id</div>';
                $('.messageDiv').html(html);
            }

       });
       </script>
    @endsection
