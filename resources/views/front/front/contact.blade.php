@extends('layouts.user')

@section('content')
<div class="ps-page--single" id="contact-us">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li>Contact Us</li>
                </ul>
            </div>
        </div>

        <div class="ps-contact-info">
            <div class="container">
                <div class="ps-section__header">
                    <h3>Contact Us</h3>
                </div>
                <div class="ps-section__content">
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 ">
                            <div class="ps-block--contact-info">
                                <h4>Contact Directly</h4>
                                <p><a href="javascript:void(0);">hi@dashshop.in</a><span>(+91) 99987 63163</span></p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 ">
                            <div class="ps-block--contact-info">
                                <h4>Head Quater</h4>
                                <p>Maruti Sharnam, Anand <br> Gujarat - India</span></p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 ">
                            <div class="ps-block--contact-info">
                                <h4>Work With Us</h4>
                                <p><span>Send your CV to our email:</span><a href="javascript:void(0);">hi@dashshop.in</a></p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 ">
                            <div class="ps-block--contact-info">
                                <h4>Customer Service</h4>
                                <p><a href="javascript:void(0);">help@dashshop.in</a><span>(+91) 99987 63163</span></p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 ">
                            <div class="ps-block--contact-info">
                                <h4>Media Relations</h4>
                                <p><a href="javascript:void(0);">hi@dashshop.in</a></p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 ">
                            <div class="ps-block--contact-info">
                                <h4>Vendor Support</h4>
                                <p><a href="javascript:void(0);">vendor@dashshop.in</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ps-contact-form">
            <div class="container">
                <!-- <form class="ps-form--contact-us" action="" method=""> -->
                    <!-- <div class="messageDiv"></div> -->
                    <div class="row">
                        <!-- <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                            <div class="form-group">
                                <input class="form-control name" type="text" placeholder="Name *" name="name" value="" required>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                            <div class="form-group">
                                <input class="form-control email" type="text" placeholder="Email *" name="email" value="" required>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <div class="form-group">
                                <input class="form-control subject" type="text" placeholder="Subject *" name="subject" value="">
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <div class="form-group">
                                <textarea class="form-control message" rows="5" placeholder="Message" name="message" value=""></textarea>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-md-12 form-group submit text-center">
                    <h3>Chat with us</h3>
                        <a class="ps-btn" id="submit" href="https://tawk.to/chat/5f6b07f1f0e7167d0012eea0/default" target="popup"
  onclick="window.open('https://tawk.to/chat/5f6b07f1f0e7167d0012eea0/default','popup','width=600,height=600'); return false;" >Chat</a>
                    </div>
                <!-- </form> -->
            </div>
        </div>
    </div>
    <script>
         $("#submit").click(function(){
            var name = $(".name").val();
            var email = $(".email").val();
            var subject = $(".subject").val();
            var message = $(".message").val();
            if((name != '')  && (email != '') && (subject !='') && (message != '')){
            $.ajax({
                url: '<?php echo URL::to('/'); ?>/get-in-touch',
                type:"post",
                data:{
                    '_token': '{{csrf_token()}}',
                    'name' : name,
                    'email' : email,
                    'subject': subject,
                    'message':message,

                },
                success: function(response){
                   response = JSON.parse(response);
                        if(response.status === 'success'){
                             var  html = '<div>SUccessfully Email Sent</div>';
                                $('.messageDiv').html(html);
                        }
                }
            });
            }else{
                var html = '<div>Please Fill all Field</div>';
                $('.messageDiv').html(html);
            }

        });
        </script>


@endsection
