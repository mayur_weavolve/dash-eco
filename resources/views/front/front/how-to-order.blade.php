@extends('layouts.user')
@section('content')
<div class="ps-page--single">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li>How to Order</li>
                </ul>
            </div>
        </div>
        <div class="ps-vendor-banner bg--cover" data-background="img/bg/vendor.jpg">
            <div class="container">
                <h2>Millions Of Shoppers Can’t Wait To See What You Have In Store</h2><a class="ps-btn ps-btn--lg" href="javascript:void(0);">Start Selling</a>
            </div>
        </div>
        <div class="ps-section--vendor ps-vendor-milestone">
            <div class="container">
                <div class="ps-section__header">
                    <h4>How to Order on DASH Shop?</h4>
                </div>
                <div class="ps-section__content">
                    <div class="ps-block--vendor-milestone">
                        <div class="ps-block__left">
                            <h4>Step 1: </h4>
                            <ul>
                                <li>Enter your pincode</li>
                            </ul>
                        </div>
                        <div class="ps-block__right"><img src="img/vendor/milestone-1.png" alt=""></div>
                        <div class="ps-block__number"><span>1</span></div>
                    </div>
                    <div class="ps-block--vendor-milestone reverse">
                        <div class="ps-block__left">
                            <h4>Step 2: </h4>
                            <ul>
                                <li>Browse local products from nearby shops  </li>
                            </ul>
                        </div>
                        <div class="ps-block__right"><img src="img/vendor/milestone-2.png" alt=""></div>
                        <div class="ps-block__number"><span>2</span></div>
                    </div>
                    <div class="ps-block--vendor-milestone">
                        <div class="ps-block__left">
                            <h4>Step 3:</h4>
                            <ul>
                                <li>Add to cart & pay with UPI, Paytm, bank transfers or bank card </li>
                            </ul>
                        </div>
                        <div class="ps-block__right"><img src="img/vendor/milestone-3.png" alt=""></div>
                        <div class="ps-block__number"><span>3</span></div>
                    </div>
                    <div class="ps-block--vendor-milestone reverse">
                        <div class="ps-block__left">
                            <h4>Step 4: </h4>
                            <ul>
                                <li>Get your same-day delivery or shop pickup </li>
                            </ul>
                        </div>
                        <div class="ps-block__right"><img src="img/vendor/milestone-4.png" alt=""></div>
                        <div class="ps-block__number"><span>4</span></div>
                    </div>

                </div>
            </div>
        </div>



        <!-- <div class="ps-vendor-banner bg--cover" data-background="img/bg/vendor.jpg">
            <div class="container">
                <h2>It's time to start making money.</h2><a class="ps-btn ps-btn--lg" href="javascript:void(0);">Start Selling</a>
            </div>
        </div> -->
    </div>
@endsection
