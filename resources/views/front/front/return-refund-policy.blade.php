@extends('layouts.user')

@section('content')
<style>

    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;

    }
    .r1class{
        width: 20%;
        padding: 0px 18px;
        text-align: center;
        background-color: #fcb800;
    }
    .dclass{
        width: 20%;
        padding: 0px 18px;
        text-align: center;
    }
    .rclass{
        width: 20%;
        padding: 0px 18px;
        text-align: left;
        font-weight: 400;
    }
    .spanText{
        font-weight: 600;
    }
    .subSpan{
        font-size: 10px;
        font-weight: 600;
    }
    .mclass{
        width: 20%;
        padding: 0px 18px;
        text-align: center;
        background-color: black;
        color: white;
    }
    .return-div h4{
            margin-bottom: 0px !important;
            margin-top: 20px !important;
        }
    @media only screen and (max-width: 600px) {
        table{
            margin-left:-15px;
            font-size: 10px;
        }

    }
</style>
<div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li>Return & Refund Policy</li>
            </ul>
        </div>
    </div>

        <div class="ps-about-intro">
            <div class="container">
                <div class="ps-section__header">
                    <h4>Return & Refund Policy</h4>
                    <table style="width:100%">
                        <tr>
                          <th class="mclass"><span class="spanText">Case Scenario Examples</span></th>
                          <th class="r1class"><span class="spanText">Refund</span></th>
                          <th class="r1class"><span class="spanText">Exchange</span><br/><span class="subSpan">(within 48 hours of purchase)</span></th>
                          <th class="r1class"><span class="spanText">Warranty</span><br/><span class="subSpan">(refers to the product warranty specified by the vendor)</span></th>
                          <th class="r1class mhide"><span class="spanText">We might be able to help you</span></th>
                        </tr>
                        <tr>
                            <td class="r1class"><span class="spanText">Incorrect product ordered</span></td>
                            <td class="rclass"><span>No</span></td>
                            <td class="rclass"><span>No</span></td>
                            <td class="rclass"><span>No</span></td>
                            <td class="rclass mhide"><span >Yes</span></td>
                        </tr>
                        <tr>
                            <td class="r1class"><span class="spanText">DASH Partner delayed pickup/delivery time by more than 48 hours</span></td>
                            <td class="rclass"><span>Yes</span></td>
                            <td class="rclass"><span>No</span></td>
                            <td class="rclass"><span>No</span></td>
                            <td class="rclass mhide"><span>No</span></td>
                        </tr>
                        <tr>
                            <td class="r1class"><span class="spanText">Faulty/Damaged product</span></td>
                            <td class="rclass"><span>No</span></td>
                            <td class="rclass"><span>Yes</span></td>
                            <td class="rclass"><span>Yes</span></td>
                            <td class="rclass mhide"><span>No</span></td>
                        </tr>
                        <tr>
                            <td class="r1class"><span class="spanText">Incorrect delivery of items by vendor</span></td>
                            <td class="rclass"><span>Yes</span></td>
                            <td class="rclass"><span>Yes</span></td>
                            <td class="rclass"><span>No</span></td>
                            <td class="rclass mhide"><span>No</span></td>
                        </tr>
                        <tr>
                            <td class="r1class"><span class="spanText">Item did not match description of product</span></td>
                            <td class="rclass"><span>Yes</span></td>
                            <td class="rclass"><span>Yes</span></td>
                            <td class="rclass"><span>No</span></td>
                            <td class="rclass mhide"><span>No</span></td>
                        </tr>
                        <tr>
                            <td class="r1class"><span class="spanText">Change of mind</span></td>
                            <td class="rclass"><span>No</span></td>
                            <td class="rclass"><span>No</span></td>
                            <td class="rclass"><span>No</span></td>
                            <td class="rclass mhide"><span>No</span></td>
                        </tr>
                        <tr>
                            <td class="r1class"><span class="spanText">Product did not meet customers expectation</span></td>
                            <td class="rclass"><span>No</span></td>
                            <td class="rclass"><span>No</span></td>
                            <td class="rclass"><span>Yes</span></td>
                            <td class="rclass mhide"><span>No</span></td>
                        </tr>
                    </table>
                    <div class="row return-div">
                        <!-- <h4>Definition</h4> -->
                        <div class="col-md-12 col-sm-12">
                            <h4>Refund</h4>
                            <p>- A repayment of a sum of money. Depending on the payment mode, a small amount might be charged as a transaction cost</p>
                            <p>- Refunds will be processed within 24 Hours but it might take up to 7 business days to reflect in your account.</p></br>

                            <h4>Exchange</h4>
                            <p>- A product replacement for another item (identical or different), or a store credit.</p><br>

                            <h4>Warranty</h4>
                            <p>- A type of guarantee that a manufacturer or similar party makes regarding the condition of its product. It also refers to the terms and situations in which repairs or exchanges will be made in the event that the product does not function as originally described or intended.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endsection
