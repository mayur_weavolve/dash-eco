@extends('layouts.user')

@section('content')
<style>
    .emailText{
        color:blue;
        text-decoration: underline;
    }
</style>
<div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li>Career</li>
            </ul>
        </div>
    </div>
    <div class="ps-page--single" id="about-us"><img src="<?php echo URL('/'); ?>/user/img/delivery_slider_image.png" alt="">
        <div class="ps-about-intro">
            <div class="container">
                <div class="ps-section__header">
                    <h4>Career</h4>
                    <p>Join our team to reshape online shopping and make products reach our customers locally with the aim to provide an ultimate shopping experience.</p><br>
                    <p>If your values align with ours then feel free to submit an Expression of Interest to be a part of the DASH Team. Simply, email a copy of your updated resume to <span class="emailText">hi@dashonduty.in</span></p>
                </div>
            </div>
        </div>
    </div>
    @endsection
