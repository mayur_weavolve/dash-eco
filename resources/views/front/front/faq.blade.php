@extends('layouts.user')

@section('content')

    <div class="ps-page--single">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li>Frequently Asked Questions</li>
                </ul>
            </div>
        </div>
        <div class="ps-faqs">
            <div class="container">
                <div class="ps-section__header">
                    <h1>Frequently Asked Questions</h1>
                </div>
                <div class="ps-section__content">
                    <div class="table-responsive">
                        <table class="table ps-table--faqs">
                            <tbody>
                                <tr>
                                    <td class="question"> Can i get a refund if i don't like my product?</td>
                                    <td>No, Unfortunately only products/ services that are damaged/ faulty are applicable for a refund.</td>
                                </tr>
                                <tr>
                                    <td class="question">What is my delivery/ pickup has been delayed by DASH partner?</td>
                                    <td>If delivery/pickup has been delayed by over 48 hours due to the partner, a refund process will be  initiated providing you with cashback or DASH shop credit.</td>
                                </tr>
                                <tr>
                                    <td class="question">What if my product is faulty/damaged?</td>
                                    <td>If the product you have received is damaged/ faulty prior to the delivery, a refund or exchange will be processed depending on Partner stock and product specific warranty.</td>
                                </tr>
                                <tr>

                                    <td class="question"> What if I ordered a wrong/unwanted product?</td>
                                    <td>In case of a mistaken order we suggest you identify the mistake and contact us as soon as possible to help us rectify it. Refund and cancellation in this scenario is subjective to our Partner refund and cancellation policy.</td>
                                </tr>
                                <tr>
                                    <td class="question">What if I have received an incorrect product?</td>
                                    <td>If you have received a wrong product please inform our customer support to help you in assisting with an exchange or refund depending on Partner stock and exchange policy.</td>
                                </tr>
                                <tr>

                                    <td class="question"> What if my product doesn't match its online description?</td>
                                    <td>If your delivered product doesn't match its online description please contact our customer support to help you in assisting a refund or an exchange for the described product. </td>
                                </tr>
                                <tr>
                                    <td class="question">What if my product doesn't match my expectations?</td>
                                    <td>In cases of product expectations please refer to the product warranty provided by the  vendor, if unhelpful please contact DASH support. </td>
                                </tr>
                                <tr>
                                    <td class="question">Where are my vendors located?</td>
                                    <td>DASH partners will be displayed from your nearby location once you have entered your zip code on the DASH Shop home page.</td>
                                </tr>
                                <tr>
                                    <td class="question">Can I choose cash on delivery?</td>
                                    <td>To protect our partners and customers from any kind of fraud we do not offer cash payments but only approved secure payment portals such as: Paytm, Bank transfer, UPI.</td>
                                </tr>
                                <tr>
                                    <td class="question">Is installation included for all products?</td>
                                    <td>Installation charges are subjective to different vendors and products.</td>
                                </tr>
                                <tr>
                                    <td class="question">What if I notice a bug/ issue while using DASH Shop?</td>
                                    <td>If you encounter any issue or bug whilst using DASH Shop, please call out customer support to help us rectify any existing issues or bugs.</td>
                                </tr>
                                <tr>
                                    <td class="question">Are there any guidelines for writing product reviews?</td>
                                    <td>Provide a relevant, unbiased overview of the product. Readers are interested in the pros and the cons of the product. Make sure your review stands the test of time, and what you write today is relevant even after years.</td>
                                </tr>
                                <tr>
                                    <td class="question">I see that the warranty terms for my product have changed from when I bought the product. Will this affect my warranty?</td>
                                    <td>No. The warranty terms for your product will be the same as when you got the product & will not affect your warranty in any way.</td>
                                </tr>
                                <tr>
                                    <td class="question">Can installation be done at different addresses?</td>
                                    <td>Currently, installation service can only be provided at the delivery address of the product.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="ps-call-to-action">
            <div class="container">
                <h3>We’re Here to Help !<a href="{{route('contactUs')}}"> Contact us</a></h3>
            </div>
        </div>
    </div>

    @endsection
