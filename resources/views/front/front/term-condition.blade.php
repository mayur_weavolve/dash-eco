@extends('layouts.user')

@section('content')
<style>
    .ps-section__header p{
        margin-bottom: 0.2rem!important;
    }
    .maintext{
        font-size: 15px!important;
    }
    .mainheader{
        font-size: 24px!important;
        font-weight: 600;
        color: black;
    }
    .subheader{
        font-size: 15px!important;
        font-weight: 600;
        color: black;
    }
    .ulheader{
        font-size: 17px!important;
        font-weight: 600;
        color: black;
    }
    .utext{
        font-size: 15px!important;
        text-decoration: underline;
    }
    .emailClass{
        font-size: 15px!important;
        text-decoration: underline;
        color:blue;
    }
</style>
<div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li> Terms & Conditions</li>
            </ul>
        </div>
    </div>
    
        <div class="ps-about-intro">
            <div class="container">
                <div class="ps-section__header">
                    <h4> Terms & Conditions</h4>
                    <p class="mainheader">Buyers Policies and Buyer Code of Conduct</p>
                    <p class="subheader">Platform for Transaction and Communication</p>
                    <p class="maintext">DASH in the very starting of its policy specifies what DASH actually is. DASH terms itself as a Web Platform rather a market where Users utilize to meet and interact with one another for their transactions. DASH excludes all the liabilities by specifying that DASH is not and cannot be a party to or control in any manner any transaction between the Web Platform’s Users.</p>

                    <p class="utext">Important terms and conditions</p>
                   
                    <p class="maintext"><span class="subheader">Contract:</span> All commercial/contractual terms are offered by and agreed to between Buyers and Sellers alone. DASH does not have any control or does not determine or advise or in any way involve itself in the offering or acceptance of such commercial/contractual terms between the Buyers and Sellers.</p>
                   
                    <p class="maintext"><span class="subheader">Representation:</span> DASH does not make any representation or Warranty as to specifics (such as quality, value, salability, etc) of the products or services proposed to be sold or offered to be sold or purchased on the Web Platform. DASH does not implicitly or explicitly support or endorse the sale or purchase of any products or services on the Web Platform. DASH accepts no liability for any errors or omissions, whether on behalf of itself or third parties.</p>
                   
                    <p class="maintext"><span class="subheader">Breach of Contract:</span>DASH is not responsible for any non-performance or breach of any contract entered into between Buyers and Sellers. DASH cannot and does not guarantee that the concerned Buyers and/or Sellers will perform any transaction concluded on the Web Platform. DASH shall not and is not required to mediate or resolve any dispute or disagreement between Buyers and Sellers.</p>
                   
                    <p class="maintext"><span class="subheader">Warranty:</span>DASH does not make any representation or warranty as to the item-specifics (such as legal title, creditworthiness, identity, etc.) of any of its Users. DASH advises to independently verify the bona fides of any particular.</p>
                   
                    <p class="maintext"><span class="subheader">Right, Title or Interest over product:</span> DASH does not hold any right, title or interest, nor have any obligations or liabilities in respect of any contracts entered into between buyers and sellers. DASH does not take any responsibility for unsatisfactory or delayed performance of services or damages or delays as a result of products which are out of stock, unavailable or bk ordered.</p>
                   
                    <p class="maintext"><span class="subheader">Bipartite contract:</span>DASH is only providing a platform for communication and it is agreed that the contract for sale of any of the products or services shall be a strictly bipartite contract between the Seller and the Buyer.</p>
                   
                    <p class="maintext"><span class="subheader">Limitation of Liability:</span>DASH shall not be liable in any special, incidental, direct or consequential damages in connection to terms of use, even if the concerned user has informed in advance about the possibility of damages.</p>
                   
                    <p class="maintext"><span class="subheader">Payment</span></p>
                    <p class="maintext">While availing any of the payment method/s available on the Web Platform, DASH will not be responsible or assume any liability, whatsoever in respect of any loss or damage arising directly or indirectly because of:</p>
                    <ol>
                            <li>Lack of authorization for any transaction/s, or</li>
                            <li>Exceeding the preset limit mutually agreed by You and between “Bank/s”, or</li>
                            <li>Any payment issues arising out of the transaction, or</li>
                            <li>Decline of transaction for any other reason/s</li>
                    </ol>
                   
                    <p class="maintext"><span class="subheader">Grievance officer</span></p>
                    <p class="maintext">In accordance with Information Technology Act 2000 and rules made there under, the name and contact details of the Grievance Officer are provided below:</p>
                    <p class="maintext">A/U Dash On Duty</p>
                    <p class="maintext">101 Maruti Sharnam, Anand - VV Nagar Road</p>
                    <p class="maintext">388001, Gujarat, India</p>
                    <p class="maintext">Email: <span class="emailClass">hi@dashonduty.in</span></p>
                    
                    <p class="maintext"><span class="subheader">Disputes (Resolutions) Policy</span></p>
                    <p class="maintext">At DASH, has a Dispute Resolution process in order to resolve disputes between Buyers and Sellers.</p>
                    </br>
                    <p class="subheader">Buyer Protection Program</p>

                    <p class="ulheader">Situations where Buyer Protection Program Works:</p>
                    <ol>
                        <li>In case of a dispute where the Seller is unable to provide a refund or a replacement, DASH will actively work towards reaching a resolution.</li>
                        <li>The Buyer Protection Program covers Buyers who are unable to successfully resolve their dispute with the Seller or are not satisfied with the resolution provided by the Seller.</li>
                        <li>The Buyer can write to <span class="emailClass">hi@dashonduty.in</span> if the issue with the Seller is not resolved. DASH’s Customer Support team will look into the case to check for possible fraud and if the Buyer has been blacklisted/blocked from making purchases on the Web Platform. Only after verifying these facts, a dispute can be registered.</li>
                        <li>In due course of resolution, DASH’s Customer Support Team will facilitate a conference call including the Seller and the Buyer.</li>
                        <li>When a dispute has been raised, DASH may provide both the parties access to each other’s Display Names, contact details including email addresses and other details pertaining to the dispute. Buyers and Sellers are subject to final consent from DASH for settling the dispute.</li>
                    </ol>

                    <p class="ulheader">If you are a buyer then you are subject to following restrictions:</p>
                    <ol>
                        <li>There is a limitation period of 24 Hours in which you have to file the complaint.</li>
                        <li>The first step which a buyer should take is contact the seller to resolve the dispute. If the Buyer doesn’t hear from the Seller or is unable to resolve the issue with the Seller even after contact, a dispute can be raised with DASH by writing an email to <span class="emailClass">hi@dashonduty.in</span> </li>
                        <li>Buyers can make a maximum of 5 claims per year on DASH.</li>
                        <li>Buyers are not entitled to immediate refund of money or replacement of the product. DASH will be first verifying the dispute and will be processing the claims that are valid and genuine.</li>
                        <li>Claims related to ‘Buyer remorse’ (i.e. instances where products are bought by the Buyer by mistake or where the Buyer chooses to change his/her mind with regard to the product purchased by him) will not be entertained through this program.</li>
                        <li>(Disclaimer) Don’t initiate invalid or false claims or don’t provide incomplete or misleading information because DASH reserves the right to initiate civil and/or criminal proceedings against you in these cases.</li>
                        <li>If there is a delay in shipment or delivery of the item by the seller then you cannot file a complaint or entertain any claim from DASH through this mechanism.</li>
                    </ol>
                    </br>
                    
                    <p class="mainheader">Selling Policies and Seller Code of Conduct</p>
                    
                    <p class="maintext">All sellers are expected to adhere to the following policies when listing products on DASH. Seller offenses and prohibited content can result in suspension of your DASH account.</p>
                    
                    <p class="maintext"><span class="subheader">Seller code of conduct</span></p>
                    <p class="maintext">This policy requires that sellers act fairly and honestly on DASH to ensure a safe buying and selling experience. All sellers must:</p>
                    <ul>
                        <li>Provide accurate information to DASH and our customers at all times</li>
                        <li>Act fairly and not misuse DASH’s features or services</li>
                        <li>Not attempt to damage or abuse another Seller, their listings or ratings</li>
                        <li>Not attempt to influence customers’ ratings, feedback, and reviews</li>
                        <li>Not send unsolicited or inappropriate communications</li>
                        <li>Not contact customers except through Buyer-Seller Messaging</li>
                        <li>Not attempt to circumvent the DASH sales process</li>
                        <li>Not operate more than one selling account on DASH without a legitimate business need</li>
                    </ul>
                    <p class="maintext">Violating the Code of Conduct or any other DASH policies may result in actions against your account, such as cancellation of listings, suspension or forfeiture of payments, and removal of selling privileges. More details about these policies are below.</p>

                    <p class="maintext"><span class="subheader">Accurate Information</span></p>
                    <p class="maintext">You must provide accurate information to DASH and our customers, and update the information if it changes. For example, this means that you must use a business name that accurately identifies your business and list your products in the correct category.</p>

                    <p class="maintext"><span class="subheader">Acting Fairly</span></p>
                    <p class="maintext">You must act fairly and lawfully and may not misuse any service provided by DASH. Examples of unfair activities include:</p>
                    <ul>
                        <li>Providing misleading or inappropriate information to DASH or our customers, such as by creating multiple detail pages for the same product or posting offensive product images</li>
                        <li>Manipulating sales rank (such as by accepting fake orders or orders that you have paid for) or making claims about sales rank in product titles or descriptions</li>
                        <li>Attempting to increase the price of a product after an order is confirmed</li>
                        <li>Artificially inflating web traffic (using bots or paying for clicks, for example)</li>
                        <li>Attempting to damage another Seller, their listings or ratings</li>
                        <li>Allowing other people to act on your behalf in a way that violates DASH’s policies or your agreement with DASH</li>
                    </ul>

                    <p class="maintext"><span class="subheader">Ratings, Feedback, and Reviews</span></p>
                    <p class="maintext">You may not attempt to influence or inflate customers’ ratings, feedback, and reviews. You may request feedback and reviews from your own customers in a neutral manner, but may not:</p>
                    <ul>
                        <li>Pay for or offer an incentive (such as coupons or free products) in exchange for providing or removing feedback or reviews</li>
                        <li>Ask customers to write only positive reviews or ask them to remove or change a review</li>
                        <li>Solicit reviews only from customers who had a positive experience</li>
                        <li>Review your own products or a competitors’ products</li>
                    </ul>

                    <p class="maintext"><span class="subheader">Communications</span></p>
                    <p class="maintext">You may not send unsolicited or inappropriate messages. All communications to customers must be sent through Buyer-Seller Messaging and be necessary for fulfilling the order or providing customer services. Marketing communications are prohibited. Contacting  a DASH  customer outside of the platform is prohibited.</p>

                    <p class="maintext"><span class="subheader">Customer Information</span></p>
                    <p class="maintext">If you receive customer information such as addresses or phone numbers to fulfill orders, you may use that information only to fulfill orders and must delete it after the order has been processed. You may not use customer information to contact customers (except through Buyer-Seller Messaging) or share it with any third-party.</p>
                    
                    <p class="maintext"><span class="subheader">Circumventing the Sales Process</span></p>
                    <p class="maintext">You may not attempt to circumvent the DASH sales process or divert DASH customers to another website. This means that you may not provide links or messages that prompt users to visit any external website or complete a transaction elsewhere.</p>

                    <p class="maintext"><span class="subheader">Multiple Selling Accounts on DASH</span></p>
                    <p class="maintext">You may only maintain one Seller Central account for each region in which you sell unless you have a legitimate business need to open a second account and all of your accounts are in good standing. If any of your accounts are not in good standing, we may deactivate all of your selling accounts until all accounts are in good standing.
                    Examples of a legitimate business justification include:</p>
                    <ul>
                        <li>You own multiple brands and maintain separate businesses for each</li>
                        <li>You manufacture products for two distinct and separate companies</li>
                        <li>You are recruited for a DASH program that requires separate accounts</li>
                    </ul>

                    <p class="maintext"><span class="subheader">Filing Infringement Notices as an Agent or Brand Protection Agency</span></p>
                    <p class="maintext">DASH understands that many brands may choose to have brand protection agencies or agents report intellectual property infringement on their behalf and accepts submissions from authorized agents. However, DASH does not permit individuals with active selling accounts to file infringement notices as an agent of a brand when the filing of those notices could benefit their own selling account (through removing competing listings, for example). Any sellers filing notices as an agent to benefit their own status as a seller may have their selling account terminated.</p>


                </div>
            </div>
        </div>
    
    @endsection
