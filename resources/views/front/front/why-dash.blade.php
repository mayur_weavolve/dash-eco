@extends('layouts.user')
@section('content')
<div class="ps-page--single">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li>Why Dash</li>
                </ul>
            </div>
        </div>
        <div class="ps-vendor-banner bg--cover" data-background="img/bg/vendor.jpg">
            <div class="container">
                <h2>Millions Of Shoppers Can’t Wait To See What You Have In Store</h2><a class="ps-btn ps-btn--lg" href="javascript:void(0);">Start Selling</a>
            </div>
        </div>
        <div class="ps-section--vendor ps-vendor-milestone">
            <div class="container">
                <div class="ps-section__header">
                    <h4>Why DASH ?</h4>
                </div>
                <div class="ps-section__content">
                    <div class="ps-block--vendor-milestone">
                        <div class="ps-block__left">
                            <h4>Local Vendors</h4>
                            <ul>
                                <li>Trust</li>
                            </ul>
                        </div>
                        <div class="ps-block__right"><img src="img/vendor/milestone-1.png" alt=""></div>
                        <div class="ps-block__number"><span>1</span></div>
                    </div>
                    <div class="ps-block--vendor-milestone reverse">
                        <div class="ps-block__left">
                            <h4>Same Day Delivery/Pickup</h4>
                            <ul>
                                <li>Fast</li>
                            </ul>
                        </div>
                        <div class="ps-block__right"><img src="img/vendor/milestone-2.png" alt=""></div>
                        <div class="ps-block__number"><span>2</span></div>
                    </div>
                    <div class="ps-block--vendor-milestone">
                        <div class="ps-block__left">
                            <h4>Online Payment</h4>
                            <ul>
                                <li>Security </li>
                            </ul>
                        </div>
                        <div class="ps-block__right"><img src="img/vendor/milestone-3.png" alt=""></div>
                        <div class="ps-block__number"><span>3</span></div>
                    </div>
                    <div class="ps-block--vendor-milestone reverse">
                        <div class="ps-block__left">
                            <h4>Wide Product Range</h4>
                            <ul>
                                <li>Variety </li>
                            </ul>
                        </div>
                        <div class="ps-block__right"><img src="img/vendor/milestone-4.png" alt=""></div>
                        <div class="ps-block__number"><span>4</span></div>
                    </div>
                    <div class="ps-block--vendor-milestone reverse">
                        <div class="ps-block__left">
                            <h4>After Sales Service</h4>
                            <ul>
                                <li>Accountability </li>
                            </ul>
                        </div>
                        <div class="ps-block__right"><img src="img/vendor/milestone-4.png" alt=""></div>
                        <div class="ps-block__number"><span>5</span></div>
                    </div>
                </div>
            </div>
        </div>



        <!-- <div class="ps-vendor-banner bg--cover" data-background="img/bg/vendor.jpg">
            <div class="container">
                <h2>It's time to start making money.</h2><a class="ps-btn ps-btn--lg" href="javascript:void(0);">Start Selling</a>
            </div>
        </div> -->
    </div>
@endsection
