@extends('layouts.user')

@section('content')
<style>
    .mob-cart-delete{
        visibility: hidden !important;
    }
    .pc-cart-delete{
        visibility: visible  !important;
    }
    @media only screen and (max-width: 600px) {
        thead,.pc-cart-delete{
            visibility: hidden !important;
        }
        .td-product{
            border-bottom:1px solid #dee2e6;
        }
        .td-product td{
            width: 100% !important;
            display: inline-block !important;
            min-width: 350px !important;
            border-top: none !important;
            padding: 0px 0px 10px 0px !important;
        }
        .mob-cart-delete{
            visibility: visible !important;
            float: left;
            margin-left: 40px;
            margin-right: -45px;

        }
    }
</style>
<div class="ps-page--simple">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="<?php echo URL::to('/'); ?>">Home</a></li>
                    <li><a href="<?php echo URL::to('/'); ?>">Shop</a></li>
                    <li>My Cart</li>
                </ul>
            </div>
        </div>
        <div class="ps-section--shopping ps-shopping-cart">
            <div class="container">
                <div class="ps-section__header">
                    <h1>Shopping Cart</h1>
                </div>
                @if(sizeof($carts) > 0)
                <form id="m_form" enctype="multipart/form-data" method="post" action="{{route('update_cart')}}">
                    {{csrf_field() }}
                <?php $total = 0; ?>
                <div class="ps-section__content">
                    <div class="table-responsive">
                        <table class="table ps-table--shopping-cart">
                            <thead>
                                <tr>
                                    <th>Product name</th>
                                    <th>PRICE</th>
                                    <th>QUANTITY</th>
                                    <th>TOTAL</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($carts as $cart)
                                <?php $subTotal =0 ;?>
                                <tr class="td-product">
                                    <td >
                                        <div class="ps-product--cart">
                                            <div class="ps-product__thumbnail"><a href="javascript:void(0);"><img src="/product_photo/{{$cart->image}}" alt="" style="margin-top:40px;"></a></div>
                                            <?php $pURL = $cart->product_id.'_'.$cart->vendor_id; ?>
                                            <div class="ps-product__content"><a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$cart->product_name)), 'id' =>  $pURL ])}}">{{$cart->product_name}}</a>
                                                <p class="cart-attr">
                                                        @if($cart->colour != '' && $cart->colour != null)
                                                            <span class="ps-variant ps-variant--color colour" data-value="{{$cart->colour}}" style="background-color: #{{$cart->colour}};"></span>
                                                        @endif
                                                       <span> @if($cart->weight != '' && $cart->weight != null) {{ $cart->weight }} @endif
                                                        @if($cart->size != '' && $cart->size != null) {{ $cart->size }} @endif
                                                        @if($cart->measurement != '' && $cart->measurement != null) {{ $cart->measurement }} @endif
                                                        @if($cart->storage != '' && $cart->storage != null) {{ $cart->storage }} @endif
</span>
                                                </p>
                                                <p>Sold By: <strong>{{$cart->product->productPrice[0]->vendor['nickname']}}</strong></p>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="price" style="text-align: center;"><a class="mob-cart-delete" href="{{route('delete_cart',$cart->cId)}}" onclick="return confirm('Are you sure you want to delete this Item?');"  title="Delete"><i class="icon-cross"></i></a><span >₹ {{number_format($cart->final_price)}}<span></td>
                                    <input type="hidden" name="price" id="price{{$cart->cId}}" value="{{$cart->final_price}}">
                                    <td style="text-align: center;">
                                        <div class="form-group--number">
                                            <button type="button" class="up" onclick="up('{{$cart->cId}}');">+</button>
                                            <button type="button" class="down" onclick="down('{{$cart->cId}}');">-</button>
                                            <input class="form-control price" type="hidden" name="cartId[]" value="{{$cart->cId}}">
                                            <input class="form-control quantity" type="number" placeholder="1" name="quantity[]" id="quantity{{$cart->cId}}" value="@if(isset($cart->quantity)){{$cart->quantity}}@else 1 @endif" readonly>
                                        </div>
                                    </td>
                                    <?php
                                        $subTotal = $cart->final_price * $cart->quantity;
                                    ?>
                                    <td style="text-align: center;"><span style="text-align: center;" id="subTotal{{$cart->cId}}" value="{{$subTotal}}">₹ {{number_format($subTotal)}}</span></td>
                                    <td class="pc-cart-delete"><a href="{{route('delete_cart',$cart->cId)}}" onclick="return confirm('Are you sure you want to delete this Item?');"  title="Delete"><i class="icon-cross"></i></a></td>
                                </tr>
                                <?php $total = $total + $subTotal; ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="ps-section__cart-actions">
                        <a class="ps-btn" href="<?php echo URL::to('/'); ?>"><i class="icon-arrow-left"></i> Back to Shop</a>
                        <button data-wizard-action="submit" class="ps-btn ps-btn--outline"><i class="icon-sync"></i> Update cart</button>
                    </div>
                </div>

            </form>

                <div class="ps-section__footer">
                    <div class="row">
                        <!-- <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 ">
                            <figure>
                                <figcaption>Coupon Discount</figcaption>
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="">
                                </div>
                                <div class="form-group">
                                    <button class="ps-btn ps-btn--outline">Apply</button>
                                </div>
                            </figure>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 ">
                            <figure>
                                <figcaption>Calculate shipping</figcaption>
                                <div class="form-group">
                                    <select class="ps-select">
                                        <option value="1">America</option>
                                        <option value="2">Italia</option>
                                        <option value="3">Vietnam</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="Town/City">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="Postcode/Zip">
                                </div>
                                <div class="form-group">
                                    <button class="ps-btn ps-btn--outline">Update</button>
                                </div>
                            </figure>
                        </div> -->
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <div class="ps-block--shopping-total">
                                <!-- <div class="ps-block__header">
                                    <p>Subtotal <span> ₹ {{number_format($total)}}</span></p>
                                </div> -->
                                <div class="ps-block__content">
                                    <!-- @foreach($carts as $cart)
                                    <ul class="ps-block__product">
                                        <li><span class="ps-block__shop">{{$cart->vName}} Shipping</span><span class="ps-block__shipping">Free Shipping</span><span class="ps-block__estimate">Estimate for <strong>Viet Nam</strong><a href="javascript:void(0);"> MVMTH Classical Leather Watch In Black ×1</a></span></li>
                                    </ul>
                                    @endforeach -->
                                    <input type="hidden" id="total" value="{{$total}}" >
                                    <h3>Total <span id="totaltext">₹ {{number_format($total)}}</span></h3>
                                </div>
                            </div><a class="ps-btn ps-btn--fullwidth" href="{{ route('checkout') }}">Proceed to checkout</a>
                        </div>
                    </div>
                </div>
                @else
                    <div class="ps-section__content">
                        <div class="text-center">
                            <div class="cart-text">Your cart is Empty</div>
                            <a class="ps-btn" href="/"><i class="icon-arrow-left"></i> Back to Shop</a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <script>
        function total(price,type){
            var total = $("#total").val();
            if(type === 'up'){
                total = Number(total) + Number(price);
            }else{
                total = Number(total) - Number(price);
            }
            $("#total").val(total);
            $("#totaltext").text('₹'+total.toFixed(2));
        }
        function up(id)
        {
            var qua = $('#quantity'+id).val();
            var price = $('#price'+id).val();
            qua++;
            NewPrice = price * qua;
           $("#quantity"+id).val(qua);
           $("#subTotal"+id).html('₹'+NewPrice.toFixed(2));
           total(price,'up');
            return;
        }

        function down(id)
        {
            var qua = $('#quantity'+id).val();
            var price = $('#price'+id).val();
            var oldQUA = qua;
            if(qua > 1){
                qua--;
                OldQua = parseInt(qua) + 1;
                NewPrice = (price * OldQua) - parseFloat(price);
                $("#quantity"+id).val(qua);
                $("#subTotal"+id).html('₹'+NewPrice.toFixed(2));
                total(price,'down');
            }else{
                document.getElementById("quantity"+id).value = 1;
            }
            return;
        }
    </script>

@endsection
