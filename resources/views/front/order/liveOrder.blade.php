@extends('layouts.user')

@section('content')
<style>
        .link{
            margin-left: 52%;
        }
        .km-span-list{
            margin-top:10% !important;
            margin-left: 47% !important;
            color: #070606 !important;
            font-weight: 800 !important;
            font-size: 18px !important;
            width: 45% !important
         }
         .main{
             margin-top:10px;
            border: 1px solid #c1c1c1;
         }
         .head{
            margin-left: 0px;
            background: #f1f1f1;
            padding-top: 10px;
         }
         .track{
            padding-top:20px;
         }
         .endcol{
            text-align: end;
         }
         .product{
            padding-bottom: 20px;
         }
         .ps-btn{
            padding: 10px 25px;
            border-radius: 14px;
         }
</style>
<main class="ps-page--my-account">
    <div class="ps-breadcrumb">
        <div class="container">
        <ul class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="/user/my-account">My Account</a></li>
               <li>Live Orders</li>
            </ul>
        </div>
    </div>
    <section class="ps-section--account">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="ps-section__left">
                        <aside class="ps-widget--account-dashboard">
                            <div class="ps-widget__header"><img src="/img/users/3.png" alt="">
                                <figure>
                                    <figcaption>Welcome,</figcaption>
                                    <p><a href="javascript:void(0);">@if(Auth::user()->name === null ) {{Auth::user()->mobileno}} @else {{ Auth::user()->name }} @endif</a></p>
                                </figure>
                            </div>
                            <div class="ps-widget__content">
                                <ul>
                                    <li><a href="{{route('my_account')}}"><i class="icon-user"></i> Account Information</a></li>
                                    <li  class="active" ><a href="{{route('live_order')}}"><i class="icon-cart"></i> Live Order</a></li>
                                    <li><a href="{{route('completed_order')}}"><i class="icon-papers"></i> My Orders</a></li>
                                    <li><a href="{{route('user_address')}}"><i class="icon-map-marker"></i> Address</a></li>
                                    <!-- <li><a href="javascript:void(0);"><i class="icon-store"></i> Recent Viewed Product</a></li>
                                    <li><a href="javascript:void(0);"><i class="icon-heart"></i> Wishlist</a></li> -->
                                    <li>
                                        <a href="{{ route('logout') }}"><i class="icon-power-switch"></i>{{ __('Logout') }}</a>
                                    </li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="ps-section__right">
                    <!-- <div class="ps-vendor-dashboard">
                            <div class="container"> -->
                                <!-- <div class="ps-section__content"> -->
                                    <div class="ps-block--vendor-dashboard">
                                        <div class="ps-block__header">
                                            <h3>Live Orders</h3>
                                        </div>
                                        @foreach($orders as $order)
                                        <div class="row main suborder-main">
                                            <div class="row col-md-12 col-sm-12 head">
                                                <div class="col-md-4 col-sm-12 ">
                                                    <p><?php echo date("l, d M Y" ,strtotime("$order->created_at")); ?></p>
                                                </div>
                                                <!-- <div class="col-md-7">Sold by</div> -->
                                                <div class="col-md-5 col-sm-12"></div>
                                                <div class="col-md-3 col-sm-12 endcol">
                                                    <span>Order #{{$order->order_id}}</span>
                                                    <div class="row link">
                                                        <div style="display:inline"><a href="{{route('vendor_order_detail',$order->oId)}}">Order Detail  </a></div>&nbsp;
                                                        <!-- <div style="display:inline"><a>Invoice</a></div> -->

                                                    </div>
                                                </div>
                                            </div>
                                            @foreach($order->subOrders as $sub)
                                            @if($sub->status == 'pending' || $sub->status == 'processing')

                                            <div class="row col-md-12 col-sm-12 track">
                                                <div class="col-md-3 col-sm-12"><span class="order-status order-{{ $sub->status }}">@if($sub->status == 'pending_payment') Unpaid
                                                                         @elseif($sub->status == 'pending') Waiting For Vendor
                                                                         @elseif($sub->status == 'processing') Order Confirmed
                                                                         @elseif($sub->status == 'delivered') Order Delivered
                                                                         @elseif($sub->status == 'completed') Order Complete
                                                                         @elseif($sub->status == 'cancelled') Canceled
                                                                         @elseif($sub->status == 'cancel_by_vendor') Waiting For Vendor
                                                                         @else In Progress @endif</span></div>
                                                <div class="col-md-6 col-sm-12"></div>
                                                <div class="col-md-3 col-sm-12 text-center hide-in-mobile"><a class="ps-btn" href="{{route('track',$sub->id)}}">Track Package</a></div>
                                            </div>

                                            <div class="row col-md-12 col-sm-12 product">
                                                <div class="col-md-1 col-sm-12 img-div">
                                                    <a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$sub->product->product_name)), 'id' =>  $sub->product_id.'_'.$sub->vendor_id ])}}"><img class="order-product-image" src="/product_photo/{{$sub->product->productImage->name}}" alt="" ></a>
                                                </div>
                                                <div class="col-md-8 col-sm-12 order-product">
                                                    <div class="ps-product__content">
                                                        <a class="ps-product__title" href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$sub->product->product_name)), 'id' =>  $sub->product_id.'_'.$sub->vendor_id ])}}">{{$sub->product->product_name}}</a>
                                                        <p> {{$sub->quantity}} x ₹{{ number_format($sub->amount)}}</p>
                                                        <!-- <p>₹{{$order->quantity * $order->amount}}</p> -->
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 text-center">
                                                    <div class="col-md-4 col-sm-12 text-center show-in-mobile">
                                                        <a class="ps-btn" href="{{route('track',$sub->id)}}">Track Package</a>
                                                    </div>
                                                    <span class="ps-btn-small code-btn" data-sid="{{ \Crypt::encrypt($sub->id) }}">Code</span>
                                                </div>

                                            </div>
                                            @endif
                                            @endforeach
                                        </div>
                                        @endforeach
                                    </div>
                            </div>
                        <!-- </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </section>

</main>
@endsection
