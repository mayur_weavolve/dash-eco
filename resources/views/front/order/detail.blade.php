@extends('layouts.user')
@section('content')
<style>
		b, strong {
			font-weight: bold !important;
        }
        .main-div{
            margin: 1% 0%;
            border: 1px solid #e1e1e1;
            padding: 15px;
        }
        .code-btn {
            margin-left: 8%;
            background: #b5b3b3;
            border: 1px solid #b5b3b3;
            color: #fff !important;
            border-radius: 15px;
            margin-bottom: 3px;
            width: 35% !important;
            display: inline-block;
            text-align: center;
            font-size: 17px;
            font-weight: 600;
            cursor: pointer;
            padding: 4px !important;
            margin-top: 15px;
        }
        .link{
            margin-left: 52%;
        }
        .km-span-list{
            margin-top:10% !important;
            margin-left: 47% !important;
            color: #070606 !important;
            font-weight: 800 !important;
            font-size: 18px !important;
            width: 45% !important
         }
         .main{
            margin: 1% 0%;
             margin-top:10px;
            border: 1px solid #c1c1c1;
         }
         .head{
            margin-left: 0px;
            background: #f1f1f1;
            padding-top: 10px;
         }
         .track{
            border-top: 1px solid #c1c1c1;
            margin-left: 0px;
            padding-top:20px;
         }
         .endcol{
            text-align: end;
         }
         .product{
            padding-bottom: 20px;
         }
         .ps-btn{
            padding: 7px 40px;
         }
        .pname{
            color:blue;
        }
        .price{
            color: #be1919;
        }
        @media only screen and (max-width: 600px) {
            .order-row{
                padding : 0px;
            }
            .p-div{
                margin-bottom:10px;
            }
        }
</style>
<?php $total = 0;?>
<div class="ps-page--single">
    <div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="/user/my-account">My Account</a></li>
               <li><a href="/user/live-order/list">Live Orders</a></li>
               <li>Order Detail - {{ $order->order_id }}</li>
            </ul>
        </div>
    </div>
</div>

<div class="ps-vendor-dashboard">
    <div class="container">
        <div class="ps-section__content">
            <div class="ps-block--vendor-dashboard">
                <div class="ps-block__header">
                    <h3>Order Details</h3>
                </div>

            </div>
        </div>
        <div class="row" style="margin-left:0px;">
            <div class="col-md-8">
                <div class="row">
                    <?php $date = date_create($order->date);?>
                        <div style="display:inline">Ordered on <?php echo date_format($date,"d F Y");?>&nbsp;&nbsp; | </div>&nbsp; &nbsp;
                        <div style="display:inline">Order# {{$order->order_id}}</div>
                </div>
            </div>
            <!-- <div class="col-md-4" style="text-align: end;"><a href="javascript:void(0);" class="pname">Invoice</a></div> -->
        </div>
        <div class="row main-div">
			<div class="col-md-4 p-div">
                <div class="row">
                    <div style="display:inline"><strong>Shipping Address</strong> </div>
                </div>
				<div class="row">
					<div style="display:inline">{{$order->shipping->name}}</div>
				</div>
				<div class="row">
					<div style="display:inline">{{$order->shipping->address_line1}}</div>
				</div>
				<div class="row">
					<div style="display:inline">{{$order->shipping->address_line2}}</div>
				</div>
				<div class="row">
					<div style="display:inline">{{$order->shipping->city}},</div>&nbsp;<div style="display:inline">{{$order->shipping->state}}</div>
                </div>
                <div class="row">
                    <div style="display:inline">{{$order->shipping->pincode}}</div>
                </div>
            </div>
            <div class="col-md-4 p-div">
                <div class="row">
                    <div style="display:inline"><strong>Payment By</strong> </div>
                </div>
				<div class="row">
					<div style="display:inline">@if($order->status == 'pending_payment') Unpaid @else Paid @endif </div>
				</div>
			</div>
            <div class="col-md-4 order-row p-div">
                <div class="row">
                    <div class="col-md-6 col-sm-6"><strong>Order Summary</strong> </div>
                </div>
				<div class="row">
					<div class="col-md-6 col-6">Item(s) Subtotal :</div><div class="col-md-6 col-6" style="text-align: end;">₹{{ number_format($order->amount) }}</div>
				</div>
				 <div class="row">
					<div class="col-md-6 col-6">Shipping :</div><div class="col-md-6 col-6" style="text-align: end;">₹0</div>
				</div>
				<!--<div class="row">
					<div class="col-md-6">Total :</div><div class="col-md-6" style="text-align: end;"></div>
				</div>
				<div class="row">
					<div class="col-md-6">Promotion Applied :</div><div class="col-md-6" style="text-align: end;"></div>
                </div> -->
                <div class="row">
                    <div class="col-md-6 col-6"><strong>Grand Total : </strong></div><div class="col-md-6 col-6" style="text-align: end;"><strong>₹{{ number_format($order->amount)}}</strong></div>
                </div>

			</div>
        </div>
        <div class="row main suborder-main">
            <div class="row col-md-12 col-sm-12 head">
                    <div><h4 class="h4Text">Shipments</h4> </div>
            </div>
            @foreach($order->subOrders as $sub)
            <div class="row col-md-12 col-sm-12 track">
                <div class="col-md-3 col-sm-12"><span class="order-status order-{{ $sub->status }}">@if($sub->status == 'pending_payment') Unpaid
                                                                         @elseif($sub->status == 'pending') Waiting For Vendor
                                                                         @elseif($sub->status == 'processing') Order Confirmed
                                                                         @elseif($sub->status == 'delivered') Order Delivered
                                                                         @elseif($sub->status == 'completed') Order Complete
                                                                         @elseif($sub->status == 'cancelled') Canceled
                                                                         @elseif($sub->status == 'cancel_by_vendor') Waiting For Vendor
                                                                         @else In Progress @endif</span></div>
                <div class="col-md-5 col-sm-12"></div>
                <div class="col-md-4 col-sm-12 text-center hide-in-mobile"><a class="ps-btn" href="{{route('track',$sub->id)}}">Track Package</a></div>
            </div>

            <div class="row col-md-12 col-sm-12 product">
                <div class="col-md-1 col-sm-12 img-div">
                    <a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$sub->product->product_name)), 'id' =>  $sub->product_id.'_'.$sub->vendor_id ])}}"><img class="order-product-image" src="<?php echo URL::to('/'); ?>/product_photo/{{$sub->product->productImage->name}}" alt="" ></a>
                </div>
                <div class="col-md-7 col-sm-12 order-product">
                    <div class="ps-product__content">
                        <a class="ps-product__title" href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$sub->product->product_name)), 'id' =>  $sub->product_id.'_'.$sub->vendor_id ])}}">{{$sub->product->product_name}}</a>
                        <p> {{$sub->quantity}} x ₹{{ number_format($sub->amount)}}</p>
                        <!-- <p>₹{{$order->quantity * $order->amount}}</p> -->
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 text-center">
                <div class="col-md-4 col-sm-12 text-center show-in-mobile"><a class="ps-btn" href="{{route('track',$order->order_id)}}">Track Package</a></div>
                    <span class="ps-btn-small code-btn" data-sid="{{ \Crypt::encrypt($sub->id) }}">Code</span>
                </div>

            </div>
            @endforeach
        </div>

	</div>
</div>
@endsection
