@extends('layouts.user')

@section('content')
<?php $deliveryOption = json_decode($suborder->delivery_option);
 $productAttribute = json_decode($suborder->product_attribute);
// $date1 = date_create('Y-m-d H:i:s');
// $date2 = date_create('Y-m-d H:i:s');
// $datediff = date_diff($date1,$date2);
// echo $datediff;
?>
<style>
     .ps-variant.ps-variant--color {
        width: 20px;
        height: 20px;
        margin-top: 4px;
    }
    .container-fluid {
        margin-top: 80px !important;
        margin-bottom: 80px !important
    }

    p {
        font-size: 14px;
        margin-bottom: 7px
    }

    .cursor-pointer {
        cursor: pointer
    }

    /* a {
        text-decoration: none !important;
        color: #651FFF !important
    }

    .bold {
        font-weight: 600
    }

    .small {
        font-size: 12px !important;
        letter-spacing: 0.5px !important
    }

    .Today {
        color: rgb(83, 83, 83)
    } */

    .btn-outline-primary {
        background-color: #fff !important;
        color: #4bb8a9 !important;
        border: 1.3px solid #4bb8a9;
        font-size: 12px;
        border-radius: 0.4em !important
    }

    .btn-outline-primary:hover {
        background-color: #4bb8a9 !important;
        color: #fff !important;
        border: 1.3px solid #4bb8a9
    }

    .btn-outline-primary:focus,
    .btn-outline-primary:active {
        outline: none !important;
        box-shadow: none !important;
        border-color: #42A5F5 !important
    }

    #progressbar {
        margin-bottom: 30px;
        overflow: hidden;
        color: #455A64;
        padding-left: 0px;
        margin-top: 30px
    }

    #progressbar li {
        list-style-type: none;
        font-size: 13px;
        width: 23.33%;
        float: left;
        position: relative;
        font-weight: 400;
        color: #455A64 !important
    }

    #progressbar #step1:before {
        content: "1";
        color: #fff;
        width: 29px;
        margin-left: 15px !important;
        padding-left: 11px !important
    }

    #progressbar #step2:before {
        content: "2";
        color: #fff;
        width: 29px
    }
    #progressbar #step3:before {
        content: "3";
        color: #fff;
        width: 29px
    }
    #progressbar #step4:before {
        content: "4";
        color: #fff;
        width: 29px
    }

    #progressbar #step5:before {
        content: "5";
        color: #fff;
        width: 29px;
    }
    #progressbar .end:before{
        margin-right: 15px !important;
        padding-right: 2px !important
    }
    #progressbar li:before {
        line-height: 29px;
        display: block;
        font-size: 12px;
        background: #455A64;
        border-radius: 50%;
        margin: auto
    }

    #progressbar li:after {
        content: '';
        width: 121%;
        height: 2px;
        background: #455A64;
        position: absolute;
        left: 0%;
        right: 0%;
        top: 15px;
        z-index: -1
    }

    #progressbar li:nth-child(2):after {
        left: 50%
    }

    #progressbar li:nth-child(1):after {
        left: 16%;
        width: 142%;
    }

    #progressbar li:nth-child(5):after {
        left: 20% !important;
        width: 60% !important
    }

    #progressbar .end:after {
        left: 20% !important;
        width: 70% !important;
    }

    #progressbar li.active:before,
    #progressbar li.active:after {
        background: #4bb8a9
    }

    .card {
        background-color: #fff;
        box-shadow: 2px 4px 15px 0px rgb(0, 108, 170);
        z-index: 0
    }

    small {
        font-size: 12px !important
    }

    .a {
        justify-content: space-between !important
    }

    .border-line {
        border-right: 1px solid rgb(226, 206, 226)
    }

    .card-footer img {
        opacity: 0.3
    }

    .card-footer h5 {
        font-size: 1.1em;
        color: #8C9EFF;
        cursor: pointer
    }
    .p-line{
        margin: 3% 13% 3% 13%;
    }
    .product{
        margin-left:0px !important;
        margin-right:0px !important;
        margin-top: 15px;
    }
    @media only screen and (max-width: 600px) {
        #progressbar li {
            width: 30.33% !important;
        }
        .p-line {
            margin: 0%  !important;
        }
        #progressbar .end:after {
            width: 60% !important;
        }
    }
</style>
<div class="ps-page--single">
    <div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="{{route('my_account')}}">My Account</a></li>
               <li><a href="{{ route('live_order') }}">Live Orders</a></li>
               <li><a href="{{ route('vendor_order_detail',$suborder->order_id)}}">Order - {{ $suborder->orders->order_id }}</a></li>
               <li>Track Order</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="ps-vendor-dashboard">
    <div class="container">
        <div class="ps-section__content">
            <div class="ps-block--vendor-dashboard">
                <div class="ps-block__header">
                    <h3>Track Order</h3>
                </div>
                <div class="main">
                        <div class="row product">
                            <div class="col-md-3 col-sm-12 product" style="text-align:center;">
                                        <a href="javascript:void(0);"><img class="order-product-image" src="/product_photo/{{$suborder->productImage->name}}" alt="" style="max-width: 100px!important;" ></a>
                            </div>
                            <div class="col-md-3 col-sm-12 product">
                                        <div class="ps-product__content">
                                            <a class="ps-product__title" href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$suborder->product->product_name)), 'id' =>  $suborder->product_id.'_'.$suborder->vendor_id ])}}">
                                                <strong>{{$suborder->product->product_name}}</strong>
                                            </a>
                                            <br>
                                            @if($productAttribute->colour != '' && $productAttribute->colour != null)
                                                <span class="ps-variant ps-variant--color colour" data-value="{{$productAttribute->colour}}" style="background-color: #{{$productAttribute->colour}};"></span>
                                            @endif
                                            <span> @if($productAttribute->weight != '' && $productAttribute->weight != null) {{ $productAttribute->weight }} @endif
                                                    @if($productAttribute->size != '' && $productAttribute->size != null) {{ $productAttribute->size }} @endif
                                                    @if($productAttribute->measurement != '' && $productAttribute->measurement != null) {{ $productAttribute->measurement }} @endif
                                                    @if($productAttribute->storage != '' && $productAttribute->storage != null) {{ $productAttribute->storage }} @endif
                                            </span>
                                            <p>{{$suborder->quantity}} x ₹{{ number_format($suborder->amount)}}<p>
                                        </div>

                                        <div class="row product">
                                            <div style="display:inline" class="km-button @if($deliveryOption->type == 'Pickup') pickup-span @endif"> @if($deliveryOption->type == 'Pickup')
                                                                Pickup Only
                                                            @else
                                                                Delivery {{$deliveryOption->day}}
                                                            @endif |
                                                            {{$deliveryOption->km}} km  away
                                            </div>
                                        </div>
                            </div>
                            <div class="col-md-3 col-sm-12 product">
                                <div class="">
                                    <div style="display:inline"><strong>Customer information</strong> </div>
                                </div>
                                <div class="">
                                    <div style="display:inline">{{$suborder->orders->shipping->name}}</div>
                                </div>
                                <div class="">
                                    <div style="display:inline">{{$suborder->orders->shipping->address_line1}}</div>
                                </div>
                                <div class="">
                                    <div style="display:inline">{{$suborder->orders->shipping->address_line2}}</div>
                                </div>
                                <div class="">
                                    <div style="display:inline">{{$suborder->orders->shipping->city}},</div>&nbsp;<div style="display:inline">{{$suborder->orders->shipping->state}}</div>
                                </div>
                                <div class="">
                                    <div style="display:inline">{{$suborder->orders->shipping->pincode}}</div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 product">
                                <div class="">
                                    <div style="display:inline"><strong>Vendor</strong> </div>
                                </div>
                                <div class="">
                                    <div style="display:inline">{{$suborder->vendor->nickname}}</div>
                                </div>
                                @if($suborder->status == 'processing' || $suborder->status == 'delivered' || $suborder->status == 'completed')
                                <div class="">
                                    <div style="display:inline">Contact : {{$suborder->vendor->user->mobileno}}</div>
                                </div>
                                <div class="">
                                    <div style="display:inline">Whatsapp : {{$suborder->vendor->user->whatsapp_no}}</div>
                                </div>
                                <div class="">
                                    <div style="display:inline">Email : {{$suborder->vendor->user->email}}</div>
                                </div>
                            @endif
                            </div>
                            @if($deliveryOption->type == 'Pickup')
                                @if($suborder->status == 'processing' || $suborder->status == 'delivered' || $suborder->status == 'completed')
                                    <div class="row mt25">
                                            <div class="col-md-2 col-sm-md-12"></div>
                                            <div class="col-md-10 col-sm-md-12">
                                                <div style="display:inline"><strong>Pickup address </strong>: {{$suborder->vendor->address}} - {{$suborder->vendor->postcode}}</div>
                                                <br/><div style="display:inline" class="map-icon"><a href="http://maps.google.co.uk/maps?q={{$suborder->vendor->latitude}},{{$suborder->vendor->longitude}}"><i class="icon-map"></i></a></div>
                                            </div>
                                    </div>
                                @endif
                            @endif
                    </div>
                    </div>
                    <div class="">
                    <br><br>
                        <div class="col-md-12 col-sm-12 p-line">
                                        <ul id="progressbar">
                                        @if($suborder->status == 'pending_payment')  <li class="step0 end @if($suborder->status == 'pending_payment') active  @else inactive @endif" id="step1" style="text-align: left;">Unpaid</li> @endif
                                        @if($suborder->status != 'pending_payment')
                                                <li class="step0 @if($suborder->status == 'pending') active @elseif($suborder->status == 'processing') active  @elseif($suborder->status == 'delivered') active @elseif($suborder->status == 'completed') active @elseif($suborder->status == 'cancel_by_vendor') active  @else inactive @endif" id="step1" style="text-align: left;">Waiting For Vendor</li>
                                        @endif
                                        @if($suborder->status != 'pending_payment' && $suborder->status != 'cancelled')
                                                <li class="step0 text-center @if($suborder->status == 'processing') active @elseif($suborder->status == 'delivered') active @elseif($suborder->status == 'completed') active  @else inactive @endif" id="step2">Order Confirmed</li>
                                                <li class="step0 text-center end @if($suborder->status == 'delivered') active @elseif($suborder->status == 'completed') active @else inactive @endif" id="step3">Order Delivered</li>
                                        @endif
                                        @if($suborder->status == 'cancelled')     <li class="step0 text-muted text-right end @if($suborder->status == 'cancelled') active @else inactive @endif" id="step2">Canceled</li> @endif

                                        </ul>
                        </div>
                        <br><br><br>
                    </div>
                </div>

            </div>
    </div>
</div>
</div>

@endsection
