@extends('layouts.user')

@section('content')
<style>
        .orders{
			border-top: none !important;
		}
		.order-status-option{
			background: white;
			color:black;
		}
		.order-status {
			border: 1px solid;
		}
        .link{
            margin-left: 52%;
        }
        .km-span-list{
            margin-top:10% !important;
            margin-left: 47% !important;
            color: #070606 !important;
            font-weight: 800 !important;
            font-size: 18px !important;
            width: 45% !important
         }
         .main{
             margin-top:10px;
            border: 1px solid #c1c1c1;
         }
         .head{
            margin-left: 0px;
            background: #f1f1f1;
            padding-top: 10px;
         }
         .track{
            border-top: 1px solid #c1c1c1;
            margin-left: 0px;
            padding-top:20px;
         }
         .endcol{
            text-align: end;
         }
         .product{
            padding-bottom: 20px;
         }
         .ps-btn{
            padding: 7px 80px;
         }
         .acceptbtn{
            padding: 8px 40px;
            font-size: 16px;
            font-weight: 600;
            line-height: 20px;
            color: #000 !important;
            border: none;
            font-weight: 600;
            border-radius: 4px;
            background-color: #ffcc00;
         }
</style>
<div class="ps-page--single">
    <div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="{{ route('vendor_home') }}">Vendor</a></li>
               <li>Live Orders</li>
            </ul>
        </div>
    </div>
</div>
<div class="ps-vendor-dashboard">
    <div class="container">
        <div class="ps-section__content">
            <div class="ps-block--vendor-dashboard">
                <div class="ps-block__header">
                    <h3>Live Orders</h3>
                </div>
                @foreach($orders as $order)
                <?php $sub = $order->subOrders[0]; ?>
                <?php $deliveryOption = json_decode($sub->delivery_option);?>
                <div class="row main">
                        <div class="row col-md-12 col-sm-12 head">
                                <div class="col-md-4 col-sm-12 ">
                                            <!-- <span class="order-status order-{{ $order->status }}">
                                                        @if($order->status == 'pending_payment') Unpaid
                                                         @elseif($order->status == 'pending') Waiting For Vendor
                                                         @elseif($order->status == 'processing') Order Confirmed
                                                         @elseif($order->status == 'delivered') Order Delivered
                                                         @elseif($order->status == 'completed') Order Complete
                                                         @elseif($order->status == 'cancelled') Canceled
                                                         @elseif($order->status == 'cancel_by_vendor') Waiting For Vendor
                                                         @else In Progress @endif
                                            </span> -->
                                    <p><?php echo date("l, d M Y" ,strtotime("$order->created_at")); ?></p>
                                </div>
                                <!-- <div class="col-md-7">Sold by</div> -->
                                <div class="col-md-5 col-sm-12"></div>
                                <div class="col-md-3 col-sm-12 endcol">
                                    <span>Order #{{$order->order_id}}</span>
                                    @if($sub->status == 'processing' || $sub->status == 'delivered'  )<span> <br> Tracking Number {{$sub->orders->tracking_number}}</span>@endif
                                    <div class="row link">
                                        {{-- <div style="display:inline"><a href="{{route('vendor_order_detail',$order->oId)}}">Order Detail  </a></div>&nbsp; --}}
                                        <!-- <div style="display:inline"><a>Invoice</a></div> -->

                                    </div>
                                </div>
                            </div>


                    <div class="row col-md-12 col-sm-12 track">

                        <div class="col-md-2 col-sm-12 text-center">
                        <form>
                            @if($sub->status == 'pending')
                                    <div class="order-status w175 order-{{$sub->status}}"> Waiting For Vendor </div>
                            @elseif($sub->status == 'cancel_by_vendor')
                                    <div class="order-status w175 order-{{ $sub->status }}">Cancel by vendor  </div>
                            @elseif($sub->status == 'completed')
                                    <div class="order-status w175 order-{{ $sub->status }} ">Order Complete</div>
                            @elseif($sub->status == 'processing')
                                    <div class="order-status w175 order-{{ $sub->status }} ">Order Confirmed</div>
                            @elseif($sub->status == 'delivered')
                                    <div class="order-status w175 order-{{ $sub->status }} ">Order Delivered</div>
                            @else
                                    <div class="order-status w175 order-{{$sub->status}}"> Waiting For Vendor </div>
                                            <!-- <select name="status" id="status{{$sub->id}}" onchange="statusUpdate(this.value ,{{$sub->id}})" class="order-status w175 order-{{$sub->status}}">
                                                <option @if($sub->status == 'pending') selected @endif class="order-status-option"  value="pending"> Waiting For Vendor</option>
                                                <option @if($sub->status == 'processing') selected  @endif class="order-status-option" value="processing">Order Confirmed</option>
                                                <option @if($sub->status == 'delivered') selected  @endif class="order-status-option" value="delivered">Order Delivered</option>
                                                <option @if($sub->status == 'completed') selected @endif class="order-status-option" value="completed">Order Complete</option>
                                            </select> -->
                            @endif
                        </form>
                        </div>
                        @if($sub->status == 'pending')
                            <div class="col-md-3 col-sm-12"></div>
                            <div class="col-md-4 col-sm-12 text-center">
                                    <div style="display:inline" class="km-button text-center @if($deliveryOption->type == 'pickup') pickup-span @endif"> @if($deliveryOption->type == 'pickup')
                                                            Pickup by customer
                                                        @else
                                                            Deliver to customer {{$deliveryOption->day}}
                                                        @endif
                                    </div>
                            </div>
                            <div class="col-md-3 col-sm-12 text-center hide-in-mobile">
                                    <div class="row">
                                        <div style="display:inline"><a class="acceptbtn" onclick="window.location.href='{{route('orderAccept',$sub->id)}}'" href="javascript:void(0)">Accept</a></div>&nbsp;
                                        <div style="display:inline"><a class="acceptbtn" onclick="window.location.href='{{route('orderReject',$sub->id)}}'" href="javascript:void(0)">Reject</a></div>
                                    </div>
                        @elseif($sub->status == 'cancel_by_vendor')
                            <div class="col-md-4 col-sm-12 text-center"></div>
                            <div class="col-md-4 col-sm-12 text-center">
                                    <div style="display:inline" class="km-button text-center @if($deliveryOption->type == 'pickup') pickup-span @endif"> @if($deliveryOption->type == 'pickup')
                                                            Pickup by customer
                                                        @else
                                                            Deliver to customer {{$deliveryOption->day}}
                                                        @endif
                                    </div>
                            </div>
                            <div class="col-md-2 col-sm-12 text-center hide-in-mobile">
                                    <div class="row">
                                        <div class="order-status w175 order-{{ $sub->status }} ">Cancel by vendor</div>
                                    </div>
                        @else
                            <div class="col-md-4 col-sm-12 text-center"></div>
                            <div class="col-md-4 col-sm-12 text-center">
                                    <div style="display:inline" class="km-button text-center @if($deliveryOption->type == 'pickup') pickup-span @endif"> @if($deliveryOption->type == 'pickup')
                                                            Pickup by customer
                                                        @else
                                                            Deliver to customer {{$deliveryOption->day}}
                                                        @endif
                                    </div>
                            </div>
                            <div class="col-md-2 col-sm-12 text-center hide-in-mobile">
                                <div class="row">
                                    @if($sub->status == 'processing')<div class="order-status w175 order-{{ $sub->status }} ">Order Confirmed</div>@endif
                                    @if($sub->status == 'delivered') <div class="order-status w175 order-{{ $sub->status }} ">Order Delivered</div> @endif
                                    @if($sub->status == 'completed') <div class="order-status w175 order-{{ $sub->status }} ">Order Complete</div> @endif
                                    &nbsp;
                                </div>
                        @endif

                        </div>
                    </div>

                    <div class="row col-md-12 col-sm-12 product">
                        <div class="col-md-1 col-sm-12 img-div">
                            <a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$sub->product->product_name)), 'id' =>  $sub->product_id.'_'.$sub->vendor_id ])}}"><img class="order-product-image" src="/product_photo/{{$sub->product->productImage->name}}" alt="" ></a>
                        </div>
                        <div class="col-md-5 col-sm-12 order-product">
                            <div class="ps-product__content">
                                <a class="ps-product__title" href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$sub->product->product_name)), 'id' =>  $sub->product_id.'_'.$sub->vendor_id ])}}">{{$sub->product->product_name}}</a>
                                <p> {{$sub->quantity}} x ₹{{ number_format($sub->amount)}}</p>
                                <!-- <p>₹{{$order->quantity * $order->amount}}</p> -->
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 text-center">
                                    <div style="display:inline" class="km-button text-center @if($deliveryOption->type == 'pickup') pickup-span @endif">
                                                        only {{$deliveryOption->km}} km away
                                    </div>
                        </div>
                        <div class="col-md-2 col-sm-12 text-center">
                            <div class="col-md-4 col-sm-12 text-center show-in-mobile">
                                @if($sub->status == 'pending')
                                <div class="row link" style="padding:10px;">
                                    <div style="display:inline"><a class="acceptbtn" href="{{route('orderAccept',$sub->id)}}">Accept</a></div>&nbsp;
                                    <div style="display:inline"><a class="acceptbtn"href="{{route('orderReject',$sub->id)}}">Reject</a></div>
                                </div>
                                @endif
                            </div>
                            <span class="ps-btn-small code-btn track-btn" data-sid="{{ \Crypt::encrypt($sub->id) }}"><a href="{{route('vendor_track',$sub->id)}}">Track</a></span>
                        </div>

                    </div>

                </div>
                @endforeach
            </div>
    </div>
</div>
</div>
<script>
	function statusUpdate(status , id)
		{

			$.ajax({
				url: '<?php echo URL::to('/'); ?>/vendor/update/status',
				type:"post",
				data:{
					'_token': '{{csrf_token()}}',
					'id' : id,
					'status' : status,

					},
				success: function(response){
					response = JSON.parse(response);
						if(response.status === 'success'){
                            alert('done');
							location.reload();
						}

				}
			});

		}
	</script>
@endsection
