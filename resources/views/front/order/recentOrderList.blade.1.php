@extends('layouts.user')

@section('content')
<style>
    .addbtn{
    background-color: #88aa00;
    border-radius: 0;
    height: 40px;
    padding: 0 30px;
    color: #ffffff;
    padding-top: 10px;
    display: inline-block;
    font-size: 16px;
    line-height: 20px;
    border: none;
    font-weight: 600;
    transition: all .4s ease;
    cursor: pointer;
    }
</style>
<div class="ps-page--single">
    <div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="{{ route('vendor_home') }}">Vendor</a></li>
               <li>Orders</li>
            </ul>
        </div>
    </div>
</div>
<div class="ps-vendor-dashboard">
    <div class="container">
        <div class="ps-section__header">
            @if($vendor)
            <h3>Hello,{{ $vendor->name }}</h3>
            <p>{{ $vendor->business_name }}<br>{{ $vendor->address }}<br> {{ $vendor->postcode }} <br> GSTIN : {{ $vendor->gst_no }} </p>
            @endif
        </div>
        <div class="ps-section__content">
            <ul class="ps-section__links">
                <li><a href="{{ route('vendor_home') }}">Dashboard</a></li>
                <li><a href="{{route('vendor_product_list')}}">Products</a></li>
                <li class="active"><a href="{{route('vendor_recent_order_list')}}">Live Order</a></li>
                <li><a href="{{route('vendor_completed_order_list')}}">Completed Order</a></li>
                <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                        </form>
                </li>

            </ul>

            <div class="ps-block--vendor-dashboard">
                <div class="ps-block__header">
                    <h3>Live Orders</h3>
                </div>
                <div class="ps-block__content">
                    <div class="table-responsive">
                        <table class="table ps-table ps-table--vendor">
                            <thead>
                                <tr>
                                    <th>date</th>
                                    <th>Order ID</th>
                                    <th>Shipping</th>
                                    <th>Total Price</th>
                                    <th>Status</th>
                                    <th>Information</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Nov 4, 2019</td>
                                    <td><a href="javascript:void(0);">MS46891357</a></td>
                                    <td>$00.00</td>
                                    <td>$295.47</td>
                                    <td><a href="javascript:void(0);">Open</a></td>
                                    <td><a href="javascript:void(0);">View Detail</a></td>
                                </tr>
                                <tr>
                                    <td>Nov 2, 2017</td>
                                    <td><a href="javascript:void(0);">AP47305441</a></td>
                                    <td>$00.00</td>
                                    <td>$25.47</td>
                                    <td>Close</td>
                                    <td><a href="javascript:void(0);">View Detail</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
