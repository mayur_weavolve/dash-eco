@extends('layouts.user')

@section('content')
<style>
        .link{
            margin-left: 52%;
        }
        .km-span-list{
            margin-top:10% !important;
            margin-left: 47% !important;
            color: #070606 !important;
            font-weight: 800 !important;
            font-size: 18px !important;
            width: 45% !important
         }
         .main{
             margin-top:10px;
            border: 1px solid #c1c1c1;
         }
         .head{
            margin-left: 0px;
            background: #f1f1f1;
            padding-top: 10px;
         }
         .track{
            border-top: 1px solid #c1c1c1;
            margin-left: 0px;
            padding-top:20px;
         }
         .endcol{
            text-align: end;
         }
         .product{
            padding-bottom: 20px;
         }
         .ps-btn{
            padding: 7px 40px;
         }
         .acceptbtn{
             background: #ffcc00;
             padding:5px;
             color: white !important;
             font-weight: 600;
         }
</style>
<div class="ps-page--single">
    <div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="{{ route('vendor_home') }}">Vendor</a></li>
               <li>Orders</li>
            </ul>
        </div>
    </div>
</div>
<div class="ps-vendor-dashboard">
    <div class="container">
        <div class="ps-section__content">
            <div class="ps-block--vendor-dashboard">
                <div class="ps-block__header">
                    <h3>Live Orders</h3>
                </div>
                @foreach($orders as $order)
                <div class="row main">
                    <div class="row col-md-12 head">
                        <div class="col-md-2">Order Placed<p><?php echo date("d l Y" ,strtotime("$order->created_at")); ?></p></div>
                        <div class="col-md-7">Sold by</div>
                        <div class="col-md-3 endcol">
                            <span>Order #{{$order->order_id}}</span>
                            <div class="row link">
                                <div style="display:inline"><a>Order Detail | </a></div>&nbsp;
                                <div style="display:inline"><a>Invoice</a></div>
                            </div>

                            @if($order->status == 'pending')
                            <div class="row link" style="padding:10px;">
                                <div style="display:inline"><a class="acceptbtn" href="{{route('orderAccept',$order->id)}}">Accept</a></div>&nbsp;
                                <div style="display:inline"><a class="acceptbtn"href="{{route('orderReject',$order->id)}}">Reject</a></div>
                            </div>
                            @endif

                        </div>
                    </div>
                    @foreach($order->subOrders as $sub)
                    <div class="row col-md-12 track">
                        <div class="col-md-3">Arrival</a></div>
                        <div class="col-md-6"></div>
                        <div class="col-md-3 endcol"><a class="ps-btn" href="{{route('track',$order->order_id)}}">Track Package</a></div>
                    </div>

                    <div class="row col-md-12 product">
                        <div class="col-md-1">
                            <a href="javascript:void(0);"><img src="/product_photo/{{$order->name}}" alt=""></a>
                        </div>
                        <div class="col-md-8">
                            <div class="ps-product__content">
                                <a class="ps-btn" href="{{route('orderAccept',$order->order_id)}}">Track Package</a>
                                <a class="ps-product__title" href="">{{$order->product_name}} , {{$order->quantity}} Pcs</a><p>₹{{ number_format($order->quantity*$order->amount)}}</span>
                            </div>
                        </div>
                        <div class="col-md-3">

                            <span class="km-span-list">Code</span>
                        </div>

                    </div>
                    @endforeach
                </div>
                @endforeach
            </div>
    </div>
</div>
</div>

@endsection
