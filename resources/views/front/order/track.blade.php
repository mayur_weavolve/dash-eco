@extends('layouts.user')

@section('content')
<?php $deliveryOption = json_decode($suborder->delivery_option);?>


<style>
    .ps-variant.ps-variant--color {
        width: 20px;
        height: 20px;
        margin-top: 4px;
    }
    .container-fluid {
        margin-top: 80px !important;
        margin-bottom: 80px !important
    }

    p {
        font-size: 14px;
        margin-bottom: 7px
    }

    .cursor-pointer {
        cursor: pointer
    }

    .submit{
        margin-top: 10%;
        height: 64%;
        width: 50%;
    }

    /* a {
        text-decoration: none !important;
        color: #651FFF !important
    }

    .bold {
        font-weight: 600
    }

    .small {
        font-size: 12px !important;
        letter-spacing: 0.5px !important
    }

    .Today {
        color: rgb(83, 83, 83)
    } */

    .btn-outline-primary {
        background-color: #fff !important;
        color: #4bb8a9 !important;
        border: 1.3px solid #4bb8a9;
        font-size: 12px;
        border-radius: 0.4em !important
    }

    .btn-outline-primary:hover {
        background-color: #4bb8a9 !important;
        color: #fff !important;
        border: 1.3px solid #4bb8a9
    }

    .btn-outline-primary:focus,
    .btn-outline-primary:active {
        outline: none !important;
        box-shadow: none !important;
        border-color: #42A5F5 !important
    }

    #progressbar {
        margin-bottom: 30px;
        overflow: hidden;
        color: #455A64;
        padding-left: 0px;
        margin-top: 30px
    }

    #progressbar li {
        list-style-type: none;
        font-size: 13px;
        width: 18.33%;
        float: left;
        position: relative;
        font-weight: 400;
        color: #455A64 !important
    }

    #progressbar #step1:before {
        content: "1";
        color: #fff;
        width: 29px;
        margin-left: 15px !important;
        padding-left: 11px !important
    }

    #progressbar #step2:before {
        content: "2";
        color: #fff;
        width: 29px
    }
    #progressbar #step3:before {
        content: "3";
        color: #fff;
        width: 29px
    }
    #progressbar #step4:before {
        content: "4";
        color: #fff;
        width: 29px
    }

    #progressbar #step5:before {
        content: "5";
        color: #fff;
        width: 29px;
    }
    #progressbar .end:before{
        margin-right: 15px !important;
        padding-right: 11px !important
    }
    #progressbar li:before {
        line-height: 29px;
        display: block;
        font-size: 12px;
        background: #455A64;
        border-radius: 50%;
        margin: auto
    }

    #progressbar li:after {
        content: '';
        width: 121%;
        height: 2px;
        background: #455A64;
        position: absolute;
        left: 0%;
        right: 0%;
        top: 15px;
        z-index: -1
    }

    #progressbar li:nth-child(2):after {
        left: 50%
    }

    #progressbar li:nth-child(1):after {
        left: 16%;
        width: 142%;
    }

    #progressbar li:nth-child(5):after {
        left: 20% !important;
        width: 60% !important
    }

    #progressbar .end:after {
        left: 20% !important;
        width: 60% !important;
    }

    #progressbar li.active:before,
    #progressbar li.active:after {
        background: #4bb8a9
    }

    .card {
        background-color: #fff;
        box-shadow: 2px 4px 15px 0px rgb(0, 108, 170);
        z-index: 0
    }

    small {
        font-size: 12px !important
    }

    .a {
        justify-content: space-between !important
    }

    .border-line {
        border-right: 1px solid rgb(226, 206, 226)
    }

    .card-footer img {
        opacity: 0.3
    }

    .card-footer h5 {
        font-size: 1.1em;
        color: #8C9EFF;
        cursor: pointer
    }
    .p-line{
        margin: 3% 13% 3% 13%;
    }
    button:disabled{
        background: #989898;
        color: #fff;
    }
</style>
<div class="ps-page--single">
    <div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="{{ route('vendor_home') }}">Vendor</a></li>
                <li><a href="{{ route('vendor_recent_order_list') }}">Live Orders</a></li>
               <li>Track Order</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="ps-vendor-dashboard">
    <div class="container">
        <div class="ps-section__content">
            <div class="ps-block--vendor-dashboard">
                <div class="ps-block__header">
                    <h3>Track Order</h3>
                </div>
                <div class="main">
                    <div class="row product">
                        <div class="col-md-3 col-sm-12" style="text-align:center;">
                                    <a href="javascript:void(0);"><img class="order-product-image" src="/product_photo/{{$suborder->productImage->name}}" alt="" style="max-width: 100px!important;" ></a>
                        </div>
                        <div class="col-md-3 col-sm-12">
                                    <div class="ps-product__content">
                                        <a class="ps-product__title" href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$suborder->product->product_name)), 'id' =>  $suborder->product_id.'_'.$suborder->vendor_id ])}}">
                                            <strong>{{$suborder->product->product_name}}</strong>
                                        </a>
                                        <?php $productAttribute = json_decode($suborder->product_attribute); ?>
                                        <br>
                                        @if($productAttribute->colour != '' && $productAttribute->colour != null)
                                            <span class="ps-variant ps-variant--color colour" data-value="{{$productAttribute->colour}}" style="background-color: #{{$productAttribute->colour}};"></span>
                                        @endif
                                        <span> @if($productAttribute->weight != '' && $productAttribute->weight != null) {{ $productAttribute->weight }} @endif
                                                @if($productAttribute->size != '' && $productAttribute->size != null) {{ $productAttribute->size }} @endif
                                                @if($productAttribute->measurement != '' && $productAttribute->measurement != null) {{ $productAttribute->measurement }} @endif
                                                @if($productAttribute->storage != '' && $productAttribute->storage != null) {{ $productAttribute->storage }} @endif
                                        </span>
                                        <p>{{$suborder->quantity}} x ₹{{ number_format($suborder->amount)}}<p>
                                    </div>
                                    <div class="row">
                                         <div style="display:inline" class="km-button text-center @if($deliveryOption->type == 'pickup') pickup-span @endif"> @if($deliveryOption->type == 'pickup')
                                                            Pickup by customer
                                                        @else
                                                            Deliver to customer {{$deliveryOption->day}}
                                                        @endif <br>
                                                        only {{$deliveryOption->km}} km away
                                        </div>
                                    </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div style="display:inline"><strong>Customer Details</strong> </div>
                            </div>
                            <div class="row">
                                <div style="display:inline">{{$suborder->orders->shipping->name}}</div>
                            </div>
                            <div class="row">
                                <div style="display:inline">{{$suborder->orders->shipping->address_line1}}</div>
                            </div>
                            <div class="row">
                                <div style="display:inline">{{$suborder->orders->shipping->address_line2}}</div>
                            </div>
                            <div class="row">
                                <div style="display:inline">{{$suborder->orders->shipping->city}},</div>&nbsp;<div style="display:inline">{{$suborder->orders->shipping->state}}</div>
                            </div>
                            <div class="row">
                                <div style="display:inline">{{$suborder->orders->shipping->pincode}}</div>
                            </div>
                            <div class="row">
                                <div style="display:inline" class="map-icon"><a href="http://maps.google.co.in/maps?q={{$suborder->orders->shipping->address_line1}},{{$suborder->orders->shipping->address_line2}},{{$suborder->orders->shipping->city}},{{$suborder->orders->shipping->state}}"><i class="icon-map"></i></a></div>
                            </div>
                        </div>
                        @if($suborder->status != 'pending' && $suborder->status != 'cancel_by_vendor' && $suborder->status != 'cancelled'  )
                        <div class="col-md-3">
                            <div class="row">
                                <div style="display:inline"><strong>Contact Info</strong> </div>
                            </div>
                                    <div class="row">
                                        <div style="display:inline">Contact : {{$suborder->orders->users->mobileno}}</div>
                                    </div>
                                    <div class="row">
                                        <div style="display:inline">Whatsapp : {{$suborder->orders->users->whatsapp_no}}</div>
                                    </div>
                                    <div class="row">
                                        <div style="display:inline">Email : {{$suborder->orders->users->email}}</div>
                                    </div>
                        </div>
                        @endif
                    </div>
                    <div class="row product">
                        <div class="col-md-12 col-sm-12 p-line">
                                        <ul id="progressbar">
                                        @if($suborder->status == 'pending_payment')  <li class="step0 end @if($suborder->status == 'pending_payment') active  @else inactive @endif" id="step1" style="text-align: left;">Unpaid</li> @endif
                                        @if($suborder->status != 'pending_payment')
                                                <li class="step0 @if($suborder->status == 'pending') active @elseif($suborder->status == 'processing') active  @elseif($suborder->status == 'delivered') active @elseif($suborder->status == 'completed') active @elseif($suborder->status == 'cancel_by_vendor') active  @else inactive @endif" id="step1" style="text-align: left;">Waiting For Vendor</li>
                                        @endif
                                        @if($suborder->status != 'pending_payment' && $suborder->status != 'cancelled')
                                                <li class="step0 text-center @if($suborder->status == 'processing') active @elseif($suborder->status == 'delivered') active @elseif($suborder->status == 'completed') active  @else inactive @endif" id="step2">Order Confirmed</li>
                                                <li class="step0 text-center @if($suborder->status == 'delivered') active @elseif($suborder->status == 'completed') active @else inactive @endif" id="step3">Order Delivered</li>
                                        @endif
                                        @if($suborder->status != 'cancelled')    <li class="step0 text-muted text-right end @if($suborder->status == 'completed') active @else inactive @endif" id="step4">Order Complete</li> @endif
                                            @if($suborder->status == 'cancelled')     <li class="step0 text-muted text-right end @if($suborder->status == 'cancelled') active @else inactive @endif" id="step2">Canceled</li> @endif

                                        </ul>
                        </div>
                    </div>
                    @if($suborder->status == 'processing')
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <form method="post" action="<?php echo URL::to('/'); ?>/update/image" enctype="multipart/form-data" >
                            {{csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12 mt10">
                                            <label for="code">* Code</label>
                                            <div>
                                                <input type="text" name="code" id="code" class="form-control m-input code" value="" required>
                                            </div>
                                            <input type="hidden" name="id" value="{{ $suborder->id }}">
                                    </div>
                                    <div class="col-md-12 mt10">
                                            <label for="name">* Upload Image</label>
                                            <div>
                                                <input type="file" name="image"  accept="image/*;capture=camera" required>
                                            </div>
                                    </div>
                                    <div class="col-md-12 mt10 text-center">
                                            <p class="error-msg">Invalid Security Code</p>
                                            <button class="ps-btn confirm"  id="submit" data-wizard-action="submit" >
                                                Submit
                                            </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    @endif
                    @if($suborder->delivered_path != '')
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4" style="margin: auto;text-align: center;">
                            <img src="/delivered_path_photo/{{ $suborder->delivered_path }}" width="150px">
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                    @endif
                </div>

            </div>
    </div>
</div>
</div>
<script>
    $('.confirm').prop('disabled',true);
    $(".error-msg").hide();
    $('.code').on('input', function() {
        var code = $('#code').val();
        if(code.length == 4){
                $('#loaders').show();
                var code = code;
                var id = <?php  echo $suborder->id; ?>;
                $.ajax({
                    url: '<?php echo URL::to('/'); ?>/update/status',
                    type:"post",
                    data:{
                        '_token': '{{csrf_token()}}',
                        'id' : id,
                        'code' : code
                        },
                    success: function(response){
                        response = JSON.parse(response);
                        if(response.status === "success"){
                            $(".error-msg").hide();
                            $('.confirm').prop('disabled',false);
                        }else{
                            $(".error-msg").show();
                            $('confirm').prop('disabled', true);
                        }
                        $("#loaders").hide();
                    }
                });
                return;
        }
        else {
            // alert('please Enter Currect Security Code');
            //  return false;
        }

    });
</script>

@endsection
