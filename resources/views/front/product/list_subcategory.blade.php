<?php
    use App\Category;
    $data = getFilter($subcategory->id,'subcategory');
    $catData = getAvailableCategory();
    $categories = $catData['categories'];
    $subcategories = $catData['subcategories'];
?>
@extends('layouts.product_list')

@section('content')
<div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="<?php echo URL::to('/'); ?>">Home</a></li>
                <li><a href="{{route('product_list', ['category' => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$category->name)), 'id' => $category->id])}}">{{$category->name}}</a></li>
                <li>{{$subcategory->name}}</li>
            </ul>
        </div>
</div>

<div class="ps-page--shop" id="shop-sidebar">
            <div class="container">
                <div class="ps-layout--shop">
                    <div class="ps-layout__left">
                        <aside class="widget widget_shop">
                            <h4 class="widget-title">Categories</h4>
                            <ul class="ps-list--categories">
                            @foreach( $categories as $categorylist)
                                <li class="current-menu-item menu-item-has-children"><a href="{{route('product_list', ['category' => strtolower(str_replace(" ","-",$categorylist['name'])), 'id' => $categorylist['id']])}}">{{ $categorylist['name'] }}</a><span class="sub-toggle @if($categorylist['id'] == $category->id) active @endif "><i class="fa fa-angle-down"></i></span>
                                    <ul class="sub-menu " style="display:@if($categorylist['id'] == $category->id) block @else none @endif;">
                                        @foreach($subcategories[$categorylist['id']] as $subcat)
                                            <li class="current-menu-item @if($subcategory->id == $subcat['id']) active @endif "><a href="{{route('product_list_subcategory', ['category' => strtolower(str_replace(" ","-",$categorylist['name'])),'subcategory' => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$subcat['name'])), 'id' => $subcat['id']])}}">{{$subcat['name']}}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endforeach
                            </ul>
                        </aside>
                        <aside class="widget widget_shop">

                            <figure>
                                <h4 class="widget-title">By Price</h4>
                                <div id="nonlinear"></div>
                                <p class="ps-slider__meta">Price:<span class="ps-slider__value">₹<span class="ps-slider__min"></span></span>-<span class="ps-slider__value">₹<span class="ps-slider__max"></span></span></p>
                            </figure>
                            <figure>
                                <h4 class="widget-title">By Distance</h4>
                                <div id="nonlineardistance"></div>
                                <p class="ps-slider__meta">Distance:<span class="ps-slider__value"><span class="ps-sliderdistance__min"></span>KM</span>-<span class="ps-slider__value"><span class="ps-sliderdistance__max"></span>KM</span></p>
                            </figure>
                            @if(sizeof($data['colours']) > 0)
                            <figure>
                                <h4 class="widget-title">By Color</h4>
                                @foreach($data['colours'] as $colour)
                                <div class="ps-checkbox ps-checkbox--color  ps-checkbox--inline">
                                    <span class="form-control colourinput colour " type="checkbox" id="{{ $colour->colour }}" value="{{ $colour->colour }}" name="colour" style="display: none;">{{ $colour->colour }}</span>
                                    <label for="{{ $colour->colour }}"  class="colourlabel" style="background-color:#{{ $colour->colour }} !important;"></label>
                                </div>
                                @endforeach
                            </figure>
                            @endif
                            @if(sizeof($data['sizes']) > 0)
                            <figure class="sizes">
                                <h4 class="widget-title">BY SIZE</h4>
                                    @foreach($data['sizes'] as $size)
                                        <a href="javascript:void(0);"  class="size" value="{{ $size->size }}">{{ $size->size }}</a>
                                    @endforeach
                            </figure>
                            @endif
                            @if(sizeof($data['weights']) > 0)
                            <figure class="sizes">
                                <h4 class="widget-title">BY WEIGHT</h4>
                                @foreach($data['weights'] as $weight)
                                        <a href="javascript:void(0);" class="weight" value="{{ $weight->weight }}">{{ $weight->weight }}</a>
                                @endforeach
                            </figure>
                            @endif
                            @if(sizeof($data['measurements']) > 0)
                            <figure class="sizes">
                                <h4 class="widget-title">BY MEASUREMENT</h4>
                                    @foreach($data['measurements'] as $measurement)
                                        <a href="javascript:void(0);"  class="measurement" value="{{ $measurement->measurement }}">{{ $measurement->measurement }}</a>
                                    @endforeach
                            </figure>
                            @endif
                            @if(sizeof($data['storages']) > 0)
                            <figure class="sizes">
                                <h4 class="widget-title">BY STORAGE</h4>
                                    @foreach($data['storages'] as $storage)
                                        <a href="javascript:void(0);"  class="storage" value="{{ $storage->storage }}">{{ $storage->storage }}</a>
                                    @endforeach
                            </figure>
                            @endif
                        </aside>
                    </div>
                    <div class="ps-layout__right">
                        <div class="ps-shopping ps-tab-root">
                            <div class="ps-shopping__header">
                            <p><strong id="productlength"></strong> Products found</p>
                                <div class="ps-shopping__actions">
                                    <select class="ps-select sorting" data-placeholder="Sort Products" id="sorting" name="sort">
                                        <option value="distance" >Sort by distance</option>
                                        <option value="id" >Sort by popularity</option>
                                        <option value="id" >Sort by average rating</option>
                                        <option value="priceasc" >Sort by price: low to high</option>
                                        <option value="pricedesc" >Sort by price: high to low</option>
                                    </select>
                                    <div class="ps-shopping__view">
                                        <p>View</p>
                                        <ul class="ps-tab-list">
                                            <li class="active"><a href="#tab-1"><i class="icon-grid"></i></a></li>
                                            <li><a href="#tab-2"><i class="icon-list4"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="ps-tabs">
                                <div class="ps-tab active" id="tab-1">

                                </div>
                                <div class="ps-tab" id="tab-2">
                                    <!-- <div class="ps-pagination">
                                        <ul class="pagination">
                                            <li class="active"><a href="javascript:void(0);">1</a></li>
                                            <li><a href="javascript:void(0);">2</a></li>
                                            <li><a href="javascript:void(0);">3</a></li>
                                            <li><a href="javascript:void(0);">Next Page<i class="icon-chevron-right"></i></a></li>
                                        </ul>
                                    </div> -->
                                </div>
                                <div class="load-product"><img src="/user/img/loading.gif"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <input type="hidden" id="subcategoryId" value="{{$subcategory->id}}">

            <script type="text/javascript">
     var page=1;
    var tab1Html = '';
    var tab2Html = '';
    var lastPage = 1;
    function getproducts(page){
        var data = {};
        if(lastPage >= page){
            if (window.matchMedia("(max-width: 767px)").matches)
            {
                var minDistance = $(".ps-sliderdistancemob__min").text();
                var maxDistance = $(".ps-sliderdistancemob__max").text();
                var minPrice = $(".ps-slidermob__min").text();
                var maxPrice = $(".ps-slidermob__max").text();
            }else{
                var minDistance = $(".ps-sliderdistance__min").text();
                var maxDistance = $(".ps-sliderdistance__max").text();
                var minPrice = $(".ps-slider__min").text();
                var maxPrice = $(".ps-slider__max").text();
            }
            var colour = [];
            $(".colour.active").text(function(index, text) {
                colour.push(text);
            });
            var storage = [];
            $(".storage.active").text(function(index, text) {
                storage.push(text);
            });
            var weight = [];
            $(".weight.active").text(function(index, text) {
                weight.push(text);
            });
            var measurement = [];
            $(".measurement.active").text(function(index, text) {
                measurement.push(text);
            });
            var size = [];
            $(".size.active").text(function(index, text) {
                size.push(text);
            });
            debugger;
            var sorting = $(".sorting").val();
            $(".load-product").show();
            var filter = "&minDistance="+minDistance+"&maxDistance="+maxDistance+"&minPrice="+minPrice+"&maxPrice="+maxPrice+"&colour="+colour.join()+"&weight="+weight.join()+"&size="+size.join()+"&measurement="+measurement.join()+"&storage="+storage.join()+"&sorting="+sorting;
            pathvalue = "categoryId=0&subcategoryId="+ $('#subcategoryId').val() +"&page="+page+filter;
            $.ajax({
                url: '/productlist?'+pathvalue,
                type: 'get',
                data: data,
                success: function(res){
                    res = JSON.parse(res);
                        //tab1
                        if(res.data.products){
                            $("#productlength").text(res.data.products.total);
                            lastPage = res.data.products.last_page;
                            if(page == 1){
                                tab1Html = '<div class="ps-shopping-product"><div class="row more-product">';
                            }else{
                                tab1Html = '';
                            }
                            var resultArray = [];
                            for (var i in  res.data.products.data) {
                                    resultArray.push(res.data.products.data[i]);
                            }
                            resultArray.forEach(async function(product) {
                                var productImage = '';
                                if(product.productImage  != null){
                                    productImage = '<?php echo URL::to('/'); ?>/product_photo/'+product.productImage;
                                }
                                var productLink = '<?php echo URL::to('/'); ?>/'+product.product_name.toLowerCase().replace(/[^a-zA-Z0-9]/g,"-")+'/'+product.product_id+'_'+product.vendorID;
                               var dt = new Date();
                                var hours = dt.getHours();
                                var deliveryText = '';
                                var pickspan = '';
                                if(product.free_delivery === '0'){
                                    deliveryText = 'pickup only';
                                    pickspan = 'pickup-span';
                                }else{
                                    if(hours >= 14){
                                        deliveryText = 'delivery tomorrow';
                                    }else{
                                        deliveryText = 'delivery today';
                                    }
                                    if(Math.round(product.distance) > Math.round(Number(product.free_delivery))){
                                        deliveryText = 'pickup only';
                                        pickspan = 'pickup-span';
                                    }
                                }
                                var kmText = Math.round(product.distance)+'km away';
                                var productName = product.product_name;
                                var discountDiv = '';
                                if(product.discount > 0){
                                    discountDiv = '<div class="ps-product__badge">-'+product.discount+'%</div>';
                                    mrpDiv =  '<del><small>₹'+Number(product.mrp).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')+'</small></del>';
                                }
                                tab1Html+='<div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-6 "><div class="ps-product"><div class="ps-product__thumbnail"><a href="'+productLink+'"><img src="'+productImage+'" alt="'+product.product_name+'"></a>'+discountDiv+'<ul class="ps-product__actions list_prodouct_action"><li><a href="'+productLink+'" data-toggle="tooltip" data-placement="top" title="Read More"><i class="icon-bag2"></i></a></li>';
                                tab1Html+="<li><a href='javascript:void(0);'  title='Quick View' data-value='"+ JSON.stringify(product).toString()+"' class='product-quickview'><i class='icon-eye'></i></a></li>";
                                tab1Html+='</ul></div><div class="ps-product__container"><span class="km-span-list '+pickspan+'">'+deliveryText+'</span> <br> <span class="km-span-list '+pickspan+'">'+kmText+'</span><a class="ps-product__vendor" href="'+productLink+'">'+product.subcategoryName+'</a><div class="ps-product__content"><a class="ps-product__title" href="'+productLink+'">'+productName+'</a><p class="ps-product__price sale">₹'+Number(product.final_price).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')+mrpDiv+'</p></div><div class="ps-product__content hover"><a class="ps-product__title" href="'+productLink+'">'+product.product_name+'</a><p class="ps-product__price sale">₹'+Number(product.final_price).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')+' <del>₹'+Number(product.mrp).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')+'</del></p></div></div></div></div>';
                            });
                            if(page == 1){
                                tab1Html+= '</div></div>';
                            }
                        }else{
                            tab1Html = '<div class="ps-shopping-product"><div class="row"><p>No product found</p></div></div>';
                            $("#productlength").text('0');
                            lastPage = 0;
                        }
                        if(page == 1){
                            $('#tab-1').html(tab1Html);
                        }else{
                            $('.more-product').append(tab1Html);
                        }

                        //tab2
                        if(res.data.products){
                            if(page == 1){
                                tab2Html = '<div class="ps-shopping-product more-data">';
                            }
                            var resultArray = [];
                            for (var i in  res.data.products.data) {
                                    resultArray.push(res.data.products.data[i]);
                            }
                            resultArray.forEach(async function(product) {
                                var productImage = '';
                                if(product.productImage  != null){
                                    productImage = '<?php echo URL::to('/'); ?>/product_photo/'+product.productImage;
                                }
                                // if(product.description.length > 120){
                                //     var descriptionText = product.description.substring(1,120);
                                // }else{
                                //     var descriptionText = product.description;
                                // }
                                descriptionText = "No Contact Delivery/Pickup <br>";
                                if(product.warranty == 0){
                                    descriptionText+= "No Warranty <br>";
                                }else{
                                    descriptionText+= product.warranty+ " Month Warranty <br>";
                                }
                                if(product.exchange == 'na'){
                                    descriptionText+= "No Replacement <br>";
                                }else{
                                    descriptionText+= product.exchange+ " Days Replacement  <br> ";
                                }
                                if(product.installation_included == 'yes'){
                                    descriptionText+= "Installation Included <br>";
                                }else if(product.installation_included == 'no'){
                                    descriptionText+= "No Installation <br> ";
                                }else{
                                    descriptionText+= "";
                                }
                                var productLink = '<?php echo URL::to('/'); ?>/'+product.product_name.toLowerCase().replace(/[^a-zA-Z0-9]/g,"-")+'/'+product.product_id+'_'+product.vendorID;
                                var dt = new Date();
                                var hours = dt.getHours();
                                var deliveryText = '';
                                var pickspan = '';
                                if(product.free_delivery === '0'){
                                    deliveryText = 'pickup only';
                                    pickspan = 'pickup-span';
                                }else{
                                    if(hours >= 14){
                                        deliveryText = 'delivery tomorrow';
                                    }else{
                                        deliveryText = 'delivery today';
                                    }
                                    if(Math.round(product.distance) > Math.round(Number(product.free_delivery))){
                                        deliveryText = 'pickup only';
                                        pickspan = 'pickup-span';
                                    }
                                }
                                var kmText = Math.round(product.distance)+'km away';
                                var productName = product.product_name.substring(0, 80);
                                var mrpDiv = '';
                                if(product.discount > 0){
                                    mrpDiv = ' <del>₹'+Number(product.mrp).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')+'</del>';
                                }
                                tab2Html+=' <div class="ps-product ps-product--wide"><div class="ps-product__thumbnail"><a href="+productLink+"><img src="'+productImage+'" alt=""></a></div><div class="ps-product__container"><div class="ps-product__content"><a class="ps-product__title" href="'+productLink+'">'+productName+'</a><p class="ps-product__vendor">Sold by: <a href="javascript:void(0);">'+product.nickname+'</a></p><div class="ps-product__desc list_product_description">'+descriptionText+'</div></div><div class="ps-product__shopping"><p class="ps-product__price sale">₹'+Number(product.final_price).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')+mrpDiv+'</p><a class="ps-btn" href="'+productLink+'">Explore</a><ul class="ps-product__actions"></ul><span class="km-span-list '+pickspan+'">'+deliveryText+'</span> <br> <span class="km-span-list '+pickspan+'">'+kmText+'</span></div></div></div>';
                            });
                            if(page == 1){
                                tab2Html+= '</div>';
                            }
                        }else{
                            tab2Html = '<div class="ps-shopping-product"><p>No product found</p></div>';
                            $('#tab-2').html(tab1Html);
                        }
                        if(page == 1){
                            $('#tab-2').html(tab2Html);
                        }else{
                            $('.more-data').append(tab2Html);
                        }
                        $(".load-product").hide();
                },
                error: function(res){
                    $(".load-product").hide();
                }
            });
        }
    }


    var distanceDiv;
    function filterData(){
        page = 1;
        lastPage = 1;
        clearTimeout(distanceDiv);
        distanceDiv =  setTimeout(() => {
                $('#tab-2').html('');
                $('#tab-1').html('');
                getproducts(1);
        }, 1000);
    }
     // select storage
     $('body').on('click', '.storage', function(){
        if($(this).hasClass('active')){
            $(this).removeClass('active');
        }else{
            $(this).addClass('active');
        }
        filterData();
    });
      // select size
      $('body').on('click', '.size', function(){
        if($(this).hasClass('active')){
            $(this).removeClass('active');
        }else{
            $(this).addClass('active');
        }
        filterData();
    });
      // select measurement
      $('body').on('click', '.measurement', function(){
        if($(this).hasClass('active')){
            $(this).removeClass('active');
        }else{
            $(this).addClass('active');
        }
        filterData();
    });
     // select weight
     $('body').on('click', '.weight', function(){
        if($(this).hasClass('active')){
            $(this).removeClass('active');
        }else{
            $(this).addClass('active');
        }
        filterData();
    });
     // check colour
     $('body').on('click', '.colourlabel', function(){
         var colorInput = $(this).attr('for');
        if($("#"+colorInput).hasClass('active')){
            $("#"+colorInput).removeClass('active');
            $(this).css('border','');
        }else{
            $("#"+colorInput).addClass('active');
            $(this).css('border','3px solid #ffcc00');
        }
        filterData();
    });
     // distance change
     $('body').on('DOMSubtreeModified', '.ps-sliderdistance__min,.ps-sliderdistance__max,.ps-sliderdistancemob__min,.ps-sliderdistancemob__max', function(){
        filterData();
    });
    // sorting change
    $('body').on('change', '.sorting', function(){
        filterData();
    });
    // Ruppess change
    $('body').on('DOMSubtreeModified', '.ps-slider__min,.ps-slider__max,.ps-slidermob__min,.ps-slidermob__max', function(){
        filterData();
    });

    $(document).ready(function() {
        clearTimeout(distanceDiv);
        distanceDiv =  setTimeout(() => {
            getproducts(page);
        }, 1000);
        $(window).scroll(function() {
            clearTimeout(distanceDiv);
            distanceDiv = setTimeout(() => {
                if($(window).scrollTop() >= $(window).height() / 8 ) {
                    page++;
                    getproducts(page);
                }
            }, 1000);
        });
    });
</script>

    @endsection


