@extends('layouts.user')

@section('content')
<style>
    #submit{
        padding: 7px 15px;
    }
    .image{
        width: 75%;
        margin: auto;
        margin-top: 10px;
        margin-bottom: 20px;
    }
    #code{
        width: 50%;
        margin: auto;
    }
</style>
<div class="ps-page--simple">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="<?php echo URL::to('/'); ?>">Home</a></li>
                    <li><a href="<?php echo URL::to('/'); ?>">Shop</a></li>
                    <li>Tracking Detail</li>
                </ul>
            </div>
        </div>
        <div class="ps-section--shopping ps-shopping-cart">
            <div class="container">
                <div class="ps-section__header">
                    <h1>Tracking Detail</h1>
                </div>
                @if(sizeof($infos) > 0)
                <div class="ps-section__content">
                    <div class="table-responsive">
                        <table class="table ps-table--shopping-cart">
                            <thead>
                                <tr>
                                    <th>Product name</th>
                                    <th>QUANTITY</th>
                                    <th>PRICE</th>
                                    <th></th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach($infos as $info)
                                <tr>
                                    <input type="hidden" value="{{$info->sId}}" name="id">
                                    <td>
                                        <div class="ps-product--cart">
                                            <div class="ps-product__thumbnail"><a href="javascript:void(0);"><img src="/product_photo/{{$info->name}}" alt="" style="margin-top:40px;"></a></div>
                                            <div class="ps-product__content"><a href="product-default.html">{{$info->product_name}}</a>
                                                <p>Sold By: <strong>{{$info->vName}}</strong></p>
                                            </div>
                                        </div>
                                    </td>
                                    <td style="text-align: center;">{{$info->quantity}}</td>
                                    <td class="price" style="text-align: center;">₹{{number_format($info->amount)}}</td>
                                    <td style="text-align: end;">
                                        @if($info->status == 'processing')
                                            <form method="post" action="<?php echo URL::to('/'); ?>/update/image" enctype="multipart/form-data" >
                                                {{csrf_field() }}
                                                    <div class="">
                                                        <div class="col-md-12 mt10">
                                                                <!-- <label for="code">* Code</label> -->
                                                                <div>
                                                                    <input type="number" placeholder="Security Code" name="code" id="code" data-id="{{ $info->sId }}" class="form-control m-input code" value="" required>
                                                                    <input type="hidden"  name="path" value="tracking_info" >
                                                                    <input type="hidden"  name="tracking_number" value="{{$info->tracking_number}}" >
                                                                    <input type="hidden"  name="mobile" value="{{$info->mobileno}}" >
                                                                </div>
                                                                <input type="hidden" name="id" id="selectedSId" value="{{ $info->sId }}">
                                                        </div>
                                                        <div class="col-md-12 mt10">
                                                                <!-- <label for="name">* Upload Image</label> -->
                                                                <div>
                                                                    <input type="file" name="image" class="image"  accept="image/*;capture=camera" required>
                                                                </div>
                                                        </div>
                                                        <div class="col-md-12 mt10 text-center">
                                                                <p class="error-msg">Invalid Security Code</p>
                                                                <button class="ps-btn confirm"  id="submit" data-wizard-action="submit" >
                                                                    Submit
                                                                </button>
                                                        </div>
                                                    </div>
                                                </form>
                                        @endif
                                        <!-- <div style="padding-left: 50px; display:inline-flex;">
                                                <div style="display:inline"><input class="form-control" type="number" placeholder="Security Code" name="security" id="security{{$info->sId}}" value=""></div>&nbsp;
                                                <div style="display:inline"><a href="javascript:void(0);" class="ps-btn" onclick="status('{{$info->sId}}');">GO</a></div>
                                        </div>
                                        <br/>
                                        <div id="images{{$info->sId}}"></div> -->
                                    </td>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="ps-section__cart-actions">
                        <a class="ps-btn" href="<?php echo URL::to('/'); ?>"><i class="icon-arrow-left"></i> Back to Shop</a>
                    </div>
                </div>
                @else
                    <div class="ps-section__content">
                        <div class="text-center">
                            <div class="cart-text">Order detail not found</div>
                            <a class="ps-btn" href="/"><i class="icon-arrow-left"></i> Back to Shop</a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <script>


         $('.confirm').prop('disabled',true);
        $(".confirm").hide();
        $(".image").hide();
        $(".error-msg").hide();
        $('.code').on('input', function() {
            debugger;
            var code = $('#code').val();
            if(code.length == 4){
                    $('#loaders').show();
                    var code = code;
                    var id = $(this).attr('data-id');
                    $.ajax({
                        url: '<?php echo URL::to('/'); ?>/update/status',
                        type:"post",
                        data:{
                            '_token': '{{csrf_token()}}',
                            'id' : id,
                            'code' : code
                            },
                        success: function(response){
                            response = JSON.parse(response);
                            if(response.status === "success"){
                                $(".error-msg").hide();
                                $('.confirm').prop('disabled',false);
                                $(".confirm").show();
                                $(".image").show();
                            }else{
                                $(".error-msg").show();
                                $('confirm').prop('disabled', true);
                                $(".confirm").hide();
                                $(".image").hide();
                            }
                            $("#loaders").hide();
                        }
                    });
                    return;
            }
            else {
                $('confirm').prop('disabled', true);
                $(".confirm").hide();
                $(".image").hide();
            }

        });

        //     function status(id)
        //     {
        //         var code = $('#security'+id).val();
        //         if(code.length==4){
        //         $('#loaders').show();
        //         var code = code;
        //         var id = id;
        //         $.ajax({
        //             url: '<?php echo URL::to('/'); ?>/update/status',
        //             type:"post",
        //             data:{
        //                 '_token': '{{csrf_token()}}',
        //                 'id' : id,
        //                 'code' : code
        //                 },
        //             success: function(response){
        //                response = JSON.parse(response);
        //                     if(response.status === 'success'){
        //                         if(response.data > 0)
        //                         {
        //                             var html ='<div  class="image'+id+'" style="padding-left: 50px; display:inline-flex;margin-top:5%;"><div style="display:inline"><input class="form-control" type="file" placeholder="Image Upload" name="image" id="image'+id+'" value=""></div>&nbsp;<div style="display:inline"><a href="javascript:void(0);" class="ps-btn" onclick="uploadImage('+id+');">Upload</a></div></div>';
        //                             $('#images'+id).html(html);
        //                         }else{
        //                             $('#images'+id).hide();
        //                         }
        //                          alert(response.message);

        //                     }
        //                 $("#loaders").hide();
        //             }
        //         });
        //         return
        //     }
        //     else {
        //         alert('please Enter Currect Security Code');
        //         return false;
        //     }

        // }

        // function uploadImage(id){
        //     var images = $('#image'+id).val();
        //     var path = $("#image"+id).attr('src', 'data:image/jpeg;base64,');
        //     var image = $('#image'+id).attr('src');
        //    var base64ImageContent = image.replace(/^data:image\/(png|jpg);base64,/, "");
        //    // var blob = base64ToBlob(base64ImageContent, 'image/png');
        //     alert(base64ImageContent);
        //     var news = JSON.stringify(path);
        //     alert(news);

        //     var id = id;

        //         $.ajax({
        //             url: '<?php echo URL::to('/'); ?>/update/image',
        //             type:"post",
        //             data:{
        //                 '_token': '{{csrf_token()}}',
        //                 'id' : id,
        //                 'image' : image,

        //                 },
        //             success: function(response){
        //                response = JSON.parse(response);
        //                     if(response.status === 'success'){
        //                         alert(response.message);
        //                         location.reload();
        //                     }

        //             }
        //         });
        //         return
        // }

    </script>

@endsection
