@extends('layouts.user')

@section('content')

<div class="ps-page--simple">
    <div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li> Order Tracking</li>
            </ul>
        </div>
    </div>
    <div class="ps-order-tracking">
        <div class="container">
            <div class="ps-section__header">
                <h3>Order Tracking</h3>
                <p>To track your order please enter your Order ID in the box below and press the "Track" button. This was given to youon your receipt and in the confirmation email you should have received.</p>
            </div>
            <div class="ps-section__content">
                <form class="ps-form--order-tracking" action="{{route('tracking_info')}}" method="post">
                    {{csrf_field() }}
                    <div class="form-group">
                        <label>Tracking Code</label>
                        <input class="form-control" type="text" placeholder="Tracking Code" name="tracking_code" value="@if(isset($data['tracking_code'])){{$data['tracking_code']}}@endif" required>
                    </div>
                    <div class="form-group">
                        <label>Vendor Mobile Number</label>
                        <input class="form-control" type="text" placeholder="Vendor Mobile Number" name="mobile" value="@if(isset($data['mobile'])){{$data['mobile']}}@endif" required>
                    </div>
                    <div class="form-group">
                        <button class="ps-btn ps-btn--fullwidth" type="submit">Track Your Order</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
