<?php
    $pin = Session::get('pincode');
    $userCarts = usercart();
    if(sizeof($userCarts) == 0){
        echo "<script>window.location.href='/user/my-cart'</script>";
        return false;
    }
?>
@extends('layouts.user')

@section('content')
<?php $final = 0;?>
<div class="ps-page--simple">
    <div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="/">Shop</a></li>
                <li>Checkout</li>
            </ul>
        </div>
    </div>
    <div class="ps-checkout ps-section--shopping">
        <div class="container">
            <div class="ps-section__header">
                <h1>Checkout</h1>
            </div>
            <div class="ps-section__content">
                    <form class="m-form m-form--label-align-left- m-form--state-" id="m_form" enctype="multipart/form-data" method="post" action="{{route('add_to_checkout')}}">
                        {{csrf_field() }}

                    <div class="row">
                        <div class="col-xl-6 col-lg-7 col-md-12 col-sm-12  ">
                            <div class="ps-form__billing-info">
                                <h3 class="ps-form__heading">Billing Details</h3>
                                <div class="form-group">
                                    <label>Name<sup>*</sup>
                                    </label>
                                    <div class="form-group__content">
                                        <input class="form-control" type="hidden" name="user_id" value="@if(isset($user->id)){{$user->id}}@endif">
                                        <input class="form-control" type="text" name="name" value="@if(isset($user->name)){{$user->name}}@endif" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Email Address<sup>*</sup>
                                    </label>
                                    <div class="form-group__content">
                                        <input class="form-control" type="email" name="email" value="@if(isset($user->email)){{$user->email}}@endif" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"  required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Phone Number<sup>*</sup>
                                    </label>
                                    <div class="form-group__content">
                                        <input class="form-control" type="text" name="phone" value="@if(isset($user->mobileno)){{$user->mobileno}}@endif" pattern="[789][0-9]{9}" required readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Whatsapp Number<sup>*</sup>
                                    </label>
                                    <div class="form-group__content">
                                        <input class="form-control" type="text" name="whatsapp_no" value="@if(isset($user->whatsapp_no)){{$user->whatsapp_no}}@else{{$user->mobileno}}@endif" pattern="[789][0-9]{9}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Address Line1<sup>*</sup>
                                    </label>
                                    <div class="form-group__content">
                                        <input class="form-control" type="text" name="addressline1" value="@if(isset($user->address->addressline1)){{$user->address->addressline1}}@endif" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Address Line2<sup>*</sup>
                                    </label>
                                    <div class="form-group__content">
                                        <input class="form-control" type="text" name="addressline2" value="@if(isset($user->address->addressline2)){{$user->address->addressline2}}@endif">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label>City<sup>*</sup>
                                        </label>
                                        <div class="form-group__content">
                                            <input class="form-control" type="text" name="city" value="@if(isset($data['city'])){{$data['city']}}@endif" required readonly>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>State<sup>*</sup>
                                        </label>
                                        <div class="form-group__content">
                                            <input class="form-control" type="text" name="state" value="@if(isset($data['state'])){{$data['state']}}@endif" required readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Pincode<sup>*</sup>
                                    </label>
                                    <div class="form-group__content">
                                        <input class="form-control" type="text" name="pincode" value="{{ $pin }}" readonly>
                                    </div>
                                </div>

                                <h3 class="mt-40"> Additional information</h3>
                                <div class="form-group">
                                    <label>Order Notes</label>
                                    <div class="form-group__content">
                                        <textarea class="form-control" rows="7" placeholder="Notes about your order, e.g. special notes for delivery." name="note" value=""></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-5 col-md-12 col-sm-12  ">
                            <div class="ps-form__total">
                                <h3 class="ps-form__heading">Your Order</h3>
                                <div class="content mt45">
                                    <table class="table ps-block__products">
                                    <div class="ps-block--checkout-total">
                                        <div class="ps-block__header">
                                            <tr>
                                            <th>Sold By</th>
                                            <th>Product</th>
                                            <th>Total</th>
                                        </div>
                                        <div class="ps-block__content">

                                                <tbody>
                                                    <?php $i = 1; ?>
                                                    @foreach($userCarts as $cart)

                                                    <input name="vendor_id[{{$i}}]" value="{{$cart->vendor['id']}}" type="hidden">
                                                    <input name="product_id[{{$i}}]" value="{{$cart->product_id}}" type="hidden">
                                                    <input name="quantity[{{$i}}]" value="{{$cart->cartQauntity}}" type="hidden">
                                                    <input name="amounts[{{$i}}]" value="{{$cart->productAttributeValue->productPrice->final_price}}" type="hidden">
                                                    <input name="product_attribute[{{$i}}]" value="{{$cart->productAttributeValue}}" type="hidden">
                                                    <input name="product_attribute_value_id[{{$i}}]" value="{{$cart->product_attribute_value_id}}" type="hidden">
                                                    <input name="product_price_id[{{$i}}]" value="{{$cart->productAttributeValue->productPrice->id}}" type="hidden">
                                                    <input name="deliverykm[{{$i}}]" value="{{getDistance($cart->vendor)}}" type="hidden">
                                                    <input name="deliveryvalue[{{$i}}]" value="{{$cart->productPrice[0]->free_delivery}}_{{$cart->productPrice[0]->mode}}" type="hidden">
                                                    <tr>
                                                    <?php $pURL = $cart->product_id.'_'.$cart->vendor_id; ?>
                                                        <td class="checkout-vendor-name"><strong>{{$cart->vendor['nickname']}}</strong></td>
                                                        <td class="checkout-product-name" ><a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$cart->product->product_name)), 'id' =>  $pURL ])}}"> {{$cart->product->product_name}}</a>
                                                        <p class="cart-attr">

                                                                <span class="space-15">{{$cart->cartQauntity}} x ₹{{ number_format($cart->productAttributeValue->productPrice->final_price)}}   </span>
                                                                @if($cart->colour != '' && $cart->colour != null)
                                                                    <span class="ps-variant ps-variant--color colour" data-value="{{$cart->colour}}" style="background-color: #{{$cart->colour}};"></span>
                                                                @endif
                                                                <span> @if($cart->weight != '' && $cart->weight != null) {{ $cart->weight }} @endif
                                                                        @if($cart->size != '' && $cart->size != null) {{ $cart->size }} @endif
                                                                        @if($cart->measurement != '' && $cart->measurement != null) {{ $cart->measurement }} @endif
                                                                        @if($cart->storage != '' && $cart->storage != null) {{ $cart->storage }} @endif
                                                                </span>
                                                        </p>
                                                            <p class="checkout-delivery" ><span class="km-span-list   @if($cart->productPrice[0]->free_delivery == 0 || getDistance($cart->vendor) > $cart->productPrice[0]->free_delivery) pickup-span @endif" style=" font-size: 15px !important;"> @if($cart->productPrice[0]->free_delivery == 0)
                                                                    Pickup Only
                                                                @else
                                                                    @if(getDistance($cart->vendor) > $cart->productPrice[0]->free_delivery )
                                                                        Pickup Only
                                                                    @else
                                                                        Delivery
                                                                        @if($cart->productPrice[0]->mode == 1)
                                                                                @if(date('H') < 14)
                                                                                    today
                                                                                @else
                                                                                    tomorrow
                                                                                @endif
                                                                            @else
                                                                                tomorrow
                                                                        @endif
                                                                    @endif
                                                                @endif </span><span class="km-span-list   @if($cart->productPrice[0]->free_delivery == 0 || getDistance($cart->vendor) > $cart->productPrice[0]->free_delivery) pickup-span @endif" style=" font-size: 15px !important;"> {{getDistance($cart->vendor)}} km  away</span></p>
                                                        </td>
                                                        <?php
                                                            $total = $cart->cartQauntity * $cart->productAttributeValue->productPrice->final_price;
                                                        ?>
                                                        <td class="checkout-item-total">₹ {{number_format($total)}}</td>
                                                    </tr>

                                                    <?php $final = $final + $total ;
                                                    $i++;
                                                    ?>
                                                    @endforeach
                                                    <tr>
                                                        <td colspan="2"> <h3>Total </h3></td>
                                                        <td> <h3><span>₹{{number_format($final)}}</span></h3></td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <input name="amount" value="{{$final}}" type="hidden">
                                        </div>
                                    </div><button class="ps-btn ps-btn--fullwidth" type="submit">Proceed to Pay</button>
                                </div>
                            </div>
                        </div>
                        <button class="hide-in-mobile ps-btn ps-btn--fullwidth" type="submit">Proceed to Pay</button>
                    </div>

            </div>
        </div>
    </div>
</div>
</form>
@endsection
