<<?php
   use App\Category;
   use App\Cart;
//    checkRole();
   $userscartProducts = usercart();
   $catData = getAvailableCategory();
   $categories = $catData['categories'];
   $subcategories = $catData['subcategories'];
?>
@extends('layouts.user')

@section('content')
    <main class="ps-page--my-account">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li>Payment Success</li>
                </ul>
            </div>
        </div>
        <section class="ps-section--account">
            <div class="container">
                <div class="ps-block--payment-success">
                    <h3>Payment Success !</h3>
                    <p>Thank you for your payment. Our DASH vendor will confirm your order shortly. We will redirect you to order page within 5 seconds. Please do not press back button.</p>
                </div>
            </div>
        </section>
        <div class="ps-newsletter">
            <div class="ps-container">
                <form class="ps-form--newsletter" action="do_action" method="post">
                    <div class="row">
                        <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <div class="ps-form__left">
                                <h3>Newsletter</h3>
                                <p>Subcribe to get information about products and coupons</p>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <div class="ps-form__right">
                                <div class="form-group--nest">
                                    <input class="form-control" type="email" placeholder="Email address">
                                    <button class="ps-btn">Subscribe</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </main>
    <script>
        setTimeout(() => {
                window.location.href="/user/live-order/list";
        }, 5000);
    </script>
    @endsection
