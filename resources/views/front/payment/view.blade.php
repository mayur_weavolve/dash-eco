@extends('layouts.user')

@section('content')
<style>
.success{
    color : green;
}
.error{
    color : red;
}
</style>
<div class="ps-page--single">
        <div class="ps-breadcrumb">
            <div class="container">
                 <ul class="breadcrumb">
                    <li><a href="<?php echo URL::to('/'); ?>">Home</a></li>
                    <li><a href="{{ route('vendor_home') }}">Vendor</a></li>
                    <li>Dashboard</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="ps-vendor-dashboard">
            <div class="container">
                <div class="ps-section__header">
                    @if($vendor)
                    <h3>Hello,{{ $vendor->name }}</h3>
                    <p>{{ $vendor->business_name }}<br>{{ $vendor->address }}<br> {{ $vendor->postcode }} <br> GSTIN : {{ $vendor->gst_no }} </p>
                    @endif
                </div>
                <div class="ps-section__content">
                    <ul class="ps-section__links">
                        <li><a href="{{ route('vendor_home') }}">Dashboard</a></li>
                        <li  ><a href="{{route('vendor_product_list')}}">Products</a></li>
                        <li><a href="{{route('vendor_recent_order_list')}}">Live Order</a></li>
                        <li><a href="{{route('vendor_completed_order_list')}}">Completed Order</a></li>
                        <li class="active"><a href="{{route('vendor_payment_view')}}">Payment</a></li>
                        <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                </form>
                        </li>

                    </ul>
<div class="container">

    <div class="ps-section__content">
        <form class="ps-form--checkout" enctype="multipart/form-data" method="post" action="{{route('vendor_payment_add')}}">

        {{csrf_field() }}

            <div class="ps-form__content" style="margin-top: 5%;">
                <div class="row">
                    <div class="col-xl-2 col-lg-2 col-md-12 col-sm-12 col-12 "></div>
                    <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12 ">
                        <div class="ps-block--shipping">

                            <h4>Payment Methods</h4>
                            @if(isset($error))
                                    <h3 class="error">{{ $error }}</h3>
                            @endif
                            @if(isset($success))
                                    <h3 class="success">{{ $success }}</h3>
                            @endif
                            <div class="ps-block--payment-method">
                                <ul class="ps-tab-list">
                                    <li class="active"><a name="bank" class="ps-btn ps-btn--sm bank" href="#bank_account">Bank Account </a></li>
                                    <li><a name="upi" class="ps-btn ps-btn--sm upi" href="#upi">UPI</a></li>
                                </ul>
                                @if(isset($errors))
                                    <input name="old_type" value="{{old('type')}}" type="hidden" id="old_type">
                                @endif
                                <input name="type" value="" type="hidden" id="type">
                                <div class="ps-tabs">

                                        <div class="ps-tab active" id="bank_account">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input class="form-control" type="text" placeholder="Enter Name" name="name" value="" >

                                            @error('name')
                                                <span class="help-block" role="alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>IFSC</label>
                                                <input class="form-control" type="text" placeholder="Enter IFSC" name="ifsc" value="" >

                                            @error('ifsc')
                                                <span class="help-block" role="alert">
                                                    <strong>{{ $errors->first('ifsc') }}</strong>
                                                </span>
                                            @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>Bank Name</label>
                                                <input class="form-control" type="text" placeholder="Enter BankName" name="bank_name" value="" >

                                            @error('bank_name')
                                                <span class="help-block" role="alert">
                                                    <strong>{{ $errors->first('bank_name') }}</strong>
                                                </span>
                                            @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>Account Number</label>
                                                <input class="form-control" type="text" placeholder="Enter Account Number" name="account_number" value="" >

                                            @error('account_number')
                                                <span class="help-block" role="alert">
                                                    <strong>{{ $errors->first('account_number') }}</strong>
                                                </span>
                                            @enderror
                                            </div>
                                        </div>
                                        <div class="ps-tab" id="upi">
                                                <label>Address</label>
                                                <input class="form-control" type="text" placeholder="Enter Address" name="address" value="" >

                                        @error('address')
                                            <span class="help-block" role="alert">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                        @enderror
                                        </div>
                                    </br>
                                        <div class="form-group submit">
                                            <button class="ps-btn ps-btn--fullwidth">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-12 col-sm-12 col-12 "></div>
                </div>
            </div>
        </form>
    </div>
</div>

</div>
            </div>
        </div>
<script>
    $( document ).ready(function() {
        $("#type").val("bank_account");
        var type = $( "#old_type" ).val();
        if(type == 'upi'){
            $('#bank_account').hide();
            $('#upi').show();
        }
        if(type == 'bank_account'){
            $('#bank_account').show();
            $('#upi').hide();
        }
    });

    $('.bank').click(function(){
        $("#type").val("bank_account");
    });
    $('.upi').click(function(){
        $("#type").val("vpa");
    });
</script>
@endsection

