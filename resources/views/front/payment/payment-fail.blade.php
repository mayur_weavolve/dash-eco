<<?php
   use App\Category;
   use App\Cart;
//    checkRole();
   $userscartProducts = usercart();
   $catData = getAvailableCategory();
   $categories = $catData['categories'];
   $subcategories = $catData['subcategories'];
?>
@extends('layouts.user')

@section('content')
    <main class="ps-page--my-account">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li>Payment Fail</li>
                </ul>
            </div>
        </div>
        <section class="ps-section--account">
            <div class="container">
                <div class="ps-block--payment-success">
                    <h3 style="color: #ef4444;">Payment Fail !</h3>
                    <p>Opps!!! Try again later.  we redirect to you in checkout page with in 15 seconds. please don't use Back button.</p>
                </div>
            </div>
        </section>
        <div class="ps-newsletter">
            <div class="ps-container">
                <form class="ps-form--newsletter" action="do_action" method="post">
                    <div class="row">
                        <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <div class="ps-form__left">
                                <h3>Newsletter</h3>
                                <p>Subcribe to get information about products and coupons</p>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <div class="ps-form__right">
                                <div class="form-group--nest">
                                    <input class="form-control" type="email" placeholder="Email address">
                                    <button class="ps-btn">Subscribe</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </main>
    <script>
        setTimeout(() => {
                window.location.href="/user/checkout";
        }, 5000);
    </script>
    @endsection
