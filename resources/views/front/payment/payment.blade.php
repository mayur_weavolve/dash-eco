<?php
   use App\Category;
   use App\Cart;
//    checkRole();
   $userscartProducts = usercart();
   $catData = getAvailableCategory();
   $categories = $catData['categories'];
   $subcategories = $catData['subcategories'];
?>
@extends('layouts.user')

@section('content')
    <main class="ps-page--my-account">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="{{ route('my_account') }}">Home</a></li>
                    <li><a href="{{ route('checkout') }}">Order</a></li>
                    <li>Payment</li>
                </ul>
            </div>
        </div>
        <section class="ps-section--account ps-checkout">
            <div class="container">
                <div class="ps-section__header">
                    <h3>Payment</h3>
                </div>
                <div class="ps-section__content">
                    <!-- <form class="ps-form--checkout" action="index.html" method="get"> -->
                        <div class="ps-form__content">
                            <div class="row">
                                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12 ">
                                    <div class="ps-block--shipping">
                                        <div class="ps-block__panel">
                                            <figure><small>Contact</small>
                                                <p><a href="javascript:void(0);">{{ $orderDetail->shipping->name }}</a></p><a href="{{ route('checkout') }}">Change</a>
                                            </figure>
                                            <figure><small>Ship to</small>
                                                <p>@if(isset($orderDetail->shipping->address_line1)){{ $orderDetail->shipping->address_line1 }}, @endif
                                                @if(isset($orderDetail->shipping->address_line2)){{ $orderDetail->shipping->address_line2 }}, @endif
                                                @if(isset($orderDetail->shipping->city)){{ $orderDetail->shipping->city }}, @endif
                                                @if(isset($orderDetail->shipping->pincode)){{ $orderDetail->shipping->pincode }} @endif</p><a href="{{ route('checkout') }}">Change</a>
                                            </figure>
                                        </div>
                                        <!-- <h4>Shipping method</h4>
                                        <div class="ps-block__panel">
                                            <figure><small>International Shipping</small><strong> ₹1.00</strong></figure>
                                        </div> -->
                                        <h4>Pay by Paytm</h4>
                                        <div class="ps-block--payment-method">
                                            <!-- <ul class="ps-tab-list">
                                                <li class="active"><a class="ps-btn ps-btn--sm" href="#visa">Paytm</a></li>
                                                <li><a class="ps-btn ps-btn--sm" href="#paypal">GooglePay</a></li>
                                            </ul> -->
                                            <div class="ps-tabs">
                                                <div class="ps-tab active" id="visa">
                                                    <form  action="{{route('payment',$orderDetail->order_id)}}" method="post">
                                                        <!-- <div class="form-group">
                                                            <label>Paytm number</label>
                                                            <input class="form-control" type="text" name = "mobile_no" placeholder="Enter Paytm Mobileno">
                                                        </div> -->
                                                        {{csrf_field() }}
                                                        <input class="form-control" type="hidden" name = "mobile_no" placeholder="Enter Paytm Mobileno" value="{{ Auth::user()->mobileno }}">
                                                        <div class="form-group submit">
                                                            <button class="ps-btn ps-btn--fullwidth">Pay</button>
                                                        </div>
                                                </div>
                                                <!-- <div class="ps-tab" id="paypal">

                                                        <div class="form-group">
                                                            <label>GooglePay Id</label>
                                                            <input class="form-control" type="text" placeholder="">
                                                        </div>

                                                        <div class="form-group submit">
                                                            <button class="ps-btn ps-btn--fullwidth">Proceed with Paypal</button>
                                                        </div>

                                                </div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 ">
                                    <div class="ps-block--checkout-order">

                                        <div class="ps-block__content payment-cart">
                                            <figure>
                                                <figcaption><strong>Product</strong><strong>Total</strong></figcaption>
                                            </figure>
                                            <?php $total = 0 ;?>
                                            @foreach($orderDetail->subOrders as $suborder)
                                            <?php $pURL = $suborder->product_id.'_'.$suborder->vendor_id; ?>
                                            <figure class="ps-block__items"><a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$suborder->product->product_name)), 'id' =>  $pURL ])}}">
                                            <p><strong>{{$suborder->product->product_name}}</strong><br>
                                            {{$suborder->quantity}} x ₹{{ number_format($suborder->amount)}}</p>
                                            <span>₹{{ number_format($suborder->quantity * $suborder->amount)}}</span></a>
                                            </figure>
                                            <?php $total = $total + ($suborder->quantity * $suborder->amount); ?>
                                            @endforeach
                                            <!-- <figure>
                                                <figcaption><strong>Subtotal</strong><strong>₹{{ number_format($total)}}</strong></figcaption>
                                            </figure> -->
                                            <!-- <figure>
                                                <figcaption><strong>Shipping</strong><strong>₹1.00</strong></figcaption>
                                            </figure> -->
                                            <figure class="ps-block__total">
                                                <h3>Total<strong>₹{{number_format($total)}}</strong></h3>
                                            </figure>
                                        </div>
                                        <input type="hidden" name="amount" value="{{$total}}">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- </form> -->
                </div>
            </div>
        </section>
        <div class="ps-newsletter">
            <div class="ps-container">
                <form class="ps-form--newsletter" action="do_action" method="post">
                    <div class="row">
                        <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <div class="ps-form__left">
                                <h3>Newsletter</h3>
                                <p>Subcribe to get information about products and coupons</p>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <div class="ps-form__right">
                                <div class="form-group--nest">
                                    <input class="form-control" type="email" placeholder="Email address">
                                    <button class="ps-btn">Subscribe</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </main>
@endsection
