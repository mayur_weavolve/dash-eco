@extends('layouts.admin')

@section('content')
<style>
 .subbox {
    margin: auto;
    /* display: inline-block; */
    padding: 0px 0px 0px 0px;
    /* font-size: 15px; */
}
.subbox input{
    width: 50%;
}
#colour{
    height: 30px;
    padding: 2px;
    margin: auto;
    width: 90%;
    font-weight: 800;
}
#colour option {
    font-weight: 800;
    border: none;
    font-size: 14px;
}
th{
    text-align: center;
    font-size: 15px;
    font-weight: 600 !important;
}
.removeAttr{
    color: red;
    font-size: 25px;
}
</style>
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Add New Product Price (Vendor Name : {{ $vendor->name }})
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			    <ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('vendor') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-reply"></i>
						    <span>Back</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item"></li>
			    </ul>
	        </div>
	    </div>



	<form class="m-form m-form--label-align-left- m-form--state-" id="m_form" enctype="multipart/form-data" method="post" action="{{route('product_price_add',[$vendor->id])}}">
	{{csrf_field() }}
		<div class="m-portlet__body">
			<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
				<div class="row">
					<div class="col-xl-10 offset-xl-1">
						<div class="m-form__section">
                            <div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Product Name:</label>
								<div class="col-xl-8 col-lg-8">
									<select name="product_id" class="form-control m-select2 product_id"  id="product_id" name="param" required>
										<option value="">Select Product Name</option>
										<?php foreach ($products as $product) { ?>
										<option value="{{$product->id}}">{{$product->product_name}}</option>
                                        <?php  } ?>

									</select>
									@error('product_name')
                       					<span class="help-block" role="alert">
                           					<strong>{{ $errors->first('product_name') }}</strong>
                        				</span>
                    				@enderror
								</div>
							</div>
							<fieldset id="buildyourform" name = "form">
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* MRP:</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input  type="number" min="0"  name="mrp" id="mrp" class="form-control m-input price" placeholder="" value="{{Request::old('mrp')}}" required>
                                        @error('mrp')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('mrp') }}</strong>
                                        </span>
                                        @enderror
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Selling Price:</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input  type="number" min="0"  name="final_price" id="final_price" class="form-control m-input price" placeholder="" value="{{Request::old('final_price')}}" required>
                                        @error('final_price')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('final_price') }}</strong>
                                        </span>
                                        @enderror
                                </div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">Discount(%):</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="discount" id="discount" class="form-control m-input" placeholder="" value="{{Request::old('discount')}}" readonly>
											@error('discount')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('discount') }}</strong>
                        					</span>
                    						@enderror
									</div>
                            </div>
                            <div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">What you will get after DASH charges:</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="text" name="dash_price" id="dash_price" class="form-control m-input" placeholder="" value="{{Request::old('dash_price')}}" readonly>
                                        @error('dash_price')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('dash_price') }}</strong>
                                        </span>
                                        @enderror
                                </div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">Warranty:</label>
									<div class="col-xl-8 col-lg-8">
                                    <input  type="number" min="0"  name="warranty" id="warranty" class="form-control m-input" placeholder="NA or no. of months" value="{{Request::old('warranty')}}" >
											@error('warranty')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('warranty') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">Installation Included:</label>
									<div class="col-xl-8 col-lg-8">
                                        <select name="installation_included" class="form-control m-input" value = "">
                                            <option value="">Select</option>
                                            <option value="yes">Yes</option>
                                            <option value="no">NO</option>
                                            <option value="na">NA</option>
                                        </select>
                                        @error('installation_included')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('installation_included') }}</strong>
                                        </span>
                                        @enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">Exchange:</label>
									<div class="col-xl-8 col-lg-8">
										<input  type="number" min="0"  name="exchange" id="exchange" class="form-control m-input" placeholder="NA or no. of days" value="{{Request::old('exchange')}}">
											@error('exchange')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('exchange') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Free Delivery / Pickup:</label>
									<div class="col-xl-8 col-lg-8">
                                        <select name="free_delivery" class="form-control m-input" value = ""required>
                                            <option value="">Select Free Delivery</option>
                                            <option value="0">Pickup Only</option>
                                            <option value="5">5 KM</option>
                                            <option value="7">7 KM</option>
                                            <option value="10">10 KM</option>
                                            <option value="15">15 KM</option>
                                            <option value="20">20 KM</option>
                                        </select>
                                        @error('free_delivery')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('free_delivery') }}</strong>
                                        </span>
                                        @enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Mode:</label>
									<div class="col-xl-8 col-lg-8">
                                        <select name="mode" class="form-control m-input" value = "" required>
                                            <option value="">Select Mode</option>
                                            <option value="1">Same day if order before 2 pm</option>
                                            <option value="0">Next Day</option>
                                        </select>
											@error('mode')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('mode') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
                            <div class="form-group m-form__group row">
								<label class="col-xl-12 col-lg-12 col-form-label">* Attributes:</label>
								<div class="col-xl-12 col-lg-12 productAttr">
                                            <h6>No Attribute Found</h6>
								</div>
								<div class="col-xl-12 col-lg-12 text-center">
                                    <input class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info attrbutton" type = "button" value = "Add Attribute" id = "add" />&nbsp;
                                    <!-- <input class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning attrbutton" type = "button" value = "Remove Attribute" id = "remove" /> -->
								</div>

							</div>
							<!-- <div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Size:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="size" id="size" class="form-control m-input" placeholder="" value="" required>
											@error('size')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('size') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Colour:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="colour" id="colour" class="form-control m-input" placeholder="" value="" required>
											@error('colour')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('colour') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Stock:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="stock" id="stock" class="form-control m-input" placeholder="" value="" required>
											@error('stock')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('stock') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div> -->
							</fieldset><br>
							<div class="col-lg-8 m--align-right">
								<button class="btn btn-primary m-btn m-btn--custom m-btn--icon" id="addProPrice" data-wizard-action="submit" >
									<span>
										<i class="la la-check"></i>&nbsp;&nbsp;
											<span>Submit</span>
									</span>
								</button>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
</div>
</div>
</div>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
<script src="<?php echo URL('/'); ?>/admin/assets/vendors/select2/dist/js/select2.full.js" type="text/javascript"></script>
<script type = "text/javascript">
$('#product_id').select2({
            placeholder: "Select a product"
        });
$('.attrbutton').hide();
$("#add").click(function(){
    $( "#copyrow" ).clone().appendTo( ".pestrow" );
});
$("body").on("click",".removeAttr",function(){
    $(this).closest('#copyrow').remove();
});
$(".price").keyup(function(){
  var mrp = $("#mrp").val();
  var final_price = $("#final_price").val();
  var discountPrice = mrp - final_price;
  var discountPercentage = (discountPrice * 100) / mrp;
  var catPercentage = 7;
  var DashPrice = final_price - ((7 * final_price) / 100);
  $('#dash_price').val(DashPrice.toFixed(2));
  $('#discount').val(discountPercentage.toFixed(2));
});
$("body").on("change",".product_id",function(){
    var html = '';
    var colorGroup = { };
    colorGroup[0] = { 'name' : "WHITE", "code" : "FFFFFF", "fontColor" : "000" };
    colorGroup[1] = { 'name' : "SILVER", "code" : "C0C0C0", "fontColor" : "000" };
    colorGroup[2] = { 'name' : "GRAY", "code" : "808080", "fontColor" : "000" };
    colorGroup[3] = { 'name' : "YELLOW", "code" : "FFFF00", "fontColor" : "000" };
    colorGroup[4] = { 'name' : "BLACK", "code" : "000000", "fontColor" : "FFFFFF" };
    colorGroup[5] = { 'name' : "RED", "code" : "FF0000", "fontColor" : "FFFFFF" };
    colorGroup[6] = { 'name' : "MAROON", "code" : "800000", "fontColor" : "FFFFFF" };
    colorGroup[7] = { 'name' : "OLIVE", "code" : "808000", "fontColor" : "FFFFFF" };
    colorGroup[8] = { 'name' : "LIME", "code" : "00FF00", "fontColor" : "FFFFFF" };
    colorGroup[9] = { 'name' : "GREEN", "code" : "008000", "fontColor" : "FFFFFF" };
    colorGroup[10] = { 'name' : "AQUA", "code" : "00FFFF", "fontColor" : "FFFFFF" };
    colorGroup[11] = { 'name' : "TEAL", "code" : "008080", "fontColor" : "FFFFFF" };
    colorGroup[12] = { 'name' : "BLUE", "code" : "0000FF", "fontColor" : "FFFFFF" };
    colorGroup[13] = { 'name' : "NAVY", "code" : "000080", "fontColor" : "FFFFFF" };
    colorGroup[14] = { 'name' : "FUCHSIA", "code" : "FF00FF", "fontColor" : "FFFFFF" };
    colorGroup[15] = { 'name' : "PURPLE", "code" : "800080", "fontColor" : "FFFFFF" };
    colorGroup[16] = { 'name' : "BROWN", "code" : "A52A2A", "fontColor" : "FFFFFF" };
    colorGroup[17] = { 'name' : "ORANGE", "code" : "FFA500", "fontColor" : "FFFFFF" };
    $.ajax({
        url: '<?php echo URL::to('/'); ?>/getproductattribute',
        type:"post",
        data:{
            'product_id' : $(this).val(),
        },
        success: function(response){
           response = JSON.parse(response);
                if(response.status === 'success'){
                    if(Object.keys(response.data).length > 0){
                        var colour = response.data.colour;
                        var weight = response.data.weight;
                        var measurement = response.data.measurement;
                        var size = response.data.size;
                        var storage = response.data.storage;
                        debugger;
                        html = '<table class="table table-striped- table-bordered table-checkable"><tr>';
                        if(colour !==  undefined){
                            html+= '<th width="15%">Colour</th>';
                        }
                        if(weight !==  undefined){
                            html+= '<th width="20%">Weight (Unit)</th>';
                        }
                        if(measurement !==  undefined){
                            html+= '<th width="20%">Measurement (Unit)</th>';
                        }
                        if(size !==  undefined){
                             html+= '<th width="15%">Size (Unit)</th>';
                        }
                        if(storage !==  undefined){
                            html+= '<th width="20%">Storage</th>';
                        }
                        html+= '<th width="10%">Quantity</th><th width="10%">#</th></tr><tbody class="pestrow"><tr class="text-center" id="copyrow"><input type="hidden" name="attrRows[]">';
                        if(colour !==  undefined){
                            html+= '<td><div class="subbox"><select  name="colour[]" id="colour" value="1" >';
                            console.log(colorGroup);
                            $.each( colorGroup, function( key, value ) {
                                html+= '<option value="'+value.code+'" style="background-color:#'+value.code+';color:#'+value.fontColor+';">'+value.name+'</option>';
                            });
                            html+= '</select></div></td>';
                        }
                        if(weight !==  undefined){
                            html+= '<td><div class="subbox"><input  type="number" min="0"  name="weight[]" value="0"><select name="weightUnit[]"  id="weightUnit">';
                            $.each( weight, function( key, value ) {
                                html+= '<option value="'+value+'">'+value+'</option>';
                            });
                            html+= '</select></div></td>';
                        }
                        if(measurement !==  undefined){
                            html+= '<td><div class="subbox"><input  type="number" min="0"  name="measurement[]" value="0"><select name="measurementUnit[]"  id="measurementUnit">';
                            $.each( measurement, function( key, value ) {
                                html+= '<option value="'+value+'">'+value+'</option>';
                            });
                            html+= '</select></div></td>';
                        }
                        if(size !==  undefined){
                            html+= '<td><div class="subbox"><select name="size[]"  id="size">';
                            $.each( size, function( key, value ) {
                                html+= '<option value="'+value+'">'+value+'</option>';
                            });
                            html+= '</select></div></td>';
                        }
                        if(storage !==  undefined){
                            html+= '<td><div class="subbox"><input  type="number" min="0"  name="storage[]" value="0"><select name="storageUnit[]"  id="storageUnit">';
                            $.each( storage, function( key, value ) {
                                html+= '<option value="'+value+'">'+value+'</option>';
                            });
                            html+= '</select></div></td>';
                        }
                        html+= '<td><div class="subbox"><input  type="number" min="0"  name="quantity[]" value="0" style="width: 50px;"></td><th width="10%"><i class="la la-times-circle removeAttr"></i></th></tr></tbody></table>';
                        document.getElementById("addProPrice").disabled = false;
                        $('.attrbutton').show();
                    }else{
                        html+= ' <h6>No Attribute Found</h6>';
                        html+='<span style="color:red;">Please Add Product Attributes.</span>';
                        document.getElementById("addProPrice").disabled = true;
                        $('.attrbutton').hide();
                    }

                }else{
                    html+= ' <h6>No Attribute Found</h6>';
                    html+='<span style="color:red;">Please Add Product Attributes.</span>';
                    document.getElementById("addProPrice").disabled = true;
                    $('.attrbutton').hide();
                }
                $('.productAttr').html(html);
        },error: function(response){
            html+= ' <h6>No Attribute Found</h6>';
            html+='<span style="color:red;">Please Add Product Attributes.</span>';
            document.getElementById("addProPrice").disabled = true;
            $('.attrbutton').hide();
            $('.productAttr').html(html);
        }
    });
});


</script>

@endsection





