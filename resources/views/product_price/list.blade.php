@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Product Price List (Vendor Name : {{ $vendor->name }})
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('product_price_add',[$vendor->id]) }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
						    <span>Product Price Add</span>
						</span>
					</a>
				</li>

				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item">

			</ul>
	    </div>
	</div>
	<div class="m-portlet__body">
		<!-- <div class = "row">
			<div class = "col-md-12">
				<form action = "" method = "get">
				<div class="row">
					<div class = "col-md-10 text-right">
						<input type="search" name="search"class="form-control" placeholder="Search"><br>
					</div>
					<div class="col-md-2 text-right">
							<button type = "submit" class ="btn btn-primary">Search</button><br>
					</div>
				</div>
				</form>
			</div>
		</div> -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
					<!-- <th> ID</th> -->
                    <th>Product Name</th>
                    <th>MRP</th>
                    <th>Final Price</th>
                    <th>Free Delivery / Pickup</th>
                    <th>Mode</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			@if(count($product_prices) == 0)
			<tr><td colspan="8"  style="text-align:center;">No record found</td>
			@endif
			<?php foreach ($product_prices as $product_price) { ?>
			<tr>
                    <td>{{ $product_price->product->product_name}}</td>
                    <td>{{ number_format($product_price->mrp)}}</td>
                    <td> {{ number_format($product_price->final_price)}}</td>
                    <td>@if($product_price->free_delivery == 0)
                            Pickup Only
                        @elseif($product_price->free_delivery == '5')
                            5KM
                        @elseif($product_price->free_delivery == '7')
                            7KM
                        @elseif($product_price->free_delivery == '10')
                            10KM
                        @elseif($product_price->free_delivery == '15')
                            15KM
                        @elseif($product_price->free_delivery == '20')
                            20KM
                        @else
                            -
                        @endif
                    </td>
                    <td>@if($product_price->mode == 1)
                            Same day
                        @elseif($product_price->mode == 0)
                            Next Day
                        @else
                            -
                        @endif
                    </td>
				<td>
						<a href="{{route('product_price_edit',[$product_price->id,$vendor->id])}}"  title="Edit"><i class="m-menu__link-icon fa fa-edit"></i></a>  &nbsp;&nbsp;
						<a href="{{route('delete_price',[$product_price->id,$vendor->id])}}" onclick="return confirm('Are you sure you want to delete this price?');"  title="Delete"><i class="m-menu__link-icon fa fa-trash-alt"></i></a> &nbsp;&nbsp;
				</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
</div>
</div>
</div>
</div>
@endsection
