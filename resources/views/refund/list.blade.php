@extends('layouts.admin')
@section('content')
<style>
.totaldiv{
        font-size: 14px;
        font-weight: 600;
    }
</style>
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Refund List
					</h3>
				</div>
			</div>

	</div>
	<div class="m-portlet__body">

		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
                    <th>Order Id</th>
					<th>Suborder Id</th>
					<th>User</th>
					<th>Product</th>
					<th>Quantity</th>
					<th>Amount</td>
					<th>Action</th>


				</tr>
			</thead>
			<tbody>
			@if(count($refunds) == 0)
			<tr><td colspan="8"  style="text-align:center;">No record found</td>
			@endif
            <?php  $total = 0;
                foreach ($refunds as $refund) { ?>
			<tr>
                <td><a href="{{route('order_detail',$refund->order->id)}}">{{$refund->order->order_id}}</a></td>
				<td>{{$refund->suborder_id}}</td>
				<td><a href="{{route('admin_info',$refund->user->id)}}">{{ $refund->user->name}}</a></td>
                <td><a href="{{route('admin_product_info',$refund->product->id)}}">{{ $refund->product->product_name}}</a></td>

				<td>{{$refund->quantity }} x ₹{{number_format($refund->price)}}</td>
				<td>₹{{ number_format($refund->amount) }}</td>
				<td><a href="{{route('refund_pay_view',[$refund->id])}}"  title="Pay"><i class="m-menu__link-icon fa fa-rupee-sign"></i></a></td>

			</tr>

            <?php  $total = $total + $refund->amount;
                } ?>
			</tbody>
            <tfoot>
                <tr class="totaldiv">
                    <td colspan="5" ><b>Total</b></td>
                    <td colspan="2"> <b>₹{{ number_format($total) }} </b></td>
                </tr>
            </tfoot>
		</table>
        {{ $refunds->links('paginator.default') }}
	</div>
</div>
</div>
</div>
</div>

@endsection
