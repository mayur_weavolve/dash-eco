@extends('layouts.admin')
@section('content')
<style>
    b{
        font-weight: bold !important;
    }
    .row{
        line-height: 30px;
    }
</style>
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						User Info
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item">

			</ul>
	    </div>
	</div>
	<div class="m-portlet__body">
            <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div style="display:inline"><b>Name : </b></div>&nbsp;
                            <div style="display:inline">{{$info->name}}</div>
                        </div>
                        <div class="row">
                            <div style="display:inline"><b>Email :</b> </div>&nbsp;
                            <div style="display:inline">{{$info->email}}</div>
                        </div>
                        
                        <div class="row">
                            <div style="display:inline"><b>Whatsapp No :</b> </div>&nbsp;
                            <div style="display:inline">{{$info->whatsapp_no}}</div>
                        </div>
        
                        <div class="row">
                            <div style="display:inline"><b>Mobile No :</b> </div>&nbsp;
                            <div style="display:inline">{{$info->mobileno}}</div>
                        </div>
        
                        
                        
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div style="display:inline"><b>Address Line1: </b></div>&nbsp;
                            <div style="display:inline">{{$info->address->addressline1}}</div>
                        </div>
        
                        <div class="row">
                            <div style="display:inline"><b>Address Line2: </b></div>&nbsp;
                            <div style="display:inline">{{$info->address->addressline2}}</div>
                        </div>
        
                        <div class="row">
                            <div style="display:inline"><b>City :</b> </div>&nbsp;
                            <div style="display:inline">{{$info->address->city}}</div>
                        </div>
                        
                        <div class="row">
                            <div style="display:inline"><b>State : </b></div>&nbsp;
                            <div style="display:inline">{{$info->address->state}} , {{$info->address->pincode}}</div>
                        </div>
                        
                        
                    </div>        
            </div>
    </div>
</div>
</div>
</div>
</div>

@endsection
