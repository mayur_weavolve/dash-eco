@extends('layouts.admin')
@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Product List
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">

				<ul class="m-portlet__nav">
					<li class="m-portlet__nav-item">
							<a href="{{ route('add_product') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
								<span>
									<i class="la la-plus"></i>
									<span>Add Product</span>
								</span>
							</a>
					</li>
				</ul>
			</div>
	</div>
	<div class="m-portlet__body">
			<div class = "row">

				<div class = "col-md-10">
					<form action="{{route('product')}}" method ="get">
					<input type="search" name="search"class="form-control" placeholder="Search Product Name, Code, Manufacturer and Manufacturer Code"><br>
				</div>
				<div class="col-md-2 text-right">
					<div class="row">
						<div class="col-md-6">
							<button type = "submit" class ="btn btn-primary">Search</button><br>
						</div>
						<div class="col-md-6">
								<a href="{{route('product')}}" class="btn btn-primary">Clear</a>
						</div>
					</div>
					</form>
				</div>


			</div>
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
					<!-- <th> ID</th> -->
                    <th>Product Code</th>
                    <th>Name</th>
					<th>Manufacturer</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			@if(count($products) == 0)
			<tr><td colspan="8"  style="text-align:center;">No record found</td>
			@endif
			<?php foreach ($products as $product) { ?>
			<tr>
				<!-- <td> <?php echo $product->id ;?> </td> -->
                <td>{{ $product->product_code}}</td>
                <td><a target="_blank" href="{{route('backend_product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$product->product_name)), 'id' => $product->id])}}">{{ $product->product_name}}</a></td>
				<td>{{ $product->manufacturer}}</td>
				<td style="text-align: center;">@if($product->status == 'Pending') <span class="m-badge  m-badge--primary m-badge--wide">Pending</span> @elseif($product->status == 'InActive')<span class="m-badge  m-badge--danger m-badge--wide">InActive</span> @else<span class="m-badge  m-badge--success m-badge--wide">Active</span> @endif</td>
				<td>
					<a href="{{route('product_edit',[$product->id])}}" title="Edit"><i class="m-menu__link-icon fa fa-edit"></i></a>  &nbsp;&nbsp;
					<a href="{{route('product_delete',[$product->id])}}" onclick="return confirm('Are you sure you want to delete this Product?');"  title="Delete"><i class="m-menu__link-icon fa fa-trash-alt"></i></a> &nbsp;&nbsp;
				</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
        {{ $products->links('paginator.default') }}
	</div>
</div>
</div>
</div>
</div>

@endsection
