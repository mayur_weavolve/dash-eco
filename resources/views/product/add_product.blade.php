@extends('layouts.admin')

@section('content')

<style>
.subbox {
    margin: auto;
    display: inline-block;
    padding: 0px 10px 5px 10px;
    font-size: 15px;
}
</style>
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Add New Product
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			    <ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('product') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-reply"></i>
						    <span>Back</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item"></li>
			    </ul>
	        </div>
	    </div>

	<form class="m-form m-form--label-align-left- m-form--state-" id="m_form" enctype="multipart/form-data" method="post" action="{{route('add_product')}}">
	{{csrf_field() }}
		<div class="m-portlet__body">
			<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
				<div class="row">
					<div class="col-xl-10 offset-xl-1">
						<div class="m-form__section">
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Manufacturer:</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="text" name="manufacturer" id="manufacturer" class="form-control m-input" placeholder="" value="{{Request::old('manufacturer')}}" >
                                        @error('manufacturer')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('manufacturer') }}</strong>
                                        </span>
                                        @enderror
                                </div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Manufacturer Code:</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="text" name="manufacturer_code" id="manufacturer_code" class="form-control m-input" placeholder="" value="{{Request::old('manufacturer_code')}}" >
                                        @error('manufacturer_code')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('manufacturer_code') }}</strong>
                                        </span>
                                        @enderror
                                </div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Product Code:</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="text" name="product_code" id="product_code" class="form-control m-input" placeholder="" value="{{Request::old('product_code')}}" required>
                                        @error('product_code')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('product_code') }}</strong>
                                        </span>
                                        @enderror
                                </div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Name:</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="text" name="product_name" id="product_name" class="form-control m-input" placeholder="" value="{{Request::old('product_name')}}" required>
                                        @error('product_name')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('product_name') }}</strong>
                                        </span>
                                        @enderror
                                </div>
							</div>
                            <div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Images:</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="file" name="product_photo[]" id="product_photo" class="form-control m-input" placeholder="" value="{{Request::old('product_photo')}}" multiple>
                                    <span># Select multiple images at one time</span>
                                </div>
							</div>

							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Description:</label>
									<div class="col-xl-8 col-lg-8">
                                    <textarea type="text" name="description" id="description" class="form-control m-input description" placeholder="" value="{{Request::old('description')}}" required></textarea>
											@error('description')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('description') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Specification:</label>
								<div class="col-xl-8 col-lg-8">
								<textarea type="text" name="specification" id="specification" class="form-control m-input specification" placeholder="" value="{{Request::old('specification')}}" ></textarea>
										@error('specification')
										<span class="help-block" role="alert">
											<strong>{{ $errors->first('specification') }}</strong>
										</span>
										@enderror
								</div>
							</div>
                            <div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Hashtags:</label>
									<div class="col-xl-8 col-lg-8">
										<select name="hashtags[]" id="hashtags" class="form-control m-input hashtags" placeholder="{{Request::old('hashtags')}}"  required multiple>
                                                @foreach($hashtags as $hashtag)
                                                    <option value="{{$hashtag->hashtag}}">{{$hashtag->hashtag}}</option>
                                                @endforeach
                                        </select>
                                            @error('hashtags')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('hashtags') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Manufacturer Product video:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="product_video" id="product_video" class="form-control m-input" placeholder="" value="" >
											@error('product_video')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('product_video') }}</strong>
                        					</span>
                    						@enderror
									</div>
                            </div>
                            <div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Meta Title:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="meta_title" id="meta_title" class="form-control m-input" placeholder="" value="" >
											@error('meta_title')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('meta_title') }}</strong>
                        					</span>
                    						@enderror
									</div>
                            </div>
                            <div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Meta Description:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="meta_description" id="meta_description" class="form-control m-input" placeholder="" value="" >
											@error('meta_description')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('meta_description') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
                            <div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Subcategory:</label>
								<div class="col-xl-8 col-lg-8">
                                    <table class="table">
                                        <tr>
                                            <td><b>Category</b></td>
                                            <td><b>Subcategory</b></td>
                                        </tr>
                                        @foreach($categories as $category)
                                        <tr>
                                            <td>{{ $category->name }}</td>
                                            <td>
                                                @foreach($category->subcategory as $cat)
                                                <div class="subbox">
                                                    <input type="checkbox" name="category[]" value="{{ $cat->id }}">  {{ $cat->name }}
                                                </div>
                                                @endforeach
                                            </td>
                                        </tr>
                                        @endforeach
                                    </table>
									@error('category')
                       					<span class="help-block" role="alert">
                           					<strong>{{ $errors->first('category') }}</strong>
                        				</span>
                    				@enderror
								</div>
							</div>
                            <div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Attributes:</label>
								<div class="col-xl-8 col-lg-8">
                                    <table class="table">
                                        <tr>
                                            <td><b>Name</b></td>
                                            <td><b>Value</b></td>
                                        </tr>
                                        <tr>
                                            <td>Colour Option</td>
                                            <td>
                                                <div class="subbox">
                                                    <input type="checkbox" name="colour" value="1">
                                                    <label>Colour Picker</label>
												</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Weight (Unit)</td>
                                            <td>
                                                <div class="subbox">
                                                    <input type="checkbox" name="weight[]" value="kg"> Kg
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="weight[]" value="tonn"> Tonn
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="weight[]" value="oz"> OZ
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="weight[]" value="l"> L
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Measurement (Unit)</td>
                                            <td>
                                                <div class="subbox">
                                                    <input type="checkbox" name="measurement[]" value="cm"> CM
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="measurement[]" value="inch"> Inch
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="measurement[]" value="m"> M
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="measurement[]" value="ft"> Ft
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="measurement[]" value="sq/ft"> Sq/Ft
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="measurement[]" value="sq/m"> Sq/M
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Size (Unit)</td>
                                            <td>
                                                <div class="subbox">
                                                    <input type="checkbox" name="size[]" value="xs"> XS
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="size[]" value="s"> S
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="size[]" value="m"> M
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="size[]" value="l"> L
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="size[]" value="xl"> XL
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="size[]" value="xxl"> XXL
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Storage</td>
                                            <td>
                                                <div class="subbox">
                                                    <input type="checkbox" name="storage[]" value="gb"> GB
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="storage[]" value="tb"> TB
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
								</div>
							</div>
							<div class="col-lg-8 m--align-right">
								<button class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit" >
									<span>
										<i class="la la-check"></i>&nbsp;&nbsp;
											<span>Submit</span>
									</span>
								</button>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
</div>
</div>
</div>
<script>
    $('.hashtags').tokenize2({delimiter:','});
    $('textarea.description').ckeditor();
    $('textarea.specification').ckeditor();
</script>
@endsection
