@extends('layouts.admin')

@section('content')

<style>
.subbox {
    margin: auto;
    display: inline-block;
    padding: 0px 10px 5px 10px;
    font-size: 15px;
}
.image-area img{
    width: 90px;
    height: 90px;
    margin: 10px 0px 0px 0px;
    text-align: center;
}
.image-block{
    margin:auto;
    display: flex;
}
.image-area {
    width: 30%;
}
</style>
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Edit Product
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			    <ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('product') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-reply"></i>
						    <span>Back</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item"></li>
			    </ul>
	        </div>
	    </div>



	<form class="m-form m-form--label-align-left- m-form--state-" id="m_form" enctype="multipart/form-data" method="post" action="">
	{{csrf_field() }}
		<div class="m-portlet__body">
			<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
				<div class="row">
					<div class="col-xl-10 offset-xl-1">
						<div class="m-form__section">
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Manufacturer:</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="text" name="manufacturer" id="manufacturer" class="form-control m-input" placeholder="" value="{{$edit->manufacturer}}" >
                                        @error('manufacturer')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('manufacturer') }}</strong>
                                        </span>
                                        @enderror
                                </div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Manufacturer Code:</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="text" name="manufacturer_code" id="manufacturer_code" class="form-control m-input" placeholder="" value="{{$edit->manufacturer_code}}" >
                                        @error('manufacturer_code')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('manufacturer_code') }}</strong>
                                        </span>
                                        @enderror
                                </div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Product Code:</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="text" name="product_code" id="product_code" class="form-control m-input" placeholder="" value="{{$edit->product_code}}" required>
                                        @error('product_code')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('product_code') }}</strong>
                                        </span>
                                        @enderror
                                </div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Name:</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="text" name="product_name" id="product_name" class="form-control m-input" placeholder="" value="{{$edit->product_name}}" required>
                                        @error('product_name')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('product_name') }}</strong>
                                        </span>
                                        @enderror
                                </div>
							</div>
                            <div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Images:</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="file" name="product_photo[]" id="product_photo" class="form-control m-input" placeholder="" value="{{$edit->product_photo}}" multiple>
                                    <span># Select multiple images at one time</span>
                                    <div class="image-block">
                                    @foreach($productImages as $image)
                                        <div class="image-area">
                                            <img src="<?php echo URL::to('/'); ?>/product_photo/{{$image->name}}" >
                                        </div>
                                    @endforeach
                                    </div>
                                </div>
							</div>

							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Description:</label>
									<div class="col-xl-8 col-lg-8">
                                    <textarea type="text" name="description" id="description" class="form-control m-input description" placeholder=""  required>{{$edit->description}}</textarea>
											@error('description')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('description') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Specification:</label>
								<div class="col-xl-8 col-lg-8">
								<textarea type="text" name="specification" id="specification" class="form-control m-input isset specification" placeholder=""  >{{$edit->specification}}</textarea>
										@error('specification')
										<span class="help-block" role="alert">
											<strong>{{ $errors->first('specification') }}</strong>
										</span>
										@enderror
								</div>
							</div>
                            <div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Hashtags:</label>
									<div class="col-xl-8 col-lg-8">
										<select name="hashtags[]" id="hashtags" class="form-control m-input hashtags" placeholder=""  required multiple>
                                                @foreach($hashtags as $hashtag)
                                                    <option value="{{$hashtag->hashtag}}">{{$hashtag->hashtag}}</option>
                                                @endforeach
                                        </select>
                                            @error('hashtags')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('hashtags') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>

							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Manufacturer Product video:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="product_video" id="product_video" class="form-control m-input" placeholder="" value="{{$edit->product_video}}" >
											@error('product_video')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('product_video') }}</strong>
                        					</span>
                    						@enderror
									</div>
                            </div>
                            <div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Meta Title:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="meta_title" id="meta_title" class="form-control m-input" placeholder="" value="{{$edit->meta_title}}" >
											@error('meta_title')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('meta_title') }}</strong>
                        					</span>
                    						@enderror
									</div>
                            </div>
                            <div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Meta Description:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="meta_description" id="meta_description" class="form-control m-input" placeholder="" value="{{$edit->meta_description}}" >
											@error('meta_description')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('meta_description') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
                            <div class="form-group m-form__group row">
                                    <label for="name" class="col-xl-3 col-lg-3 col-form-label">* Status:</label>
                                    <div class="col-xl-8 col-lg-8">
                                        <select name="status" class="form-control m-input" value = "" required>
                                            <option value="Pending" @if($edit->status == 'Pending') selected @endif>Pending</option>
                                            <option value="Active" @if($edit->status == 'Active') selected @endif>Active</option>
                                            <option value="InActive" @if($edit->status == 'InActive') selected @endif >InActive</option>
                                            
                                        </select>
                                        @error('status')
                                            <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('status') }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                            </div>
                            <div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Subcategory:</label>
								<div class="col-xl-8 col-lg-8">
                                    <table class="table">
                                        <tr>
                                            <td><b>Category</b></td>
                                            <td><b>Subcategory</b></td>
                                        </tr>
                                        @foreach($categories as $category)
                                        <tr>
                                            <td>{{ $category->name }}</td>
                                            <td>
                                                @foreach($category->subcategory as $cat)
                                                <div class="subbox">
                                                    <input type="checkbox" name="category[]" value="{{ $cat->id }}" @if(in_array($cat->id,$productSubcategory)) checked @endif>  {{ $cat->name }}
                                                </div>
                                                @endforeach
                                            </td>
                                        </tr>
                                        @endforeach
                                    </table>
									@error('category')
                       					<span class="help-block" role="alert">
                           					<strong>{{ $errors->first('category') }}</strong>
                        				</span>
                    				@enderror
								</div>
							</div>
                            <div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Attributes:</label>
								<div class="col-xl-8 col-lg-8">
                                    <table class="table">
                                        <tr>
                                            <td><b>Name</b></td>
                                            <td><b>Value</b></td>
                                        </tr>
                                        <tr>
                                            <td>Colour Option</td>
                                            <td>
                                                <div class="subbox">
                                                    <input type="checkbox" name="colour" value="1" @if(isset($productAttributes['colour']) &&  $productAttributes['colour'][0] === '1') checked @endif>
                                                    <label>Colour Picker </label>
												</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Weight (Unit)</td>
                                            <td>
                                                <div class="subbox">
                                                    <input type="checkbox" name="weight[]" value="kg"  @if(isset($productAttributes['weight']) &&  in_array('kg',$productAttributes['weight'])) checked @endif> Kg
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="weight[]" value="tonn" @if(isset($productAttributes['weight']) &&  in_array('tonn',$productAttributes['weight'])) checked @endif > Tonn
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="weight[]" value="oz" @if(isset($productAttributes['weight']) &&  in_array('oz',$productAttributes['weight'])) checked @endif > OZ
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="weight[]" value="l" @if(isset($productAttributes['weight']) && in_array('l',$productAttributes['weight'])) checked @endif > L
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Measurement (Unit)</td>
                                            <td>
                                                <div class="subbox">
                                                    <input type="checkbox" name="measurement[]" value="cm" @if(isset($productAttributes['measurement']) && in_array('cm',$productAttributes['measurement'])) checked @endif > CM
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="measurement[]" value="inch" @if(isset($productAttributes['measurement']) && in_array('inch',$productAttributes['measurement'])) checked @endif > Inch
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="measurement[]" value="m" @if(isset($productAttributes['measurement']) && in_array('m',$productAttributes['measurement'])) checked @endif > M
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="measurement[]" value="ft" @if( isset($productAttributes['measurement']) && in_array('ft',$productAttributes['measurement'])) checked @endif> Ft
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="measurement[]" value="sq/ft" @if(isset($productAttributes['measurement']) && in_array('sq/ft',$productAttributes['measurement'])) checked @endif> Sq/Ft
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="measurement[]" value="sq/m" @if(isset($productAttributes['measurement']) && in_array('sq/m',$productAttributes['measurement'])) checked @endif> Sq/M
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Size (Unit)</td>
                                            <td>
                                                <div class="subbox">
                                                    <input type="checkbox" name="size[]" value="xs"  @if( isset($productAttributes['size']) && in_array('xs',$productAttributes['size'])) checked @endif> XS
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="size[]" value="s" @if(isset($productAttributes['size']) && in_array('s',$productAttributes['size'])) checked @endif> S
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="size[]" value="m" @if(isset($productAttributes['size']) && in_array('tomnn',$productAttributes['size'])) checked @endif> M
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="size[]" value="l" @if(isset($productAttributes['size']) && in_array('l',$productAttributes['size'])) checked @endif> L
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="size[]" value="xl" @if( isset($productAttributes['size']) && in_array('xl',$productAttributes['size'])) checked @endif> XL
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="size[]" value="xxl" @if( isset($productAttributes['size']) && in_array('xxl',$productAttributes['size'])) checked @endif> XXL
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Storage</td>
                                            <td>
                                                <div class="subbox">
                                                    <input type="checkbox" name="storage[]" value="gb" @if(isset($productAttributes['storage']) && in_array('gb',$productAttributes['storage'])) checked @endif> GB
                                                </div>
                                                <div class="subbox">
                                                    <input type="checkbox" name="storage[]" value="tb" @if(isset($productAttributes['storage']) && in_array('tb',$productAttributes['storage'])) checked @endif> TB
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
								</div>
							</div>
							<div class="col-lg-8 m--align-right">
								<button class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit" >
									<span>
										<i class="la la-check"></i>&nbsp;&nbsp;
											<span>Submit</span>
									</span>
								</button>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
</div>
</div>
</div>
<script>
$('.hashtags').tokenize2({delimiter:','});
    <?php foreach($productHashtags as $hashtag){ ?>
        $('.hashtags').tokenize2().trigger('tokenize:tokens:add', ['<?php echo $hashtag->hashtag; ?>', '<?php echo $hashtag->hashtag; ?>', true]);
    <?php } ?>
$('textarea.description').ckeditor();
$('textarea.specification').ckeditor();
</script>
@endsection
