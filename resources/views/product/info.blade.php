@extends('layouts.admin')
@section('content')
<style>
    b{
        font-weight: bold !important;
    }
    .row{
        line-height: 30px;
    }
</style>
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Product Info
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item">

			</ul>
	    </div>
	</div>
	<div class="m-portlet__body">
                                <div class="row main">
                                <div class="col-md-6" style="padding-left: 30px;">
                                    <div class="row">
                                        <div style="display:inline"><b>Product Name : </b></div>&nbsp;
                                        <div style="display:inline">{{$info->product_name}}</div>
                                    </div>
                                    <div class="row">
                                        <div style="display:inline"><b>Product Code :</b> </div>&nbsp;
                                        <div style="display:inline">{{$info->product_code}}</div>
                                    </div>
                                    
                                    <div class="row">
                                        <div style="display:inline"><b>Manufacturer :</b> </div>&nbsp;
                                        <div style="display:inline">{{$info->manufacturer}}</div>
                                    </div>
                    
                                    
                        
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div style="display:inline"><b>Manufacturer Code :</b> </div>&nbsp;
                                        <div style="display:inline">{{$info->manufacturer_code}}</div>
                                    </div>
                                    
                                    <div class="row">
                                    
                                        <div style="display:inline"><b>Warranty : </b></div>&nbsp;
                                        <div style="display:inline">{{$info->productPrice[0]->warranty}}</div>
                                    </div>
                                    <div class="row">
                                        <div style="display:inline"><b>Final Price : </b></div>&nbsp;
                                        <div style="display:inline">₹ {{number_format($info->productPrice[0]->final_price)}}</div>
                                    </div>
                                </div>  
                                </div>
                                <div class="col-md-12">
                                        <div class="row">
                                                <div style="display:inline"><b>Description : </b></div>&nbsp;
                                                <div style="display:inline">{{$info->description}}</div>
                                            </div>
                                            
                                            <div class="row">
                                                <div style="display:inline"><b>Specification: </b></div>&nbsp;
                                                <div style="display:inline">{{$info->specification}}</div>
                                            </div>
                                        
                                </div>
                                
                        
                </div>
            </div>
        </div>
    </div>
</div>



@endsection
