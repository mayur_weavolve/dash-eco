<?php
    $star5 = 0; $star4 = 0; $star3 = 0;  $star2 = 0; $star1 = 0;
?>
@extends('layouts.product')

@section('content')


<div class="ps-breadcrumb">
        <div class="ps-container">
            <ul class="breadcrumb">
                <li><a href="<?php echo URL::to('/'); ?>">Admin</a></li>
                <li><a href="{{route('product_list', ['category' => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$products->productsubcategory->subcategory->category->name)), 'id' => $products->productsubcategory->subcategory->category->id])}}">{{$products->productsubcategory->subcategory->category->name}}</a></li>
                <li><a href="{{route('product_list_subcategory', ['category' => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$products->productsubcategory->subcategory->category->name)), 'subcategory' => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$products->productsubcategory->subcategory->name)), 'id' => $products->productsubcategory->subcategory->id])}}">{{$products->productsubcategory->subcategory->name}}</a></li>
                <li>{{ $products->product_name }} </li>
            </ul>
        </div>
    </div>
    <div class="ps-page--product">
        <div class="ps-container">
            <div class="ps-page__container">
                <div class="ps-page__left">
                    <div class="ps-product--detail ps-product--fullwidth">
                        <div class="ps-product__header">
                            <div class="ps-product__thumbnail" data-vertical="true">
                                <figure>
                                    <div class="ps-wrapper">

                                        <div class="ps-product__gallery" data-arrow="true">
                                            @foreach($products->productImages as $image)
                                            <div class="item"><a href="<?php echo URL::to('/'); ?>/product_photo/{{ $image->name }}"><img src="<?php echo URL::to('/'); ?>/product_photo/{{$image->name}}" alt=""></a></div>
                                            @endforeach
                                        </div>
                                    </div>
                                </figure>
                                <div class="ps-product__variants" data-item="4" data-md="4" data-sm="4" data-arrow="false">
                                    @foreach($products->productImages as $image)
                                        <div class="item"><img src="<?php echo URL::to('/'); ?>/product_photo/{{$image->name}}" alt=""></a></div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="ps-product__info">
                                <h1>{{$products->product_name}}</h1>
                                <div class="ps-product__meta">
                                    <?php $vPrice = $productPrice->vendor;   ?>
                                    <p>Brand: <a href="javascript:void(0);">{{$products->manufacturer}}</a></p><span class="km-div @if($productPrice->free_delivery == 0 || getDistance($vPrice) > $productPrice->free_delivery) pickup-span @endif">  @if($productPrice->free_delivery == 0)
                                                            Pickup Only
                                                        @else
                                                            @if(getDistance($vPrice) > $productPrice->free_delivery)
                                                                Pickup Only
                                                            @else
                                                                Delivery @if($productPrice->mode == 1)
                                                                    @if(date('H') < 14)
                                                                        today
                                                                    @else
                                                                        tomorrow
                                                                    @endif
                                                                @else
                                                                    tomorrow
                                                                @endif
                                                            @endif

                                                        @endif | {{getDistance($vPrice)}} km  away </span>
                                    <!-- <div class="ps-product__rating">
                                        <select class="ps-rating" data-read-only="true">
                                            <option value="1">1</option>
                                            <option value="1">2</option>
                                            <option value="1">3</option>
                                            <option value="1">4</option>
                                            <option value="2">5</option>
                                        </select><span>(1 review)</span>
                                    </div> -->
                                </div>
                                <h4 class="ps-product__price sale">₹ {{number_format($productPrice->final_price)}} <del>₹ {{number_format($productPrice->mrp)}}</del> <small>-{{$productPrice->discount}}%</small></h4>
                                <div class="row product-function-grid">
                                    <div class="col-md-3">
                                        <img src="/user/img/products/no_contact.svg">
                                        <p>No Contact  <br> Delivery/Pickup</p>
                                    </div>
                                    <div class="col-md-3">
                                        <img src="/user/img/products/warranty.svg">
                                        @if($productPrice->warranty == 0)
                                         <p>No <br> Warranty</p>
                                        @else
                                            <p>{{ $productPrice->warranty}} Month  <br> Warranty</p>
                                        @endif

                                    </div>
                                    <div class="col-md-3">
                                        <img src="/user/img/products/replacement.svg">
                                        <p>@if($productPrice->exchange == 'na') No Replacement @else {{ $productPrice->exchange}} Days  <br> Replacement @endif</p>
                                    </div>
                                    @if($productPrice->installation_included != 'na')
                                    <div class="col-md-3">
                                        <img src="/user/img/products/installation.svg">
                                        <p> @if($productPrice->installation_included == 'yes')Installation <br> Included @elseif($productPrice->installation_included == 'no')  No <br> Installation @else  @endif</a>
                                    </div>
                                    @endif
                                </div>
                                <div class="ps-product__desc">
                                    <p>Sold By:<a href="javascript:void(0);" class="mr-20"><strong> {{ $productPrice->vendor['nickname']}}</strong></a> Status:<a href="javascript:void(0);"><strong class="ps-tag--in-stock"> In Stock</strong></p>
                                    <div class="ps-product__variations">
                                            @if(sizeof($productAttributeValue['colour']) > 0)
                                            <figure>
                                                <figcaption>Color</figcaption>
                                                @foreach($productAttributeValue['colour'] as $productAttr)
                                                    <div class="ps-variant ps-variant--color colour" data-value="{{ $productAttr->colour }}" style="background-color: #{{ $productAttr->colour }};border: 1px solid  #{{ $productAttr->colour }};"></div>
                                                @endforeach
                                            </figure>
                                            @endif
                                            @if(sizeof($productAttributeValue['size']) > 0)
                                            <figure>
                                                <figcaption>Size <strong> Choose an option</strong></figcaption>
                                                @foreach($productAttributeValue['size'] as $productAttr)
                                                <div class="ps-variant ps-variant--size size" data-value="{{ $productAttr->size }}" ><span class="ps-variant__tooltip">{{ $productAttr->size }}</span><span class="ps-variant__size">{{ $productAttr->size }}</span></div>
                                                @endforeach
                                            </figure>
                                            @endif
                                            @if(sizeof($productAttributeValue['weight']) > 0)
                                            <figure>
                                                <figcaption>Weight <strong> Choose an option</strong></figcaption>
                                                @foreach($productAttributeValue['weight'] as $productAttr)
                                                    <div class="ps-variant ps-variant--size weight" data-value="{{ $productAttr->weight }}" ><span class="ps-variant__tooltip">{{ $productAttr->weight }}</span><span class="ps-variant__size">{{ $productAttr->weight }}</span></div>
                                                @endforeach
                                            </figure>
                                            @endif
                                            @if(sizeof($productAttributeValue['measurement']) > 0)
                                            <figure>
                                                <figcaption>Measurement <strong> Choose an option</strong></figcaption>
                                                @foreach($productAttributeValue['measurement'] as $productAttr)
                                                    <div class="ps-variant ps-variant--size measurement" data-value="{{ $productAttr->measurements }}" ><span class="ps-variant__tooltip">{{ $productAttr->measurements }}</span><span class="ps-variant__size">{{ $productAttr->measurement }}</span></div>
                                                @endforeach
                                            </figure>
                                            @endif
                                            @if(sizeof($productAttributeValue['storage']) > 0)
                                            <figure>
                                                <figcaption>Storage <strong> Choose an option</strong></figcaption>
                                                @foreach($productAttributeValue['storage'] as $productAttr)
                                                    <div class="ps-variant ps-variant--size storage" data-value="{{ $productAttr->storage }}"><span class="ps-variant__tooltip">{{ $productAttr->storage }}</span><span class="ps-variant__size">{{ $productAttr->storage }}</span></div>
                                                @endforeach
                                            </figure>
                                            @endif
                                        </div>
                                </div>
                                <div class="attrbibute-error"></div>
                                <div class="ps-product__shopping">
                                    <figure>
                                        <figcaption>Quantity</figcaption>
                                        <div class="form-group--number">
                                            <button class="up qty-plus"><i class="fa fa-plus"></i></button>
                                            <button class="down qty-minus"><i class="fa fa-minus"></i></button>
                                            <input class="form-control quantity" type="text" name="quantity" id="quantity" value="1" placeholder="1">
                                        </div>
                                    </figure>
                                    <input type="hidden" name="productAttributeId" id="productAttributeId" value="">
                                    <a class="ps-btn ps-btn--black addToCart" href="javascript:void(0);" id="addToCart"  data-name="cart">Add to cart</a>
                                    <a class="ps-btn addToCart" href="javascript:void(0);" id="addToCart"  data-name="checkout" >Buy Now</a>
                                    <!-- <div class="ps-product__actions"><a href="javascript:void(0);"><i class="icon-heart"></i></a><a href="javascript:void(0);"><i class="icon-chart-bars"></i></a></div> -->
                                    </div>

                                <div class="ps-product__specification">
                                    <span class="km-button @if($productPrice->free_delivery == 0 || getDistance($productPrice->vendor) > $productPrice->free_delivery) pickup-span @endif">  @if($productPrice->free_delivery == 0)
                                                            Pickup Only
                                                        @else
                                                            @if(getDistance($vPrice) > $productPrice->free_delivery)
                                                                Pickup Only
                                                            @else
                                                                Delivery @if($productPrice->mode == 1)
                                                                    @if(date('H') < 14)
                                                                        today
                                                                    @else
                                                                        tomorrow
                                                                    @endif
                                                                @else
                                                                    tomorrow
                                                                @endif
                                                            @endif

                                                        @endif | {{getDistance($vPrice)}} km  away </span>
                                    <br> <br>
                                    <a class="report" href="{{ route('contactUs') }}">Report Abuse</a>
                                    @if($products->product_code != null)<p><strong>SKU:</strong> {{ $products->product_code  }}</p>@endif
                                    <p class="categories"><strong> Categories:</strong>
                                        <a href="{{route('product_list', ['category' => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$products->productsubcategory->subcategory->category->name)), 'id' => $products->productsubcategory->subcategory->category->id])}}">{{ $products->productsubcategory->subcategory->category->name }}</a>,
                                        @foreach($products->productSubcategories as $subcategory)
                                            <a href="{{route('product_list_subcategory', ['category' => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$products->productsubcategory->subcategory->category->name)), 'subcategory' => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$subcategory->subcategory->name)), 'id' =>$subcategory->subcategory->id])}}">{{ $subcategory->subcategory->name }}</a>
                                        @endforeach
                                    </p>
                                    <p class="tags"><strong> Tags</strong>
                                        @foreach($products->productHashtags as $hashtag)
                                            <a href="javascript:void(0);">{{ $hashtag->hashtag }}</a>,
                                        @endforeach
                                    </p>
                                </div>
                                <div class="ps-product__sharing">
                                        <!--  onclick="window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href),'facebook-share-dialog', 'width=626,height=436'); return false;"  -->
                                    <a class="facebook" target="_blank" href="http://www.facebook.com/sharer.php?u={{ url()->current() }}" ><i class="fa fa-facebook"></i></a>
                                    <a class="twitter" target="_blank" href="https://twitter.com/intent/tweet?url={{ url()->current() }}"><i class="fa fa-twitter"></i></a>
                                    <!-- <a class="google" target="_blank" href="javascript:void(0);"><i class="fa fa-google-plus"></i></a> -->
                                    <a class="linkedin" target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url={{ url()->current() }}&title={{ urlencode($products->product_name) }}&source=Dashshop"><i class="fa fa-linkedin"></i></a>
                                    <!-- <a class="instagram" target="_blank" href="https://www.instagram.com/?url={{ url()->current() }}"><i class="fa fa-instagram"></i></a> -->
                                    <a class="whatsapp" target="_blank" href="https://api.whatsapp.com/send?text={{ url()->current() }}"><i class="fa fa-whatsapp"></i></a>
                                </div>
                            </div>
                        </div>
                        @if(sizeof($productPrices) > 0)
                        <h4 class="other-sellers" >Compare with Other Sellers</h4>
                        <div class="ps-product__groupped ps-product__header">


                                <!-- <table class="table ps-table--product-groupped"> -->
                                    <!-- <tbody> -->
                                    <div class="row vendor-products">
                                        <?php $p=0; ?>
                                    @foreach($productPrices as $productPrice)
                                        @if($productAttributeValue['vendorId'] != $productPrice->vendor['id'])
                                        <?php $pURL = $products->id.'_'.$productPrice->vendor['id']; ?>
                                        <!-- <div> -->
                                            <div class="col-md-4 col-sm-12 @if($p != 0) vendor-item @endif" ><a class="ps-btn vendor-button" href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$products->product_name)), 'id' =>  $pURL ])}}">
                                               <?php $p++; ?>
                                                @if($productPrice->free_delivery == 0)
                                                    Pickup Only
                                                @else
                                                    Delivery @if($productPrice->mode == 1)
                                                        @if(date('H') < 14)
                                                            today
                                                        @else
                                                            tomorrow
                                                        @endif
                                                    @else
                                                        tomorrow
                                                    @endif
                                                @endif <br>
                                                {{getDistance($productPrice->vendor)}} km  away
                                            </a>
                                            </div>
                                            <div  class="col-md-4 col-sm-12" ><a class="title" href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$products->product_name)), 'id' =>  $pURL ])}}">
                                                    {{ $products->product_name }}
                                                </a>
                                            </div>
                                            <div class="col-md-4 col-sm-12">
                                                <p class="seller-price">₹ {{number_format($productPrice->final_price)}} </p>
                                                <p><b>{{ $productPrice->vendor['nickname']}} </b>:  @if($products->getQuantity($products,$productPrice->vendor['id']) == 0) <span class="ps-tag--out-stock"> Out of stock</span> @else  <span class="ps-tag--in-stock"> In stock</span>  @endif</p>
                                            </div>

                                        <!-- </tr> -->
                                        @endif
                                    @endforeach
                                    <!-- </tbody> -->
                                <!-- </table> -->
                                </div>
                        </div>
                        @endif
                        <div class="ps-product__content ps-tab-root">
                            <ul class="ps-tab-list">
                                <li class="active"><a href="#tab-1">Description</a></li>
                                @if($products->specification != null)
                                    <li><a href="#tab-2">Specification</a></li>
                                @endif
                                <!-- <li><a href="#tab-3">Vendor</a></li> -->
                                <li><a href="#tab-4">Reviews</a></li>
                                <!-- <li><a href="#tab-5">Questions and Answers</a></li>
                                <li><a href="#tab-6">More Offers</a></li> -->
                            </ul>
                            <div class="ps-tabs">
                                <div class="ps-tab active" id="tab-1">
                                    <div class="ps-document">
                                        {!! $products->description !!}
                                        @if($products->product_video != '' &&  $products->product_video != NULL &&  filter_var( $products->product_video, FILTER_VALIDATE_URL))
                                            <iframe width="700" height="400" src="{{ $products->product_video }}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        @endif
                                    </div>
                                </div>
                                @if($products->specification != null)
                                <div class="ps-tab" id="tab-2">
                                    {!! $products->specification !!}
                                </div>
                                @endif
                                <!-- <div class="ps-tab" id="tab-3">
                                    <h4>GoPro</h4>
                                    <p>Digiworld US, New York’s no.1 online retailer was established in May 2012 with the aim and vision to become the one-stop shop for retail in New York with implementation of best practices both online</p><a href="javascript:void(0);">More Products from gopro</a>
                                </div> -->

                                <div class="ps-tab" id="tab-4">

                                    <div class="row">
                                            @if(count($proReviews[0]->reviews) > 0 )
                                            <?php $Ctotal = count($proReviews[0]->reviews);
                                            $rate = 0;
                                                foreach($proReviews[0]->reviews as $review){
                                                   $rate = $rate + $review->rating ;
                                                }

                                                $finalRate = round($rate / $Ctotal) ;

                                            ?>
                                        <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12 ">
                                            <div class="ps-block--average-rating">
                                                <div class="ps-block__header">
                                                    <h3>{{$finalRate}}.00</h3>
                                                    <select class="ps-rating" data-read-only="true">
                                                         @for($i=1 ; $i<= $finalRate ; $i++)
                                                        <option value="1">{{$i}}</option>
                                                        @endfor
                                                    </select><span>{{$Ctotal}} Review</span>
                                                </div>


                                                <?php foreach($proReviews[0]->reviews as $review){

                                                 if($review->rating == 5){
                                                     $star5 = $star5 +1 ;
                                                 }
                                                 if($review->rating == 4){
                                                    $star4 = $star4 +1 ;
                                                 }
                                                 if($review->rating == 3){
                                                    $star3 = $star3 +1 ;
                                                }
                                                if($review->rating == 2){
                                                    $star2 = $star2 +1 ;
                                                }

                                                if($review->rating == 1){
                                                    $star1 = $star1 +1 ;
                                                }

                                                $per5 = $star5 / $Ctotal * 100;

                                                $per4 = $star4 / $Ctotal * 100;

                                                $per3 = $star3 / $Ctotal *100 ;
                                                $per2 = $star2 / $Ctotal *100;
                                                $per1 = $star1 / $Ctotal *100;
                                                }?>
                                                <div class="ps-block__star"><span>5 Star</span>
                                                    <div class="ps-progress" data-value="{{$per5}}"><span style="width: {{$star5 / $Ctotal * 100}}% !important;"></span></div><span>{{$per5}}%</span>
                                                </div>
                                                <div class="ps-block__star"><span>4 Star</span>
                                                    <div class="ps-progress" data-value="{{$per4}}"><span style="width: {{$per4}}%;"></span></div><span>{{$per4}}%</span>
                                                </div>
                                                <div class="ps-block__star"><span>3 Star </span>
                                                    <div class="ps-progress" data-value="{{$per3}}"><span style="width: {{$per3}}%;"></span></div><span>{{$per3}}%</span>
                                                </div>
                                                <div class="ps-block__star"><span>2 Star</span>
                                                    <div class="ps-progress" data-value="{{$per2}}"><span style="width: {{$per2}}%;"></span></div><span>{{$per2}}%</span>
                                                </div>
                                                <div class="ps-block__star"><span>1 Star</span>
                                                    <div class="ps-progress" data-value="{{$per1}}"><span style="width: {{$per1}}%;"></span></div><span>{{$per1}}%</span>
                                                </div>
                                            </div>
                                            <h5><span style="color:red;font-weight: 800;">{{ count($proReviews[0]->reviews)}}</span> Reviews</h5>
                                            <div class="row">
                                                @foreach($proReviews[0]->reviews as $review)

                                                    <div class="col-md-3">{{$review->name}}</div>
                                                    <div class="col-md-9" style="margin-bottom:5%;">

                                                        <div>
                                                            <select class="ps-rating" data-read-only="true"  readonly>
                                                                @for($i=1 ; $i<= $review->rating ; $i++)
                                                                <option value="{{$review->rating}}">{{$review->rating}}</option>
                                                                @endfor
                                                            </select>
                                                        </div>
                                                        <div>{{$review->review}}</div>
                                                    </div>

                                                @endforeach
                                            </div>
                                        </div>
                                        @else
                                        <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12 ">
                                                <div class="ps-block--average-rating">
                                                        <div class="ps-block__header">
                                                            <h3>0.00</h3>
                                                            <span> Review</span>
                                                        </div>



                                                        <div class="ps-block__star"><span>5 Star</span>
                                                            <div class="ps-progress" data-value="0"><span style="width: 100% !important;"></span></div><span>0%</span>
                                                        </div>
                                                        <div class="ps-block__star"><span>4 Star</span>
                                                            <div class="ps-progress" data-value="0"><span style="width: 100%;"></span></div><span>0%</span>
                                                        </div>
                                                        <div class="ps-block__star"><span>3 Star </span>
                                                            <div class="ps-progress" data-value="0"><span style="width: 100%"></span></div><span>0%</span>
                                                        </div>
                                                        <div class="ps-block__star"><span>2 Star</span>
                                                            <div class="ps-progress" data-value="0"><span style="width: 100%;"></span></div><span>0%</span>
                                                        </div>
                                                        <div class="ps-block__star"><span>1 Star</span>
                                                            <div class="ps-progress" data-value="0"><span style="width: 100%;"></span></div><span>0%</span>
                                                        </div>
                                                    </div>
                                        </div>
                                        @endif
                                        <div class="col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12 ">
                                            <form class="ps-form--review" action="#">
                                                <h4>Submit Your Review</h4>
                                                <p>Your email address will not be published. Required fields are marked<sup>*</sup></p>
                                                <div class="form-group form-group__rating">
                                                    <label>Your rating of this product</label>
                                                    <select class="ps-rating" data-read-only="false" name="rating" id="rating">
                                                        <option value="0">0</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="6" placeholder="Write your review here" name="review" id="review"></textarea>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12  ">
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" placeholder="Your Name" name="name" id="name">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12  ">
                                                        <div class="form-group">
                                                            <input class="form-control" type="number" placeholder="Your Mobile" name="mobile" id="mobile">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group submit">
                                                    <a class="ps-btn" id="addToReview">Submit Review</a>
                                                </div>
                                                <div class="success-msg"></div>
                                                <div class="loaders"><img src="/user/img/loading.gif"></div>
                                            </form>
                                        </div>
                                    </div>

                                </div>

                                <!-- <div class="ps-tab" id="tab-5">
                                    <div class="ps-block--questions-answers">
                                        <h3>Questions and Answers</h3>
                                        <div class="form-group">
                                            <input class="form-control" type="text" placeholder="Have a question? Search for answer?">
                                        </div>
                                    </div>
                                </div>
                                <div class="ps-tab active" id="tab-6">
                                    <p>Sorry no more offers available</p>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ps-page__right">
                    <aside class="widget widget_product widget_features">
                        <p><i class="icon-network"></i> Free Delivery or Pickup</p>
                        <p><i class="icon-map-marker-user"></i> Reach DASH Vendor within your city</p>
                        <p><i class="icon-receipt"></i> DASH Vendor Invoices</p>
                        <p><i class="icon-credit-card"></i> Pay online with UPI, Paytm and more</p>
                    </aside>
                    <aside class="widget widget_sell-on-site">
                        <p><i class="icon-store"></i> Sell on DASH?<a href="{{ route('become_a_vendor') }}"> Register Now !</a></p>
                    </aside>
                    <!-- <aside class="widget widget_ads"><a href="javascript:void(0);"><img src="img/ads/product-ads.png" alt=""></a></aside> -->
                    <aside class="widget widget_same-brand">
                        <h3>Same Brand</h3>
                        <div class="widget__content detail-page-box">
                            @foreach($productFromSameBrands as $productBrand)
                            <div class="ps-product">
                                <?php $pbURL= $productBrand->product_id.'_'.$productBrand->vendorID; ?>
                                <div class="ps-product__thumbnail"><a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$productBrand->product_name)), 'id' =>  $pbURL ])}}"><img src="<?php echo URL::to('/'); ?>/product_photo/{{ $productBrand->productImage}}" alt=""></a>
                                    <div class="ps-product__badge">-{{ $productBrand->discount }}%</div>
                                    <ul class="ps-product__actions list_prodouct_action width-300">
                                        <li><a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Read More"><i class="icon-bag2"></i></a></li>
                                        <li><a href="javascript:void(0);" data-placement="top" title="Quick View" class="product-quickview" data-value="{{$productBrand}}" ><i class="icon-eye"></i></a></li>
                                        <!-- <li><a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Add to Whishlist"><i class="icon-heart"></i></a></li>
                                        <li><a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Compare"><i class="icon-chart-bars"></i></a></li> -->
                                    </ul>
                                </div>
                                <div class="ps-product__container"><a class="ps-product__vendor" href="javascript:void(0);">{{ $productBrand->nickname }}</a>
                                    <div class="ps-product__content"><a class="ps-product__title" href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$productBrand->product_name)), 'id' =>  $pbURL ])}}">{{ $productBrand->product_name }}</a>
                                        <div class="ps-product__rating">
                                            <select class="ps-rating" data-read-only="true">
                                                <option value="1">1</option>
                                                <option value="1">2</option>
                                                <option value="1">3</option>
                                                <option value="1">4</option>
                                                <option value="2">5</option>
                                            </select><span>20</span>
                                        </div>
                                        <p class="ps-product__price sale">₹ {{number_format($productBrand->final_price)}} <del>₹ {{number_format($productBrand->mrp)}} </del></p>
                                    </div>
                                    <div class="ps-product__content hover"><a class="ps-product__title" href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$productBrand->product_name)), 'id' =>  $pbURL ])}}">{{ $productBrand->product_name }}</a>
                                        <p class="ps-product__price sale">₹ {{number_format($productBrand->final_price)}} <del>₹ {{number_format($productBrand->mrp)}} </del></p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </aside>
                </div>
            </div>
            <!-- <div class="ps-section--default ps-instagrams">
                <div class="ps-section__header">
                    <h3>See It Styled On Instagram</h3>
                </div>
                <div class="ps-section__content">
                    <div class="ps-carousel--nav owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="10" data-owl-nav="true" data-owl-dots="true" data-owl-item="7" data-owl-item-xs="3" data-owl-item-sm="4" data-owl-item-md="5" data-owl-item-lg="6" data-owl-duration="1000" data-owl-mousedrag="on"><a class="ps-block--instagram" href="javascript:void(0);"><img src="img/instagram/1.jpg" alt=""></a><a class="ps-block--instagram" href="javascript:void(0);"><img src="img/instagram/2.jpg" alt=""></a><a class="ps-block--instagram" href="javascript:void(0);"><img src="img/instagram/3.jpg" alt=""></a><a class="ps-block--instagram" href="javascript:void(0);"><img src="img/instagram/4.jpg" alt=""></a><a class="ps-block--instagram" href="javascript:void(0);"><img src="img/instagram/5.jpg" alt=""></a><a class="ps-block--instagram" href="javascript:void(0);"><img src="img/instagram/6.jpg" alt=""></a><a class="ps-block--instagram" href="javascript:void(0);"><img src="img/instagram/7.jpg" alt=""></a><a class="ps-block--instagram" href="javascript:void(0);"><img src="img/instagram/8.jpg" alt=""></a></div>
                </div>
            </div> -->
            <div class="ps-section--default ps-customer-bought">
                <div class="ps-section__header">
                    <h3>Customers who bought this item also bought</h3>
                </div>
                <div class="ps-section__content">
                    <div class="row">
                        @foreach($customerBroughtProducts as $customerProduct)
                        <?php $pCURL= $customerProduct->product_id.'_'.$customerProduct->vendorID; ?>
                        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6 detail-page-box">
                            <div class="ps-product">
                                <div class="ps-product__thumbnail"><a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$customerProduct->product_name)), 'id' =>  $pCURL ])}}"><img src="<?php echo URL::to('/'); ?>/product_photo/{{ $customerProduct->productImage }}" alt=""></a>
                                    <div class="ps-product__badge">-{{ $customerProduct->discount }}%</div>
                                    <ul class="ps-product__actions list_prodouct_action width-300">
                                        <li><a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Read More"><i class="icon-bag2"></i></a></li>
                                        <li><a href="javascript:void(0);" data-placement="top" title="Quick View" data-value="{{$customerProduct}}" class="product-quickview"><i class="icon-eye"></i></a></li>
                                        <!-- <li><a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Add to Whishlist"><i class="icon-heart"></i></a></li>
                                        <li><a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Compare"><i class="icon-chart-bars"></i></a></li> -->
                                    </ul>
                                </div>
                                <div class="ps-product__container"><a class="ps-product__vendor" href="javascript:void(0);">{{ $customerProduct->nickname }}</a>
                                    <div class="ps-product__content"><a class="ps-product__title" href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$customerProduct->product_name)), 'id' =>  $pCURL ])}}">{{ $customerProduct->product_name }}</a>
                                        <div class="ps-product__rating">
                                            <select class="ps-rating" data-read-only="true">
                                                <option value="1">1</option>
                                                <option value="1">2</option>
                                                <option value="1">3</option>
                                                <option value="1">4</option>
                                                <option value="2">5</option>
                                            </select><span>26</span>
                                        </div>
                                        <p class="ps-product__price sale">₹ {{number_format($customerProduct->final_price)}} <del>₹ {{number_format($customerProduct->mrp)}} </del></p>
                                    </div>
                                    <div class="ps-product__content hover"><a class="ps-product__title" href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$customerProduct->product_name)), 'id' =>  $pCURL ])}}">{{ $customerProduct->product_name }}</a>
                                        <p class="ps-product__price sale">₹ {{number_format($customerProduct->final_price)}} <del>₹ {{number_format($customerProduct->mrp)}} </del></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="ps-section--default">
                <div class="ps-section__header">
                    <h3>Related products</h3>
                </div>
                <div class="ps-section__content">
                    <div class="ps-carousel--nav owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="10000" data-owl-gap="30" data-owl-nav="true" data-owl-dots="true" data-owl-item="6" data-owl-item-xs="2" data-owl-item-sm="2" data-owl-item-md="3" data-owl-item-lg="4" data-owl-item-xl="5" data-owl-duration="1000" data-owl-mousedrag="on">
                        @foreach($productbysubcategory as $subactegoryProduct)
                        <?php $psURL= $subactegoryProduct->product_id.'_'.$subactegoryProduct->vendorID; ?>
                            <div class="ps-product detail-page-box">
                                <div class="ps-product__thumbnail"><a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$subactegoryProduct->product_name)), 'id' => $psURL])}}"><img src="<?php echo URL::to('/'); ?>/product_photo/{{$subactegoryProduct->productImage}}" alt=""></a>
                                    <ul class="ps-product__actions list_prodouct_action width-300">
                                        <li><a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$subactegoryProduct->product_name)), 'id' => $psURL])}}" data-toggle="tooltip" data-placement="top" title="Read More"><i class="icon-bag2"></i></a></li>
                                        <li><a href="javascript:void(0);" data-placement="top" title="Quick View" data-value="{{$subactegoryProduct}}" class="product-quickview"><i class="icon-eye"></i></a></li>
                                        <!-- <li><a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Add to Whishlist"><i class="icon-heart"></i></a></li>
                                        <li><a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Compare"><i class="icon-chart-bars"></i></a></li> -->
                                    </ul>
                                </div>
                                <div class="ps-product__container"><a class="ps-product__vendor" href="javascript:void(0);">@if($subactegoryProduct->nickname) {{ $subactegoryProduct->nickname }} @else Dash @endif</a>
                                    <div class="ps-product__content"><a class="ps-product__title" href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$subactegoryProduct->product_name)), 'id' => $psURL])}}">{{ $subactegoryProduct->product_name }}</a>
                                        <!-- <div class="ps-product__rating">
                                            <select class="ps-rating" data-read-only="true">
                                                <option value="1">1</option>
                                                <option value="1">2</option>
                                                <option value="1">3</option>
                                                <option value="1">4</option>
                                                <option value="2">5</option>
                                            </select><span>01</span>
                                        </div> -->
                                        <p class="ps-product__price">₹ {{number_format($subactegoryProduct->final_price)}}</p>
                                    </div>
                                    <div class="ps-product__content hover"><a class="ps-product__title" href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$subactegoryProduct->product_name)), 'id' => $psURL])}}">{{ $subactegoryProduct->product_name }}</a>
                                        <p class="ps-product__price">₹ {{number_format($subactegoryProduct->final_price)}}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
            var selectVendorId = 0;
            var selectedsize = null;
            var selectedcolour = null;
            var selectedweight = null;
            var selectedmeasurement = null;
            var selectedstorage = null;
            var productAttributeValue = '<?php echo $productAttValueArray; ?>';
            productAttributeValue = JSON.parse(productAttributeValue);
            console.log(productAttributeValue);
            $('div[data-value="'+productAttributeValue[0].size+'"]').addClass('select');
            $('div[data-value="'+productAttributeValue[0].colour+'"]').addClass('select');
            $('div[data-value="'+productAttributeValue[0].storage+'"]').addClass('select');
            $('div[data-value="'+productAttributeValue[0].weight+'"]').addClass('select');
            $('div[data-value="'+productAttributeValue[0].measurement+'"]').addClass('select');
            // $(".colour").first().addClass('select');
            // $(".size").first().addClass('select');
            // $(".weight").first().addClass('select');
            // $(".measurement").first().addClass('select');
            // $(".storage").first().addClass('select');
            if(productAttributeValue[0].id){
                $("#productAttributeId").val(productAttributeValue[0].id);
            }
            function checkAttribute(filter){
                $.each( productAttributeValue, function( key, value ) {
                    selectVendorId = 0;
                     selectedsize =  $(".size.select").attr('data-value');
                     selectedcolour =  $(".colour.select").attr('data-value');
                     selectedweight =  $(".weight.select").attr('data-value');
                     selectedmeasurement =  $(".measurement.select").attr('data-value');
                     selectedstorage =  $(".storage.select").attr('data-value');
                     var v1 = false;
                        if(filter == 'colour'){
                             v1 = value.colour == selectedcolour;
                        }else  if(filter == 'weight'){
                             v1 = value.weight == selectedweight;
                        }else if(filter == 'size'){
                             v1 = value.size == selectedsize;
                        }else if(filter == 'measurement'){
                             v1 = value.measurement == selectedmeasurement;
                        }else if(filter == 'storage'){
                             v1 = value.storage == selectedstorage;
                        }else {
                            v1 = false;
                        }
                    if(v1){
                        $("#productAttributeId").val(value.id);
                        selectVendorId = value.id;
                        if(value.colour != selectedcolour){
                            $(".colour").removeClass('select');
                            $('div[data-value="'+value.colour+'"]').addClass('select');
                        }
                        if(value.size != selectedsize){
                            $(".size").removeClass('select');
                            $('div[data-value="'+value.size+'"]').addClass('select');
                        }
                        if(value.weight != selectedweight){
                            $(".weight").removeClass('select');
                            $('div[data-value="'+value.weight+'"]').addClass('select');
                        }
                        if(value.measurement != selectedmeasurement){
                            $(".measurement").removeClass('select');
                            $('div[data-value="'+value.measurement+'"]').addClass('select');
                        }
                        if(value.storage != selectedstorage){
                            $(".storage").removeClass('select');
                            $('div[data-value="'+value.storage+'"]').addClass('select');
                        }
                    }
                });
                // if(selectVendorId == 0){
                //     $("#productAttributeId").val(0);
                // }
            }
            $(".storage").click(function(){
                $(".storage").removeClass('select');
                $(this).addClass('select');
                checkAttribute('storage');
            });
            $(".weight").click(function(){
                $(".weight").removeClass('select');
                $(this).addClass('select');
                checkAttribute('weight');
            });
            $(".measurement").click(function(){
                $(".measurement").removeClass('select');
                $(this).addClass('select');
                checkAttribute('measurement');
            });
            $(".size").click(function(){
                $(".size").removeClass('select');
                $(this).addClass('select');
                checkAttribute('size');
            });
            $(".colour").click(function(){
                $(".colour").removeClass('select');
                $(this).addClass('select');
                checkAttribute('colour');
            });
             $('.loaders').hide();
             var checkpage = '';
            $(".addToCart").click(function(){
                $('.attrbibute-error').hide();
                if($("#productAttributeId").val() != 0){
                    checkpage = $(this).attr('data-name');
                    var macAdd ='{{$macAdd}}';
                    var ipadd ='{{$ipadd}}'
                    var productId ='{{ $productPrice->product_id}}';
                    var userId = '<?php if(Auth::user()){ echo Auth::user()->id; }else{ echo '0'; } ?>';
                    var vendorId = '<?php if(isset($productPrice->vendor["id"])){ echo $productPrice->vendor["id"] ; }else{ echo '0'; } ?>';
                    var price = '{{ $productPrice->final_price }}';
                    var productAttributeId = $("#productAttributeId").val();
                    var quantity = $("#quantity").val();
                    $.ajax({
                        url: '<?php echo URL::to('/'); ?>/add/cart',
                        type:"post",
                        data:{
                            '_token': '{{csrf_token()}}',
                            'macAdd' : macAdd,
                            'ipadd' : ipadd,
                            'productId': productId,
                            'userId':userId,
                            'vendorId': vendorId,
                            'quantity':quantity,
                            'productAttributeId':productAttributeId,

                        },
                        success: function(response){
                        response = JSON.parse(response);
                                if(response.status === 'success'){
                                    if(checkpage === "checkout"){
                                        window.location.href = '<?php echo URL::to('/'); ?>/user/checkout';
                                    }else{
                                        window.location.reload();
                                    }
                                }
                        }
                    });
                }else{
                    $('.attrbibute-error').text('Please select option before add to cart');
                    $('.attrbibute-error').show();
                }
           });

            $("#addToReview").click(function(){
                $('.loaders').show();
                var product_id ='{{ $products->productPrice[0]->product_id}}';
                var name =$('#name').val();
                var mobile_number =$('#mobile').val();
                var rating = $('#rating').val();
                var review = $('#review').val();
                $.ajax({
                    url: '<?php echo URL::to('/'); ?>/add/review',
                    type:"post",
                    data:{
                        '_token': '{{csrf_token()}}',
                        'product_id' : product_id,
                        'name' : name,
                        'mobile_number': mobile_number,
                        'rating':rating,
                        'review': review,
                        },
                    success: function(response){
                       response = JSON.parse(response);
                            if(response.status === 'success'){
                                $('#review').val('');
                                $('#rating').val('');
                                 $(".success-msg").text('Your review sent successfully');
                                 $(".success-msg").show();
                            }
                        $(".loaders").hide();
                    }
                });

           });

           $(".up").click(function(){
                var qua = $('#quantity').val();
                qua++;
                $("#quantity").val(qua);
                return;
            });

            $(".down").click(function(){
                var qua = $('#quantity').val();
                if(qua > 1){
                    qua--;
                        $("#quantity").val(qua);
                }else{
                    document.getElementById("quantity").value = 1;
                }
                return;
            });
           </script>
@endsection
