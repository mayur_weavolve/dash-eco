@extends('layouts.admin')

@section('content')
<style>
.vendorImage{
    width: 90px;
    margin: 10px 0px 0px 0px;
    text-align: center;
}
</style>
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Edit Category
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			    <ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('category') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-reply"></i>
						    <span>Back</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item"></li>
			    </ul>
	        </div>
	    </div>



	<form class="m-form m-form--label-align-left- m-form--state-" id="m_form" enctype="multipart/form-data" method="post" action="">
	{{csrf_field() }}
		<div class="m-portlet__body">
			<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
				<div class="row">
					<div class="col-xl-8 offset-xl-2">
						<div class="m-form__section">
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Category Name :</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="text" name="name" class="form-control m-input" value="{{$edit->name}}" required>
                                    @error('name')
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @enderror
                                </div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Category Commission :</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="number" name="commision" class="form-control m-input" value="{{$edit->commision}}" step="0.01" required>
                                    @error('commision')
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('commision') }}</strong>
                                    </span>
                                    @enderror
                                </div>
							</div>
                            <div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Image:</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="file" name="image" id="image" class="form-control m-input" placeholder="" value="">
                                    <img  class="vendorImage" src="<?php echo URL::to('/'); ?>/category_photo/{{$edit->image}}" />
                                    @error('image')
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                    @enderror
                                </div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Category Status:</label>
								<div class="col-xl-8 col-lg-8">
									<select name="status" class="form-control m-input" required>
										<option value="">Select Category Status</option>
										<option value="active"<?php if($edit->status == "active"){ echo 'selected'; }?>>Active</option>
										<option value="inactive"<?php if($edit->status == "inactive"){ echo 'selected'; }?>>In-Active</option>
									</select>
									@error('status')
                       					<span class="help-block" role="alert">
                           					<strong>{{ $errors->first('status') }}</strong>
                        				</span>
                    				@enderror
								</div>
							</div>
							<div class="col-lg-8 m--align-right">
								<button class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit" >
									<span>
										<i class="la la-check"></i>&nbsp;&nbsp;
											<span>Submit</span>
									</span>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
</div>
</div>
</div>

@endsection
