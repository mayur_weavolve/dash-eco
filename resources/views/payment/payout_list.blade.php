@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Payment Payout List
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">

	    </div>
	</div>
	<div class="m-portlet__body">
			<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
				    <th>Name</th>
                    <th>Order Id</th>
					<th>Transaction Id </th>
					{{--  <th>Refund Transaction Id </th>  --}}
                    <th>Amount</th>
                   	<th>Date</th>
				</tr>
			</thead>
			<tbody>
			@if(count($payouts) == 0)
			<tr><td colspan="8"  style="text-align:center;">No record found</td>
			@endif
			@foreach ($payouts as $payout)
			<tr>
				<td><a href="{{route('admin_vendor_info',$payout->user->id)}}">{{ $payout->user->name}}</a></td>
                <td>{{ $payout->order->order_id}}</td>
				<td>{{ $payout->transaction_id}}</td>
				{{--  <td>{{ $payout->refund_transaction_id}}</td>  --}}
				<td>{{ number_format($payout->amount)}}</td>
				<td>{{ $payout->created_at}}</td>
			</tr>
			@endforeach
			</tbody>
		</table>
		{{ $payouts->links('paginator.default') }}
	</div>
</div>
</div>
</div>
</div>

@endsection
