@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Payment List
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">

	    </div>
	</div>
	<div class="m-portlet__body">
			<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
				    <th>Name</th>
                    <th>Order Id</th>
                    <th>Transaction Id </th>
                    <th>Amount</th>
                   	<th>Date</th>
				</tr>
			</thead>
			<tbody>
			@if(count($payments) == 0)
			<tr><td colspan="8"  style="text-align:center;">No record found</td>
			@endif
			@foreach ($payments as $payment)
			<tr>
				<td><a href="{{route('admin_info',$payment->user->id)}}">{{ $payment->user->name}}</a></td>
                <td>{{ $payment->orderDetail->order_id}}</td>
                <td>{{ $payment->transaction_id}}</td>
				<td>{{ number_format($payment->amount)}}</td>
				<td>{{ $payment->created_at}}</td>
			</tr>
			@endforeach
			</tbody>
		</table>
		{{ $payments->links('paginator.default') }}
	</div>
</div>
</div>
</div>
</div>

@endsection
