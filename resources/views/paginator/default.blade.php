<?php $link_limit = 25; ?>
@if ($paginator->lastPage() > 1)
<div class="dataTables_wrapper">
    <div class="dataTables_paginate">
        <ul class="pagination" style="justify-content: flex-end;" >
             <li class="{{ ($paginator->currentPage() == 1) ? 'paginate_button page-item first disabled' : ' paginate_button page-item previous' }}">
                <a class="page-link" href="{{ $paginator->url(1) }}" >
                <<
                </a>
            </li>
            <li class="{{ ($paginator->currentPage() == 1) ? 'paginate_button page-item previous disabled' : ' paginate_button page-item previous' }}">
                <a title="Previous" href="{{ $paginator->url($paginator->currentPage()-1) }}" class="page-link">
                <
                </a>
            </li>
            @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            <?php
                $half_total_links = floor($link_limit / 2);
                $from = $paginator->currentPage() - $half_total_links;
                $to = $paginator->currentPage() + $half_total_links;
                if ($paginator->currentPage() < $half_total_links) {
                $to += $half_total_links - $paginator->currentPage();
                }
                if ($paginator->lastPage() - $paginator->currentPage() < $half_total_links) {
                    $from -= $half_total_links - ($paginator->lastPage() - $paginator->currentPage()) - 1;
                }
                ?>
                @if ($from < $i && $i < $to)
                    <li class="{{ ($paginator->currentPage() == $i) ? ' page-item active' : 'page-item' }}">
                        <a class="page-link" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                    </li>
                @endif
            @endfor
            <!-- @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                <li class="{{ ($paginator->currentPage() == $i) ? ' page-item active' : 'page-item' }}">
                    <a class="page-link" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                </li>
            @endfor -->
            <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? 'paginate_button page-item next disabled' : 'paginate_button page-item next' }}">
                <a class="page-link" href="{{ $paginator->url($paginator->currentPage()+1) }}" >
              >
                </a>
            </li>
            <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? 'paginate_button page-item last disabled' : 'paginate_button page-item next' }}">
                <a class="page-link" href="{{ $paginator->url($paginator->lastPage()) }}" >
              >>
                </a>
            </li>
        </ul>
    </div>
</div>
@endif
