@if ($paginator->lastPage() > 1)
<div class="dataTables_wrapper">
    <div class="dataTables_paginate">
        <ul class="pagination" style="justify-content: flex-end;" >
             <li class="{{ ($paginator->currentPage() == 1) ? 'paginate_button page-item first disabled' : ' paginate_button page-item previous' }}">
                <a class="page-link" href="{{ $paginator->url(1) }}" >
                <i class="pe-7s-angle-left"></i><i class="pe-7s-angle-left"></i>
                </a>
            </li>
            <li class="{{ ($paginator->currentPage() == 1) ? 'paginate_button page-item previous disabled' : ' paginate_button page-item previous' }}">
                <a title="Previous" href="{{ $paginator->url($paginator->currentPage()-1) }}" class="page-link">
                    <i class="pe-7s-angle-left"></i>
                </a>
            </li>
            @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                <li class="{{ ($paginator->currentPage() == $i) ? ' page-item active' : 'page-item' }}">
                    <a class="page-link" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                </li>
            @endfor
            <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? 'paginate_button page-item next disabled' : 'paginate_button page-item next' }}">
                <a class="page-link" href="{{ $paginator->url($paginator->currentPage()+1) }}" >
                <i class="pe-7s-angle-right"></i>
                </a>
            </li>
            <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? 'paginate_button page-item last disabled' : 'paginate_button page-item next' }}">
                <a class="page-link" href="{{ $paginator->url($paginator->lastPage()) }}" >
                <i class="pe-7s-angle-right"></i><i class="pe-7s-angle-right"></i>
                </a>
            </li>
        </ul>
    </div>
</div>
@endif
