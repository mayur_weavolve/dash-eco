@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						User List
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('user_add') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
						    <span>Add User</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item">

			</ul>
	    </div>
	</div>
	<div class="m-portlet__body">
			<div class = "row">
			
				<div class = "col-md-10">
					<form action="{{route('user')}}" method ="get">
					<input type="search" name="search"class="form-control" placeholder="Search User Name"><br>
				</div>
				<div class="col-md-2 text-right">
					<div class="row">
						<div class="col-md-6">
							<button type = "submit" class ="btn btn-primary">Search</button><br>
						</div>
						<div class="col-md-6">
								<a href="{{route('user')}}" class="btn btn-primary">Clear</a>
						</div>
					</div>
					</form>
				</div>
			
				
			</div>
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
				    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile No</th>
                    <th>Whatsapp No</th>
                   	<th>Action</th>
				</tr>
			</thead>
			<tbody>
			@if(count($users) == 0)
			<tr><td colspan="8"  style="text-align:center;">No record found</td>
			@endif
			<?php foreach ($users as $user) { ?>
			<tr>
				<td>{{ $user->name}}</td>
                <td>{{ $user->email}}</td>
                <td>{{ $user->mobileno}}</td>
				<td>{{ $user->whatsapp_no}}</td>
				<td>
						<a href="{{route('user_edit_view',[$user->id])}}"  title="Edit"><i class="m-menu__link-icon fa fa-edit"></i></a>  &nbsp;&nbsp;
						<a href="{{route('user_delete',[$user->id])}}" onclick="return confirm('Are you sure you want to delete this User?');"  title="Delete"><i class="m-menu__link-icon fa fa-trash-alt"></i></a> &nbsp;&nbsp;
						
				</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
		{{ $users->links('paginator.default') }}
	</div>
</div>
</div>
</div>
</div>

@endsection
