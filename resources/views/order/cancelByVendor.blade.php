@extends('layouts.admin')
@section('content')
<style>
		b, strong {
			font-weight: bold !important;
		}
		.orders{
			border-top: none !important;
		}
		.order-status-option{
			background: white;
			color:black;
		}
		.order-status {
			border: 1px solid;
		}

		.code-btn{
			margin-left: 17%;
			background: #ffcc00;
		}
		.code-btn a:hover {
			 color: white !important; 
			 text-decoration: none !important; 
		}
		.code-btn a{
			color: white !important; 
			text-decoration: none !important; 
	   	}
		{{-- .inactive{
			background: red;
			padding: 8px;
			color: white;
		}
		.activeClass{
			background: green;
			padding: 8px 15px;
			color: white;
		}
		.pending{
			background: yellow;
			padding: 8px 15px;
			color:black;
		} --}}
</style>
<?php $total = 0;?>
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Order Detail
					</h3>
				</div>
			</div>
			
		</div>
	<div class="m-portlet__body">
		<div class="main">
                <div class="row product">
                    <div class="col-md-3 col-sm-12" style="text-align:center;">
                                <a href="javascript:void(0);"><img class="order-product-image" src="/product_photo/{{$suborder->productImage->name}}" alt="" style="max-width: 100px!important;" ></a>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="ps-product__content">
                            <a class="ps-product__title" href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$suborder->product->product_name)), 'id' =>  $suborder->product_id.'_'.$suborder->vendor_id ])}}">
                                <strong>{{$suborder->product->product_name}}</strong>
                            </a>
                            <p>{{$suborder->quantity}} x ₹{{number_format($suborder->amount)}}<p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div style="display:inline"><strong>Customer Detail</strong> </div>
                        </div>
                        <div class="row">
                            <div style="display:inline"><strong>Name :</strong> </div>&nbsp;<div style="display:inline">{{$list[0]->users->name}}</div>
                        </div>
                        <div class="row">					
                            <div style="display:inline"><strong>Email :</strong> </div>&nbsp;<div style="display:inline">{{$list[0]->users->email}}</div>
                        </div>
                        <div class="row">					
                            <div style="display:inline"><strong>Whatsapp No :</strong> </div>&nbsp;<div style="display:inline">{{$list[0]->users->whatsapp_no}}</div>
                        </div>
                        <div class="row">					
                            <div style="display:inline"><strong>Mobile No : </strong></div>&nbsp;<div style="display:inline">{{$list[0]->users->mobileno}}</div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div style="display:inline"><strong>Address</strong> </div>
                        </div>
                        <div class="row">
                            <div style="display:inline">{{$list[0]->address->addressline1}}</div>
                        </div>
                        <div class="row">					
                            <div style="display:inline">{{$list[0]->address->addressline2}}</div>
                        </div>
                        <div class="row">					
                            <div style="display:inline">{{$list[0]->address->city}} , {{$list[0]->address->state}}</div>
                        </div>
                        <div class="row">					
                            <div style="display:inline">{{$list[0]->address->pincode}}</div>
                        </div>
                    </div>
                    
				</div>
				
				
					<form class="m-form m-form--label-align-left- m-form--state-" id="m_form" enctype="multipart/form-data" method="post" action="{{route('add_sub_order')}}">
						{{csrf_field() }}
						<input name="id" value="{{$sub->id}}" type="hidden">
						<input name="order_id" value="{{$sub->order_id}}" type="hidden">
						<input name="product_id" value="{{$sub->product_id}}" type="hidden">
						<input name="quantity" value="{{$sub->quantity}}" type="hidden">
						<input name="amounts" value="{{$sub->amount}}" type="hidden">
						<input name="product_attribute" value="{{$sub->product_attribute}}" type="hidden">
						                           
						<h5><strong>Select Vendors</strong></h5>
						<div class="row">
						<div class="col-md-12">
							<label for="vendor_id" class="col-md-12">
								<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
									<tbody>
									@foreach($details as $detail)
												<tr>
													<th><input type="radio" id="vendor_id" name="vendor_id" value="{{$detail->id}}"></th>
													<th>{{$detail->name}}</th>
													<th>{{$detail->nickname}}</th>
													<th>{{$detail->business_name}}</th>
												</tr>
												
									@endforeach
									</tbody>
								</table>
							</label>
						</div>
						</div>
						<div class="row">
							<div class="col-lg-12 m--align-left">
								<button class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit" >
									<span>
										<i class="la la-check"></i>&nbsp;&nbsp;
											<span>Submit</span>
									</span>
								</button>
							</div>
						</div>
					</form>
				
                
            </div>

		
	</div>
</div>
</div>
</div>
</div>
@endsection
