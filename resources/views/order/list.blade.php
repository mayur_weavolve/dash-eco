@extends('layouts.admin')
@section('content')
<style>
		{{-- .inactive{
			background: red;
			padding: 8px;
			color: white;
		}
		.activeClass{
			background: green;
			padding: 8px 15px;
			color: white;
		}
		.pending{
			background: yellow;
			padding: 8px 15px;
			color:black;
		} --}}
</style>
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Order List
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">

				<ul class="m-portlet__nav">
					<li class="m-portlet__nav-item">
							{{-- <a href="{{ route('add_product') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
								<span>
									<i class="la la-plus"></i>
									<span>Add Product</span>
								</span>
							</a> --}}
					</li>
				</ul>
			</div>
	</div>
	<div class="m-portlet__body">
			<div class = "row">

				<div class = "col-md-10">
					<form action="{{route('order_list')}}" method ="get">
					<input type="search" name="search"class="form-control" placeholder="Search Order" value="@if(isset($search)){{$search}}@endif"><br>
				</div>
				<div class="col-md-2 text-right">
					<div class="row">
						<div class="col-md-6">
							<button type = "submit" class ="btn btn-primary">Search</button><br>
						</div>
						<div class="col-md-6">
								<a href="{{route('order_list')}}" class="btn btn-primary">Clear</a>
						</div>
					</div>
					</form>
				</div>


			</div>
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
					<th>Name</th>
					<th>Order Id</th>
					<th>Tracking Number</th>
					<th>Order Date</th>
					<th>Pincode</th>
					<th>Status</th>
					<th>Payment</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			@if(count($orders) == 0)
			<tr><td colspan="8"  style="text-align:center;">No record found</td>
			@endif
			<?php foreach ($orders as $order) { ?>
			<tr>
				<td><a href="{{route('admin_info',$order->users->id)}}">{{ $order->users->name}}</a></td>
				<td>{{ $order->order_id}}</td>
                <td>{{ $order->tracking_number}}</td>
				<td>{{$order->created_at }}</td>
				<td>{{$order->address->pincode }}</td>
				<td>{{$order->status }}</td>
				<td>@if($order->status == 'pending_payment')<strong> UnPaid </strong>@else <strong> Paid </strong>@endif</td>
				<td><a href="{{route('order_detail',$order->id)}}" title="Detail"><i class="m-menu__link-icon fa fa-book"></i></a></td>
			</tr>

			<?php } ?>
			</tbody>
		</table>
        {{ $orders->links('paginator.default') }}
	</div>
</div>
</div>
</div>
</div>

@endsection
