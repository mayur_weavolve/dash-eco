@extends('layouts.admin')
@section('content')
<style>
		b, strong {
			font-weight: bold !important;
		}
		.orders{
			border-top: none !important;
		}
		.order-status-option{
			background: white;
			color:black;
		}
		.order-status {
			border: 1px solid;
		}

		.code-btn{
			margin-left: 17%;
			background: #ffcc00;
		}
		.code-btn a:hover {
			 color: white !important;
			 text-decoration: none !important;
		}
		.code-btn a{
			color: white !important;
			text-decoration: none !important;
		   }
		   .main{
			margin-top:10px;
		   border: 1px solid #c1c1c1;
		}
		.head{
		   margin-left: 0px;
		   background: #f1f1f1;
		   padding: 20px 0px;
		}
		.track{
		   border-top: 1px solid #c1c1c1;
		   margin-left: 0px;
		   padding-top:20px;
		}
		.endcol{
		   text-align: end;
		}
		.product{
		   padding-bottom: 20px;
		}
		{{-- .inactive{
			background: red;
			padding: 8px;
			color: white;
		}
		.activeClass{
			background: green;
			padding: 8px 15px;
			color: white;
		}
		.pending{
			background: yellow;
			padding: 8px 15px;
			color:black;
		} --}}
</style>
<?php $total = 0;?>
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Order Detail
					</h3>
				</div>
			</div>

		</div>
	<div class="m-portlet__body">
		<div class="row" style="padding: 0px 25px;">
			<div class="col-md-4">
				<div class="row">
					<div style="display:inline"><h4><strong>Customer</strong></h4></div>&nbsp;
				</div>
				<div class="row">
					<div style="display:inline">{{$order->users->name}}</div>
				</div>
				<div class="row">
					<div style="display:inline">{{$order->users->email}}</div>
				</div>
				<div class="row">
					<div style="display:inline">{{$order->users->whatsapp_no}}</div>
				</div>
				<div class="row">
					<div style="display:inline">{{$order->users->mobileno}}</div>
				</div>


			</div>
			<div class="col-md-4">
					<div class="row">
						<div style="display:inline"><h4><strong>Shipping Address</strong></h4></div>&nbsp;
					</div>

					<div class="row">
						<div style="display:inline">
							<div>{{$order->address->addressline1}}</div>
							<div>{{$order->address->addressline2}}</div>
							<div>{{$order->address->city}},{{$order->address->state}},{{$order->address->pincode}}</div>
						</div>
					</div>
			</div>
			<div class="col-md-4">
					<div class="row">
						<div style="display:inline"><h4><strong>Payment Details</strong></h4></div>&nbsp;
					</div>
					<div class="row">
						<div style="display:inline">Amount : </div>&nbsp;
						<div style="display:inline"><strong>{{number_format($order->amount)}}</strong></div>
					</div>
					<div class="row">Status :&nbsp;
							@if($order->status == 'pending_payment')<strong> UnPaid </strong>@else <strong> Paid </strong>@endif
					</div>
					@if($order->status != 'pending_payment' && isset($order->payment->transaction_id))
                        <div class="row">
                            <div style="display:inline"><strong>Transaction Id :</strong></div>&nbsp;
                            <div style="display:inline"><strong>{{$order->payment->transaction_id}}</strong></div>
                        </div>
					@endif
			</div>
		</div>

		<div class="row main">
				<div class="row col-md-12 col-sm-12 head">
						<div class="col-md-4 col-sm-12 ">
								<?php
								$date=date_create($order->created_at);
								$createdAt = date_format($date,"d F Y");
								?>
								<strong>{{$createdAt}}</strong>
						</div>
						<div class="col-md-5 col-sm-12"></div>
						<div class="col-md-3 col-sm-12 endcol">
							<span>Order #{{$order->order_id}}</span>
						</div>
				</div>

				@foreach($order->subOrders as $sub)

					<div class="row col-md-12 col-sm-12 product track">
                        <div class="col-md-1 col-sm-12 img-div">
                            <a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$sub->product->product_name)), 'id' =>  $sub->product_id.'_'.$sub->vendor_id ])}}"><img class="order-product-image" src="/product_photo/{{$sub->product->productImage->name}}" alt="" style="width:50%;" ></a>
                        </div>
                        <div class="col-md-5 col-sm-12 order-product">
                            <div class="ps-product__content">
                                <a class="ps-product__title" target="_blank" href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$sub->product->product_name)), 'id' =>  $sub->product_id.'_'.$sub->vendor_id ])}}">{{$sub->product->product_name}}</a>
                                <p> {{$sub->quantity}} x ₹{{number_format($sub->amount)}}</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 text-center">
                            <div class="ps-product__content">
                                <p><strong>Sold By :</strong> &nbsp; <a href="{{route('admin_vendor_info',$sub->vendor->id)}}">{{$sub->vendor->name}}</a></p>
                                <p> Code :  <strong>{{$sub->security_code}}</strong></p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 text-center">

                                	<form>
											<select name="status" id="status{{$sub->id}}" onchange="statusUpdate(this.value , {{$sub->id}})" class="order-status order-{{ $sub->status }}">
												<option @if($sub->status == 'pending_payment') selected @endif class="order-status-option"  value="pending_payment">Unpaid</option>
												<option @if($sub->status == 'pending') selected @endif class="order-status-option"  value="pending"> Waiting For Vendor</option>
												<option @if($sub->status == 'processing') selected  @endif class="order-status-option" value="processing">Order Confirmed</option>
												<option @if($sub->status == 'delivered') selected  @endif class="order-status-option" value="delivered">Order Delivered</option>
												<option @if($sub->status == 'completed') selected @endif class="order-status-option" value="completed">Order Complete</option>
												<option @if($sub->status == 'cancelled') selected  @endif class="order-status-option" value="cancelled">Canceled</option>
												<option @if($sub->status == 'cancel_by_vendor') selected @endif class="order-status-option" value="cancel_by_vendor">Cancel By Vendor</option>
												<option class="order-status-option" value="">In Progress</option>
												<option @if($sub->status == 'transfer') selected @endif class="order-status-option" value="transfer">Transfer</option>
											</select>
										</form>
										@if($sub->status == 'cancel_by_vendor')
										<div>
											<span class="ps-btn-small code-btn"><a href="{{route('cancel_by_vendor',$sub->id)}}">Transfer</a></span>
										</div>
										@endif



                        </div>

                    </div>
            	 @endforeach
		</div>



	</div>
</div>
</div>
</div>
</div>
	<script>
	function statusUpdate(status , id)
		{

			$.ajax({
				url: '<?php echo URL::to('/'); ?>/backend/update/status',
				type:"post",
				data:{
					'_token': '{{csrf_token()}}',
					'id' : id,
					'status' : status,
					},
				success: function(response){
					response = JSON.parse(response);
						if(response.status === 'success'){
							location.reload();
						}

				}
			});
			return
		}
	</script>
@endsection
