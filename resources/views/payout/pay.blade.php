@extends('layouts.admin')
@section('content')
<style>
    .totaldiv{
        font-size: 14px;
        font-weight: 600;
    }
</style>
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Payout Pay
					</h3>
				</div>
			</div>

	</div>
	<div class="m-portlet__body">
		<form class="m-form m-form--label-align-left- m-form--state-" id="m_form" enctype="multipart/form-data" method="post" action="{{route('payout_pay_add')}}">
		{{csrf_field() }}

			<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
                    <th>Order Id</th>
					<th>Suborder Id</th>
					<th>Product</th>
					<th>Quantity</th>
					<th>Amount</td>


				</tr>
			</thead>
			<tbody>
            <?php $total = 0; ?>
			@if(sizeof($payouts) == 0)
			    <tr><td colspan="8"  style="text-align:center;">No record found</td>
			@else
                <?php
                    foreach ($payouts as $payout) { ?>
                        <input type="hidden" id="id" name="payId[]" value="{{$payout->id}}">
                <tr>
                    <td>{{$payout->order->order_id}}</td>
                    <td>{{$payout->suborder_id}}</td>
                    <td>{{ $payout->product->product_name}}</td>
                    <td>{{$payout->quantity }} x ₹{{number_format($payout->price) }}</td>
                    <td>₹{{ number_format($payout->amount) }}</td>

                </tr>
                <?php  $total = $total + $payout->amount;
                    } ?>
                </tbody>
                <tfoot>
                    <tr class="totaldiv">
                        <td colspan="4" ><b>Total</b></td>
                        <td > <b>₹{{ number_format($total) }} </b></td>
                    </tr>
                </tfoot>
            @endif
		</table>
       	<input type="hidden" id="account_number" name="account_number" value="">
			<input type="hidden" id="amount" name="amount" value="{{$total}}">

			<div class="col-lg-8 m--align-left">
                <br>
				<p><strong>Select Mode</strong></p>

                @foreach($paymentAccounts as $paymentAccount)
                    @if($paymentAccount->type == 'bank_account')
                        <input type="radio" id="fa_account_number" name="fa_account_number" value="{{$paymentAccount->type}}?{{$paymentAccount->account_id}}">
                        <label for="vendor_id">Bank Account -                   ****{{ $paymentAccount->account_name }}</label><br>
                    @endif
                    @if($paymentAccount->type == 'vpa')
                        <input type="radio" id="fa_account_number" name="fa_account_number" value="{{$paymentAccount->type}}?{{$paymentAccount->account_id}}">
                        <label for="vendor_id">UPI - {{ $paymentAccount->account_name }}</label><br>
                    @endif
                @endforeach
			</div>

			<div class="col-lg-8 m--align-left">
            <br>
				<button class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit" >
					<span>
						<i class="fa fa-rupee-sign"></i>&nbsp;&nbsp;
							<span>Pay</span>
					</span>
				</button>
			</div>
		</form>
	</div>
</div>
</div>
</div>
</div>

@endsection
