@extends('layouts.admin')
@section('content')
<style>
    .totaldiv{
        font-size: 14px;
        font-weight: 600;
    }
</style>
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Payout List
					</h3>
				</div>
			</div>

	</div>
	<div class="m-portlet__body">

		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
                    <th>Order Id</th>
					<th>Suborder Id</th>
					<th>User</th>
                    <th>Vendor</th>
					<th>Product</th>
					<th>Quantity</th>
					<th>Pay Status</th>
					<th>Paid At</th>
                    <th>Amount</td>
					<th>Action</th>

				</tr>
			</thead>
			<tbody>
			@if(count($payouts) == 0)
			<tr><td colspan="8"  style="text-align:center;">No record found</td>
			@endif
            <?php  $total = 0;
                foreach ($payouts as $payout) { ?>
			<tr>
                <td><a href="{{route('order_detail',$payout->order->id)}}">{{$payout->order->order_id}}</a></td>
				<td>{{$payout->suborder_id}}</td>
				<td><a href="{{route('admin_info',$payout->user->id)}}">{{ $payout->user->name}}</a></td>
				<td><a href="{{route('admin_vendor_info',$payout->vendor->id)}}">{{ $payout->vendor->name}}</a> <a href="{{route('payout_pay_view',[$payout->vendor_id])}}"  title="Pay"><i class="m-menu__link-icon fa fa-rupee-sign"></i></a></td>
                <td>{{ $payout->product->product_name}}</td>
				<td>{{$payout->quantity }} x ₹{{ number_format($payout->price) }}</td>
				<td>@if($payout->pay_status == 0)<span class="m-badge  m-badge--primary m-badge--wide">Pending </span>@elseif($payout->pay_status == 1)<span class="m-badge  m-badge--success m-badge--wide"> Accept </span>@else <span class="m-badge  m-badge--danger m-badge--wide">Reject</span> @endif</td>
				<td>{{$payout->paid_at }}</td>
				<td>₹{{number_format($payout->amount) }}</td>
				<td><a href="{{route('payout_status_view',[$payout->id])}}"  title="Pay"><i class="m-menu__link-icon fa fa-pen"></i></a></td>
			</tr>
            <?php  $total = $total + $payout->amount;
                } ?>
			</tbody>
            <tfoot>
                <tr class="totaldiv">
                    <td colspan="8" ><b>Total</b></td>
                    <td colspan="2" > <b>₹{{ number_format($total) }} </b></td>
                </tr>
            </tfoot>
		</table>
        {{ $payouts->links('paginator.default') }}
	</div>
</div>
</div>
</div>
</div>

@endsection
