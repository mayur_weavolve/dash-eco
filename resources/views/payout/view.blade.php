@extends('layouts.admin')
@section('content')
<style>
		
</style>
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Payout 
					</h3>
				</div>
			</div>
			
	</div>
	<form class="m-form m-form--label-align-left- m-form--state-" id="m_form" enctype="multipart/form-data" method="post" action="{{route('payout_status_add')}}">
        {{csrf_field() }}
			<div class="m-portlet__body">
                    <div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
                        <div class="row">
                            <div class="col-xl-10 offset-xl-1">
                                <div class="m-form__section">
                                    <fieldset id="buildyourform" name = "form">
                                        
                                    <div class="form-group m-form__group row">
                                        <label for="name" class="col-xl-3 col-lg-3 col-form-label">*Name:</label>
                                        <div class="col-xl-8 col-lg-8">
                                            <input  type="text" name="user_name" id="product_name" class="form-control m-input" placeholder="" value="{{$payout->user->name}}" readonly>
                                                @error('user_name')
                                                <span class="help-block" role="alert">
                                                    <strong>{{ $errors->first('user_name') }}</strong>
                                                </span>
                                                @enderror
                                        </div>
                                    </div>
                                                
                                    <div class="form-group m-form__group row">
                                        <label for="name" class="col-xl-3 col-lg-3 col-form-label">* Order Id:</label>
                                        <div class="col-xl-8 col-lg-8">
                                            <input  type="text" name="order_id" id="order_id" class="form-control m-input price" placeholder="" value="{{$payout->order->order_id}}" readonly>
                                                @error('order_id')
                                                <span class="help-block" role="alert">
                                                    <strong>{{ $errors->first('order_id') }}</strong>
                                                </span>
                                                @enderror
                                        </div>
                                    </div>
                                    <input  type="hidden" name="order" id="order" class="form-control m-input" placeholder="" value="{{$payout->order->id}}">
                                    <div class="form-group m-form__group row">
                                        <label for="name" class="col-xl-3 col-lg-3 col-form-label">* Transaction Id:</label>
                                        <div class="col-xl-8 col-lg-8">
                                            <input  type="text" name="transaction_id" id="transaction_id" class="form-control m-input price" placeholder="" value="{{$payout->transaction_id}}" readonly>
                                                @error('transaction_id')
                                                <span class="help-block" role="alert">
                                                    <strong>{{ $errors->first('transaction_id') }}</strong>
                                                </span>
                                                @enderror
                                        </div>
                                    </div>
                                    
                                    <div class="form-group m-form__group row">
                                        <label for="name" class="col-xl-3 col-lg-3 col-form-label">* Product Name:</label>
                                        <div class="col-xl-8 col-lg-8">
                                            <input  type="text" name="product_name" id="product_name" class="form-control m-input" placeholder="" value="{{$payout->product->product_name}}" readonly>
                                                @error('product_name')
                                                <span class="help-block" role="alert">
                                                    <strong>{{ $errors->first('product_name') }}</strong>
                                                </span>
                                                @enderror
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="name" class="col-xl-3 col-lg-3 col-form-label">Quantity</label>
                                        <div class="col-xl-8 col-lg-8">
                                            <input type="text" name="quantity" id="quantity" class="form-control m-input" placeholder="" value="{{$payout->quantity}}" readonly>
                                                @error('quantity')
                                                <span class="help-block" role="alert">
                                                    <strong>{{ $errors->first('quantity') }}</strong>
                                                </span>
                                                @enderror
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="name" class="col-xl-3 col-lg-3 col-form-label">Amount:</label>
                                            <div class="col-xl-8 col-lg-8">
                                            <input  type="number" name="amount" id="amount" class="form-control m-input" placeholder="" value="{{$payout->amount}}" readonly>
                                                    @error('amount')
                                                        <span class="help-block" role="alert">
                                                        <strong>{{ $errors->first('amount') }}</strong>
                                                    </span>
                                                    @enderror
                                            </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <input  type="hidden" name="pay_id" value="{{$payout->id}}">
                                        <label for="name" class="col-xl-3 col-lg-3 col-form-label">* Pay Status:</label>
                                        <div class="col-xl-8 col-lg-8">
                                            <select name="pay_status" id="pay_status" class="form-control m-input price">
                                                <option value="0" @if($payout->pay_status == '0') selected @endif>Pending</option>
                                                <option value="1" @if($payout->pay_status == '1') selected @endif>Accept</option>
                                                <option value="2" @if($payout->pay_status == '2') selected @endif>Reject</option>
                                            </select>
                                                @error('pay_status')
                                                <span class="help-block" role="alert">
                                                    <strong>{{ $errors->first('pay_status') }}</strong>
                                                </span>
                                                @enderror
                                        </div>
                                    </div>
                                    
                                    </fieldset><br>
                                    <div class="col-lg-8 m--align-right">
                                        <button class="btn btn-primary m-btn m-btn--custom m-btn--icon" id="addProPrice" data-wizard-action="submit" >
                                            <span>
                                                <i class="las la-pen"></i>&nbsp;&nbsp;
                                                    <span>Submit</span>
                                            </span>
                                        </button>
        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
		
            </form>
</div>
</div>
</div>
</div>

@endsection
