@extends('layouts.user')

@section('content')
<style>
    .main{
        background-color: white;
    }
    .adsub{
        border: 1px solid black;
        border-radius: 1.25rem;
        padding: 1%;
        padding-bottom: 2.75rem;
        background-color: black;
        color: white;
        font-weight: 600;
        font-size: 12px;
    }
    .subbox select{
        height: 35px;
        font-size: 14px;
        font-weight: 400;
    }
    .subbox input{
        height: 35px;
        font-size: 15px;
        font-weight: 400;
    }
    #colour{
        width: 70%;
    }
    th{
        text-align: center;
        font-size: 15px;
        font-weight: 600 !important;
    }
    #colorSpan{
        border: 1px solid red;
        border-radius: 50%;
        height: 100%;
        display: inline-block;
        cursor: pointer;
    }
    .removeAttr{
        color: red;
        font-size: 14px;
        padding: 0px 3px 0px 3px;
    }
</style>
<div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
            <li><a href="<?php echo URL::to('/'); ?>">Home</a></li>
                    <li><a href="{{ route('vendor_home') }}">Vendor</a></li>
                    <li>Product Price</li>
            </ul>
        </div>
</div>

<div class="container">
    <div class="ps-section__header" style="margin-top:15px;">
        <h3>Product Price</h3>
    </div>
    <div class="ps-section__content">
    	<div class="ps-form__content">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <div class="ps-block--shipping">
                            <div class="ps-block--payment-method">
                                <div class="ps-tabs">
                                    <form class="ps-form--visa" id="m_form" enctype="multipart/form-data" method="post" action="{{route('product_detail_edit_save',[$edit->id,$vendor->id])}}">
                                    <input id="token" name="_token" type="hidden" value="{{csrf_token()}}">

                                    <div class="form-group">
                                        <label>Product Name</label>
                                        <input class="form-control" type="hidden" name="id" value="{{$product->id}}">
                                        <input class="form-control" type="text" value="{{$product->product_name}}" readonly>
                                    </div>

                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>MRP</label>
                                                <input type="number" name="mrp" id="mrp" class="form-control m-input price" placeholder="" value="{{$edit->mrp}}" required>
                                                @error('mrp')
                                                <span class="help-block" role="alert">
                                                    <strong>{{ $errors->first('mrp') }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Selling Price</label>
                                                <input type="number" name="final_price" id="final_price" class="form-control m-input price" placeholder="" value="{{$edit->final_price}}" required>
                                                @error('final_price')
                                                <span class="help-block" role="alert">
                                                    <strong>{{ $errors->first('final_price') }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Discount(%)</label>
                                                    <input type="text" name="discount" id="discount" class="form-control m-input" placeholder="" value="{{$edit->discount}}" readonly>
                                                    @error('discount')
                                                    <span class="help-block" role="alert">
                                                        <strong>{{ $errors->first('discount') }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>What you will get after DASH charges</label>
                                                    <input type="text" name="dash_price" id="dash_price" class="form-control m-input" placeholder="" value="" readonly>
                                                    @error('dash_price')
                                                    <span class="help-block" role="alert">
                                                        <strong>{{ $errors->first('dash_price') }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Warranty</label>
                                                    <input type="number" name="warranty" id="warranty" class="form-control m-input" placeholder="NA or no. of months" value="{{$edit->warranty}}" >
                                                    @error('warranty')
                                                        <span class="help-block" role="alert">
                                                        <strong>{{ $errors->first('warranty') }}</strong>
                                                        </span>
                                                    @enderror
									            </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Installation Included</label>
                                                    <select name="installation_included" class="form-control m-input" value = "">
                                                        <option value="" @if($edit->installation_included == '') selected @endif >Select Installation</option>
                                                        <option value="yes" @if($edit->installation_included == 'yes') selected @endif >Yes</option>
                                                        <option value="no" @if($edit->installation_included == 'no') selected @endif >NO</option>
                                                        <option value="na" @if($edit->installation_included == 'na') selected @endif >NA</option>
                                                    </select>
                                                    @error('installation_included')
                                                    <span class="help-block" role="alert">
                                                        <strong>{{ $errors->first('installation_included') }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Exchange</label>
                                                    <input type="number" name="exchange" id="exchange" class="form-control m-input" placeholder="NA or no. of days" value="{{$edit->exchange}}">
                                                    @error('exchange')
                                                        <span class="help-block" role="alert">
                                                        <strong>{{ $errors->first('exchange') }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Free Delivery / Pickup:</label>
                                                    <select name="free_delivery" class="form-control m-input" value = ""required>
                                                            <option value="" @if($edit->free_delivery == '') selected @endif >Select Free Delivery</option>
                                                            <option value="0" @if($edit->free_delivery == 0) selected @endif >Pickup Only</option>
                                                            <option value="5" @if($edit->free_delivery == 5) selected @endif >5 KM</option>
                                                            <option value="7" @if($edit->free_delivery == 7) selected @endif >7 KM</option>
                                                            <option value="10" @if($edit->free_delivery == 10) selected @endif >10 KM</option>
                                                            <option value="15" @if($edit->free_delivery == 15) selected @endif >15 KM</option>
                                                            <option value="20"  @if($edit->free_delivery == 20) selected @endif >20 KM</option>
                                                    </select>
                                                    @error('free_delivery')
                                                    <span class="help-block" role="alert">
                                                        <strong>{{ $errors->first('free_delivery') }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Mode</label>
                                                <select name="mode" class="form-control m-input" value = "" required>
                                                    <option value="" @if($edit->mode == '') selected @endif>Select Mode</option>
                                                    <option value="1" @if($edit->mode == 1) selected @endif >Same day if order before 2 pm</option>
                                                    <option value="0" @if($edit->mode == 0) selected @endif>Next Day</option>
                                                </select>
                                                @error('mode')
                                                    <span class="help-block" role="alert">
                                                    <strong>{{ $errors->first('mode') }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        
                                        </div>
                                        <div class="form-group">
                                                <label>Attributes</label>
                                                <div class="col-xl-12 col-lg-12 productAttr">
                                                        <h6>No Attribute Found</h6>
                                                </div>
                                                <div class="col-xl-12 col-lg-12 text-center">
                                                    <input class="adsub" type = "button" value = "Add Attribute" id = "add" />&nbsp;
                                                </div>
                                        </div>
							<div class="form-group submit">
								<button class="ps-btn ps-btn--fullwidth" data-wizard-action="submit" >
									<span>Submit</span>
								</button>
                            </div>
                        </form>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
    </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type = "text/javascript">
$('.attrbutton').hide();
$("#add").click(function(){
    $( "#copyrow" ).clone().appendTo( ".pestrow" );
});
$("body").on("click",".removeAttr",function(){
    $(this).closest('#copyrow').remove();
});
function getPrices(){
    var mrp = $("#mrp").val();
    var final_price = $("#final_price").val();
    var discountPrice = mrp - final_price;
    var discountPercentage = (discountPrice * 100) / mrp;
    var catPercentage = 7;
    var DashPrice = final_price - ((7 * final_price) / 100);
    $('#dash_price').val(DashPrice.toFixed(2));
    $('#discount').val(discountPercentage.toFixed(2));
}
$(".price").keyup(function(){
    getPrices();
});
getPrices();
function getAttribute(){
    var html = '';
    var colorGroup = { };
    colorGroup[0] = { 'name' : "WHITE", "code" : "FFFFFF", "fontColor" : "000" };
    colorGroup[1] = { 'name' : "SILVER", "code" : "C0C0C0", "fontColor" : "000" };
    colorGroup[2] = { 'name' : "GRAY", "code" : "808080", "fontColor" : "000" };
    colorGroup[3] = { 'name' : "YELLOW", "code" : "FFFF00", "fontColor" : "000" };
    colorGroup[4] = { 'name' : "BLACK", "code" : "000000", "fontColor" : "FFFFFF" };
    colorGroup[5] = { 'name' : "RED", "code" : "FF0000", "fontColor" : "FFFFFF" };
    colorGroup[6] = { 'name' : "MAROON", "code" : "800000", "fontColor" : "FFFFFF" };
    colorGroup[7] = { 'name' : "OLIVE", "code" : "808000", "fontColor" : "FFFFFF" };
    colorGroup[8] = { 'name' : "LIME", "code" : "00FF00", "fontColor" : "FFFFFF" };
    colorGroup[9] = { 'name' : "GREEN", "code" : "008000", "fontColor" : "FFFFFF" };
    colorGroup[10] = { 'name' : "AQUA", "code" : "00FFFF", "fontColor" : "FFFFFF" };
    colorGroup[11] = { 'name' : "TEAL", "code" : "008080", "fontColor" : "FFFFFF" };
    colorGroup[12] = { 'name' : "BLUE", "code" : "0000FF", "fontColor" : "FFFFFF" };
    colorGroup[13] = { 'name' : "NAVY", "code" : "000080", "fontColor" : "FFFFFF" };
    colorGroup[14] = { 'name' : "FUCHSIA", "code" : "FF00FF", "fontColor" : "FFFFFF" };
    colorGroup[15] = { 'name' : "PURPLE", "code" : "800080", "fontColor" : "FFFFFF" };
    colorGroup[16] = { 'name' : "BROWN", "code" : "A52A2A", "fontColor" : "FFFFFF" };
    colorGroup[17] = { 'name' : "ORANGE", "code" : "FFA500", "fontColor" : "FFFFFF" };
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: '<?php echo URL::to('/'); ?>/editvendorproductattributevalue',
        type:"post",
        data:{
            'product_id' : '<?php echo $product->id; ?>',
            'vendor_id' : '<?php echo $edit->vender_id; ?>',

        },
        success: function(response){
           response = JSON.parse(response);
                if(response.status === 'success'){
                    if(Object.keys(response.data).length > 0){
                        var colour = response.data.colour;
                        var weight = response.data.weight;
                        var measurement = response.data.measurement;
                        var size = response.data.size;
                        var storage = response.data.storage;
                        debugger;
                        html = '<div class="table-responsive main"><table class="table table-striped- table-bordered table-checkable"><tr>';
                        if(colour !==  undefined){
                            html+= '<th width="15%">Colour</th>';
                        }
                        if(weight !==  undefined){
                            html+= '<th width="20%">Weight (Unit)</th>';
                        }
                        if(measurement !==  undefined){
                            html+= '<th width="20%">Measurement (Unit)</th>';
                        }
                        if(size !==  undefined){
                             html+= '<th width="15%">Size (Unit)</th>';
                        }
                        if(storage !==  undefined){
                            html+= '<th width="20%">Storage</th>';
                        }

                        html+= '<th width="10%">Quantity</th><th width="10%">#</th></tr><tbody class="pestrow">';
                        if(response.dataproductAttrValue.length > 0){

                            var weightVal, measurementVal, storageVal = '';
                            $.each( response.dataproductAttrValue, function( key , attrValue ){
                                html+='<tr class="text-center" id="copyrow"><input type="hidden" name="attrRows[]">';
                                console.log(attrValue);
                                if(colour !==  undefined){
                                    html+= '<td><div class="subbox"><select  name="colour[]" id="colour" value="1" >';
                                    $.each( colorGroup, function( key, value ) {
                                        if(attrValue.colour == value.code){
                                            var selectColour = 'selected';
                                        }else{
                                            var selectColour = '';
                                        }
                                        html+= '<option value="'+value.code+'" style="background-color:#'+value.code+';color:#'+value.fontColor+';" '+selectColour+'>'+value.name+'</option>';
                                    });
                                    html+= '</select></div></td>';
                                }
                                if(weight !==  undefined){
                                    weightVal = attrValue.weight.split(/([0-9]+)/);
                                    html+= '<td><div class="subbox"><input type="number" name="weight[]" value="'+weightVal[1]+'"><select name="weightUnit[]"  id="weightUnit">';
                                    $.each( weight, function( key, value ) {
                                        if(weightVal[2] == value){
                                            var selectWeight = 'selected';
                                        }else{
                                            var selectWeight = '';
                                        }
                                        html+= '<option value="'+value+'" '+selectWeight+'>'+value+'</option>';
                                    });
                                    html+= '</select></div></td>';
                                }
                                if(measurement !==  undefined){
                                    measurementVal = attrValue.measurement.split(/([0-9]+)/);
                                    html+= '<td><div class="subbox"><input type="number" name="measurement[]" value="'+measurementVal[1]+'"><select name="measurementUnit[]"  id="measurementUnit">';
                                    $.each( measurement, function( key, value ) {
                                        if(measurementVal[2] == value){
                                            var selectMeasurement = 'selected';
                                        }else{
                                            var selectMeasurement = '';
                                        }
                                        html+= '<option value="'+value+'" '+selectMeasurement+'>'+value+'</option>';
                                    });
                                    html+= '</select></div></td>';
                                }
                                if(size !==  undefined){
                                    html+= '<td><div class="subbox"><select name="size[]"  id="size">';
                                    $.each( size, function( key, value ) {
                                        if(attrValue.size == value){
                                            var selectSize = 'selected';
                                        }else{
                                            var selectSize = '';
                                        }
                                        html+= '<option value="'+value+'" '+selectSize+'>'+value+'</option>';
                                    });
                                    html+= '</select></div></td>';
                                }
                                if(storage !==  undefined){
                                    storageVal = attrValue.storage.split(/([0-9]+)/);
                                    html+= '<td><div class="subbox"><input type="number" name="storage[]" value="'+storageVal[1]+'"><select name="storageUnit[]"  id="storageUnit">';
                                    $.each( storage, function( key, value ) {
                                        if(storageVal[2] == value){
                                            var selectStorage = 'selected';
                                        }else{
                                            var selectStorage = '';
                                        }
                                        html+= '<option value="'+value+'" '+selectStorage+'>'+value+'</option>';
                                    });
                                    html+= '</select></div></td>';
                                }
                                html+= '<td><div class="subbox"><input type="number" name="quantity[]" value="'+attrValue.quantity+'" style="width: 50px;"></td><th width="10%"><span id="colorSpan"><i  class="icon-cross removeAttr"></i></span></th></tr>';
                            });
                            html+= '</tbody></table></div>';
                        }
                            $('.attrbutton').show();
                    }else{
                        html+= ' <h6>No Attribute Found</h6>';
                        $('.attrbutton').hide();
                    }

                }else{
                    html+= ' <h6>No Attribute Found</h6>';
                    $('.attrbutton').hide();
                }
                $('.productAttr').html(html);
        },error: function(response){
            html+= ' <h6>No Attribute Found</h6>';
            $('.attrbutton').hide();
            $('.productAttr').html(html);
        }
    });
}
getAttribute();


</script>

@endsection





