@extends('layouts.user')

@section('content')
<style>
    .checkbtn{
        height: 12px !important;
    }
    .main{
        background-color: white;
    }
</style>
<div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
            <li><a href="<?php echo URL::to('/'); ?>">Home</a></li>
                    <li><a href="{{ route('vendor_home') }}">Vendor</a></li>
                    <li>Add Product</li>
            </ul>
        </div>
</div>

<div class="container">
    <div class="ps-section__header" style="margin-top:15px;">
        <h3>Add Product</h3>
    </div>
    <div class="ps-section__content">
    	<div class="ps-form__content">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <div class="ps-block--shipping">
                            <div class="ps-block--payment-method">
                                <div class="ps-tabs">
                                    <form class="ps-form--visa" id="m_form" enctype="multipart/form-data" method="post" action="{{route('vendor_save_product')}}">
                                    <input id="token" name="_token" type="hidden" value="{{csrf_token()}}">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Manufacturer</label>
                                                <input type="text" name="manufacturer" id="manufacturer" class="form-control m-input" placeholder="" value="{{Request::old('manufacturer')}}" >
                                                    @error('manufacturer')
                                                    <span class="help-block" role="alert">
                                                        <strong>{{ $errors->first('manufacturer') }}</strong>
                                                    </span>
                                                    @enderror
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Manufacturer Code</label>
                                                <input type="text" name="manufacturer_code" id="manufacturer_code" class="form-control m-input" placeholder="" value="{{Request::old('manufacturer_code')}}" >
                                                    @error('manufacturer_code')
                                                    <span class="help-block" role="alert">
                                                        <strong>{{ $errors->first('manufacturer_code') }}</strong>
                                                    </span>
                                                    @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Product Code</label>
                                                    <input type="text" name="product_code" id="product_code" class="form-control m-input" placeholder="" value="{{Request::old('product_code')}}" required>
                                                        @error('product_code')
                                                        <span class="help-block" role="alert">
                                                            <strong>{{ $errors->first('product_code') }}</strong>
                                                        </span>
                                                        @enderror
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Product Name</label>
                                                    <input type="text" name="product_name" id="product_name" class="form-control m-input" placeholder="" value="{{Request::old('product_name')}}" required>
                                                        @error('product_name')
                                                        <span class="help-block" role="alert">
                                                            <strong>{{ $errors->first('product_name') }}</strong>
                                                        </span>
                                                        @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Specification</label>
                                                    <textarea type="text" name="specification" id="specification" class="form-control m-input specification" placeholder="" value="{{Request::old('specification')}}" required></textarea>
                                                        @error('specification')
                                                        <span class="help-block" role="alert">
                                                            <strong>{{ $errors->first('specification') }}</strong>
                                                        </span>
                                                        @enderror
                                                    
									            </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Description</label>
                                                    <textarea type="text" name="description" id="description" class="form-control m-input description" placeholder="" value="{{Request::old('description')}}" required></textarea>
                                                        @error('description')
                                                        <span class="help-block" role="alert">
                                                        <strong>{{ $errors->first('description') }}</strong>
                                                        </span>
                                                        @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Images</label>
                                                    <input type="file" name="product_photo[]" id="product_photo" class="form-control m-input" placeholder="" value="{{Request::old('product_photo')}}" multiple style="padding-top: 11px;" required>
                                                    <span style="color: #fcb800;"># Select multiple images at one time</span>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Product Video</label>
                                                    <input type="text" name="product_video" id="product_video" class="form-control m-input" placeholder="" value="" required>
                                                        @error('product_video')
                                                        <span class="help-block" role="alert">
                                                        <strong>{{ $errors->first('product_video') }}</strong>
                                                        </span>
                                                        @enderror
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Hashtags</label>
                                                    <select name="hashtags[]" id="hashtags" class="form-control m-input hashtags" placeholder="{{Request::old('hashtags')}}"  required multiple>
                                                            @foreach($hashtags as $hashtag)
                                                                <option value="{{$hashtag->hashtag}}">{{$hashtag->hashtag}}</option>
                                                            @endforeach
                                                    </select>
                                                        @error('hashtags')
                                                            <span class="help-block" role="alert">
                                                            <strong>{{ $errors->first('hashtags') }}</strong>
                                                        </span>
                                                        @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Subcategory:</label>
                                                <table class="table main">
                                                    <tr>
                                                        <td><b>Category</b></td>
                                                        <td><b>Subcategory</b></td>
                                                    </tr>
                                                    @foreach($categories as $category)
                                                    <tr>
                                                        <td>{{ $category->name }}</td>
                                                        <td>
                                                            <div class="row">
                                                            @foreach($category->subcategory as $cat)
                                                            <div class="col-md-4">
                                                                    <input class="checkbtn" type="checkbox" name="category[]" value="{{ $cat->id }}"> <label>{{ $cat->name }}</label>
                                                                
                                                            </div>
                                                            @endforeach
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </table>
                                                @error('category')
                                                        <span class="help-block" role="alert">
                                                            <strong>{{ $errors->first('category') }}</strong>
                                                    </span>
                                                @enderror
                                               
                                        </div>
                                            <div class="form-group">
                                                <label>Attributes</label>
                                                    <table class="table main">
                                                        <tr>
                                                            <td><b>Name</b></td>
                                                            <td><b>Value</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Colour Option</td>
                                                            <td>
                                                                <div class="subbox">
                                                                    <input class="checkbtn" type="checkbox" name="colour" value="1">
                                                                    <label>Colour Picker</label>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Weight (Unit)</td>
                                                            <td>
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="subbox">
                                                                            <input class="checkbtn" type="checkbox" name="weight[]" value="kg"> Kg
                                                                        </div>
                                                                        <div class="subbox">
                                                                            <input class="checkbtn" type="checkbox" name="weight[]" value="tonn"> Tonn
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="subbox">
                                                                            <input class="checkbtn" type="checkbox" name="weight[]" value="oz"> OZ
                                                                        </div>
                                                                        <div class="subbox">
                                                                            <input class="checkbtn" type="checkbox" name="weight[]" value="l"> L
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Measurement (Unit)</td>
                                                            <td>
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="subbox">
                                                                            <input class="checkbtn" type="checkbox" name="measurement[]" value="cm"> CM
                                                                        </div>
                                                                        <div class="subbox">
                                                                            <input class="checkbtn" type="checkbox" name="measurement[]" value="inch"> Inch
                                                                        </div>
                                                                    </div>
                                                                
                                                                    <div class="col-md-4">
                                                                        <div class="subbox">
                                                                            <input class="checkbtn" type="checkbox" name="measurement[]" value="m"> M
                                                                        </div>
                                                                        <div class="subbox">
                                                                            <input class="checkbtn" type="checkbox" name="measurement[]" value="ft"> Ft
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <div class="subbox">
                                                                            <input class="checkbtn" type="checkbox" name="measurement[]" value="sq/ft"> Sq/Ft
                                                                        </div>
                                                                        <div class="subbox">
                                                                            <input class="checkbtn" type="checkbox" name="measurement[]" value="sq/m"> Sq/M
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Size (Unit)</td>
                                                            <td>
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="subbox">
                                                                            <input class="checkbtn" type="checkbox" name="size[]" value="xs"> XS
                                                                        </div>
                                                                        <div class="subbox">
                                                                            <input class="checkbtn" type="checkbox" name="size[]" value="s"> S
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="subbox">
                                                                            <input class="checkbtn" type="checkbox" name="size[]" value="m"> M
                                                                        </div>
                                                                        <div class="subbox">
                                                                            <input class="checkbtn" type="checkbox" name="size[]" value="l"> L
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="subbox">
                                                                            <input class="checkbtn" type="checkbox" name="size[]" value="xl"> XL
                                                                        </div>
                                                                        <div class="subbox">
                                                                            <input class="checkbtn" type="checkbox" name="size[]" value="xxl"> XXL
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Storage</td>
                                                            <td>
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="subbox">
                                                                            <input class="checkbtn" type="checkbox" name="storage[]" value="gb"> GB
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="subbox">
                                                                            <input class="checkbtn" type="checkbox" name="storage[]" value="tb"> TB
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            
                                        
							<div class="form-group submit">
								<button class="ps-btn ps-btn--fullwidth" data-wizard-action="submit" >
									<span>Submit</span>
								</button>
                            </div>
                        </form>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
    </div>
    </div>
</div>

    <script>
        $('.hashtags').tokenize2({delimiter:','});
        $('textarea.description').ckeditor();
        $('textarea.specification').ckeditor();
    </script>
        
@endsection





