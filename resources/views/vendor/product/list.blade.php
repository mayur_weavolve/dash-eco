@extends('layouts.user')

@section('content')
<style>
    .addbtn{
    background-color: #88aa00;
    border-radius: 0;
    height: 40px;
    padding: 0 30px;
    color: #ffffff;
    padding-top: 10px;
    display: inline-block;
    font-size: 16px;
    line-height: 20px;
    border: none;
    font-weight: 600;
    transition: all .4s ease;
    cursor: pointer;
    }

</style>
<div class="ps-page--single">
        <div class="ps-breadcrumb">
            <div class="container">
                 <ul class="breadcrumb">
                    <li><a href="<?php echo URL::to('/'); ?>">Home</a></li>
                    <li><a href="{{ route('vendor_home') }}">Vendor</a></li>
                    <li>Dashboard</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="ps-vendor-dashboard">
            <div class="container">
                <div class="ps-section__header">
                    @if($vendor)
                    <h3>Hello,{{ $vendor->name }}</h3>
                    <p>{{ $vendor->business_name }}<br>{{ $vendor->address }}<br> {{ $vendor->postcode }} <br> GSTIN : {{ $vendor->gst_no }} </p>
                    @endif
                </div>
                <div class="ps-section__content">
                    <ul class="ps-section__links">
                        <li><a href="{{ route('vendor_home') }}">Dashboard</a></li>
                        <li class="active" ><a href="{{route('vendor_product_list')}}">Products</a></li>
                        <li><a href="{{route('vendor_recent_order_list')}}">Live Order</a></li>
                        <li><a href="{{route('vendor_completed_order_list')}}">Completed Order</a></li>
                        <li><a href="{{route('vendor_payment_view')}}">Payment</a></li>
                        <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                </form>
                        </li>

                    </ul>
                    <div class="ps-block--vendor-dashboard">
                        <div class="ps-block__header">
                            <h3>Products</h3>
                        </div>
                        <div class="ps-block__content">
                            <form class="ps-form--vendor-datetimepicker" action="#" method="get">
                                <div class="row">
                                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-12 ">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text" id="time-from">From</span></div>
                                            <input class="form-control" aria-label="Username" aria-describedby="time-from">
                                        </div>
                                    </div>
                                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-12 ">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text" id="time-form">To</span></div>
                                            <input class="form-control" aria-label="Username" aria-describedby="time-to">
                                        </div>
                                    </div>
                                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-12 ">
                                        <button class="ps-btn"><i class="icon-sync2"></i> Update</button>
                                    </div>
                                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12" style="text-align: end;">
                                            {{--  <a class="addbtn" href="{{route('vendor_product_price')}}"><i class="icon-plus"></i> Add Product Price</a></button>  --}}
                                            <a class="addbtn" href="{{route('vendor_product_price_add', $vendor->id)}}"><i class="icon-plus"></i> Add Product Price</a></button>
                                        </div>
                                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12" style="text-align: end;">
                                        <a class="addbtn" href="{{route('vendor_add_product')}}"><i class="icon-plus"></i> Add Product</a></button>
                                    </div>
                                </div>
                            </form>

                            <div class="table-responsive">
                                <table class="table ps-table ps-table--vendor">
                                    <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <!-- <th>Sold</th> -->
                                            <th>Discount</th>
                                            <th>Selling Price</th>
                                            <th>Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($products as $product)

                                        <tr>
                                            <td>{{$product->product_code}}</td>
                                            <td><a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$product->product_name)), 'id' => $product->product_id])}}">{{$product->product_name}}</a></td>
                                            <td>₹ {{ number_format($product->mrp)}}</td>
                                            <!-- <td>100</td> -->
                                            <td>{{$product->discount}}%</td>
                                            <td>₹ {{ number_format($product->final_price)}}</td>
                                            <td><a href="{{route('product_detail_edit_view', ['id' =>$product->pro_id, 'vid' => $product->vender_id])}}"><i class="icon-pencil5"></i></a></td>
                                        </tr>
                                        @endforeach
                                        <!-- <tr>
                                            <td colspan="3"><strong>Total</strong></td>
                                            <td><strong></strong></td>
                                            <td colspan="3"><strong></strong></td>
                                        </tr> -->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        @endsection
