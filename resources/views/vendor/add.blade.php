@extends('layouts.admin')

@section('content')

<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Add New Vendor
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			    <ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('vendor') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-reply"></i>
						    <span>Back</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item"></li>
			    </ul>
	        </div>
	    </div>



	<form class="m-form m-form--label-align-left- m-form--state-" id="m_form" enctype="multipart/form-data" method="post" action="{{route('vendor_add')}}">
	{{csrf_field() }}
		<div class="m-portlet__body">
			<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
				<div class="row">
					<div class="col-xl-10 offset-xl-1">
						<div class="m-form__section">
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Name:</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="text" name="name" id="name" class="form-control m-input" placeholder="" value="{{ old('name') }}" required>
                                        @error('name')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @enderror
                                </div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Email Id:</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="email" name="email" id="email" class="form-control m-input" placeholder="" value="{{ old('email') }}" required>
                                        @error('email')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @enderror
                                </div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Password:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="password" name="password" id="password" class="form-control m-input" placeholder="" value="{{ old('password') }}" required>
											@error('password')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('password') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Business Name:</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="text" name="business_name" id="business_name" class="form-control m-input" placeholder="" value="{{ old('business_name') }}" required>
                                        @error('business_name')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('business_name') }}</strong>
                                        </span>
                                        @enderror
                                </div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* NickName:</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="text" name="nickname" id="nickname" class="form-control m-input" placeholder="" value="{{ old('nickname') }}" required>
                                        @error('nickname')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('nickname') }}</strong>
                                        </span>
                                        @enderror
                                </div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Whatsapp No:</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="tel" name="whatsapp_no" id="whatsapp_no" class="form-control m-input" placeholder="" value="{{ old('whatsapp_no') }}" required>
                                        @error('whatsapp_no')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('whatsapp_no') }}</strong>
                                        </span>
                                        @enderror
                                </div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Mobile No:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="tel" name="mobileno" id="mobileno" class="form-control m-input" placeholder="" value="{{ old('mobileno') }}" required>
											@error('mobileno')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('mobileno') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Address:</label>
								<div class="col-xl-8 col-lg-8">
								<textarea type="text" name="address" id="address" class="form-control m-input" placeholder="" value="{{ old('address') }}" required></textarea>
										@error('address')
										<span class="help-block" role="alert">
											<strong>{{ $errors->first('address') }}</strong>
										</span>
										@enderror
								</div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Latitude:</label>
								<div class="col-xl-8 col-lg-8">
									<input type="text" name="latitude" id="latitude" class="form-control m-input" placeholder="" value="{{ old('latitude') }}" required>
										@error('latitude')
										<span class="help-block" role="alert">
											<strong>{{ $errors->first('latitude') }}</strong>
										</span>
										@enderror
								</div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Longitude:</label>
								<div class="col-xl-8 col-lg-8">
									<input type="text" name="longitude" id="longitude" class="form-control m-input" placeholder="" value="{{ old('longitude') }}" required>
										@error('longitude')
										<span class="help-block" role="alert">
											<strong>{{ $errors->first('longitude') }}</strong>
										</span>
										@enderror
								</div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* GST No:</label>
								<div class="col-xl-8 col-lg-8">
								<input type="text" name="gst_no" id="gst_no" class="form-control m-input" placeholder="" value="{{ old('gst_no') }}" required>
										@error('gst_no')
										<span class="help-block" role="alert">
											<strong>{{ $errors->first('gst_no') }}</strong>
										</span>
										@enderror
								</div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* PostCode:</label>
									<div class="col-xl-8 col-lg-8">
                                    <input type="tel" name="postcode" id="postcode" class="form-control m-input" placeholder="" value="{{ old('postcode') }}" required>
											@error('postcode')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('postcode') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Photo:</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="file" name="photo" id="photo" class="form-control m-input" placeholder="" value="">
                                </div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Business Category:</label>
								<div class="col-xl-8 col-lg-8">
									<select name="business_category[]" class="form-control m-input" multiple="multiple" required>
										<?php foreach ($categories as $category) { ?>
										<option value="{{$category->id}}">{{$category->name}}</option>
										<?php } ?>
									</select>
									@error('business_category')
                       					<span class="help-block" role="alert">
                           					<strong>{{ $errors->first('business_category') }}</strong>
                        				</span>
                    				@enderror
								</div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Business Established:</label>
								<div class="col-xl-8 col-lg-8">
									<input type="text" name="business_established" id="business_established" class="form-control m-input" placeholder="" value="{{ old('business_established') }}" required>
										@error('business_established')
										<span class="help-block" role="alert">
											<strong>{{ $errors->first('business_established') }}</strong>
										</span>
										@enderror
								</div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Business hours and days:</label>
								<div class="col-xl-8 col-lg-8">
								<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
									<thead>
										<tr>
											<th><b>Day</b></th>
											<th><b>Start Time</b></th>
											<th><b>End Time</b></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>SUN</td>
											<td><input type="time" name="start_time[]" class="form-control m-input" value=""></td>
											<td><input type="time" name="end_time[]" class="form-control m-input" value=""></td>
										</tr>
										<tr>
											<td>MON</td>
											<td><input type="time" name="start_time[]" class="form-control m-input" value=""></td>
											<td><input type="time" name="end_time[]" class="form-control m-input" value=""></td>
										</tr>
										<tr>
											<td>TUE</td>
											<td><input type="time" name="start_time[]" class="form-control m-input" value=""></td>
											<td><input type="time" name="end_time[]" class="form-control m-input" value=""></td>
										</tr>
										<tr>
											<td>WED</td>
											<td><input type="time" name="start_time[]" class="form-control m-input" value=""></td>
											<td><input type="time" name="end_time[]" class="form-control m-input" value=""></td>
										</tr>
										<tr>
											<td>THUR</td>
											<td><input type="time" name="start_time[]" class="form-control m-input" value=""></td>
											<td><input type="time" name="end_time[]" class="form-control m-input" value=""></td>
										</tr>
										<tr>
											<td>FRI</td>
											<td><input type="time" name="start_time[]" class="form-control m-input" value=""></td>
											<td><input type="time" name="end_time[]" class="form-control m-input" value=""></td>
										</tr>
										<tr>
											<td>SAT</td>
											<td><input type="time" name="start_time[]" class="form-control m-input" value=""></td>
											<td><input type="time" name="end_time[]" class="form-control m-input" value=""></td>
										</tr>
									</tbody>
								</table>
								</div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label"> Note:</label>
								<div class="col-xl-8 col-lg-8">
									<input type="text" name="notes" id="notes" class="form-control m-input" placeholder="" value="{{ old('notes') }}" >
										@error('notes')
										<span class="help-block" role="alert">
											<strong>{{ $errors->first('notes') }}</strong>
										</span>
										@enderror
								</div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Status:</label>
								<div class="col-xl-8 col-lg-8">
									<select name="vendor_block" class="form-control m-input" required>
										<option value="">Select</option>
										<option value="0">Active</option>
										<option value="1">Inactive</option>
									</select>
									@error('vendor_block')
									<span class="help-block" role="alert">
										<strong>{{ $errors->first('vendor_block') }}</strong>
									</span>
									@enderror
								</div>
							</div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Bank UPI Id:</label>
								<div class="col-xl-8 col-lg-8">
									<input type="text" name="bank_upi" id="bank_upi" class="form-control m-input" placeholder="" value="{{ old('bank_upi') }}" required>
									@error('bank_upi')
									<span class="help-block" role="alert">
										<strong>{{ $errors->first('bank_upi') }}</strong>
									</span>
									@enderror
								</div>
							</div>

							<div class="col-lg-8 m--align-right">
								<button class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit" >
									<span>
										<i class="la la-check"></i>&nbsp;&nbsp;
											<span>Submit</span>
									</span>
								</button>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
</div>
</div>
</div>

@endsection
