@extends('layouts.user')

@section('content')
<div class="ps-page--single">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="<?php echo URL::to('/'); ?>">Home</a></li>
                    <li><a href="{{ route('vendor_home') }}">Vendor</a></li>
                    <li>Dashboard</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="ps-vendor-dashboard pro">
        <div class="container">
            <div class="ps-section__header">
                @if($vendor)
                    <h3>Hello,{{ $vendor->name }}</h3>
                    <p>{{ $vendor->business_name }}<br>{{ $vendor->address }}<br> {{ $vendor->postcode }} <br> GSTIN : {{ $vendor->gst_no }} </p>
                @endif
            </div>
            <div class="ps-section__content">
                <ul class="ps-section__links">
                    <li class="active"><a href="javascript:void(0);">Dashboard</a></li>
                    <li><a href="{{route('vendor_product_list')}}">Products</a></li>
                    <li><a href="{{route('vendor_recent_order_list')}}">Live Order</a></li>
                    <li><a href="{{route('vendor_completed_order_list')}}">Completed Order</a></li>
                    <li><a href="{{route('vendor_payment_view')}}">Payment</a></li>
                    <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                            </form>
                    </li>

                </ul>
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                        <figure class="ps-block--vendor-status">
                            <figcaption>Live Orders</figcaption>
                            <table class="table ps-table ps-table--vendor">
                                <thead>
                                    <tr>
                                        <th>Order ID</th>
                                        <th>Product</th>
                                        <th>Totals</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($orders as $order)
                                    
                                    <tr>
                                        <td><a href="javascript:void(0);">{{$order->order_id}}</a>
                                            <?php
                                                $date=date_create($order->created_at);
                                                $createdAt = date_format($date,"M d, Y");
                                            ?>
								            
                                            <p>{{$createdAt}}</p>
                                        </td>
                                        <td><a href="javascript:void(0);">{{$order->quantity}} x {{$order->product_name}}</a>
                                           
                                        </td>
                                        <td>
                                            <p>₹{{ number_format($order->total_amount)}}</p>
                                           
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="ps-block__footer"><a href="{{route('vendor_recent_order_list')}}">View All Orders</a></div>
                        </figure>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                        <figure class="ps-block--vendor-status">
                            <figcaption>Recent Products</figcaption>
                            <table class="table ps-table ps-table--vendor">
                                <thead>
                                    <tr>
                                        <th><i class="icon-picture"></i></th>
                                        <th>Product</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($products as $product)

                                        <tr>
                                            <td><a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$product->product_name)), 'id' => $product->product_id])}}"><img src="/product_photo/{{$product->image}}" alt="" width="50"></a></td>
                                            <td><a href="{{route('product_detail', ['product' =>strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-',$product->product_name)), 'id' => $product->product_id])}}">{{$product->product_name}}</a>
                                                <p>₹ {{ number_format($product->mrp)}}</p>
                                            </td>
                                            <td>
                                                <p class="ps-tag--in-stock">@if($product->stock > 0) In Stock @else  In Stock @endif</p>
                                                <!-- <p>Published: </p> -->
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            <div class="ps-block__footer"><a href="{{route('vendor_product_list')}}">View All Products</a></div>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
