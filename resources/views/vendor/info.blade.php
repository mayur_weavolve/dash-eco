@extends('layouts.admin')
@section('content')
<style>
    b{
        font-weight: bold !important;
    }
    .row{
        line-height: 30px;
    }
</style>
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Vendor Info
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item">

			</ul>
	    </div>
	</div>
	<div class="m-portlet__body">
                                <div class="row main">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div style="display:inline"><b>Name : </b></div>&nbsp;
                                        <div style="display:inline">{{$info->name}}</div>
                                    </div>
                                    <div class="row">
                                        <div style="display:inline"><b>Email :</b> </div>&nbsp;
                                        <div style="display:inline">{{$info->user->email}}</div>
                                    </div>
                                    
                                    <div class="row">
                                        <div style="display:inline"><b>Whatsapp No :</b> </div>&nbsp;
                                        <div style="display:inline">{{$info->user->whatsapp_no}}</div>
                                    </div>
                    
                                    <div class="row">
                                        <div style="display:inline"><b>Mobile No :</b> </div>&nbsp;
                                        <div style="display:inline">{{$info->user->mobileno}}</div>
                                    </div>
                    
                                    
                                    <div class="row">
                                        <div style="display:inline"><b>Address : </b></div>&nbsp;
                                        <div style="display:inline">{{$info->address}}</div>
                                    </div>
                                    
                                    <div class="row">
                                        <div style="display:inline"><b>PostCode: </b></div>&nbsp;
                                        <div style="display:inline">{{$info->postcode}}</div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div style="display:inline"><b>Business Name : </b></div>&nbsp;
                                        <div style="display:inline">{{$info->business_name}}</div>
                                    </div>
                    
                                    <div class="row">
                                        <div style="display:inline"><b>Nick Name : </b></div>&nbsp;
                                        <div style="display:inline">{{$info->nickname}}</div>
                                    </div>
                    
                                    <div class="row">
                                        <div style="display:inline"><b>GST No :</b> </div>&nbsp;
                                        <div style="display:inline">{{$info->gst_no}}</div>
                                    </div>
                                    
                                    <div class="row">
                                        <div style="display:inline"><b>Business Established : </b></div>&nbsp;
                                        <div style="display:inline">{{$info->business_established}}</div>
                                    </div>
                                    
                                    <div class="row">
                                        <div style="display:inline"><b>Bank Upi : </b></div>&nbsp;
                                        <div style="display:inline">{{$info->bank_upi}}</div>
                                    </div>
                                </div>        
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection
