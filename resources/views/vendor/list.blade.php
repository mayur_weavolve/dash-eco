@extends('layouts.admin')
@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Vendor List
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('vendor_add') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
						    <span>Add Vendor</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item">

			</ul>
	    </div>
	</div>
	<div class="m-portlet__body">
			<div class = "row">
			
				<div class = "col-md-10">
					<form action="{{route('vendor')}}" method ="get">
					<input type="search" name="search"class="form-control" placeholder="Search Vendor Name or Nickname "><br>
				</div>
				<div class="col-md-2 text-right">
					<div class="row">
						<div class="col-md-6">
							<button type = "submit" class ="btn btn-primary">Search</button><br>
						</div>
						<div class="col-md-6">
								<a href="{{route('vendor')}}" class="btn btn-primary">Clear</a>
						</div>
					</div>
					</form>
				</div>
			
				
			</div>
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
					<!-- <th> ID</th> -->
                    <!-- <th>User Id</th> -->
                    <th>Name</th>
                    <th>Business Name</th>
                    <th>Mobile No</th>
                    <th>Address</th>
                    <th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			@if(count($vendors) == 0)
			<tr><td colspan="8"  style="text-align:center;">No record found</td>
			@endif
			<?php foreach ($vendors as $vendor) { ?>
			<tr>
				<!-- <td> <?php echo $vendor->id ;?> </td> -->
                <!-- <td>{{ $vendor->user_id}}</td> -->
                <td>{{ $vendor->name}}</td>
                <td>{{ $vendor->business_name}}</td>
                <td>{{ $vendor->user->mobileno}}</td>
				<td>{{ $vendor->address}}</td>
				<td class="text-center">
                    @if ($vendor->vendor_block == '0')
                        <span class="m-badge  m-badge--success m-badge--wide">Active</span>
                    @else
                        <span class="m-badge  m-badge--danger m-badge--wide">Inactive</span>
                    @endif
                </td>
				<td>
						<a href="{{route('vendor_edit',[$vendor->id])}}"  title="Edit"><i class="m-menu__link-icon fa fa-edit"></i></a>  &nbsp;&nbsp;
						<!-- <a href="" onclick="return confirm('Are you sure you want to delete this Store?');"  title="Delete"><i class="m-menu__link-icon fa fa-trash-alt"></i></a> &nbsp;&nbsp; -->
						<a href="{{route('product_price',[$vendor->id])}}"  title="product price"><i class="m-menu__link-icon fa fa-tag"></i></a>  &nbsp;&nbsp;
				</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
		{{ $vendors->links('paginator.default') }}
	</div>
</div>
</div>
</div>
</div>

@endsection
