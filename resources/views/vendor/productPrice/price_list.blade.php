@extends('layouts.user')

@section('content')
<style>
    .addbtn{
    background-color: #88aa00;
    border-radius: 0;
    height: 40px;
    padding: 0 30px;
    color: #ffffff;
    padding-top: 10px;
    display: inline-block;
    font-size: 16px;
    line-height: 20px;
    border: none;
    font-weight: 600;
    transition: all .4s ease;
    cursor: pointer;
    }
</style>
<div class="ps-page--single">
        <div class="ps-breadcrumb">
            <div class="container">
                 <ul class="breadcrumb">
                    <li><a href="<?php echo URL::to('/'); ?>">Home</a></li>
                    <li><a href="{{ route('vendor_home') }}">Vendor</a></li>
                    <li>Product</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="ps-vendor-dashboard">
            <div class="container">
                <div class="ps-section__header">
                    <h3>Hello,{{ $vendor->name }}</h3>
                    <p>{{ $vendor->business_name }}<br>{{ $vendor->address }}<br> {{ $vendor->postcode }} <br> GSTIN : {{ $vendor->gst_no }} </p>
                </div>
                <div class="ps-section__content">
                    <ul class="ps-section__links">
                        <li><a href="{{ route('vendor_home') }}">Dashboard</a></li>
                        <li class="active" ><a href="{{route('vendor_product_list')}}">Products</a></li>
                        <li><a href="javascript:void(0);">Order</a></li>
                        <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                </form>
                        </li>

                    </ul>
                    <div class="ps-block--vendor-dashboard">
                        <div class="ps-block__header">
                            <h3>Sale Report</h3>
                        </div>
                        <div class="ps-block__content">
                            <form class="ps-form--vendor-datetimepicker" action="index.html" method="get">
                                <div class="row">
                                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-12 ">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text" id="time-from">From</span></div>
                                            <input class="form-control" aria-label="Username" aria-describedby="time-from">
                                        </div>
                                    </div>
                                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-12 ">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text" id="time-form">To</span></div>
                                            <input class="form-control" aria-label="Username" aria-describedby="time-to">
                                        </div>
                                    </div>
                                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-12 ">
                                        <button class="ps-btn"><i class="icon-sync2"></i> Update</button>
                                    </div>
                                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12" style="text-align: end;">
                                            
                                    </div>
                                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12" style="text-align: end;">
                                            <a class="addbtn" href="{{route('vendor_product_price_add', 10)}}"><i class="icon-plus"></i> Add Product Price</a></button>
                                    </div>
                                    
                                </div>
                            </form>
                            
                            <div class="table-responsive">
                                <table class="table ps-table ps-table--vendor">
                                    <thead>
                                        <tr>
                                                <th>Product Name</th>
                                                <th>MRP</th>
                                                <th>Final Price</th>
                                                <th>Free Delivery / Pickup</th>
                                                <th>Mode</th>
                                                <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            @if(count($product_prices) == 0)
                                            <tr><td colspan="8"  style="text-align:center;">No record found</td>
                                            @endif
                                            <?php foreach ($product_prices as $product_price) { ?>
                                            <tr>
                                                    <td>{{ $product_price->product->product_name}}</td>
                                                    <td> {{ number_format($product_price->mrp)}}</td>
                                                    <td> {{ number_format($product_price->final_price)}}</td>
                                                    <td>@if($product_price->free_delivery == 0)
                                                            Pickup Only
                                                        @elseif($product_price->free_delivery == '5')
                                                            5KM
                                                        @elseif($product_price->free_delivery == '7')
                                                            7KM
                                                        @elseif($product_price->free_delivery == '10')
                                                            10KM
                                                        @elseif($product_price->free_delivery == '15')
                                                            15KM
                                                        @elseif($product_price->free_delivery == '20')
                                                            20KM
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    <td>@if($product_price->mode == 1)
                                                            Same day
                                                        @elseif($product_price->mode == 0)
                                                            Next Day
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                <td>
                                                        <a href="{{route('product_price_edit',[$product_price->id,$vendor->id])}}"  title="Edit"><i class="m-menu__link-icon fa fa-edit"></i></a>  &nbsp;&nbsp;
                                                        <a href="{{route('delete_price',[$product_price->id,$vendor->id])}}" onclick="return confirm('Are you sure you want to delete this price?');"  title="Delete"><i class="m-menu__link-icon fa fa-trash-alt"></i></a> &nbsp;&nbsp;
                                                </td>
                                                </tr>
                                            <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        @endsection
