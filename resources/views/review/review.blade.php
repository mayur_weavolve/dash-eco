@extends('layouts.admin')
@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Review List
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools"></div>
	</div>
	<div class="m-portlet__body">
			<div class = "row">

				<div class = "col-md-10">
					<form action="{{route('review_list')}}" method ="get">
					<input type="search" name="search"class="form-control" placeholder="Search Review"><br>
				</div>
				<div class="col-md-2 text-right">
					<div class="row">
						<div class="col-md-6">
							<button type = "submit" class ="btn btn-primary">Search</button><br>
						</div>
						<div class="col-md-6">
								<a href="{{route('review_list')}}" class="btn btn-primary">Clear</a>
						</div>
					</div>
					</form>
				</div>


			</div>
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
					<th>Product</th>
                    <th>Name</th>
                    <th>Mobile Number</th>
                    <th>Rating</th>
                    <th>Review</td>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			@if(count($reviews) == 0)
			<tr><td colspan="8"  style="text-align:center;">No record found</td>
			@endif
			<?php foreach ($reviews as $review) { ?>
			<tr>
				<td>{{ $review->product->product_name}}</td>
                <td>{{$review->name}}</td>
                <td>{{ $review->mobile_number}}</td>
                <td>{{$review->rating}}</td>
                <td>{{$review->review}}</td>
				<td style="text-align: center;">@if($review->status == 'Pending') <span class="m-badge  m-badge--primary m-badge--wide">Pending</span> @elseif($review->status == 'Inactive')<span class="m-badge  m-badge--danger m-badge--wide">InActive</span> @else<span class="m-badge  m-badge--success m-badge--wide">Active</span> @endif</td>
				<td>
					<a href="{{route('review_edit_view',[$review->id])}}" title="Edit"><i class="m-menu__link-icon fa fa-edit"></i></a>  &nbsp;&nbsp;
				</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
        {{ $reviews->links('paginator.default') }}
	</div>
</div>
</div>
</div>
</div>

@endsection
