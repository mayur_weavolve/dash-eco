@extends('layouts.admin')

@section('content')

<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
                        Edit Review
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			    <ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('review_list') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-reply"></i>
						    <span>Back</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item"></li>
			    </ul>
	        </div>
	    </div>



	<form class="m-form m-form--label-align-left- m-form--state-" id="m_form" enctype="multipart/form-data" method="post" action="{{route('review_edit')}}">
	{{csrf_field() }}
		<div class="m-portlet__body">
			<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
				<div class="row">
					<div class="col-xl-8 offset-xl-2">
						<div class="m-form__section">
                            <div class="form-group m-form__group row">
                                <label for="name" class="col-xl-3 col-lg-3 col-form-label">* Product Name :</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="hidden" name="id" value="{{$edit->id}}">
                                    <input type="text" name="product_name" class="form-control m-input" value="{{$edit->product->product_name}}" readonly>
                                    @error('product_name')
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('product_name') }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
							<div class="form-group m-form__group row">
								<label for="name" class="col-xl-3 col-lg-3 col-form-label">* Name :</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="text" name="name" class="form-control m-input" value="{{$edit->name}}" readonly>
                                    @error('name')
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                    <label for="email" class="col-xl-3 col-lg-3 col-form-label">* Mobile Number :</label>
                                    <div class="col-xl-8 col-lg-8">
                                        <input type="text" name="mobile_number" class="form-control m-input" value="{{$edit->mobile_number}}" readonly> 
                                        @error('mobile_number')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('mobile_number') }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                            </div>
                            <div class="form-group m-form__group row">
                                    <label for="rating" class="col-xl-3 col-lg-3 col-form-label">* Rating :</label>
                                    <div class="col-xl-8 col-lg-8">
                                        <input type="text" name="rating" class="form-control m-input" value="{{$edit->rating}}" readonly>
                                        @error('rating')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('rating') }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                            </div>
                            <div class="form-group m-form__group row">
                                    <label for="review" class="col-xl-3 col-lg-3 col-form-label">* Review  :</label>
                                    <div class="col-xl-8 col-lg-8">
                                        <input type="text" name="review" class="form-control m-input" value="{{$edit->review}}" readonly>
                                        @error('review')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('review') }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                            </div>
                            <div class="form-group m-form__group row">
                                    <label for="name" class="col-xl-3 col-lg-3 col-form-label">* Status:</label>
                                    <div class="col-xl-8 col-lg-8">
                                        <select name="status" class="form-control m-input" value = "" required>
                                            <option value="Pending" @if($edit->status == 'Pending') selected @endif>Pending</option>
                                            <option value="Active" @if($edit->status == 'Active') selected @endif>Active</option>
                                            <option value="Inactive" @if($edit->status == 'Inactive') selected @endif >InActive</option>
                                            
                                        </select>
                                        @error('status')
                                            <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('status') }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                            </div>
							<div class="col-lg-8 m--align-right">
								<button class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit" >
									<span>
										<i class="la la-check"></i>&nbsp;&nbsp;
											<span>Submit</span>
									</span>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
</div>
</div>
</div>

@endsection
