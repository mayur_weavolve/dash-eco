<!DOCTYPE html>

<html lang="en">

	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>DASH-ECO | Login Page</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>

		<!--end::Web font -->

		<!--begin:: Global Mandatory Vendors -->
		<link href="<?php echo URL('/'); ?>/admin/assets/vendors/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

		<!--begin::Global Theme Styles -->
		<link href="<?php echo URL('/'); ?>/admin/assets/demo/base/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Global Theme Styles -->
		<link rel="shortcut icon" href="<?php echo URL('/'); ?>/user/img/coming-soon-logo.png" />
          <style>
            .m-login.m-login--1 .m-login__content .m-login__welcome, .m-login.m-login--1 .m-login__content .m-login__msg  {
                padding: 50px;
                text-align: center;
                font-weight:500;
            }
            .btn-focus {
                color: #fff;
                background-color: #ffcc00 !important;
                border-color: #ffcc00 !important;
            }
          </style>
    </head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_login">
				<div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
					<div class="m-stack m-stack--hor m-stack--desktop">
						<div class="m-stack__item m-stack__item--fluid">
							<div class="m-login__wrapper">
								<div class="m-login__logo">
									<a href="javascript:void(0);">
										<img src="<?php echo URL('/'); ?>/user/img/coming-soon-logo.png" width="90px">
									</a>
								</div>
								<div class="m-login__signin">
									<div class="m-login__head">
										<h3 class="m-login__title">Sign In To Dash Admin</h3>
									</div>
                                    <form class="m-login__form m-form" method="POST" action="{{ route('login') }}">
                                        @csrf
                                        <div class="form-group m-form__group">
											<input class="form-control m-input"  id="email" type="text" placeholder="Username" name="email" value="{{ old('email') }}" autocomplete="email" autofocus required>
											@error('email')
                                    			<span class="help-block" role="alert">
                                        			<strong>{{ $errors->first("email") }}</strong>
                                    			</span>
                                            @enderror
										</div>
                                        <div class="form-group m-form__group">
											<input class="form-control m-input m-login__form-input--last" id="password" type="password" placeholder="Password" name="password" required autocomplete="current-password">
											@error('password')
                                    		<span class="help-block" role="alert">
                                        		<strong>{{ $errors->first('password') }}</strong>
                                    		</span>
                                			@enderror
										</div>

                                        <!-- <div class="form-group row">
                                            <div class="col-md-6 offset-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                                    <label class="form-check-label" for="remember">
                                                        {{ __('Remember Me') }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row mb-0">
                                            <div class="col-md-8 offset-md-4">
                                                <button type="submit" class="btn btn-primary">
                                                    {{ __('Login') }}
                                                </button>

                                                @if (Route::has('password.request'))
                                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                                        {{ __('Forgot Your Password?') }}
                                                    </a>
                                                @endif
                                            </div>
                                        </div> -->
                                        <div class="m-login__form-action">
											<button type="submit" id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">Sign In</button>
										</div>

                                    </form>
                                </div>
							</div>
						</div>
					</div>
				</div>
                <div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1	m-login__content m-grid-item--center" style=" background: #ffcc00;">
					<div class="m-grid__item">
						<h3 class="m-login__welcome">Sell local <br> Buy local <br> Support local</h3>
						<p class="m-login__msg">
                        Buy from local shops online and get same day home delivery or pickup. Shops ranges from Hardware, computers, smart phones mobile, grocery, clothes, shoes, stationary, home decor, construction products and lot more
						</p>
					</div>
				</div>
			</div>
		</div>
        </body>

	<!-- end::Body -->
</html>

