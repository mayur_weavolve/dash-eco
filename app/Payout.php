<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Payout extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','order_id','suborder_id','user_id','product_id','price','quantity','amount','is_paid ','transaction_id','paid_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    public function order()
    {
        return $this->hasOne('App\Order','id','order_id');
    }
    public function product()
    {
        return $this->hasOne('App\Product','id','product_id');
    }
    public function productImage()
    {
        return $this->hasOne('App\ProductImage','product_id','product_id');
    }
    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }

    public function vendor()
    {
        return $this->hasOne('App\Vendor','id','vendor_id');
    }

    public function vendorPaymentAccounts(){
        return $this->hasOne('App\VendorPaymentAccount','vendor_id','vendor_id');
    }
}
