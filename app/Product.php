<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\City;
class Product extends Model
{
    protected $fillable = [
        'manufacturer', 'manufacturer_code', 'product_code','product_name','product_photo',
        'description', 'specification','category','hashtags','is_deleted',

    ];

    public function productSubcategory()
    {
        return $this->hasOne('App\ProductSubcategory','product_id','id');
    }

    public function productSubcategories()
    {
        return $this->hasMany('App\ProductSubcategory','product_id','id');
    }

    public function productHashtag()
    {
        return $this->hasOne('App\ProductHashtag','product_id','id');
    }

    public function productHashtags()
    {
        return $this->hasMany('App\ProductHashtag','product_id','id');
    }

    public function productImage()
    {
        return $this->hasOne('App\ProductImage','product_id','id');
    }

    public function productImages()
    {
        return $this->hasMany('App\ProductImage','product_id','id');
    }

    public function productPrice()
    {
        return $this->hasMany('App\ProductPrice','product_id','id');
    }

    public function reviews()
    {
        return $this->hasMany('App\Review','product_id','id');
    }

    public function productAttributeValue()
    {
        return $this->hasOne('App\ProductAttributeValue','product_id','id');
    }

    public function getQuantity($products,$vendorId){
       $productAttributeValue = ProductAttributeValue::where('product_id',$products->id)->where("vendor_id", $vendorId)->orderBy('id','desc')->get()->first();
            if($productAttributeValue != null){
                return  $productAttributeValue->quantity;
            }else{
                return  0;
            }
            return 0;
    }

    public function getDistance($vendor){
        $earthRadius = 6371000;
        // $earthRadius = 3259;
        $latitudeVendor = $vendor->latitude;
        $longitudeVendor = $vendor->longitude;

        $pincode = session("pincode");
        $latitudeUser =  session("latitude");
        $longitudeUser = session("longitude");
        if($pincode == ''){
            $pincode = '388001';
        }
        if($latitudeUser == ''){
              $city = City::where('zipcode', $pincode)->get()->first();
              $latitudeUser = $city->latitude;
              $longitudeUser = $city->longitude;
        }

      // convert from degrees to radians
        $latFrom = deg2rad($latitudeVendor);
        $lonFrom = deg2rad($longitudeVendor);
        $latTo = deg2rad($latitudeUser);
        $lonTo = deg2rad($longitudeUser);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
            cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        $distanceInMeter = $angle * $earthRadius;
        return  round($distanceInMeter / 1000);

        exit;
    }


}
