<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductAttributeValue extends Model
{
    protected $fillable = [
        'product_id', 'colour', 'weight','measurement','size','storage','quantity'
    ];

    public function productPrice()
    {
        return $this->hasOne('App\ProductPrice','id','product_price_id');
    }

}
