<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Product;

class ProductImport implements ToModel,WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function model(array $row)
    {
        $check = Product::where('product_code',$row['product_code'])->get()->first();
        if(!$check)
        {
            return new Product([
                'manufacturer'=> $row['manufacturer'],
                'manufacturer_code'=> $row['manufacturer_code'], 
                'product_code'=> $row['product_code'], 
                'product_name'=> $row['product_name'], 
                'description'=> $row['description'], 
                'specification'=> $row['specification'], 
                'category'=> $row['category'], 
                'hashtags'=> $row['hashtags'], 
                'is_deleted'=> 0, 
            ]);
        }
        else{
            $check->manufacturer = $row['manufacturer'];
            $check->manufacturer_code = $row['manufacturer_code'];
            $check->product_code = $row['product_code'];
            $check->product_name = $row['product_name'];
            $check->description = $row['description'];
            $check->specification = $row['specification'];
            $check->category = $row['category'];
            $check->hashtags = $row['hashtags'];
            $check->save();
        }
        
    }
}
