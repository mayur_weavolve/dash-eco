<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Auth;
class SubOrders extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id','vendor_id','product_id','quantity','amount','product_attribute','status','security_code'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public function orders()
    {
        return $this->hasOne('App\Order','id','order_id');
    }
    public function product()
    {
        return $this->hasOne('App\Product','id','product_id');
    }
    public function productImage()
    {
        return $this->hasOne('App\ProductImage','product_id','product_id');
    }
    public function productPrice()
    {
        return $this->hasOne('App\ProductPrice','id','product_price_id');
    }
    public function productAttributeValue()
    {
        return $this->hasOne('App\ProductAttributeValue','id','product_attribute_value_id');
    }
    public function vendor()
    {
        return $this->hasOne('App\Vendor','id','vendor_id');
    }

    public function payment()
    {
        return $this->hasOne('App\Payment','order_id','order_id');
    }
}
