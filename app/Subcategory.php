<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $fillable = [
        'category_id','name','status'
    ];

    public $table = "subcategories";


    public function category()
    {
        return $this->hasOne('App\Category','id','category_id');
    }
}
