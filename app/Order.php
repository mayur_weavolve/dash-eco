<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Auth;
class Order extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id','user_id','tracking_number','amount','total_amount','status','note'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public function subOrders()
    {
        return $this->hasMany('App\SubOrders','order_id','id');
    }
    public function subOrder()
    {
        return $this->hasMany('App\SubOrders','order_id','id')->where('status','!=','cancel_by_vendor')->orWhere('status','!=','pending_payment')->get();
    }

    public function shipping()
    {
        return $this->hasOne('App\OrderShippingDetail','order_id','id');
    }

    public function users()
    {
        return $this->hasOne('App\User','id','user_id');

    }

    public function address()
    {
        return $this->hasOne('App\UserAddress','user_id','user_id');
    }

    public function payment()
    {
        return $this->hasOne('App\Payment','order_id','order_id');
    }

}
