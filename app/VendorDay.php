<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorDay extends Model
{
    protected $fillable = [
        'vendor_id','start_time','end_time','day',
    ];
}
