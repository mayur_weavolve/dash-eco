<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'user_id', 'order_id', 'amount','status','transaction_id',
        'transaction_status', 'transaction',

    ];

    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }

    public function order()
    {
        return $this->hasOne('App\Order','id','order_id');
    }

    public function orderDetail()
    {
        return $this->hasOne('App\Order','order_id','order_id');
    }
}
