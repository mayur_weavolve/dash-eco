<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorPaymentAccount extends Model
{
    protected $fillable = [
        'vendor_id','user_id',
        'contact_id',
        'type',
        'account_id',
        'account_name'
    ];
}
