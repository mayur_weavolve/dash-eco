<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPrice extends Model
{
    protected $fillable = [
        'vender_id','product_id','mrp','discount','final_price','warranty','installation_included','exchange','free_delivery','mode','size','colour','stock',

    ];

    public function product()
    {
        return $this->hasOne('App\Product','id','product_id');
    }

    public function vendor()
    {
        return $this->hasOne('App\Vendor','id','vender_id');
    }
}
