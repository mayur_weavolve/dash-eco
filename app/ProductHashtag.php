<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductHashtag extends Model
{
    protected $fillable = [
        'product_id','hashtag'
    ];

    public $table = "product_hashtags";


}
