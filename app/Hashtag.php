<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hashtag extends Model
{
    protected $fillable = [
        'id','hashtag'
    ];

    public $table = "hashtags";

}
