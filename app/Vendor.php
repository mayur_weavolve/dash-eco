<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $fillable = [
        'user_id', 'name', 'business_name','nickname','address',
        'gst_no','postcode','photo','business_category','business_established','latitude','longitude',
        'notes','vendor_block','bank_upi',
    ];

    public $table = 'vendor';

    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }
}
