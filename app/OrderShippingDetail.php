<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Auth;
class OrderShippingDetail extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id','name','email','mobile','address_line1','address_line2','city','state','pincode'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    public function order()
    {
        return $this->hasOne('App\Order','id','order_id');
    }

}
