<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ProductView extends Model
{
    protected $fillable = [
        'user_id', 'product_id', 'latitude','longitude'
    ];


}
