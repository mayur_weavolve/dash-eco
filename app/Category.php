<?php

namespace App;
use App\Subcategory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name','status','commision',
    ];

    public $table = "categories";

    public function scopePcount($query)
    {
     return $query->where('pcount', '>', 0);
    }

    public function availableSubcategory($id)
    {
       $subArry =  Subcategory::where('category_id','=',$id)->where('pcount', '>', 0)->get();
       return $subArry;
    }

    public function subcategory()
    {
        return $this->hasMany('App\Subcategory','category_id','id');
    }
}
