<?php

namespace App\Http\Controllers;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\PaginationServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\User;
use App\Review;


use Maatwebsite\Excel\Facades\Excel;
use Auth;
class ReviewController extends Controller
{
    public function review(Request $request){
        $search = $request->get('search');
        if($search != ''){
            $reviews =Review::where('name','like',"%{$search}%")
                        ->orWhere('mobile_number','like','%{$search}%')
                        ->orWhere('review','like','%{$search}%')
                        ->orWhere('rating','like','%{$search}%')
                        ->orWhere('status','like','%{$search}%')
                        ->paginate('20');
        }else{
            $reviews = Review::with('product')->paginate('20');
        }
        return view('review.review')->with('reviews',$reviews);
    }

    public function review_edit_view($id)
    {
        $edit = Review::find($id);
        $data = [];
        $data['edit'] = $edit;
        return view('review.edit',$data);
    }

    public function review_edit(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'status' => 'required',

        ]);
        if ($validator->fails()) {
            return redirect(route('review_edit_view',$data['id']))
                        ->withErrors($validator)
                        ->withInput();
        }

        $edit = Review::find($data['id']);
        $edit->status = $data['status'];
        $edit->save();

        return redirect('/backend/admin/review/list');
    }



}
