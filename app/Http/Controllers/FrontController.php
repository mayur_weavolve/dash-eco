<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;
use App\Subcategory;
use App\User;
use App\Cart;
use App\Vendor;
use App\Subscribe;
use Mail;
use Auth;
use DB;
class FrontController extends Controller
{
    public function front()
    {
        $category = Category::with('subcategory')->where('status','active')->get();
        $hotdeals = Product::with('productImages','productSubcategory')->where('is_deleted','0')->orderby('id','desc')->limit(10)->get();
        $bestsellers = Product::with('productImage')->where('is_deleted','0')->limit(4)->get();
        $products = Product::where('is_deleted','0')->get();
        $data = [];
        $data['categories'] = $category;
        $data['hotdeals'] = $hotdeals;
        $data['bestsellers'] = $bestsellers;
        $data['products'] = $products;
        return view('front.front.home',$data);
    }

    public function aboutus()
    {
        return view('front.front.aboutus');
    }
    public function termCondition ()
    {
        return view('front.front.term-condition');
    }
    public function returnRefundPolicy(){
        return view('front.front.return-refund-policy');
    }
    public function wCondition(){
        return view('front.front.term-condition');
    }
    public function career(){
        return view('front.front.career');
    }
    public function contactUs(){
        return view('front.front.contact');
    }
    public function why_dash()
    {
        return view('front.front.why-dash');
    }
    public function how_to_order()
    {
        return view('front.front.how-to-order');
    }

    public function subscribe(Request $request){
        $response = [];
        $response['status'] = 'error';
        $response['message'] = 'User Email Id Not Subscribe';
        $data = $request->all();
        $sub = Subscribe::where('email','=',$data['email'])->get();
        if(count($sub) > 0){
            $response['status'] = 'success';
            $response['message'] = 'This User Email Id Already Subscribe SuccessFully';
        }
        else{
            $subs = new Subscribe();
            $subs->email = $data['email'];
            $subs->save();

            if($subs->id > 0){
                $response['status'] = 'success';
                $response['message'] = 'User Email Id Subscribe SuccessFully';
            }
        }
        echo json_encode($response);
    }
    public function my_cart()
    {
        if(Auth::user()){
            $id = Auth::user()->id;
        }else{
            $id = 0;
        }
        $ipadd = $_SERVER["REMOTE_ADDR"];
        $mac = exec("getmac");
        $macAdd = strtok($mac, ' ');
        $dataDelete =DB::delete('delete from carts where created_at < (NOW() - INTERVAL 30 MINUTE)');

        $vendor = Vendor::where('user_id',$id)->get();
        $carts = Cart::rightJoin('products', 'products.id', '=', 'carts.product_id')
        ->leftJoin('product_images', 'product_images.product_id', '=', 'products.id')
        ->leftJoin('product_prices', 'product_prices.product_id', '=', 'products.id')
        ->leftJoin('vendor','carts.vendor_id','=','vendor.id')
        ->where('carts.user_id',$id)
        ->orWhere('carts.ip_address',$ipadd)
        ->orWhere('carts.mac_address',$macAdd)
        ->select('products.*','product_images.*','product_images.name as image','product_prices.*','vendor.name as vName','carts.id as cId','carts.*')
        ->groupBy('products.id')->get();
        // print_r($carts); exit;
        return view('front.mycart')->with('carts',$carts)->with('vendor',$vendor);
    }

    public function delete_cart(Request $request, $id){
        Cart::where('id',$id)->delete();
        return redirect('/user/my-cart');

    }
    public function productList($name,$id){
        $products = Subcategory::rightJoin('product_subcategories', 'product_subcategories.subcategory_id', '=', 'subcategories.id')
                                ->rightJoin('products', 'products.id', '=', 'product_subcategories.product_id')
                                ->leftJoin('product_images', 'product_images.product_id', '=', 'products.id')
                                ->leftJoin('product_prices', 'product_prices.product_id', '=', 'products.id')
                                ->where('subcategories.category_id',$id)
                                ->select('products.*', 'subcategories.*', 'product_subcategories.*','product_images.*','product_prices.*','subcategories.name as subcategoryName')
                                ->distinct('products.id')->get();

                                // print_r(count($products)); exit;
        return view('front.product.productlist')->with('products',$products);
    }

    public function sendMail(Request $request) {
        $response = [];
        $response['status'] = 'error';
        $response['message'] = '';
        $data = $request->all();
        Mail::send(['html' => 'front.mail'], $data, function($message) {
           $message->to('hi@dashshop.in', 'Get In Touch')->subject
              ($data['subject']);
           $message->from($data['email'],$data['name']);
        });


        $response['status'] = 'success';
        $response['message'] = 'Email Sent';
        echo json_encode($response);

     }
    public function faq()
    {
        return view('front.front.faq');
    }

    public function checkout(){
        return view('front.front.checkout');
    }

    public function updateCart(Request $request){
        $data = $request->all();

        foreach($request->get('cart', []) as $cart) {
            $edit = Cart::find($cart['id']);
            $edit->quantity = $cart['quantity'];
            $edit->save();
        }

        return redirect('/user/my-cart');

    }

}
