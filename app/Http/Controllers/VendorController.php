<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Vendor;
use App\Category;
use App\VendorCategory;
use App\VendorDay;

class VendorController extends Controller
{
    public function vendor(Request $request)
    {
        $search = $request->get('search');
        if($search != ''){
            $list = Vendor::with('user')
                            ->where('name','like',"%{$search}%")
                            ->orWhere('nickname','like',"%{$search}%")
                            ->paginate('20');
        }else{
            $list = Vendor::with('user')->paginate('20');
        }

        $data = [];
        $data['vendors'] = $list;
        return view('vendor.list',$data);
    }
    public function add()
    {
        $category = Category::where('status','active')->get();
        $data = [];
        $data['categories'] = $category;
        return view('vendor.add',$data);
    }

    public function save_vendor(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'business_name' => 'required',
            'nickname' => 'required|unique:vendor',
            'whatsapp_no' => 'required|int',
            'mobileno' => 'required|int|unique:users',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'gst_no' => 'required',
            'postcode' => 'required',
            'business_category' => 'required',
            'business_established' => 'required',
            'vendor_block' => 'required',
            'bank_upi' => 'required',

        ]);
        if ($validator->fails()) {
            return redirect(route('vendor_add'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->whatsapp_no = $data['whatsapp_no'];
        $user->mobileno = $data['mobileno'];
        $user->password = Hash::make($data['password']);
        $user->type = 'vendor';
        $user->email_verifiy = '1';
        $user->save();
        if($user)
        {
            $add = new Vendor();
            $add->user_id = $user->id;
            $add->name = $data['name'];
            $add->business_name = $data['business_name'];
            $add->nickname = $data['nickname'];
            $add->address = $data['address'];
            $add->latitude = $data['latitude'];
            $add->longitude = $data['longitude'];
            $add->gst_no = $data['gst_no'];
            $add->postcode = $data['postcode'];
            $add->business_established = $data['business_established'];
            $add->notes = $data['notes'];
            $add->vendor_block = $data['vendor_block'];
            $add->bank_upi = $data['bank_upi'];
            if ($request->file('photo')!=null){
                $add->photo = $request->file('photo')->hashName('');
                $request->file('photo')
                ->store('vendor_photo', ['disk' => 'public']);
            }
            $add->save();

            foreach($data['business_category'] as $string){
                $category = new VendorCategory();
                $category->vendor_id = $add->id;
                $category->category_id = $string;
                $category->save();
            }
            $days = 'sun,mon,tue,wed,thur,fri,sat';
            $i=0;
            $day = explode(',', $days);
            foreach($data['start_time'] as $start_time)
            {
                $start_time = new VendorDay();
                $start_time->day = $day[$i];
                $start_time->vendor_id = $add->id;
                $start_time->start_time = $data['start_time'][$i];
                $start_time->end_time = $data['end_time'][$i];
                $start_time->save();
                $i++;
            }
        }

        return redirect('/backend/admin/vendor/list');
    }

    public function edit($id)
    {
        $edit = Vendor::find($id);
        $category = Category::where('status','active')->get();
        $vendorCategories = VendorCategory::select('category_id')->where('vendor_id',$id)->get();
        $vendorDays = VendorDay::where('vendor_id',$id)->orderBy('id')->get();
        $data = [];
        $data['categories'] = $category;
        $vendorCategoryArray = [];
        foreach($vendorCategories as $vc){
            array_push($vendorCategoryArray,$vc->category_id);
        }
        $data['vendorCategory'] = $vendorCategoryArray;
        $data['vendorDays'] = $vendorDays;
        return view('vendor.edit',['edit'=>$edit],$data);
    }

    public function edit_save(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'business_name' => 'required',
            'nickname' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'gst_no' => 'required',
            'postcode' => 'required',
            'business_category' => 'required',
            'business_established' => 'required',
            'vendor_block' => 'required',
            'bank_upi' => 'required',

        ]);
        if ($validator->fails()) {
            return redirect(route('vendor_edit',$id))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $edit = Vendor::find($id);
        $edit->name = $data['name'];
        $edit->business_name = $data['business_name'];
        $edit->nickname = $data['nickname'];
        $edit->address = $data['address'];
        $edit->latitude = $data['latitude'];
        $edit->longitude = $data['longitude'];
        $edit->gst_no = $data['gst_no'];
        $edit->postcode = $data['postcode'];
        $edit->business_established = $data['business_established'];
        $edit->notes = $data['notes'];
        $edit->vendor_block = $data['vendor_block'];
        $edit->bank_upi = $data['bank_upi'];
        if ($request->file('photo')!=null){
            $edit->photo = $request->file('photo')->hashName('');
            $request->file('photo')
            ->store('vendor_photo', ['disk' => 'public']);
        }
        $edit->save();
        if($data['business_category'] != null)
        {
            $delete = VendorCategory::where('vendor_id',$edit->id)->get();
            if(sizeof($delete) > 0)
            {
                $delete = VendorCategory::where('vendor_id',$id)->delete();
            }
            $business_category =  $data['business_category'];
            foreach($business_category as $string){
                $category = new VendorCategory();
                $category->vendor_id = $id;
                $category->category_id = $string;
                $category->save();
            }
        }
        if($data['start_time']!=null)
        {
            $delete_day = VendorDay::where('vendor_id',$edit->id)->get();
            if(sizeof($delete_day) > 0)
            {
                $delete_day = VendorDay::where('vendor_id',$edit->id)->delete();
            }
            $days = 'sun,mon,tue,wed,thur,fri,sat';
            $i=0;
            $day = explode(',', $days);
            foreach($data['start_time'] as $start_time)
            {
                $start_time = new VendorDay();
                $start_time->day = $day[$i];
                $start_time->vendor_id = $id;
                $start_time->start_time = $data['start_time'][$i];
                $start_time->end_time = $data['end_time'][$i];
                $start_time->save();
                $i++;
            }
        }
        return redirect('/backend/admin/vendor/list');
    }
    public function delete($id)
    {
        $delete = Vendor::find($id);
        $delete->delete();
        return redirect('/backend/admin/vendor/list');
    }
    public function become_a_vendor()
    {
        return view('user.become-a-vendor');
    }

    public function productList(Request $request){
        $user = Auth::User();
        $products = Vendor::join('vendor_categories', 'vendor.id', '=', 'vendor_categories.vendor_id')
                            ->join('subcategories', 'vendor_categories.id', '=', 'subcategories.category_id')
                            ->join('product_subcategories', 'subcategories.id', '=', 'product_subcategories.subcategory_id')
                            ->join('products', 'product_subcategories.product_id', '=', 'products.id')
                            ->join('product_prices', 'products.id','=' ,'product_prices.product_id')
                            ->where('vendor.user_id',$id)
                            ->select('products.*', 'product_prices.*')
                            ->get();

        return view('front.productlist')->with('products',$products);
    }
}
