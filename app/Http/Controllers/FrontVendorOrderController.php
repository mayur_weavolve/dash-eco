<?php

namespace App\Http\Controllers;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\PaginationServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\User;
use App\Product;
use App\Category;
use App\Subcategory;
use App\UserAddress;
use App\Vendor;
use App\ProductPrice;
use App\ProductAttributeValue;
use App\ProductAttribute;
use App\ProductImage;
use App\ProductSubcategory;
use App\ProductHashtag;
use App\Cart;
use App\Hashtag;
use App\Review;
use App\SubOrders;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use DB;
use App\Order;
use Auth;
use Redirect;
class FrontVendorOrderController extends Controller
{
    public function VendorRecentOrderList(Request $request){
        $user = Auth::user();
        $vendor = Vendor::where('user_id',$user->id)->get()->first();

        $orders = Order::join('sub_orders', 'sub_orders.order_id', '=', 'orders.id')
                   ->whereIn('sub_orders.status',["pending","processing","cancel_by_vendor"])
                    ->where('orders.status',"=","pending")
                    ->where('sub_orders.vendor_id','=',$vendor->id)
                    // ->orWhere('sub_orders.status','!=','pending_payment')
                    ->select('orders.*' ,'sub_orders.vendor_id','sub_orders.quantity','sub_orders.amount')
                    ->orderBy('orders.id')
                    ->groupBy('orders.id')->get();
        return view('front.order.recentOrderList')->with('orders',$orders);
    }
    public function VendorCompletedOrderList(Request $request){
        $user = Auth::user();
        $vendor = Vendor::where('user_id',$user->id)->get()->first();
        $orders = Order::with('subOrders')
                    ->join('sub_orders', 'sub_orders.order_id', '=', 'orders.id')
                    ->where('sub_orders.vendor_id',$vendor->id)
                    ->whereIn('sub_orders.status',["completed","delivered"])
                    ->select('orders.*' ,'sub_orders.vendor_id','sub_orders.quantity','sub_orders.amount')
                    ->orderBy('orders.id')
                    ->groupBy('orders.id')->get();
        return view('front.order.completedOrderList')->with('vendor',$vendor)->with('orders',$orders);
    }

    public function orderAccept(Request $request ,$id){
        $data = $request->all();
        $order = SubOrders::find($id);
        $order->status = 'processing';
        $order->save();

        return redirect('vendor/recent/order');
    }
    public function orderReject(Request $request ,$id){
        $data = $request->all();
        $order = SubOrders::find($id);
        $order->status = 'cancel_by_vendor';
        $order->save();

        return redirect('vendor/recent/order');
    }
    public function LiveOrder(Request $request){
        $user = Auth::user();
        $orders = Order::with('subOrders')
                    ->Join('sub_orders', 'sub_orders.order_id', '=', 'orders.id')
                    ->Join('products', 'products.id', '=', 'sub_orders.product_id')
                    ->leftJoin('product_images', 'product_images.product_id', '=', 'products.id')
                    ->where('orders.user_id',$user->id)
                    ->where('orders.status','!=','pending_payment')
                    ->whereIn('sub_orders.status',['pending','processing'])
                    ->select('orders.*' ,'orders.id as oId','sub_orders.id as sId','sub_orders.vendor_id','sub_orders.quantity','sub_orders.amount' ,'products.product_name' ,'product_images.name')
                    ->orderBy('orders.id', 'DESC')
                    ->groupBy('orders.id')->get();
        return view('front.order.liveOrder')->with('orders',$orders);
    }

    public function CompletedOrder(Request $request){
        $user = Auth::user();
        $orders = Order::with('subOrders')
                    ->Join('sub_orders', 'sub_orders.order_id', '=', 'orders.id')
                    ->Join('products', 'products.id', '=', 'sub_orders.product_id')
                    ->leftJoin('product_images', 'product_images.product_id', '=', 'products.id')
                    ->where('orders.user_id',$user->id)
                    ->where('orders.status','!=','pending_payment')
                    ->whereIn('sub_orders.status',['completed','delivered'])
                    ->select('orders.*' ,'orders.id as oId','sub_orders.id as sId','sub_orders.vendor_id','sub_orders.quantity','sub_orders.amount' ,'products.product_name' ,'product_images.name')
                    ->orderBy('orders.id', 'DESC')
                    ->groupBy('orders.id')->get();
        return view('front.order.completedOrder')->with('orders',$orders);
    }

    public function trackInfo(Request $request ,$id){
        $user = Auth::user();
        $info= SubOrders::where('id',$id)->get()->first();
        return view('front.order.trackinfo')->with('suborder',$info);
    }


public function tracking_view(Request $request){
        $data = $request->all();
        return view('front.track.track')->with('data', $data);
    }

    public function tracking_info(Request $request){
        $data = $request->all();
        $info= Order::with('subOrders')
                    ->Join('sub_orders', 'sub_orders.order_id', '=', 'orders.id')
                    ->join('vendor','vendor.id','=','sub_orders.vendor_id')
                    ->join('users','users.id','=','vendor.user_id')
                    ->Join('products', 'products.id', '=', 'sub_orders.product_id')
                    ->leftJoin('product_images', 'product_images.product_id', '=', 'products.id')
                    ->leftJoin('product_prices', 'product_prices.product_id', '=', 'products.id')
                    ->where('orders.tracking_number',$data['tracking_code'])
                    ->where('users.mobileno',$data['mobile'])
                    ->select('orders.*' ,'sub_orders.id as sId','orders.tracking_number','users.mobileno','sub_orders.vendor_id','sub_orders.status','sub_orders.quantity','sub_orders.amount','users.*','products.product_name' ,'product_images.name','vendor.name as vName')
                    ->orderBy('orders.id', 'DESC')
                    ->groupBy('sub_orders.id')->get();
        return view('front.track.info')->with('infos',$info);
    }

    public function updateStatus(Request $request){
        $response = [];
        $response['status'] = 'error';
        $response['message'] = '';
        $data = $request->all();


        $order = SubOrders::where('id',$data['id'])->where('security_code' ,$data['code'])->get();

        if(count($order) > 0)
        {
            $response['status'] = "success";
            $response['data'] = count($order);
            $response['message'] = 'Security Code Verified';
        }else{
            $response['status'] = "error";
            $response['data'] = "0";
            $response['message'] = 'Please Enter Correct Security Code';
        }
        echo json_encode($response);
    }

    public function updateImage(Request $request){
        $response = [];
        $response['status'] = 'error';
        $response['message'] = '';
        $data = $request->all();


        $sub = SubOrders::find($data['id']);

        if ($request->file('image') != null){
            $sub->delivered_path = $request->file('image')->hashName('');
            $request->file('image')->store('delivered_path_photo', ['disk' => 'public']);
        }
        $sub->status = 'delivered';
        $sub->save();
        if($request->ajax()){
            $response['status'] = 'success';
            $response['message'] = 'Your Order Delivered successfully';
            echo json_encode($response);
        }else{
            if(isset($data['path']) && $data['path'] != ''){
                return redirect(route('track_view',['tracking_code' => $data['tracking_number'],'mobile' => $data['mobile']] ));
            }else{
                return redirect(route('vendor_recent_order_list'));
            }

        }

    }

    public function orderDetail(Request $request ,$id){
     //   print_r($id); exit;
        $user = Auth::user();
        // $orders = Order::Join('order_shipping_details','orders.id','=' ,'order_shipping_details.order_id')
        //             ->Join('payments', 'orders.id', '=', 'payments.order_id')
        //             ->where('orders.id',$id)
        //             ->select('orders.*' ,'order_shipping_details.*','payments.*','orders.amount as Amount','orders.created_at as date','orders.order_id as oId')
        //             ->orderBy('orders.id', 'DESC')
        //             ->groupBy('orders.id')->get();
        $order = Order::where('id',$id)->get()->first();
        // $orderInfo = Order::with('subOrders')
        // ->Join('sub_orders', 'sub_orders.order_id', '=', 'orders.id')
        // ->Join('products', 'products.id', '=', 'sub_orders.product_id')
        // ->leftJoin('product_images', 'product_images.product_id', '=', 'products.id')
        // ->where('orders.user_id',$user->id)->where('orders.status','!=','completed')
        // ->select('orders.*' ,'sub_orders.vendor_id','sub_orders.quantity','sub_orders.amount' ,'products.product_name' ,'product_images.name')
        // ->orderBy('orders.id', 'DESC')
        // ->groupBy('orders.id')->get();
        // print_r($orders); exit;
        return view('front.order.detail')->with('order',$order);

    }

   function getsecuritycode($id){
        $response = [];
        $response['status'] = 'error';
        $response['message'] = '';
        $subID = \Crypt::decrypt($id);
        $sub = SubOrders::where('id',$subID)->get()->first();

        if($sub){
            $response['status'] = 'success';
            $response['code'] = $sub->security_code;
            $response['message'] = 'Security Code';
        }else{
            $response['message'] = 'Not found';
        }

        echo json_encode($response);
    }

    public function vendorUpdateStatus(Request $request){
        $response = [];

        $response['status'] = 'error';
        $response['message'] = '';
        $data = $request->all();


        $order = SubOrders::find($data['id']);
        $order->status = $data['status'];
        $order->save();

        $orders = Order::with('subOrders')
                    ->Join('sub_orders', 'sub_orders.order_id', '=', 'orders.id')
                    ->Join('products', 'products.id', '=', 'sub_orders.product_id')
                    ->leftJoin('product_images', 'product_images.product_id', '=', 'products.id')
                    ->where('sub_orders.id',$data['id'])
                    ->select('orders.*' ,'orders.id as oId','sub_orders.id as sId','sub_orders.vendor_id','sub_orders.quantity','sub_orders.amount' ,'products.product_name' ,'product_images.name')
                    ->orderBy('orders.id', 'DESC')
                    ->groupBy('orders.id')->get();

        $user = Order::with('users')->where('id',$order->id)->get()->first();

        if($data['status'] == 'processing'){
            Mail::send(['html' => 'front.mail.order_confirm'], ['user'=> $user,'orders'=>$orders], function($message) {
                $message->to($user->users->email,$user->users->name)->subject('Order Confirmation from DASH Shop');
                $message->from('hi@dashshop.in','DASH Shop');
            });
        }
        if($data['status'] == 'delivered'){
            Mail::send(['html' => 'front.mail.order_delivered'], ['user'=> $user,'orders'=>$orders], function($message) {
                $message->to($user->users->email,$user->users->name)->subject('Order Delivered from DASH Shop');
                $message->from('hi@dashshop.in','DASH Shop');
            });

            addToPayout($data['id']);

        }

        $response['status'] = 'success';
        $response['message'] = 'Status Updated Successfully Done';


        echo json_encode($response);

    }

    public function vendorTrack(Request $request,$id){

        $user = Auth::user();
        $info= SubOrders::where('id',$id)->get()->first();

        return view('front.order.track')->with('suborder',$info);
    }

    public function vendorInfo(Request $request){

        $user = Auth::user();
        $vendor = Vendor::where('user_id',$user->id)->get()->first();
        $info= Vendor::with('user')->where('user_id',$user->id)->get()->first();

        return view('vendor.info')->with('info',$info)->with('vendor',$vendor);
    }
}
