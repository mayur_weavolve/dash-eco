<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\ProductPrice;
use App\Vendor;
use App\Product;
use App\ProductAttributeValue;

class ProductPriceController extends Controller
{
    public function product_price($id)
    {
        $vendor = Vendor::find($id);
        $price = ProductPrice::where('vender_id',$vendor->id)->get();
        $data = [];
        $data['product_prices'] = $price;
        return view('product_price.list',['vendor'=>$vendor],$data);
    }
    public function add($id)
    {
        $vendor = Vendor::find($id);
        $product = Product::get();
        $data = [];
        $data['products'] = $product;
        return view('product_price.add',['vendor'=>$vendor],$data);
    }
    public function save_product_price(Request $request,$id)
    {
        $vendor = Vendor::find($id);
        $validator = Validator::make($request->all(), [
            'mrp' => 'required',
            'final_price' => 'required',
            'free_delivery' => 'required',
            'mode' => 'required',
        ]);
        $data = $request->all();
        $i = 0;

            $add = new ProductPrice();
            $add->vender_id = $vendor->id;
            $add->product_id = $data['product_id'];
            $add->mrp = $data['mrp'];
            $add->discount = $data['discount'];
            $add->final_price = $data['final_price'];
            $add->warranty = $data['warranty'];
            $add->installation_included = $data['installation_included'];
            $add->exchange = $data['exchange'];
            $add->free_delivery = $data['free_delivery'];
            $add->mode = $data['mode'];
            $add->status = 'active';
            $add->save();

            foreach($data['attrRows'] as $attrRow)
            {
                $productAttributeValue = new ProductAttributeValue();
                $productAttributeValue->product_price_id = $add->id;
                $productAttributeValue->product_id = $data['product_id'];
                $productAttributeValue->vendor_id =  $vendor->id;
                if(isset($data['colour'][$i])){
                    $productAttributeValue->colour = $data['colour'][$i];
                }
                if(isset($data['weight'][$i])){
                    $productAttributeValue->weight = $data['weight'][$i].$data['weightUnit'][$i];
                }
                if(isset($data['measurement'][$i])){
                    $productAttributeValue->measurement = $data['measurement'][$i].$data['measurementUnit'][$i];
                }
                if(isset($data['size'][$i])){
                    $productAttributeValue->size = $data['size'][$i];
                }
                if(isset($data['quantity'][$i])){
                    $productAttributeValue->quantity = $data['quantity'][$i];
                }
                if(isset($data['storage'][$i])){
                    $productAttributeValue->storage = $data['storage'][$i].$data['storageUnit'][$i];
                }
                $productAttributeValue->save();
                $i++;
            }
        return redirect(route('product_price',$id));
    }
    public function edit($id,$vid)
    {

        $data = [];
        $edit = ProductPrice::find($id);
        $vendor = Vendor::find($vid);
        $product = Product::find($edit->product_id);
        $data['product'] = $product;
        $data['edit'] = $edit;
        $data['vendor'] = $vendor;
        return view('product_price.edit',$data);
    }
    public function edit_save(Request $request,$id,$vid)
    {
        $validator = Validator::make($request->all(), [
            'mrp' => 'required',
            'final_price' => 'required',
            'free_delivery' => 'required',
            'mode' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect(route('product_price_edit',$id))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $edit = ProductPrice::find($id);
        $edit->vender_id = $vid;
        $edit->mrp = $data['mrp'];
        $edit->discount = $data['discount'];
        $edit->final_price = $data['final_price'];
        $edit->warranty = $data['warranty'];
        $edit->installation_included = $data['installation_included'];
        $edit->exchange = $data['exchange'];
        $edit->free_delivery = $data['free_delivery'];
        $edit->mode = $data['mode'];
        $edit->status = 'active';
        $edit->save();
        $i = 0;
        ProductAttributeValue::where('product_id',$edit->product_id)->where('vendor_id',$vid)->delete();
        foreach($data['attrRows'] as $attrRow)
        {
            $productAttributeValue = new ProductAttributeValue();
            $productAttributeValue->product_price_id = $edit->id;
            $productAttributeValue->product_id = $edit->product_id;
            $productAttributeValue->vendor_id =  $vid;
            if(isset($data['colour'][$i])){
                $productAttributeValue->colour = $data['colour'][$i];
            }
            if(isset($data['weight'][$i])){
                $productAttributeValue->weight = $data['weight'][$i].$data['weightUnit'][$i];
            }
            if(isset($data['measurement'][$i])){
                 $productAttributeValue->measurement = $data['measurement'][$i].$data['measurementUnit'][$i];
            }
            if(isset($data['size'][$i])){
                $productAttributeValue->size = $data['size'][$i];
            }
            if(isset($data['quantity'][$i])){
                $productAttributeValue->quantity = $data['quantity'][$i];
            }
            if(isset($data['storage'][$i])){
                $productAttributeValue->storage = $data['storage'][$i].$data['storageUnit'][$i];
            }
            $productAttributeValue->save();
            $i++;
        }
        return redirect(route('product_price',$vid));

    }
    public function delete($id,$vid)
    {
        $delete = ProductPrice::where('id',$id)->delete();
        return redirect(route('product_price',$vid));
    }
}
