<?php

namespace App\Http\Controllers;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\PaginationServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\User;
use App\Product;
use App\Category;
use App\Subcategory;
use App\UserAddress;
use App\Vendor;
use App\ProductPrice;
use App\ProductView;
use App\ProductAttributeValue;
use App\ProductAttribute;
use App\ProductImage;
use App\ProductSubcategory;
use App\ProductHashtag;
use App\Cart;
use App\Hashtag;
use App\Review;
use App\City;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use DB;
use Auth;
class FrontUserController extends Controller
{
    public function comingsoon()
    {
        return view('front.user.comingsoon');
    }
    public function become_a_vendor()
    {
        return view('front.user.become-vendor');
    }
    public function signup()
    {
        $user = Auth::user();
        if($user){
            return redirect(route('my_account'));
        }
        return view('front.user.signup');

    }
    public function verification()
    {
        return view('front.user.verification');
    }
    public function deliveryOrPickup()
    {
        return view('front.user.delivery');
    }

    public function check_signup(Request $request)
    {
        $data = $request->all();
        $user = User::where('mobileno',$data['mobile_number'])->get()->first();
        if(!$user)
        {
            $user = new User();
            $user->mobileno = $data['mobile_number'];
            $user->type = 'user';
            $user->save();

            $credentials = [
                'mobileno' => $data['mobile_number'],
                'id' => $user->id,
                'password' => '',
                'name' => '',
            ];

        }else{
            $user = $user;
            $credentials = [
                'mobileno' => $user['mobileno'],
                'id' => $user['id'],
                'password' => $user['password'],
                'name'=> $user['name']
            ];
        }
        Auth::login($user);

        if($user->type == 'vendor'){
            return redirect('/vendor/home');
        }else{

            $check = Session::get('signupfor');
            if($check === 'checkout'){
                Session::put('signupfor','');
                return redirect('/user/checkout');
            }else{
                return redirect('/user/my-account');
            }
        }


    }

    public function checkUser($mobile,Request $request){
        $user = User::where("mobileno", '=', $mobile)->get();
        $nubUser = sizeof($user);
        return response()->json(['user'=>$nubUser, 'success'=>true]);

    }

    // user side pages
    public function myaccount(Request $request){
        $data = Auth::user();
        $user = User::find($data['id']);
        return view('front.user.myaccount')->with('user',$user);
    }

    public function userInfoAdd(Request $request){
        $data = $request->all();
        $user = User::find($data['id']);
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'name' => 'required',
            'mobileno' => 'required',
            'whatsapp_no' => 'required',
            'pincode' => 'required',

        ]);
        if ($validator->fails()) {
            return redirect(route('my_account'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $user = User::find($data['id']);
        $user->email = $data['email'];
        $user->name = $data['name'];
        $user->mobileno = $data['mobileno'];
        $user->whatsapp_no = $data['whatsapp_no'];
        $user->pincode = $data['pincode'];
        $user->save();

        return redirect(route('my_account'));

    }

    public function userAddress(Request $request){
        $data = Auth::user();
        $user = UserAddress::where('user_id',$data['id'])->get();
        return view('front.user.address')->with('user',$user);
    }

    public function userAddressAdd(Request $request){

        $data = $request->all();
        $user = UserAddress::where('user_id',$data['id'])->get();
        $validator = Validator::make($request->all(), [
            'addressline1' => 'required',
            'city' => 'required',
            'state' => 'required',
            'pincode' => 'required',

        ]);
        if ($validator->fails()) {
            return redirect(route('user_address'))
                        ->withErrors($validator)
                        ->withInput();
        }
        if(count($user) >0 ){
            UserAddress::where('user_id',$data['id'])->delete();
        }

        $user = new UserAddress();
        $user->user_id = $data['id'];
        $user->addressline1 = $data['addressline1'];
        $user->addressline2 = $data['addressline2'];
        $user->city = $data['city'];
        $user->state = $data['state'];
        $user->pincode = $data['pincode'];
        $user->save();

        return redirect(route('my_account'));
    }

    // global pages
    public function productListForCategory($name,$id){
        $category = Category::select('id','name')->where('id',$id)->get()->first();
         return view('front.product.list_category')->with('category',$category);
    }
    public function productListForSubcategory($cat,$name,$id){
        $subcategory = Subcategory::select('id','name','category_id')->where('id',$id)->get()->first();
            $category = Category::select('id','name')->where('id',$subcategory->category_id)->get()->first();
            return view('front.product.list_subcategory')->with('category',$category)->with('subcategory',$subcategory);
    }


    public function productlist(Request $request){
        $response = [];
        $response['status'] = 'error';
        $response['message'] = '';
        $data = $request->all();
        $where = [];
        $whereIn = [];
        $minDistance = 0;
        $maxDistance = 30;
        $minPrice = 0;
        $maxPrice = 1000000;
        $orderby = 'distance';
        $order = 'ASC';
        if($data['minDistance'] > 0){
            $minDistance = $data['minDistance'];
        }
        if($data['maxDistance'] > 0 && $data['minDistance'] < $data['maxDistance'] ){
            $maxDistance = $data['maxDistance'];
        }
        if($data['minPrice'] > 0){
            $minPrice = $data['minPrice'];
        }
        if($data['maxPrice'] > 0 && $data['minPrice'] < $data['maxPrice']){
            $maxPrice = $data['maxPrice'];
        }
        if($data['sorting'] != '' && $data['sorting'] != 'undefined'){
            if($data['sorting'] == 'id'){
                $orderby = 'products.id';
                $order = 'DESC';
            }
            if($data['sorting'] == 'priceasc'){
                $orderby = 'product_prices.final_price';
                $order = 'ASC';
            }
            if($data['sorting'] == 'pricedesc'){
                $orderby = 'product_prices.final_price';
                $order = 'DESC';
            }
        }

        if($data['subcategoryId'] != 0){
            $where[] = [ 'subcategories.id','=',$data['subcategoryId'] ];
        }else{
            $where[] = [ 'subcategories.category_id','=',$data['categoryId'] ];
        }
        // print_r($where); exit;
        if(Session::get('pincode') == ''){
            $latitude = '22.56986';
            $longitude = '72.963773';
        }else{
            $latitude = Session::get('latitude');
            $longitude = Session::get('longitude');
        }
        $vendorArray  = [];
        $sqlDistance = DB::raw('( 6371 * acos( cos( radians(' . $latitude . ') )
            * cos( radians( vendor.latitude ) )
            * cos( radians( vendor.longitude )
            - radians(' . $longitude  . ') )
            + sin( radians(' . $latitude . ') )
            * sin( radians( vendor.latitude ) ) ) )');
            $vendors = Vendor::whereBetween($sqlDistance,[$minDistance,$maxDistance])->get();
            foreach( $vendors as $vendor){
              array_push($vendorArray,$vendor->id);
            }
            $vendorArray = implode(",",$vendorArray);
            $filter = '1=1';

            if($data['colour'] != ''){
                $filter.= " AND FIND_IN_SET(left(product_attribute_values.colour,10),'".$data['colour']."') > 0 ";
            }
            if($data['weight'] != ''){
                $filter.= " AND FIND_IN_SET(left(product_attribute_values.weight,10),'".$data['weight']."') > 0 ";
            }
            if($data['size'] != ''){
                $filter.= " AND FIND_IN_SET(left(product_attribute_values.size,10),'".$data['size']."') > 0 ";
            }
            if($data['storage'] != ''){
                $filter.= " AND FIND_IN_SET(left(product_attribute_values.storage,10),'".$data['storage']."') > 0 ";
            }
            if($data['measurement'] != ''){
                $filter.= " AND FIND_IN_SET(left(product_attribute_values.measurement,10),'".$data['measurement']."') > 0 ";
            }
        //     echo "SELECT products.product_name,products.id as product_id,product_images.name as productImage,product_prices.final_price,product_prices.mrp,product_prices.free_delivery,product_prices.mode,product_prices.discount,product_prices.installation_included,product_prices.warranty,product_prices.exchange,subcategories.name as subcategoryName,vendor.nickname,vendor.id as vendorID,vendor.longitude as vLongitude,vendor.latitude as vLatitude FROM `subcategories`
        //     INNER JOIN `product_subcategories` ON `product_subcategories`.`subcategory_id` = `subcategories`.`id`
        //     INNER JOIN `products` ON `products`.`id` = `product_subcategories`.`product_id`
        //     INNER JOIN `product_attribute_values` ON `product_attribute_values`.`product_id` = `products`.`id`
        //     INNER JOIN `product_prices` ON `product_prices`.`product_id` = `products`.`id`  AND `product_prices`.`final_price` BETWEEN '".$minPrice."' AND '".$maxPrice."' AND `product_prices`.`final_price` = (select min(`product_prices`.`final_price`) from `product_prices` where `product_prices`.`product_id` = `products`.`id`)
        //     INNER JOIN `vendor` ON `vendor`.`id` = `product_prices`.`vender_id`
        //     INNER JOIN `product_images` ON `product_images`.`product_id` = `products`.`id`
        //     WHERE `product_prices`.`final_price` BETWEEN '".$minPrice."' AND '".$maxPrice."' AND vendor.`id` IN (".$vendorArray.") AND ".$filter." GROUP BY `product_subcategories`.`product_id`
        //     UNION ALL
        //    ( SELECT products.product_name,products.id as product_id,product_images.name as productImage,product_prices.final_price,product_prices.mrp,product_prices.free_delivery,product_prices.mode,product_prices.discount,product_prices.installation_included,product_prices.warranty,product_prices.exchange,subcategories.name as subcategoryName,vendor.nickname,vendor.id as vendorID,vendor.longitude as vLongitude,vendor.latitude as vLatitude FROM `subcategories`
        //     INNER JOIN `product_subcategories` ON `product_subcategories`.`subcategory_id` = `subcategories`.`id`
        //     INNER JOIN `products` ON `products`.`id` = `product_subcategories`.`product_id`
        //     INNER JOIN `product_attribute_values` ON `product_attribute_values`.`product_id` = `products`.`id`
        //     INNER JOIN `product_images` ON `product_images`.`product_id` = `products`.`id`
        //     INNER JOIN `product_prices` ON `product_prices`.`product_id` = `products`.`id`  AND `product_prices`.`final_price` BETWEEN '".$minPrice."' AND '".$maxPrice."' AND `product_prices`.`final_price` = (select min(`product_prices`.`final_price`) from `product_prices` where `product_prices`.`product_id` = `products`.`id`)
        //     LEFT JOIN `product_hashtags` ON `product_hashtags`.`product_id` = `products`.`id`
        //     INNER JOIN `vendor` ON `vendor`.`id` = `product_prices`.`vender_id`
        //     WHERE  vendor.`id` IN (".$vendorArray.")   GROUP BY `product_subcategories`.`product_id` ORDER BY  product_prices.final_price DESC )"; exit;
          $products =  DB::select("SELECT $sqlDistance as distance,products.product_name,products.id as product_id,product_images.name as productImage,product_prices.final_price,product_prices.mrp,product_prices.free_delivery,product_prices.mode,product_prices.discount,product_prices.installation_included,product_prices.warranty,product_prices.exchange,subcategories.name as subcategoryName,subcategories.id as subcategoryid,subcategories.category_id as categoryid,vendor.nickname,vendor.id as vendorID,vendor.longitude as vLongitude,vendor.latitude as vLatitude FROM `subcategories`
            INNER JOIN `product_subcategories` ON `product_subcategories`.`subcategory_id` = `subcategories`.`id`
            INNER JOIN `products` ON `products`.`id` = `product_subcategories`.`product_id`
            INNER JOIN `product_attribute_values` ON `product_attribute_values`.`product_id` = `products`.`id`
            INNER JOIN `product_prices` ON `product_prices`.`product_id` = `products`.`id`  AND `product_prices`.`final_price` BETWEEN '".$minPrice."' AND '".$maxPrice."' AND `product_prices`.`final_price` = (select min(`product_prices`.`final_price`) from `product_prices` where `product_prices`.`product_id` = `products`.`id`)
            INNER JOIN `vendor` ON `vendor`.`id` = `product_prices`.`vender_id`
            INNER JOIN `product_images` ON `product_images`.`product_id` = `products`.`id`
            WHERE `product_prices`.`final_price` BETWEEN '".$minPrice."' AND '".$maxPrice."' AND vendor.`id` IN (".$vendorArray.") AND ".$filter." GROUP BY products.id  ORDER BY ".$orderby." ".$order." ");


        // $products = Subcategory::Join('product_subcategories', 'product_subcategories.subcategory_id', '=', 'subcategories.id')
        //     ->Join('products', 'products.id', '=', 'product_subcategories.product_id')
        //     ->Join('product_attribute_values', 'product_attribute_values.product_id', '=', 'products.id')
        //     ->leftJoin('product_images', 'product_images.product_id', '=', 'products.id')
        //     ->leftJoin('product_prices', 'product_prices.product_id', '=', 'products.id')
        //     ->Join('vendor', 'vendor.id', '=', 'product_prices.vender_id')
        //     // ->min('product_prices.final_price')
        //     ->orderBy('product_prices.final_price','asc')
        //     ->orderBy($orderby,$order)
        //     ->groupBy('product_subcategories.product_id')
        //     ->where($where)
        //     ->whereBetween('product_prices.final_price',[$minPrice,$maxPrice])
        //     ->whereBetween($sqlDistance,[$minDistance,$maxDistance]);
        //     if($data['colour'] != ''){
        //         $products->whereIn('product_attribute_values.colour',explode(",",$data['colour']));
        //     }
        //     if($data['weight'] != ''){
        //         $products->whereIn('product_attribute_values.weight',explode(",",$data['weight']));
        //     }
        //     if($data['size'] != ''){
        //         $products->whereIn('product_attribute_values.size',explode(",",$data['size']));
        //     }

        //     if($data['storage'] != ''){
        //         $products->whereIn('product_attribute_values.storage',explode(",",$data['storage']));
        //     }
        //     if($data['measurement'] != ''){
        //         $products->whereIn('product_attribute_values.measurement',explode(",",$data['measurement']));
        //     }
        //     $products = $products->paginate(12,array(DB::raw('( 6371  * acos( cos( radians(' . $latitude . ') )
        //     * cos( radians( vendor.latitude ) )
        //     * cos( radians( vendor.longitude )
        //     - radians(' . $longitude  . ') )
        //     + sin( radians(' . $latitude . ') )
        //     * sin( radians( vendor.latitude ) ) ) ) AS distance '),'products.product_name','products.id as product_id', 'product_images.name as productImage','product_prices.final_price','product_prices.mrp','product_prices.free_delivery','product_prices.mode','product_prices.discount','product_prices.installation_included','product_prices.warranty','product_prices.exchange','subcategories.name as subcategoryName','vendor.nickname','vendor.id as vendorID'));

        if(sizeof($products) > 0){
             $category = Category::select('id','name')->where('id',$products[0]->categoryid)->get()->first();
        }else{
            $category = [];
        }
        $subcategory = Subcategory::select('id','name')->where('id',$data['subcategoryId'])->get()->first();

        $responseData = [];

        $responseData['category'] = $category;
        $responseData['subcategory'] = $subcategory;
        if(sizeof($products) > 0){
            $products = $this->arrayPaginator($products, $request);
            $responseData['products'] = $products;
            $responseData['latitude'] = $latitude;
            $responseData['longitude'] = $longitude;
        }
        $response['status'] = 'success';
        $response['message'] = 'Product List';
        $response['data'] = $responseData;
        echo json_encode($response);
    }

    public function productlisthome(Request $request){
        $response = [];
        $response['status'] = 'error';
        $response['message'] = '';
        $data = $request->all();
        $where = [];
        $whereIn = [];
        $minDistance = 0;
        $maxDistance = 30;
        $minPrice = 0;
        $maxPrice = 1000000;
        $orderby = '';
        $order = '';
        if($data['minDistance'] > 0 || $data['minDistance'] != ''){
            $minDistance = $data['minDistance'];
        }
        if($data['maxDistance'] > 0 && $data['minDistance'] < $data['maxDistance'] ){
            $maxDistance = $data['maxDistance'];
        }
        if($data['minPrice'] > 0){
            $minPrice = $data['minPrice'];
        }
        if($data['maxPrice'] > 0 && $data['minPrice'] < $data['maxPrice']){
            $maxPrice = $data['maxPrice'];
        }
        if($data['sorting'] != '' && $data['sorting'] != 'undefined'){
            if($data['sorting'] == 'id'){
                $orderby = 'products.id';
                $order = 'desc';
            }
            if($data['sorting'] == 'priceasc'){
                $orderby = 'product_prices.final_price';
                $order = 'asc';
            }
            if($data['sorting'] == 'pricedesc'){
                $orderby = 'product_prices.final_price';
                $order = 'desc';
            }
            if($data['sorting'] == 'distance'){
                $orderby = 'distance';
                $order = 'asc';
            }
        }
        if($orderby == ''){
            $orderby = 'RAND()';
            $order = '';
        }
        // print_r($where); exit;
        if(Session::get('pincode') == ''){
            $latitude = '22.56986';
            $longitude = '72.963773';
        }else{
            $latitude = Session::get('latitude');
            $longitude = Session::get('longitude');
        }
        $sqlDistance = DB::raw('( 6371 * acos( cos( radians(' . $latitude . ') )
            * cos( radians( vendor.latitude ) )
            * cos( radians( vendor.longitude )
            - radians(' . $longitude  . ') )
            + sin( radians(' . $latitude . ') )
            * sin( radians( vendor.latitude ) ) ) )');
            $vendorArray = [];
            $vendors = Vendor::whereBetween($sqlDistance,[$minDistance,$maxDistance])->get();
            foreach( $vendors as $vendor){
              array_push($vendorArray,$vendor->id);
            }
            $vendorArray = implode(",",$vendorArray);
            $filter = '1=1';

            if($data['colour'] != ''){
                $filter.= " AND FIND_IN_SET(left(product_attribute_values.colour,10),'".$data['colour']."') > 0 ";
            }
            if($data['weight'] != ''){
                $filter.= " AND FIND_IN_SET(left(product_attribute_values.weight,10),'".$data['weight']."') > 0 ";
            }
            if($data['size'] != ''){
                $filter.= " AND FIND_IN_SET(left(product_attribute_values.size,10),'".$data['size']."') > 0 ";
            }
            if($data['storage'] != ''){
                $filter.= " AND FIND_IN_SET(left(product_attribute_values.storage,10),'".$data['storage']."') > 0 ";
            }
            if($data['measurement'] != ''){
                $filter.= " AND FIND_IN_SET(left(product_attribute_values.measurement,10),'".$data['measurement']."') > 0 ";
            }
            $products =  DB::select("SELECT {$sqlDistance} as distance,products.product_name,products.id as product_id,product_images.name as productImage,product_prices.final_price,product_prices.mrp,product_prices.free_delivery,product_prices.mode,product_prices.discount,product_prices.installation_included,product_prices.warranty,product_prices.exchange,subcategories.name as subcategoryName,subcategories.id as subcategoryid,subcategories.category_id as categoryid,vendor.nickname,vendor.id as vendorID,vendor.longitude as vLongitude,vendor.latitude as vLatitude FROM `subcategories`
            INNER JOIN `product_subcategories` ON `product_subcategories`.`subcategory_id` = `subcategories`.`id`
            INNER JOIN `products` ON `products`.`id` = `product_subcategories`.`product_id`
            INNER JOIN `product_attribute_values` ON `product_attribute_values`.`product_id` = `products`.`id`
            INNER JOIN `product_prices` ON `product_prices`.`product_id` = `products`.`id`  AND `product_prices`.`final_price` BETWEEN '".$minPrice."' AND '".$maxPrice."' AND `product_prices`.`final_price` = (select min(`product_prices`.`final_price`) from `product_prices` where `product_prices`.`product_id` = `products`.`id`)
            INNER JOIN `vendor` ON `vendor`.`id` = `product_prices`.`vender_id`
            INNER JOIN `product_images` ON `product_images`.`product_id` = `products`.`id`
            WHERE `product_prices`.`final_price` BETWEEN '".$minPrice."' AND '".$maxPrice."' AND vendor.`id` IN (".$vendorArray.") AND ".$filter." GROUP BY products.id  ORDER BY ".$orderby." ".$order." ");

        // $products = Subcategory::Join('product_subcategories', 'product_subcategories.subcategory_id', '=', 'subcategories.id')
        //     ->Join('products', 'products.id', '=', 'product_subcategories.product_id')
        //     ->Join('product_attribute_values', 'product_attribute_values.product_id', '=', 'products.id')
        //     ->leftJoin('product_images', 'product_images.product_id', '=', 'products.id')
        //     ->Join('product_prices', 'product_prices.product_id', '=', 'products.id')
        //     ->Join('vendor', 'vendor.id', '=', 'product_prices.vender_id')
        //     ->groupBy('product_subcategories.product_id')
        //     ->where($where)
        //     ->whereBetween('product_prices.final_price',[$minPrice,$maxPrice])
        //     ->whereBetween($sqlDistance,[$minDistance,$maxDistance]);
        //     if($orderby != ''){
        //         $products->orderBy($orderby,$order);
        //     }else{
        //         $products->inRandomOrder();
        //     }
        //     if($data['colour'] != ''){
        //         $products->whereIn('product_attribute_values.colour',explode(",",$data['colour']));
        //     }
        //     if($data['weight'] != ''){
        //         $products->whereIn('product_attribute_values.weight',explode(",",$data['weight']));
        //     }
        //     if($data['size'] != ''){
        //         $products->whereIn('product_attribute_values.size',explode(",",$data['size']));
        //     }

        //     if($data['storage'] != ''){
        //         $products->whereIn('product_attribute_values.storage',explode(",",$data['storage']));
        //     }
        //     if($data['measurement'] != ''){
        //         $products->whereIn('product_attribute_values.measurement',explode(",",$data['measurement']));
        //     }
            // if($data['delivery'] != '' && $data['delivery'] != 'undefined'){
            //     if($data['delivery'] == 'Delivery'){
            //         $products->where('product_prices.free_delivery','>','0');
            //     }else if($data['delivery'] == 'Pickup'){
            //         $products->where('product_prices.free_delivery','=','0');
            //     }else{

            //     }
            // }
            // $products = $products->paginate(12,array(DB::raw('( 6371  * acos( cos( radians(' . $latitude . ') )
            // * cos( radians( vendor.latitude ) )
            // * cos( radians( vendor.longitude )
            // - radians(' . $longitude  . ') )
            // + sin( radians(' . $latitude . ') )
            // * sin( radians( vendor.latitude ) ) ) ) AS distance '),'products.product_name','products.id as product_id', 'product_images.name as productImage','product_prices.final_price','product_prices.mrp','product_prices.free_delivery','product_prices.mode','product_prices.discount','product_prices.installation_included','product_prices.warranty','product_prices.exchange','subcategories.name as subcategoryName','vendor.nickname','vendor.id as vendorID'));



        $responseData = [];
        if(sizeof($products) > 0){
            $products = $this->arrayPaginator($products, $request);
            $responseData['products'] = $products;
        }
        $response['status'] = 'success';
        $response['message'] = 'Product List';
        $response['data'] = $responseData;
        echo json_encode($response);
    }

    public function productDetail(Request $request,$product,$id){

        // $NewCat = [];
        // $category = Category::get();
        // foreach($category as $cat){
        //     $NewCat[$cat->id] = 0;
        // }
        // $NewSUbCat = [];
        // $subcategory = Subcategory::get();
        // foreach($subcategory as $scat){
        //     $NewSUbCat[$scat->id] = 0;
        // }
        // $products = ProductSubcategory::get();
        // foreach($products as $product){
        //     $NewSUbCat[$product->subcategory_id] = $NewSUbCat[$product->subcategory_id] + 1;
        //     $NewCat[$product->subcategory->category_id] = $NewCat[$product->subcategory->category_id] + 1;
        // }
        // print_r($NewSUbCat);
        // print_r($NewCat);
        // $category = Category::get();
        // foreach($category as $cat){
        //     $upCat = Category::find($cat->id);
        //     $upCat->pcount = $NewCat[$cat->id];
        //     $upCat->save();
        // }
        // $Subcategory = Subcategory::get();
        // foreach($Subcategory as $scat){
        //     $upscat = Subcategory::find($scat->id);
        //     $upscat->pcount = $NewSUbCat[$scat->id];
        //     $upscat->save();
        // }
        // exit;
        $minDistance = 0;
        $maxDistance = 30;
        $data = $request->all();
        $idArray = explode("_",$id);
        $vid =  $idArray[1];
        $pid =  $idArray[0];
        $ipadd = $_SERVER["REMOTE_ADDR"];
        $mac = exec("getmac");
        $macAdd = strtok($mac, ' ');
        $where = [];
        if(isset($data['colour'])){
            $where[] = ['colour','=',$data['colour']];
        }
        if(Session::get('pincode') == ''){
            $latitude = '22.56986';
            $longitude = '72.963773';
        }else{
            $latitude = Session::get('latitude');
            $longitude = Session::get('longitude');
        }
        $sqlDistance = DB::raw('( 6371 * acos( cos( radians(' . $latitude . ') )
        * cos( radians( vendor.latitude ) )
        * cos( radians( vendor.longitude )
        - radians(' . $longitude  . ') )
        + sin( radians(' . $latitude . ') )
        * sin( radians( vendor.latitude ) ) ) )');

        if(Auth::user()){
            $id = Auth::user()->id;
            if($id != 0){
                $user_id = $id;
            }else{
                $user_id = 0;
            }
        }else{
            $user_id = 0;
        }
        $proView = new ProductView();
        $proView->user_id = $user_id;
        $proView->product_id = $pid;
        $proView->latitude = $latitude;
        $proView->longitude = $longitude;
        $proView->save();

        $products = Product::where('id',$pid)->get()->first();
        $proReviews = Product::with('reviews')->where('id',$pid)->get();

        $productPrice = ProductPrice::where('vender_id',$vid)->where('product_id',$pid)->get()->first();
        $productPrices = ProductPrice::select('product_prices.*')->join('vendor','vendor.id','=','product_prices.vender_id')->where('product_prices.vender_id','!=',$vid)->where('product_prices.product_id',$pid)->whereBetween($sqlDistance,[$minDistance,$maxDistance])->get();

        $productAttributeElement = ProductAttribute::where('product_id',$pid)->select('product_attributes.attribute_name')->distinct('product_attributes.attribute_name')->get();
        $storages = ProductAttributeValue::where('product_price_id',$productPrice->id)->select('product_attribute_values.storage')->where('product_attribute_values.storage','!=',null)->distinct('product_attribute_values.storage')->get();
        $colours = ProductAttributeValue::where('product_price_id',$productPrice->id)->select('product_attribute_values.colour')->where('product_attribute_values.colour','!=',null)->groupby('product_attribute_values.colour')->get();
        $measurements = ProductAttributeValue::where('product_price_id',$productPrice->id)->select('product_attribute_values.measurement')->where('product_attribute_values.measurement','!=',null)->distinct('product_attribute_values.measurement')->get();
        $weights = ProductAttributeValue::where('product_price_id',$productPrice->id)->select('product_attribute_values.weight')->where('product_attribute_values.weight','!=',null)->distinct('product_attribute_values.weight')->get();
        $sizes = ProductAttributeValue::where('product_price_id',$productPrice->id)->select('product_attribute_values.size')->where('product_attribute_values.size','!=',null)->distinct('product_attribute_values.size')->get();
        $productAttValueArray = ProductAttributeValue::where('product_price_id',$productPrice->id)->select('product_attribute_values.*')->get();
        $productAttr = [];
        $productAttr['colour'] = $colours;
        $productAttr['storage'] = $storages;
        $productAttr['measurement'] = $measurements;
        $productAttr['weight'] = $weights;
        $productAttr['size'] = $sizes;
        $productAttr['vendorId'] = $vid;
        $productSubcategoryId = $products->productsubcategory->subcategory_id;
        $productCategoryId = $products->productsubcategory->subcategory->category_id;
        $productbysubcategory = Subcategory::Join('product_subcategories', 'product_subcategories.subcategory_id', '=', 'subcategories.id')
        ->Join('products', 'products.id', '=', 'product_subcategories.product_id')
        ->Join('product_images', 'product_images.product_id', '=', 'products.id')
        ->Join('product_prices', 'product_prices.product_id', '=', 'products.id')
        ->Join('vendor', 'vendor.id', '=', 'product_prices.vender_id')
        ->where('product_subcategories.subcategory_id',$productSubcategoryId)
        ->groupBy('product_subcategories.product_id')
        ->select('products.*', 'subcategories.*', 'product_subcategories.*','product_images.name as productImage','product_prices.*','subcategories.name as subcategoryName','vendor.nickname','vendor.id as vendorID')->get();
        $customerBroughtProducts = Subcategory::Join('product_subcategories', 'product_subcategories.subcategory_id', '=', 'subcategories.id')
        ->Join('products', 'products.id', '=', 'product_subcategories.product_id')
        ->rightjoin('product_images', 'product_images.product_id', '=', 'products.id')
        ->Join('product_prices', 'product_prices.product_id', '=', 'products.id')
        ->Join('vendor', 'vendor.id', '=', 'product_prices.vender_id')
        ->groupBy('product_subcategories.product_id')
        ->orderby('products.created_at','DESC')
        ->select('products.*', 'subcategories.*', 'product_subcategories.*','product_images.name as productImage','product_prices.*','subcategories.name as subcategoryName','vendor.nickname','vendor.id as vendorID')->skip(0)->take(12)->get();
        $productFromSameBrands = Subcategory::Join('product_subcategories', 'product_subcategories.subcategory_id', '=', 'subcategories.id')
            ->Join('products', 'products.id', '=', 'product_subcategories.product_id')
            ->Join('product_images', 'product_images.product_id', '=', 'products.id')
            ->Join('product_prices', 'product_prices.product_id', '=', 'products.id')
            ->Join('vendor', 'vendor.id', '=', 'product_prices.vender_id')
            ->where('products.manufacturer',$products->manufacturer)
            ->groupBy('products.id')
            ->select('products.*', 'subcategories.*', 'product_subcategories.*','product_images.name as productImage','product_prices.*','subcategories.name as subcategoryName','vendor.nickname','vendor.id as vendorID')->skip(0)->take(3)->get();;

        // $proAtt = ProductAttributeValue::where('product_id',$id)->get();
        // print_r($productAttributeElement); exit;
        return view('front.product.detail')->with('products',$products)->with('ipadd',$ipadd)->with('macAdd',$macAdd)->with('productbysubcategory',$productbysubcategory)->with('customerBroughtProducts',$customerBroughtProducts)->with('productFromSameBrands',$productFromSameBrands)->with('productAttributeValue',$productAttr)->with('productPrice',$productPrice)->with('productPrices',$productPrices)->with('productAttValueArray',$productAttValueArray)->with('proReviews',$proReviews);
    }


    public function searchList(Request $request){
        $data = $request->all();
        return view('front.product.search_list')->with('search',$data['search']);
    }

    public function searchAjax(Request $request){
        $response = [];
        $response['status'] = 'error';
        $response['message'] = '';
        $data = $request->all();
        $orWhere = [];
        $whereIn = [];
        $minDistance = 0;
        $maxDistance = 30;
        $minPrice = 0;
        $maxPrice = 1000000;
        // $orderby = 'distance';
        // $order = 'asc';
        $orderby = 'final_price';
        $order = 'ASC';
        if($data['minDistance'] > 0){
            $minDistance = $data['minDistance'];
        }
        if($data['maxDistance'] > 0 && $data['minDistance'] < $data['maxDistance'] ){
            $maxDistance = $data['maxDistance'];
        }
        if($data['minPrice'] > 0){
            $minPrice = $data['minPrice'];
        }
        if($data['maxPrice'] > 0 && $data['minPrice'] < $data['maxPrice']){
            $maxPrice = $data['maxPrice'];
        }
        if($data['sorting'] != '' && $data['sorting'] != 'undefined'){
            if($data['sorting'] == 'id'){
                $orderby = 'product_id';
                $order = 'DESC';
            }
            if($data['sorting'] == 'priceasc'){
                $orderby = 'final_price';
                $order = 'ASC';
            }
            if($data['sorting'] == 'pricedesc'){
                $orderby = 'final_price';
                $order = 'DESC';
            }
        }

        // print_r($where); exit;
        $vendorArray = [];
        if(Session::get('pincode') == ''){
            $latitude = '22.56986';
            $longitude = '72.963773';
        }else{
            $latitude = Session::get('latitude');
            $longitude = Session::get('longitude');
        }
        $sqlDistance = DB::raw('( 6371 * acos( cos( radians(' . $latitude . ') )
            * cos( radians( vendor.latitude ) )
            * cos( radians( vendor.longitude )
            - radians(' . $longitude  . ') )
            + sin( radians(' . $latitude . ') )
            * sin( radians( vendor.latitude ) ) ) )');
            $vendors = Vendor::whereBetween($sqlDistance,[$minDistance,$maxDistance])->get();
            foreach( $vendors as $vendor){
              array_push($vendorArray,$vendor->id);
            }
            $vendorArray = implode(",",$vendorArray);
        // $products = Subcategory::Join('product_subcategories', 'product_subcategories.subcategory_id', '=', 'subcategories.id')
        //     ->Join('products', 'products.id', '=', 'product_subcategories.product_id')
        //     ->Join('product_attribute_values', 'product_attribute_values.product_id', '=', 'products.id')
        //     ->leftJoin('product_images', 'product_images.product_id', '=', 'products.id')
        //     ->leftJoin('product_prices', 'product_prices.product_id', '=', 'products.id')
        //     ->leftJoin('categories', 'categories.id', '=', 'subcategories.category_id')
        //     ->leftJoin('product_hashtags', 'product_hashtags.product_id', '=', 'products.id')
        //     ->leftJoin('vendor', 'vendor.id', '=', 'product_prices.vender_id')
        //     // ->min('product_prices.final_price')
        //     ->orderBy('product_prices.final_price','asc')
        //     ->orderBy($orderby,$order)
        //     ->groupBy('product_subcategories.product_id')
        //     ->whereBetween('product_prices.final_price',[$minPrice,$maxPrice]);
        //     // ->whereBetween($sqlDistance,[$minDistance,$maxDistance]);
        //     $products->whereIn('product_prices.vender_id',explode(",",$vendorArray));
        //     if($data['colour'] != ''){
        //         $products->whereIn('product_attribute_values.colour',explode(",",$data['colour']));
        //     }
        //     if($data['weight'] != ''){
        //         $products->whereIn('product_attribute_values.weight',explode(",",$data['weight']));
        //     }
        //     if($data['size'] != ''){
        //         $products->whereIn('product_attribute_values.size',explode(",",$data['size']));
        //     }

        //     if($data['storage'] != ''){
        //         $products->whereIn('product_attribute_values.storage',explode(",",$data['storage']));
        //     }
        //     if($data['measurement'] != ''){
        //         $products->whereIn('product_attribute_values.measurement',explode(",",$data['measurement']));
        //     }
        //     if($data['search'] != ''){
        //         // $orWhere[] = [ 'products.product_name','like','%'.$data['search'].'%' ];
        //         // $orWhere[] = [ 'product_hashtags.hashtag','like','%'.$data['search'].'%' ];
        //         // $orWhere[] = [ 'products.description','like','%'.$data['search'].'%' ];
        //         // $orWhere[] = [ 'products.specification','like','%'.$data['search'].'%' ];
        //         // $orWhere[] = [ 'subcategory.name','like','%'.$data['search'].'%' ];
        //         // $orWhere[] = [ 'category.name','like','%'.$data['search'].'%' ];
        //         $products->orWhere('products.product_name','like','%'.$data['search'].'%');
        //          $products->orWhere('product_hashtags.hashtag','=',$data['search']);
        //         // $products->orWhere('products.description','like','%'.$data['search'].'%');
        //         // $products->orWhere('products.specification','like','%'.$data['search'].'%');
        //         // $products->orWhere('categories.name','like','%'.$data['search'].'%');
        //         // $products->orWhere('subcategories.name','like','%'.$data['search'].'%');
        //     }
            // $products = $products->paginate(12,array(DB::raw('( 6371  * acos( cos( radians(' . $latitude . ') )
            // * cos( radians( vendor.latitude ) )
            // * cos( radians( vendor.longitude )
            // - radians(' . $longitude  . ') )
            // + sin( radians(' . $latitude . ') )
            // * sin( radians( vendor.latitude ) ) ) ) AS distance '),'products.product_name','products.id as product_id', 'product_images.name as productImage','product_prices.final_price','product_prices.mrp','product_prices.free_delivery','product_prices.mode','product_prices.discount','product_prices.installation_included','product_prices.warranty','product_prices.exchange','subcategories.name as subcategoryName','vendor.nickname','vendor.id as vendorID'));

        // if(sizeof($products) > 0){
        //      $category = Category::select('id','name')->where('id',$products[0]->category_id)->get()->first();
        // }else{
        //     $category = [];
        // }
        // $subcategory = Subcategory::select('id','name')-->get()->first();
        $filter = '';
        // print_r($data); exit;
        if($data['colour'] != ''){
            $filter.= " AND FIND_IN_SET(left(product_attribute_values.colour,10),'".$data['colour']."') > 0 ";
        }
        if($data['weight'] != ''){
            $filter.= " AND FIND_IN_SET(left(product_attribute_values.weight,10),'".$data['weight']."') > 0 ";
        }
        if($data['size'] != ''){
            $filter.= " AND FIND_IN_SET(left(product_attribute_values.size,10),'".$data['size']."') > 0 ";
        }
        if($data['storage'] != ''){
            $filter.= " AND FIND_IN_SET(left(product_attribute_values.storage,10),'".$data['storage']."') > 0 ";
        }
        if($data['measurement'] != ''){
            $filter.= " AND FIND_IN_SET(left(product_attribute_values.measurement,10),'".$data['measurement']."') > 0 ";
        }
    //     echo "SELECT products.product_name,products.id as product_id,product_images.name as productImage,product_prices.final_price,product_prices.mrp,product_prices.free_delivery,product_prices.mode,product_prices.discount,product_prices.installation_included,product_prices.warranty,product_prices.exchange,subcategories.name as subcategoryName,vendor.nickname,vendor.id as vendorID,vendor.longitude as vLongitude,vendor.latitude as vLatitude FROM `subcategories`
    //     INNER JOIN `product_subcategories` ON `product_subcategories`.`subcategory_id` = `subcategories`.`id`
    //     INNER JOIN `products` ON `products`.`id` = `product_subcategories`.`product_id`
    //     INNER JOIN `product_attribute_values` ON `product_attribute_values`.`product_id` = `products`.`id`
    //     INNER JOIN `product_prices` ON `product_prices`.`product_id` = `products`.`id`  AND `product_prices`.`final_price` BETWEEN '".$minPrice."' AND '".$maxPrice."' AND `product_prices`.`final_price` = (select min(`product_prices`.`final_price`) from `product_prices` where `product_prices`.`product_id` = `products`.`id`)
    //     INNER JOIN `vendor` ON `vendor`.`id` = `product_prices`.`vender_id`
    //     INNER JOIN `product_images` ON `product_images`.`product_id` = `products`.`id`
    //     WHERE `product_prices`.`final_price` BETWEEN '".$minPrice."' AND '".$maxPrice."' AND vendor.`id` IN (".$vendorArray.") AND `products`.`product_name` LIKE  '".$data['search']."' ".$filter." GROUP BY `product_subcategories`.`product_id`
    //     UNION ALL
    //    ( SELECT products.product_name,products.id as product_id,product_images.name as productImage,product_prices.final_price,product_prices.mrp,product_prices.free_delivery,product_prices.mode,product_prices.discount,product_prices.installation_included,product_prices.warranty,product_prices.exchange,subcategories.name as subcategoryName,vendor.nickname,vendor.id as vendorID,vendor.longitude as vLongitude,vendor.latitude as vLatitude FROM `subcategories`
    //     INNER JOIN `product_subcategories` ON `product_subcategories`.`subcategory_id` = `subcategories`.`id`
    //     INNER JOIN `products` ON `products`.`id` = `product_subcategories`.`product_id`
    //     INNER JOIN `product_attribute_values` ON `product_attribute_values`.`product_id` = `products`.`id`
    //     INNER JOIN `product_images` ON `product_images`.`product_id` = `products`.`id`
    //     INNER JOIN `product_prices` ON `product_prices`.`product_id` = `products`.`id`  AND `product_prices`.`final_price` BETWEEN '".$minPrice."' AND '".$maxPrice."' AND `product_prices`.`final_price` = (select min(`product_prices`.`final_price`) from `product_prices` where `product_prices`.`product_id` = `products`.`id`)
    //     LEFT JOIN `product_hashtags` ON `product_hashtags`.`product_id` = `products`.`id`
    //     INNER JOIN `vendor` ON `vendor`.`id` = `product_prices`.`vender_id`
    //     WHERE `product_hashtags`.`hashtag` = '".$data['search']."' ".$filter." AND vendor.`id` IN (".$vendorArray.")   GROUP BY `product_subcategories`.`product_id` ORDER BY ".$orderby." ".$order." )"; exit;
        $products =  DB::select("SELECT * FROM ((
            SELECT products.product_name,products.id as product_id,product_images.name as productImage,product_prices.final_price,product_prices.mrp,product_prices.free_delivery,product_prices.mode,product_prices.discount,product_prices.installation_included,product_prices.warranty,product_prices.exchange,subcategories.name as subcategoryName,vendor.nickname,vendor.id as vendorID,vendor.longitude as vLongitude,vendor.latitude as vLatitude FROM `subcategories`
        INNER JOIN `product_subcategories` ON `product_subcategories`.`subcategory_id` = `subcategories`.`id`
        INNER JOIN `products` ON `products`.`id` = `product_subcategories`.`product_id`
        INNER JOIN `product_attribute_values` ON `product_attribute_values`.`product_id` = `products`.`id`
        INNER JOIN `product_prices` ON `product_prices`.`product_id` = `products`.`id`  AND `product_prices`.`final_price` BETWEEN '".$minPrice."' AND '".$maxPrice."' AND `product_prices`.`final_price` = (select min(`product_prices`.`final_price`) from `product_prices` where `product_prices`.`product_id` = `products`.`id`)
        INNER JOIN `vendor` ON `vendor`.`id` = `product_prices`.`vender_id`
        INNER JOIN `product_images` ON `product_images`.`product_id` = `products`.`id`
        WHERE vendor.`id` IN (".$vendorArray.") AND `products`.`product_name` LIKE  '%".$data['search']."%' ".$filter.")
        UNION
        ( SELECT products.product_name,products.id as product_id,product_images.name as productImage,product_prices.final_price,product_prices.mrp,product_prices.free_delivery,product_prices.mode,product_prices.discount,product_prices.installation_included,product_prices.warranty,product_prices.exchange,subcategories.name as subcategoryName,vendor.nickname,vendor.id as vendorID,vendor.longitude as vLongitude,vendor.latitude as vLatitude FROM `subcategories`
         INNER JOIN `product_subcategories` ON `product_subcategories`.`subcategory_id` = `subcategories`.`id`
        INNER JOIN `products` ON `products`.`id` = `product_subcategories`.`product_id`
        INNER JOIN `product_attribute_values` ON `product_attribute_values`.`product_id` = `products`.`id`
        INNER JOIN `product_images` ON `product_images`.`product_id` = `products`.`id`
        INNER JOIN `product_prices` ON `product_prices`.`product_id` = `products`.`id`  AND `product_prices`.`final_price` BETWEEN '".$minPrice."' AND '".$maxPrice."' AND `product_prices`.`final_price` = (select min(`product_prices`.`final_price`) from `product_prices` where `product_prices`.`product_id` = `products`.`id`)
        LEFT JOIN `product_hashtags` ON `product_hashtags`.`product_id` = `products`.`id`
        INNER JOIN `vendor` ON `vendor`.`id` = `product_prices`.`vender_id`
        WHERE vendor.`id` IN (".$vendorArray.")  AND  `product_hashtags`.`hashtag` LIKE '%".$data['search']."%' ".$filter."
        ) ) as t1 GROUP BY `product_id` ORDER BY ".$orderby." ".$order."  ");

        $responseData = [];

        // $responseData['category'] = $category;
        // $responseData['subcategory'] = $subcategory;
        if(sizeof($products) > 0){
            $products = $this->arrayPaginator($products, $request);
            $responseData['products'] = $products;
            $responseData['latitude'] = $latitude;
            $responseData['longitude'] = $longitude;
        }
        $response['status'] = 'success';
        $response['message'] = 'Product List';
        $response['data'] = $responseData;
        echo json_encode($response);
    }

    public function addReview(Request $request){

        $response = [];
        $response['status'] = 'error';
        $response['message'] = '';
        $data = $request->all();

        $review = new Review();
        $review->product_id = $data['product_id'];
        $review->name = $data['name'];
        $review->mobile_number = $data['mobile_number'];
        $review->rating = $data['rating'];
        $review->review = $data['review'];
        $review->status='Pending';
        $review->updated_by= '0';
        $review->save();

        if($review->id >0){
            $response['status'] = 'success';
            $response['message'] = 'Your review sent successfully';
        }
        echo json_encode($response);

    }

    public function setpincode(Request $request){
        $response = [];
        $oldPin = Session::get('pincode');
        $response['status'] = 'error';
        $response['message'] = 'We are not available in this location, please change the pincode.';
        $data = $request->all();
        $LatLong = City::where('zipcode',$data['pincode'])->get()->first();
        if($LatLong){
            $sqlDistance = DB::raw('( 6371  * acos( cos( radians(' . $LatLong->latitude . ') )
                * cos( radians( vendor.latitude ) )
                * cos( radians( vendor.longitude )
                - radians(' . $LatLong->longitude  . ') )
                + sin( radians(' . $LatLong->latitude . ') )
                * sin( radians( vendor.latitude ) ) ) )');
            $products = Vendor::Join('product_prices', 'product_prices.vender_id', '=', 'vendor.id')
                ->Join('products', 'products.id', '=', 'product_prices.product_id')
                ->where('product_prices.status','active')
                ->where('products.is_deleted','0')
                ->selectRaw("{$sqlDistance} AS distance")
                ->having('distance','<',30)
                ->groupBy('product_prices.product_id')
                ->get();
            $productCount = sizeof($products);
        }else{
            $productCount = 0;
        }
        if( $productCount > 0){
            if($oldPin != $data['pincode']){
                if(Auth::user()){
                    $id = Auth::user()->id;
                }else{
                    $id = 0;
                }
                $ipadd = $_SERVER["REMOTE_ADDR"];
                $mac = exec("getmac");
                $macAdd = strtok($mac, ' ');
                Cart::where('carts.user_id',$id)
                    ->orWhere('carts.ip_address',$ipadd)
                    ->orWhere('carts.mac_address',$macAdd)
                    ->delete();
            }
            Session::put('pincode', $data['pincode']);
            Session::put('latitude', $LatLong->latitude);
            Session::put('longitude', $LatLong->longitude);
            $response['data'] =  $productCount;
            $response['status'] = 'success';
            $response['message'] = 'Pincode set successfully';
            $response['latitude'] = $LatLong->latitude;
            $response['longitude'] = $LatLong->longitude;
        }
        echo json_encode($response);
    }
    public function verify_user(Request $request)
    {
        $data = $request->all();
        $user = User::where('mobileno',$data['mobile_number'])->get()->first();
        if(!$user)
        {
            $user = new User();
            $user->mobileno = $data['mobile_number'];
            $user->type = 'user';
            $user->save();

            $credentials = [
                'mobileno' => $data['mobile_number'],
                'id' => $user->id,
                'password' => '',
                'name' => '',
            ];

        }else{
            $user = $user;
            $credentials = [
                'mobileno' => $user['mobileno'],
                'id' => $user['id'],
                'password' => $user['password'],
                'name'=> $user['name']
            ];
        }
        Auth::login($user);

        if($user){
            return redirect('/user/checkout');
        }


    }
    public function userInfo(Request $request){

        $user = Auth::user();
        $info= User::with('address')->where('id',$user->id)->get()->first();

        return view('front.user.info')->with('info',$info);
    }

    public function arrayPaginator($array, $request)
    {
        $data = $request->all();
        $page = $data['page'];
        if($page  == '' || $page  == 0){
            $page = 1;
        }
        $perPage = 12;
        $offset = ($page * $perPage) - $perPage;

        return new \Illuminate\Pagination\LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,
            ['path' => $request->url(), 'query' => $request->query()]);
    }

}
