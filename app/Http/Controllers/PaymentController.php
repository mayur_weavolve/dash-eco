<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Payment;
use App\Order;
use App\Cart;
use App\Refund;
use App\Payout;
use PaytmWallet;
use Mail;

class PaymentController extends Controller
{
    public function payment($orderId)
    {
        $orderDetail = Order::where('order_id',$orderId)->get()->first();
        return view('front.payment.payment')->with('orderDetail',$orderDetail);
    }
    public function invoice(Request $request)
    {
        return view('front.payment.invoice');
    }
    public function payment_success()
    {
        return view('front.payment.payment-success');
    }
    public function payment_fail()
    {
        return view('front.payment.payment-fail');
    }
    public function payment_check($orderId,Request $request)
    {
        $user = Auth::user();
        $input = $request->all();
        $input['user_id'] = $user->id;
        $input['order_id'] = $orderId;

        Payment::create($input);

        $payment = PaytmWallet::with('receive');
        $name = 'Dash';
        $email = 'dashliveapp@gmail.com';
    //    if($user->name != ''){
    //     $name = $user->name;
    //    }
    //    if($user->email != ''){
    //     $email = $user->email;
    //    }
    // echo $input['amount']; echo  $input['order_id'];
        $payment->prepare([
          'order' => $input['order_id'],
          'user' =>  'dash',
          'mobile_number' =>  '8401504264',
          'email' => 'dashliveapp@gmail.com',
          'amount' => $input['amount'],
          'callback_url' => url('api/payment/status')
        ]);
        return $payment->receive();
    }
    public function paymentCallback()
    {
        $transaction = PaytmWallet::with('receive');
        // $user = Auth::user();
// echo  $user->id; exit;
        $response = $transaction->response();
       $order_id = $transaction->getOrderId();


        if($transaction->isSuccessful()){
            Payment::where('order_id',$order_id)
                    ->update(['status'=>1,
                    'transaction_status'=>1,
                    'transaction_id'=>$transaction->getTransactionId()]);
            Order::where('order_id',$order_id)
                ->update(['status'=> 'pending']);

                //send email to customer
                $orders = Order::with('subOrders')
                ->Join('sub_orders', 'sub_orders.order_id', '=', 'orders.id')
                ->Join('products', 'products.id', '=', 'sub_orders.product_id')
                ->leftJoin('product_images', 'product_images.product_id', '=', 'products.id')
                ->where('orders.order_id',$order_id)
                ->select('orders.*' ,'orders.id as oId','sub_orders.id as sId','sub_orders.vendor_id','sub_orders.quantity','sub_orders.amount' ,'products.product_name' ,'product_images.name')
                ->orderBy('orders.id', 'DESC')
                ->groupBy('orders.id')->get();

                $user = Order::where('order_id',$order_id)->get()->first();

                if($user->users->email != ''){
                    Mail::send(['html' => 'front.mail.order_placed'], ['user'=> $user,'orders'=>$orders], function($message) use ($user) {
                        $message->to($user->users->email,$user->users->name)->subject('Order from DASH Shop');
                        $message->from('orders@dashshop.in','DASH Shop');
                    });
                }

                if($user->users->whatsapp_no != ''){
                    //send whatsapp Message user
                    $to = "91".$user->users->whatsapp_no;
                    $link = route('vendor_order_detail',$user->id);
                    if(isset($user->users->name) || $user->users->name != ''){
                        $name = $user->users->name;
                    }else{
                        $name = "Dash User";
                    }
                    $message = "Hi {$name}, thank you for shopping at DASH Shop.

Check your email for order details.

Order tracking Link - {$link}

Thank you once again.
DASH Team";

                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "https://api.gupshup.io/sm/api/v1/app/opt/in/DashOnDuty",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => "user=" . $to ,
                        CURLOPT_HTTPHEADER => array(
                        "apikey: 423ad0b56eef4ab0ca92ba6792af23d2",
                        "Content-Type: application/x-www-form-urlencoded"
                        ),
                    ));
                    $response = curl_exec($curl);
                    curl_close($curl);


                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "https://api.gupshup.io/sm/api/v1/msg",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => "channel=whatsapp&source=919313201429&destination=" . $to . "&message.isHSM=true&message.type=text&message.text=" . $message . "&src.name=DashOnDuty",
                        CURLOPT_HTTPHEADER => array(
                        "apikey: 423ad0b56eef4ab0ca92ba6792af23d2",
                        "Content-Type: application/x-www-form-urlencoded"
                        ),
                    ));
                    $response = curl_exec($curl);
                    curl_close($curl);
                }

                //send whatsapp Message vendor
                $vendorList = [];
                if(sizeof($orders[0]->subOrders) > 0){
                    foreach($orders[0]->subOrders as $orderData){
                        if(!in_array($orderData->vendor->user->whatsapp_no,$vendorList)){
                            array_push($vendorList,$orderData->vendor->user->whatsapp_no);
                        }
                    }

                    $link1 = route('vendor_recent_order_list');
                    foreach($vendorList  as $vendor){
                           $to = "91".$vendor;
                              $message = "Congratulations, you have received a new order from DASH Shop.

Please accept your order - {$link1}

For support call us on +91 99987 63163 or simply message us 'help'.

Thank you
DASH Team";

                            $curl = curl_init();
                            curl_setopt_array($curl, array(
                                CURLOPT_URL => "https://api.gupshup.io/sm/api/v1/app/opt/in/DashOnDuty",
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 0,
                                CURLOPT_FOLLOWLOCATION => true,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "POST",
                                CURLOPT_POSTFIELDS => "user=" . $to ,
                                CURLOPT_HTTPHEADER => array(
                                "apikey: 423ad0b56eef4ab0ca92ba6792af23d2",
                                "Content-Type: application/x-www-form-urlencoded"
                                ),
                            ));
                            $response = curl_exec($curl);
                            curl_close($curl);

                            $curl = curl_init();
                            curl_setopt_array($curl, array(
                                CURLOPT_URL => "https://api.gupshup.io/sm/api/v1/msg",
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 0,
                                CURLOPT_FOLLOWLOCATION => true,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "POST",
                                CURLOPT_POSTFIELDS => "channel=whatsapp&source=919313201429&destination=" . $to . "&message.isHSM=true&message.type=text&message.text=" . $message . "&src.name=DashOnDuty",
                                CURLOPT_HTTPHEADER => array(
                                "apikey: 423ad0b56eef4ab0ca92ba6792af23d2",
                                "Content-Type: application/x-www-form-urlencoded"
                                ),
                            ));
                            $response = curl_exec($curl);
                            curl_close($curl);
                    }
                }
            // delete cart
            $ipadd = $_SERVER["REMOTE_ADDR"];
            $macAdd =  substr(shell_exec('getmac'), 159,20);
            // Cart::where("carts.user_id", $user->id)->orWhere('carts.ip_address',$ipadd)->orWhere('carts.mac_address',$macAdd)->delete();
            Cart::where('carts.ip_address',$ipadd)->orWhere('carts.mac_address',$macAdd)->delete();

        //   dd('Payment Successfully Paid.');
         return redirect(route('payment_success'));
        }else if($transaction->isFailed()){
            Payment::where('order_id',$order_id)
                    ->update(['status'=>0,'transaction_status'=>0, 'transaction_id'=>$transaction->getTransactionId()]);
        //   dd('Payment Failed.');
          return redirect(route('payment_fail'));

        }
    }

    public function payment_list(Request $request)
    {
        $payments = Payment::with('user')->paginate('20');
        return view('payment.list')->with('payments',$payments);
    }

    public function payment_refund_list(Request $request){

        $refunds = Refund::with('user')->with('order')->where('is_refunded','1')->paginate('20');
        return view('payment.refund_list')->with('refunds',$refunds);
    }

    public function payment_payout_list(Request $request){

        $payouts = Payout::with('user')->with('order')->where('is_paid','1')->paginate('20');
        return view('payment.payout_list')->with('payouts',$payouts);
    }
}
