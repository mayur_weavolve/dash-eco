<?php

namespace App\Http\Controllers;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\PaginationServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\User;
use App\Product;
use App\Category;
use App\Subcategory;
use App\UserAddress;
use App\Vendor;
use App\ProductPrice;
use App\ProductAttributeValue;
use App\ProductAttribute;
use App\ProductImage;
use App\ProductSubcategory;
use App\ProductHashtag;
use App\Cart;
use App\Hashtag;
use App\Review;
use App\City;

use Maatwebsite\Excel\Facades\Excel;
use Session;
use DB;
use Auth;
class UserController extends Controller
{
    public function user(Request $request)
    {
        $search = $request->get('search');
        if($search != ''){
            $user =User::where('name','like',"%{$search}%")->where('type','!=','vendor')->where('type','!=','admin')->paginate('20');
        }else{
            $user = User::where('type','!=','vendor')->where('type','!=','admin')->paginate('20');
        }
        $data = [];
        $data['users'] = $user;
        return view('user.list',$data);
    }
    public function add()
    {
        return view('user.add');
    }
    public function save_add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required',
            'mobileno' => 'required',
            'whatsapp_no' => 'required',

        ]);
        if ($validator->fails()) {
            return redirect(route('user_add'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $add = new User();
        $add->name = $data['name'];
        $add->email = $data['email'];
        $add->password = Hash::make($data['password']);
        $add->mobileno = $data['mobileno'];
        $add->whatsapp_no = $data['whatsapp_no'];
        $add->type = 'user';
        $add->save();
        return redirect('/backend/admin/user/list');
    }
    public function edit($id)
    {
        $edit = User::find($id);
        $data = [];
        $data['edit'] = $edit;
        return view('user.edit',$data);
    }

    public function user_edit(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required',

        ]);
        if ($validator->fails()) {
            return redirect(route('user_edit_view',$data['id']))
                        ->withErrors($validator)
                        ->withInput();
        }

        $edit = User::find($data['id']);
        $edit->name = $data['name'];
        $edit->save();

        return redirect('/backend/admin/user/list');
    }
    public function user_delete($id)
    {
        $delete = User::find($id);
        $delete->delete();
        return redirect('/backend/admin/user/list');
    }

    public function review(Request $request){
        $search = $request->get('search');
        if($search != ''){
            $reviews =Review::where('name','like',"%{$search}%")
                        ->orWhere('mobile_number','like','%{$search}%')
                        ->orWhere('review','like','%{$search}%')
                        ->orWhere('rating','like','%{$search}%')
                        ->orWhere('status','like','%{$search}%')
                        ->paginate('20');
        }else{
            $reviews = Review::with('product')->paginate('20');
        }
        return view('review.review')->with('reviews',$reviews);
    }

    public function review_edit_view($id)
    {
        $edit = Review::find($id);
        $data = [];
        $data['edit'] = $edit;
        return view('review.edit',$data);
    }

    public function review_edit(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'status' => 'required',

        ]);
        if ($validator->fails()) {
            return redirect(route('review_edit_view',$data['id']))
                        ->withErrors($validator)
                        ->withInput();
        }

        $edit = Review::find($data['id']);
        $edit->status = $data['status'];
        $edit->save();

        return redirect('/backend/admin/review/list');
    }

    public function adminInfo(Request $request,$id){

        $user = Auth::user();
        $info= User::with('address')->where('id',$id)->get()->first();

        return view('info')->with('info',$info);
    }

    public function adminVendorInfo(Request $request,$id){

        $user = Auth::user();
        $vendor = Vendor::where('user_id',$user->id)->get()->first();
        $info= Vendor::with('user')->where('id',$id)->get()->first();

        return view('vendor.info')->with('info',$info)->with('vendor',$vendor);
    }

    public function adminProductInfo(Request $request,$id){

        $user = Auth::user();
        $info= Product::with('productPrice')->where('id',$id)->get()->first();

        return view('product.info')->with('info',$info);
    }
}
