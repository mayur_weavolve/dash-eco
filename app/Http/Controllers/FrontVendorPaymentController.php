<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Vendor;
use Maatwebsite\Excel\Facades\Excel;
use App\VendorPaymentAccount;
use Auth;
class FrontVendorPaymentController extends Controller
{
    public function payment_view(Request $request){
        $user = Auth::user();
        if(isset($user)){
            $vendor = Vendor::where('user_id',$user->id)->get()->first();
        }else{
            $vendor = '';
        }
        return view('front.payment.view')->with('user',$user)->with('vendor',$vendor);
    }

    public function payment_add(Request $request){
        $user = Auth::user();
        if(isset($user)){
            $vendor = Vendor::where('user_id',$user->id)->get()->first();
        }else{
            $vendor = '';
        }
        $user = Auth::user();
        $vendor = Vendor::where('user_id',$user->id)->get()->first();

        $data = $request->all();
        if($data['type'] == 'bank_account'){
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'ifsc' => 'required',
                'bank_name' => 'required',
                'account_number' => 'required',

            ]);
        }
        else{
            $validator = Validator::make($request->all(), [
                'address' => 'required',
            ]);
        }
        if ($validator->fails()) {
        return redirect(route('vendor_payment_view'))
                    ->withErrors($validator)
                    ->withInput()
                    ->with('type' ,$data['type']);
        }
        if($vendor->contact_id == ''){
            $url = 'https://api.razorpay.com/v1/contacts';
            $ch = curl_init($url);
            $username = 'rzp_test_kDo8L2Su1nuZqW';
            $password = 'k73VtcnqxWDnI19B2GHgY4sH';

            $fields = array();
            $fields["name"] = $user->name;
            $fields["email"] = $user->email;
            $fields["contact"] = $user->mobileno;
            $fields["reference_id"] = $user->mobileno;
            $fields["type"] = "vendor";

            // $note[] = array();
            // $note["notes_key_1"]  = $vendor->name;
            // $note["notes_key_2"]  = $vendor->name;
            // $fields['notes'] = json_encode($note);

            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type:application/json']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);

            if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200) {
                $dresult = json_decode($result, TRUE);
                $contact_id = $dresult['id'];
                $new = Vendor::find($vendor->id);
                $new->contact_id = $contact_id;
                $new->save();
            }
        }
            $accountId = 0;
            $errorMsg = '';
            $vendor = Vendor::where('user_id',$user->id)->get()->first();
            $url = 'https://api.razorpay.com/v1/fund_accounts';
            $chh = curl_init($url);
            $username = 'rzp_test_kDo8L2Su1nuZqW';
            $password = 'k73VtcnqxWDnI19B2GHgY4sH';
            $account = array();
            $account["contact_id"] = $vendor->contact_id;
            $account["account_type"] = $data['type'];

            if($data['type'] == 'bank_account'){
                $bank_account = array(
                    "name" =>$data['name'],
                    "ifsc"  => $data['ifsc'],
                    "account_number"  => $data['account_number']
                );
                $account['bank_account'] = $bank_account;
            }else{
                $vpa = array(
                    "address" => $data['address']
                );
                $account['vpa'] = $vpa;
            }

            curl_setopt($chh, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($chh, CURLOPT_POSTFIELDS, json_encode($account));
            curl_setopt($chh, CURLOPT_HTTPHEADER, ['Content-Type:application/json']);
            curl_setopt($chh, CURLOPT_RETURNTRANSFER, true);
            $results = curl_exec($chh);

            if ($results) {
                $dresult = json_decode($results, TRUE);
                $accountId = $dresult['id'];

            }else{
                $dresult = json_decode($results, TRUE);
                $errorMsg =   $dresult['error']['description'];
            }
            $contact_id = $vendor->contact_id;

            if($accountId != '0'){
                $account = new VendorPaymentAccount();
                $account->vendor_id = $vendor->id;
                $account->user_id = $vendor->user_id;
                $account->contact_id = $contact_id;
                $account->type = $data['type'];
                $account->account_id = $accountId;
                if($data['type'] == 'bank_account'){
                    $newNumber = substr($data['account_number'], -4);
                    $account->account_name = $newNumber;
                }else{
                    $account->account_name = $data['address'];
                }
                $account->save();
                return view('front.payment.view')->with('success','Payment method added successfully')->with('user',$user)->with('vendor',$vendor);
            }else{
                return view('front.payment.view')->with('error',$errorMsg)->with('user',$user)->with('vendor',$vendor);
            }


        $payouts = Payout::with('user')->with('vendor')->with('order')->with('product')->with('productImage')->where('vendor_id',$vendor->id)->get();

        foreach($payouts as $payout){
            Mail::send(['html' => 'front.mail.payout'], $payout, function($message) {
                $message->to($payout->user->email,$user->users->name)->subject('Payout from DASH SHOP');
                $message->from('hi@dashshop.in','DASH Shop');
            });
        }
        return view('front.payment.view');
    }

}

