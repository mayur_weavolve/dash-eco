<?php

namespace App\Http\Controllers;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\PaginationServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\User;
use App\Product;
use App\Category;
use App\Subcategory;
use App\UserAddress;
use App\Vendor;
use App\ProductPrice;
use App\ProductAttributeValue;
use App\ProductAttribute;
use App\ProductImage;
use App\ProductSubcategory;
use App\ProductHashtag;
use App\Cart;
use App\Hashtag;
use App\Review;
use App\Order;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use DB;
use Auth;
class FrontVendorProductController extends Controller
{
    public function vendorHome(Request $request){
        $user = Auth::user();
        if(isset($user)){
            $vendor = Vendor::where('user_id',$user->id)->get()->first();
            $products = Vendor::join('product_prices', 'vendor.id','=' ,'product_prices.vender_id')
                            ->leftJoin('products', 'product_prices.product_id','=' ,'products.id')
                            ->leftJoin('product_images', 'products.id', '=', 'product_images.product_id')
                            ->where('vendor.user_id',$user['id'])
                            ->select('products.*', 'product_prices.*','product_images.name as image')
                            ->groupBy('products.id')->orderBy('product_prices.id','desc')->skip(0)->take(3)->get();

            $orders = Order::with('subOrders')
                    ->Join('sub_orders', 'sub_orders.order_id', '=', 'orders.id')
                    ->Join('products', 'products.id', '=', 'sub_orders.product_id')
                    ->leftJoin('product_images', 'product_images.product_id', '=', 'products.id')
                    ->Join('vendor', 'vendor.id', '=', 'sub_orders.vendor_id')
                    ->where('vendor.user_id',$user['id'])
                    ->whereIn('sub_orders.status',["pending","processing","cancel_by_vendor"])
                    ->select('orders.*' ,'orders.id as oId','sub_orders.id as sId','sub_orders.vendor_id','sub_orders.quantity','sub_orders.amount' ,'products.product_name' ,'product_images.name')
                    ->orderBy('orders.id', 'DESC')
                    ->groupBy('orders.id')
                    ->take(3)->get();

        }else{
            $vendor = '';
            $products = productPrice::leftJoin('products', 'product_prices.product_id','=' ,'products.id')
                            ->leftJoin('product_images', 'products.id', '=', 'product_images.product_id')
                            ->select('products.*', 'product_prices.*','product_images.name as image')
                            ->groupBy('products.id')->orderBy('product_prices.id','desc')->skip(0)->take(3)->get();
            $orders ='';
        }

        return view('vendor.home')->with('user',$user)->with('products',$products)->with('vendor',$vendor)->with('orders',$orders);
    }

    public function VendorProductList(Request $request){
        $user = Auth::user();
        if($user){
        $vendor = Vendor::where('user_id',$user->id)->get()->first();
        $products = Vendor::leftJoin('product_prices', 'vendor.id','=' ,'product_prices.vender_id')
                            ->leftJoin('products', 'product_prices.product_id','=' ,'products.id')
                            ->where('vendor.user_id',$user->id)
                            ->select('products.*', 'product_prices.*','product_prices.id as pro_id','products.status as proStatus')
                            ->orderBy('product_prices.id','desc')->get();
        }else{
            $vendor = [];
            $products = [];
            // $products = ProductPrice::leftJoin('products', 'product_prices.product_id','=' ,'products.id')
            //                 ->select('products.*', 'product_prices.*','product_prices.id as pro_id','products.status as proStatus')
            //                 ->orderBy('products.id','desc')->get();
        }
        return view('vendor.product.list')->with('user',$user)->with('products',$products)->with('vendor',$vendor);
    }

    public function vendor_add_product(Request $request)
    {
        $category = Category::with('subcategory')->where('status','active')->get();
        $hashtags = Hashtag::get();
        $data = [];
        $data['categories'] = $category;
        $data['hashtags'] = $hashtags;
        return view('vendor.product.add_product',$data);
    }
    public function vendor_save_product_add(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'product_code' => 'required',
            'product_name' => 'required',
            'description' => 'required',
            'hashtags' => 'required',

        ]);

        if ($validator->fails()) {
            return redirect(route('vendor_add_product'))
                        ->withErrors($validator)
                        ->withInput();
        }

        $edit = new Product();
        $data = $request->all();
        $edit->manufacturer = $data['manufacturer'];
        $edit->manufacturer_code = $data['manufacturer_code'];
        $edit->product_code = $data['product_code'];
        $edit->product_name = $data['product_name'];
        $edit->description = $data['description'];
        $edit->specification = $data['specification'];
        $edit->product_video = $data['product_video'];
        $edit->is_deleted = '0';
        $edit->status = 'Pending';
        $edit->save();
        if($files = $request->file('product_photo')){
            foreach($files as $file){
                $name = $file->hashName('');
                $file->store('product_photo', ['disk' => 'public']);
                $productImage = new ProductImage();
                $productImage->product_id = $edit->id;
                $productImage->name = $name;
                $productImage->save();
            }
        }
        if(isset($data['category'])){
            if ($data['category'] != null){
                foreach($data['category'] as $cat){
                    $productSubcategory = new ProductSubcategory();
                    $productSubcategory->product_id = $edit->id;
                    $productSubcategory->subcategory_id = $cat;
                    $productSubcategory->save();
                }
            }
        }
        if ($data['hashtags'] != null && sizeof($data['hashtags']) > 0 ){
            foreach( $data['hashtags'] as $hashtag){
                $productHashtag = new ProductHashtag();
                $productHashtag->product_id = $edit->id;
                $productHashtag->hashtag = $hashtag;
                $productHashtag->save();
            }
        }
        $color_attribute_value = 0;
        if (isset($data['colour'] ))
        {
            $color_attribute_value = 1;
        }else{
            $color_attribute_value = 0;
        }
        $productColour = new ProductAttribute();
        $productColour->product_id = $edit->id;
        $productColour->attribute_name = 'colour';
        $productColour->attribute_value =  $color_attribute_value;
        $productColour->save();
        if (isset($data['weight'])){
            foreach($data['weight'] as $weight){
                $productWeight = new ProductAttribute();
                $productWeight->product_id = $edit->id;
                $productWeight->attribute_name = 'weight';
                $productWeight->attribute_value = $weight;
                $productWeight->save();
            }
        }
        if (isset($data['measurement'])){
            foreach($data['measurement'] as $measurement){
                $productMeasurement = new ProductAttribute();
                $productMeasurement->product_id = $edit->id;
                $productMeasurement->attribute_name = 'measurement';
                $productMeasurement->attribute_value = $measurement;
                $productMeasurement->save();
            }
        }
        if (isset($data['size'])){
            foreach($data['size'] as $size){
                $productSize = new ProductAttribute();
                $productSize->product_id = $edit->id;
                $productSize->attribute_name = 'size';
                $productSize->attribute_value = $size;
                $productSize->save();
            }
        }
        if (isset($data['storage'])){
            foreach($data['storage'] as $storage){
                $productStorage = new ProductAttribute();
                $productStorage->product_id = $edit->id;
                $productStorage->attribute_name = 'storage';
                $productStorage->attribute_value = $storage;
                $productStorage->save();
            }
        }

        return redirect('/vendor/product');
    }
    public function product_price()
    {
        $user_id = Auth::user()->id;
        $vendor_id = Vendor::where('user_id',$user_id)->get();
        $vendor = Vendor::find($vendor_id[0]->id);
        $price = ProductPrice::where('vender_id',$vendor->id)->get();
        $data = [];
        $data['product_prices'] = $price;
        return view('vendor.productPrice.price_list',['vendor'=>$vendor],$data);
    }
    public function vendor_product_price_add($id)
    {
        $vendor = Vendor::find($id);
        $product = Product::get();
        $data = [];
        $data['products'] = $product;
        return view('vendor.productPrice.add_product_price',['vendor'=>$vendor],$data);
    }
    public function vendor_save_product_price(Request $request,$id)
    {
        $vendor = Vendor::find($id);
        // $user = Auth::user();
        // $vendor = Vendor::where('user_id',$user->id)->first();
        // print_r( $vendor); exit;
        $validator = Validator::make($request->all(), [
            'mrp' => 'required',
            'final_price' => 'required',
            'free_delivery' => 'required',
            'mode' => 'required',
        ]);
        $data = $request->all();
        $i = 0;
            $add = new ProductPrice();
            $add->vender_id = $vendor->id;
            $add->product_id = $data['product_id'];
            $add->mrp = $data['mrp'];
            $add->discount = $data['discount'];
            $add->final_price = $data['final_price'];
            $add->warranty = $data['warranty'];
            $add->installation_included = $data['installation_included'];
            $add->exchange = $data['exchange'];
            $add->free_delivery = $data['free_delivery'];
            $add->mode = $data['mode'];
            $add->status = 'active';
            $add->save();

            foreach($data['attrRows'] as $attrRow)
            {
                $productAttributeValue = new ProductAttributeValue();
                $productAttributeValue->product_id = $data['product_id'];
                $productAttributeValue->vendor_id =  $vendor->id;
                if(isset($data['colour'][$i])){
                    $productAttributeValue->colour = $data['colour'][$i];
                }
                if(isset($data['weight'][$i])){
                    $productAttributeValue->weight = $data['weight'][$i].$data['weightUnit'][$i];
                }
                if(isset($data['measurement'][$i])){
                    $productAttributeValue->measurement = $data['measurement'][$i].$data['measurementUnit'][$i];
                }
                if(isset($data['size'][$i])){
                    $productAttributeValue->size = $data['size'][$i];
                }
                if(isset($data['quantity'][$i])){
                    $productAttributeValue->quantity = $data['quantity'][$i];
                }
                if(isset($data['storage'][$i])){
                    $productAttributeValue->storage = $data['storage'][$i].$data['storageUnit'][$i];
                }
                $productAttributeValue->save();
                $i++;
            }
            return redirect('/vendor/product');
    }
    public function get_product_attribute(Request $request)
    {
        $response = [];
        $response['status'] = 'error';
        $response['message'] = 'attribute not found';
        $data = $request->all();
        $productAttribute = ProductAttribute::where('product_id',$data['product_id'])->orderby('id','asc')->get();
        $productAttr = [];
        foreach($productAttribute as $attribute){
            $productAttr[$attribute->attribute_name][] = $attribute->attribute_value;
        }
        if(sizeof($productAttr)  > 0){
            $response['status'] = 'success';
            $response['message'] = 'Product Attribute Listing';
            $response['data'] = $productAttr;
        }

        echo json_encode($response);
    }
    public function productDetailEditView($id, $vid){
        $data = [];
        $edit = ProductPrice::find($id);
        $vendor = Vendor::find($vid);
        $product = Product::find($edit->product_id);
        $data['product'] = $product;
        $data['edit'] = $edit;
        $data['vendor'] = $vendor;
        return view('vendor.product.product_edit',$data);
    }
    public function edit_save(Request $request,$id,$vid)
    {
        $validator = Validator::make($request->all(), [
            'mrp' => 'required',
            'final_price' => 'required',
            'free_delivery' => 'required',
            'mode' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect(route('product_detail_edit_view',$id,$vid))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();


        $edit = ProductPrice::find($id);
        $edit->vender_id = $vid;
        $edit->mrp = $data['mrp'];
        $edit->discount = $data['discount'];
        $edit->final_price = $data['final_price'];
        $edit->warranty = $data['warranty'];
        $edit->installation_included = $data['installation_included'];
        $edit->exchange = $data['exchange'];
        $edit->free_delivery = $data['free_delivery'];
        $edit->mode = $data['mode'];
        $edit->status = 'active';
        $edit->save();
        $i = 0;
        ProductAttributeValue::where('product_id',$edit->product_id)->where('vendor_id',$vid)->delete();
        foreach($data['attrRows'] as $attrRow)
        {
            $productAttributeValue = new ProductAttributeValue();
            $productAttributeValue->product_id = $edit->product_id;
            $productAttributeValue->vendor_id =  $vid;
            if(isset($data['colour'][$i])){
                $productAttributeValue->colour = $data['colour'][$i];
            }
            if(isset($data['weight'][$i])){
                $productAttributeValue->weight = $data['weight'][$i].$data['weightUnit'][$i];
            }
            if(isset($data['measurement'][$i])){
                 $productAttributeValue->measurement = $data['measurement'][$i].$data['measurementUnit'][$i];
            }
            if(isset($data['size'][$i])){
                $productAttributeValue->size = $data['size'][$i];
            }
            if(isset($data['quantity'][$i])){
                $productAttributeValue->quantity = $data['quantity'][$i];
            }
            if(isset($data['storage'][$i])){
                $productAttributeValue->storage = $data['storage'][$i].$data['storageUnit'][$i];
            }
            $productAttributeValue->save();
            $i++;
        }
        return redirect(route('vendor_product_list'));

    }
    public function edit_vendor_product_attributeValue(Request $request)
    {

        $response = [];
        $response['status'] = 'error';
        $response['message'] = 'attribute not found';
        $data = $request->all();
        $productAttribute = ProductAttribute::where('product_id',$data['product_id'])->orderby('id','asc')->get();
        $productAttr = [];
        foreach($productAttribute as $attribute){
            $productAttr[$attribute->attribute_name][] = $attribute->attribute_value;
        }
        $productAttributeValue = ProductAttributeValue::where('product_id',$data['product_id'])->where('vendor_id',$data['vendor_id'])->orderby('id','asc')->get();
        $productAttrValue = [];
        $m = 0;
        foreach($productAttributeValue as $attributeValue){
            $productAttrValue[$m]['colour'] = $attributeValue->colour;
            $productAttrValue[$m]['weight'] = $attributeValue->weight;
            $productAttrValue[$m]['measurement'] = $attributeValue->measurement;
            $productAttrValue[$m]['size'] = $attributeValue->size;
            $productAttrValue[$m]['storage'] = $attributeValue->storage;
            $productAttrValue[$m]['quantity'] = $attributeValue->quantity;
            $m++;
        }

        if(sizeof($productAttr)  > 0){
            $response['status'] = 'success';
            $response['message'] = 'Product Attribute Listing';
            $response['data'] = $productAttr;
            $response['dataproductAttrValue'] = $productAttrValue;
        }
        echo json_encode($response);
    }



}
