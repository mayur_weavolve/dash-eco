<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Category;

class CategoryController extends Controller
{
    public function category(Request $request)
    {
        $search = $request->get('search');
        if($search != ''){
            $category =Category::where('name','like',"%{$search}%")->paginate('20');
        }else{
            $category = Category::paginate('20');
        }
        $data = [];
        $data['categories'] = $category;
        return view('category.list',$data);
    }
    public function add()
    {
        return view('category.add');
    }
    public function save_add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'commision' => 'required',
            'status' => 'required',

        ]);
        if ($validator->fails()) {
            return redirect(route('category_add'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $add = new Category();
        $add->name = $data['name'];
        $add->commision = $data['commision'];
        $add->status = $data['status'];
        if ($request->file('image')!=null){
            $add->image = $request->file('image')->hashName('');
            $request->file('image')
            ->store('category_photo', ['disk' => 'public']);
        }
        $add->save();
        return redirect('backend/admin/category/list');
    }
    public function edit($id)
    {
        $edit = Category::find($id);
        return view('category.edit',['edit'=>$edit]);
    }
    public function edit_save(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'commision' => 'required',
            'status' => 'required',

        ]);
        if ($validator->fails()) {
            return redirect(route('category_edit'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $edit = Category::find($id);
        $edit->name = $data['name'];
        $edit->commision = $data['commision'];
        $edit->status = $data['status'];
        if ($request->file('image')!=null){
            $edit->image = $request->file('image')->hashName('');
            $request->file('image')
            ->store('category_photo', ['disk' => 'public']);
        }
        $edit->save();
        return redirect('backend/admin/category/list');
    }
    public function category_delete($id)
    {
        $delete = Category::find($id);
        $delete->status = 'inactive';
        $delete->save();
        return redirect('backend/admin/category/list');
    }


}
