<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;
use App\Subcategory;
use App\User;
use App\Cart;
use App\Vendor;
use App\Subscribe;
use Mail;
use Auth;
use DB;
class FrontMyCartController extends Controller
{
    public function my_cart()
    {
        if(Auth::user()){
            $id = Auth::user()->id;
        }else{
            $id = 0;
        }
        $ipadd = $_SERVER["REMOTE_ADDR"];
        $mac = exec("getmac");
        $macAdd = strtok($mac, ' ');
        $dataDelete =DB::delete('delete from carts where created_at < (NOW() - INTERVAL 30 MINUTE)');

        $vendor = Vendor::where('user_id',$id)->get();
        $carts = Cart::rightJoin('products', 'products.id', '=', 'carts.product_id')
        ->leftJoin('product_images', 'product_images.product_id', '=', 'products.id')
        ->leftJoin('product_attribute_values', 'product_attribute_values.id', '=', 'carts.product_attribute_value_id')
        ->leftJoin('product_prices', 'product_prices.id', '=', 'product_attribute_values.product_price_id')
        // ->leftjoin('vendor','product_prices.vendor_id','=','vendor.id')
        ->where('carts.user_id',$id)
        ->orWhere('carts.ip_address',$ipadd)
        ->orWhere('carts.mac_address',$macAdd)
        ->select('products.*','product_images.*','product_images.name as image','product_prices.*','product_attribute_values.*','carts.id as cId','carts.*')
        ->groupBy('carts.product_attribute_value_id')->get();
        // print_r($carts); exit;
        return view('front.myCart.mycart')->with('carts',$carts)->with('vendor',$vendor);
    }

    public function delete_cart(Request $request, $id){
        $data = Auth::user();
        Cart::where('id',$id)->delete();
        return redirect('/user/my-cart');
        // Mail::send(['html' => 'front.cart'], $data, function($message) {
        //     $message->to($data['email'], 'Cart Info')->subject
        //        ('Delete Cart Item ');
        //     $message->from('hi@dashshop.in','Dashshop');
        //  });
    }

    public function cart_delete(Request $request, $id){
        Cart::where('id',$id)->delete();
        return redirect('/user/my-cart');
    }

    public function updateCart(Request $request){
        $data = $request->all();
        // print_r($data); exit;
        $i = 0;
        foreach($data['quantity'] as $quantity) {
            $edit = Cart::find($data['cartId'][$i]);
            $edit->quantity = $quantity;
            $edit->save();
            $i++;
        }

        return redirect('/user/my-cart');

    }

    public function addToCart(Request $request){

        $response = [];
        $response['status'] = 'error';
        $response['message'] = '';
        $data = $request->all();

        $carts = Cart::where('product_id', '=', $data['productId'])
                  ->where('ip_address', '=', $data['ipadd'])
                  ->where('product_attribute_value_id', '=', $data['productAttributeId'])
                  ->where('mac_address', '=', $data['macAdd'])->where('user_id', '=', $data['userId'])->get();

        if(sizeof($carts) != 0){
            $oldCart = Cart::find($carts[0]->id);
            $oldCart->quantity = $carts[0]->quantity +  $data['quantity'];
            $oldCart->save();
            $response['status'] = 'success';
            $response['message'] = 'Already Added to Cart';
        }
        else{
            $cart = new Cart();
            $cart->user_id = $data['userId'];
            $cart->vendor_id = $data['vendorId'];
            $cart->mac_address = $data['macAdd'];
            $cart->ip_address = $data['ipadd'];
            $cart->product_id = $data['productId'];
            $cart->product_attribute_value_id = $data['productAttributeId'];
            $cart->quantity = $data['quantity'];
            $cart->save();

            if($cart->id >0){
                $response['status'] = 'success';
                $response['message'] = 'Add to Cart Successfully Done';
            }
        }
        echo json_encode($response);

    }



}
