<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\PaginationServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\Imports\ProductImport;
use App\Product;
use App\ProductImage;
use App\ProductSubcategory;
use App\ProductHashtag;
use App\Category;
use App\Hashtag;
use App\ProductAttribute;
use App\ProductAttributeValue;
use App\ProductView;
use App\ProductPrice;
use App\Subcategory;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Session;
use Auth;

class ProductController extends Controller
{
    public function product(Request $request)
    {
        $data = $request->all();
        $search = $request->get('search');
        if($search != ''){
            $list = Product::where('manufacturer','like',"%{$search}%")
                            ->orWhere('manufacturer_code','like',"%{$search}%")
                            ->orWhere('product_code','like',"%{$search}%")
                            ->orWhere('product_name','like',"%{$search}%")
                            ->paginate('20');
        }else{
            $list = Product::where('is_deleted',0)->paginate('20');
        }
        $data = [];
        $data['products'] = $list;
        // $request->session()->put('my_name',$list);
        return view('product.list',$data);
    }
    public function add()
    {

        return view('product.add');
    }
    public function add_product()
    {
        $category = Category::with('subcategory')->where('status','active')->get();
        $hashtags = Hashtag::get();
        $data = [];
        $data['categories'] = $category;
        $data['hashtags'] = $hashtags;
        return view('product.add_product',$data);
    }

    public function save_product(Request $request)
    {
        Excel::import(new ProductImport,request()->file('file'));
        return redirect('backend/admin/product/list');
    }
    public function save_product_add(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'product_code' => 'required',
            'product_name' => 'required|unique:products',
            'description' => 'required',
            'hashtags' => 'required',
            'meta_title'=> 'required',
            'meta_description' => 'required'

        ]);
        print_r($validator); exit;
        if ($validator->fails()) {
            print_r($request->all()); exit;
            return redirect(route('add_product'))
                        ->withErrors($validator)
                        ->withInput();
        }

        $edit = new Product();
        $data = $request->all();
        $edit->manufacturer = $data['manufacturer'];
        $edit->manufacturer_code = $data['manufacturer_code'];
        $edit->product_code = $data['product_code'];
        $edit->product_name = $data['product_name'];
        $edit->description = $data['description'];
        $edit->specification = $data['specification'];
        $edit->product_video = $data['product_video'];
        $edit->status = 'Active';
        $edit->is_deleted = '0';
        $edit->meta_title = $data['meta_title'];
        $edit->meta_description = $data['meta_description'];
        $edit->save();
        if($files = $request->file('product_photo')){
            foreach($files as $file){
                $name = $file->hashName('');
                $file->store('product_photo', ['disk' => 'public']);
                $productImage = new ProductImage();
                $productImage->product_id = $edit->id;
                $productImage->name = $name;
                $productImage->save();
            }
        }
        if(isset($data['category'])){
            if ($data['category'] != null){
                foreach($data['category'] as $cat){
                    $productSubcategory = new ProductSubcategory();
                    $productSubcategory->product_id = $edit->id;
                    $productSubcategory->subcategory_id = $cat;
                    $productSubcategory->save();
                }
            }
        }
        if ($data['hashtags'] != null && sizeof($data['hashtags']) > 0 ){
            foreach( $data['hashtags'] as $hashtag){
                $productHashtag = new ProductHashtag();
                $productHashtag->product_id = $edit->id;
                $productHashtag->hashtag = $hashtag;
                $productHashtag->save();
            }
        }
        $color_attribute_value = 0;
        if (isset($data['colour'] ))
        {
            $color_attribute_value = 1;
        }else{
            $color_attribute_value = 0;
        }
        $productColour = new ProductAttribute();
        $productColour->product_id = $edit->id;
        $productColour->attribute_name = 'colour';
        $productColour->attribute_value =  $color_attribute_value;
        $productColour->save();
        if (isset($data['weight'])){
            foreach($data['weight'] as $weight){
                $productWeight = new ProductAttribute();
                $productWeight->product_id = $edit->id;
                $productWeight->attribute_name = 'weight';
                $productWeight->attribute_value = $weight;
                $productWeight->save();
            }
        }
        if (isset($data['measurement'])){
            foreach($data['measurement'] as $measurement){
                $productMeasurement = new ProductAttribute();
                $productMeasurement->product_id = $edit->id;
                $productMeasurement->attribute_name = 'measurement';
                $productMeasurement->attribute_value = $measurement;
                $productMeasurement->save();
            }
        }
        if (isset($data['size'])){
            foreach($data['size'] as $size){
                $productSize = new ProductAttribute();
                $productSize->product_id = $edit->id;
                $productSize->attribute_name = 'size';
                $productSize->attribute_value = $size;
                $productSize->save();
            }
        }
        if (isset($data['storage'])){
            foreach($data['storage'] as $storage){
                $productStorage = new ProductAttribute();
                $productStorage->product_id = $edit->id;
                $productStorage->attribute_name = 'storage';
                $productStorage->attribute_value = $storage;
                $productStorage->save();
            }
        }

        return redirect('backend/admin/product/list');
    }

    public function edit($id)
    {
        $edit = Product::find($id);
        $category = Category::with('subcategory')->where('status','active')->get();
        $productImages = ProductImage::where('product_id',$id)->get();
        $productSubcategories = ProductSubcategory::select('subcategory_id')->where('product_id',$id)->get();
        $productSubcategoryArray = [];
        foreach($productSubcategories as $vc){
            array_push($productSubcategoryArray,$vc->subcategory_id);
        }
        $hashtags = Hashtag::get();
        $productHashtags = ProductHashtag::where('product_id',$id)->get();
        $productAttribute = ProductAttribute::where('product_id',$id)->get();
        $productAttr = [];
        foreach($productAttribute as $attribute){
            $productAttr[$attribute->attribute_name][] = $attribute->attribute_value;
        }
        $data = [];
        $data['productSubcategory'] = $productSubcategoryArray;
        $data['productImages'] = $productImages;
        $data['categories'] = $category;
        $data['hashtags'] = $hashtags;
        $data['productHashtags'] = $productHashtags;
        $data['productAttributes'] = $productAttr;
        return view('product.edit',['edit'=>$edit],$data);
    }
    public function edit_save(Request $request,$id)
    {

        $validator = Validator::make($request->all(), [
            'product_code' => 'required',
            'product_name' => 'required',
            'description' => 'required',
            'hashtags' => 'required',
            'meta_title'=> 'required',
            'meta_description' => 'required'


        ]);
        if ($validator->fails()) {
            return redirect(route('product_edit',$id))
                        ->withErrors($validator)
                        ->withInput();
        }
        if(isset($data['category']))
        {
            $category = $data['category'];
        }
        else{
            $category = '';
        }

        $data = $request->all();
        $edit = Product::find($id);
        $edit->manufacturer = $data['manufacturer'];
        $edit->manufacturer_code = $data['manufacturer_code'];
        $edit->product_code = $data['product_code'];
        $edit->product_name = $data['product_name'];
        $edit->description = $data['description'];
        $edit->specification = $data['specification'];
        $edit->status = $data['status'];
        $edit->category = $category;
        $edit->hashtags = $data['hashtags'];
        $edit->meta_title = $data['meta_title'];
        $edit->meta_description = $data['meta_description'];

        $edit->save();
        if($files = $request->file('product_photo')){
            ProductImage::where('product_id',$id)->delete();
            foreach($files as $file){
                $name = $file->hashName('');
                $file->store('product_photo', ['disk' => 'public']);
                $productImage = new ProductImage();
                $productImage->product_id = $edit->id;
                $productImage->name = $name;
                $productImage->save();
            }
        }
        ProductSubcategory::where('product_id',$id)->delete();
        if(isset($data['category']))
        {
            if ($data['category'] != null){
                foreach($data['category'] as $cat){
                    $productSubcategory = new ProductSubcategory();
                    $productSubcategory->product_id = $edit->id;
                    $productSubcategory->subcategory_id = $cat;
                    $productSubcategory->save();
                }
            }
        }
        ProductHashtag::where('product_id',$id)->delete();
        if ($data['hashtags'] != null && sizeof($data['hashtags']) > 0 ){
            foreach( $data['hashtags'] as $hashtag){
                $productHashtag = new ProductHashtag();
                $productHashtag->product_id = $edit->id;
                $productHashtag->hashtag = $hashtag;
                $productHashtag->save();
            }
        }
        ProductAttribute::where('product_id',$id)->delete();
        if (isset($data['colour']))
        {
            $color_attribute_value = 1;
        }else{
            $color_attribute_value = 0;
        }
        $productColour = new ProductAttribute();
        $productColour->product_id = $edit->id;
        $productColour->attribute_name = 'colour';
        $productColour->attribute_value =  $color_attribute_value;
        $productColour->save();
        if (isset($data['weight'])){
            foreach($data['weight'] as $weight){
                $productWeight = new ProductAttribute();
                $productWeight->product_id = $edit->id;
                $productWeight->attribute_name = 'weight';
                $productWeight->attribute_value = $weight;
                $productWeight->save();
            }
        }
        if (isset($data['measurement'])){
            foreach($data['measurement'] as $measurement){
                $productMeasurement = new ProductAttribute();
                $productMeasurement->product_id = $edit->id;
                $productMeasurement->attribute_name = 'measurement';
                $productMeasurement->attribute_value = $measurement;
                $productMeasurement->save();
            }
        }
        if (isset($data['size'])){
            foreach($data['size'] as $size){
                $productSize = new ProductAttribute();
                $productSize->product_id = $edit->id;
                $productSize->attribute_name = 'size';
                $productSize->attribute_value = $size;
                $productSize->save();
            }
        }
        if (isset($data['storage'])){
            foreach($data['storage'] as $storage){
                $productStorage = new ProductAttribute();
                $productStorage->product_id = $edit->id;
                $productStorage->attribute_name = 'storage';
                $productStorage->attribute_value = $storage;
                $productStorage->save();
            }
        }
        return redirect('backend/admin/product/list');
    }
    public function delete($id)
    {
        $delete = Product::find($id);
        $delete->is_deleted = 1;
        $delete->save();
        return redirect('backend/admin/product/list');
    }

    public function get_product_attribute(Request $request)
    {
        $response = [];
        $response['status'] = 'error';
        $response['message'] = 'attribute not found';
        $data = $request->all();
        $productAttribute = ProductAttribute::where('product_id',$data['product_id'])->orderby('id','asc')->get();
        $productAttr = [];
        foreach($productAttribute as $attribute){
            $productAttr[$attribute->attribute_name][] = $attribute->attribute_value;
        }
        if(sizeof($productAttr)  > 0){
            $response['status'] = 'success';
            $response['message'] = 'Product Attribute Listing';
            $response['data'] = $productAttr;
        }

        echo json_encode($response);
    }

    public function edit_product_attributeValue(Request $request)
    {
        $response = [];
        $response['status'] = 'error';
        $response['message'] = 'attribute not found';
        $data = $request->all();
        $productAttribute = ProductAttribute::where('product_id',$data['product_id'])->orderby('id','asc')->get();
        $productAttr = [];
        foreach($productAttribute as $attribute){
            $productAttr[$attribute->attribute_name][] = $attribute->attribute_value;
        }
        $productAttributeValue = ProductAttributeValue::where('product_id',$data['product_id'])->where('vendor_id',$data['vendor_id'])->orderby('id','asc')->get();
        $productAttrValue = [];
        $m = 0;
        foreach($productAttributeValue as $attributeValue){
            $productAttrValue[$m]['colour'] = $attributeValue->colour;
            $productAttrValue[$m]['weight'] = $attributeValue->weight;
            $productAttrValue[$m]['measurement'] = $attributeValue->measurement;
            $productAttrValue[$m]['size'] = $attributeValue->size;
            $productAttrValue[$m]['storage'] = $attributeValue->storage;
            $productAttrValue[$m]['quantity'] = $attributeValue->quantity;
            $m++;
        }
        // print_r($productAttrValue); exit;
        if(sizeof($productAttr)  > 0){
            $response['status'] = 'success';
            $response['message'] = 'Product Attribute Listing';
            $response['data'] = $productAttr;
            $response['dataproductAttrValue'] = $productAttrValue;
        }

        echo json_encode($response);
    }
    public function productDetail(Request $request,$product,$id){

        $minDistance = 0;
        $maxDistance = 30;
        $data = $request->all();
        $idArray = explode("_",$id);
        $vid =  9;
        $pid =  214;

        $ipadd = $_SERVER["REMOTE_ADDR"];
        $mac = exec("getmac");
        $macAdd = strtok($mac, ' ');
        $where = [];
        if(isset($data['colour'])){
            $where[] = ['colour','=',$data['colour']];
        }
        if(Session::get('pincode') == ''){
            $latitude = '22.56986';
            $longitude = '72.963773';
        }else{
            $latitude = Session::get('latitude');
            $longitude = Session::get('longitude');
        }
        $sqlDistance = DB::raw('( 6371 * acos( cos( radians(' . $latitude . ') )
        * cos( radians( vendor.latitude ) )
        * cos( radians( vendor.longitude )
        - radians(' . $longitude  . ') )
        + sin( radians(' . $latitude . ') )
        * sin( radians( vendor.latitude ) ) ) )');

        if(Auth::user()){
            $id = Auth::user()->id;
            if($id != 0){
                $user_id = $id;
            }else{
                $user_id = 0;
            }
        }else{
            $user_id = 0;
        }
        $proView = new ProductView();
        $proView->user_id = $user_id;
        $proView->product_id = $pid;
        $proView->latitude = $latitude;
        $proView->longitude = $longitude;
        $proView->save();

        $products = Product::where('id',$pid)->get()->first();
        $proReviews = Product::with('reviews')->where('id',$pid)->get();

        $productPrice = ProductPrice::where('vender_id',$vid)->where('product_id',$pid)->get()->first();
        $productPrices = ProductPrice::select('product_prices.*')->join('vendor','vendor.id','=','product_prices.vender_id')->where('product_prices.vender_id','!=',$vid)->where('product_prices.product_id',$pid)->whereBetween($sqlDistance,[$minDistance,$maxDistance])->get();

        $productAttributeElement = ProductAttribute::where('product_id',$pid)->select('product_attributes.attribute_name')->distinct('product_attributes.attribute_name')->get();
        $storages = ProductAttributeValue::where('product_price_id',$productPrice->id)->select('product_attribute_values.storage')->where('product_attribute_values.storage','!=',null)->distinct('product_attribute_values.storage')->get();
        $colours = ProductAttributeValue::where('product_price_id',$productPrice->id)->select('product_attribute_values.colour')->where('product_attribute_values.colour','!=',null)->groupby('product_attribute_values.colour')->get();
        $measurements = ProductAttributeValue::where('product_price_id',$productPrice->id)->select('product_attribute_values.measurement')->where('product_attribute_values.measurement','!=',null)->distinct('product_attribute_values.measurement')->get();
        $weights = ProductAttributeValue::where('product_price_id',$productPrice->id)->select('product_attribute_values.weight')->where('product_attribute_values.weight','!=',null)->distinct('product_attribute_values.weight')->get();
        $sizes = ProductAttributeValue::where('product_price_id',$productPrice->id)->select('product_attribute_values.size')->where('product_attribute_values.size','!=',null)->distinct('product_attribute_values.size')->get();
        $productAttValueArray = ProductAttributeValue::where('product_price_id',$productPrice->id)->select('product_attribute_values.*')->get();
        $productAttr = [];
        $productAttr['colour'] = $colours;
        $productAttr['storage'] = $storages;
        $productAttr['measurement'] = $measurements;
        $productAttr['weight'] = $weights;
        $productAttr['size'] = $sizes;
        $productAttr['vendorId'] = $vid;
        $productSubcategoryId = $products->productsubcategory->subcategory_id;
        $productCategoryId = $products->productsubcategory->subcategory->category_id;
        $productbysubcategory = Subcategory::Join('product_subcategories', 'product_subcategories.subcategory_id', '=', 'subcategories.id')
        ->Join('products', 'products.id', '=', 'product_subcategories.product_id')
        ->Join('product_images', 'product_images.product_id', '=', 'products.id')
        ->Join('product_prices', 'product_prices.product_id', '=', 'products.id')
        ->Join('vendor', 'vendor.id', '=', 'product_prices.vender_id')
        ->where('product_subcategories.subcategory_id',$productSubcategoryId)
        ->groupBy('product_subcategories.product_id')
        ->select('products.*', 'subcategories.*', 'product_subcategories.*','product_images.name as productImage','product_prices.*','subcategories.name as subcategoryName','vendor.nickname','vendor.id as vendorID')->get();
        $customerBroughtProducts = Subcategory::Join('product_subcategories', 'product_subcategories.subcategory_id', '=', 'subcategories.id')
        ->Join('products', 'products.id', '=', 'product_subcategories.product_id')
        ->rightjoin('product_images', 'product_images.product_id', '=', 'products.id')
        ->Join('product_prices', 'product_prices.product_id', '=', 'products.id')
        ->Join('vendor', 'vendor.id', '=', 'product_prices.vender_id')
        ->groupBy('product_subcategories.product_id')
        ->orderby('products.created_at','DESC')
        ->select('products.*', 'subcategories.*', 'product_subcategories.*','product_images.name as productImage','product_prices.*','subcategories.name as subcategoryName','vendor.nickname','vendor.id as vendorID')->skip(0)->take(12)->get();
        $productFromSameBrands = Subcategory::Join('product_subcategories', 'product_subcategories.subcategory_id', '=', 'subcategories.id')
            ->Join('products', 'products.id', '=', 'product_subcategories.product_id')
            ->Join('product_images', 'product_images.product_id', '=', 'products.id')
            ->Join('product_prices', 'product_prices.product_id', '=', 'products.id')
            ->Join('vendor', 'vendor.id', '=', 'product_prices.vender_id')
            ->where('products.manufacturer',$products->manufacturer)
            ->groupBy('products.id')
            ->select('products.*', 'subcategories.*', 'product_subcategories.*','product_images.name as productImage','product_prices.*','subcategories.name as subcategoryName','vendor.nickname','vendor.id as vendorID')->skip(0)->take(3)->get();;

        // $proAtt = ProductAttributeValue::where('product_id',$id)->get();
        // print_r($productAttributeElement); exit;
        return view('product.detail')->with('products',$products)->with('ipadd',$ipadd)->with('macAdd',$macAdd)->with('productbysubcategory',$productbysubcategory)->with('customerBroughtProducts',$customerBroughtProducts)->with('productFromSameBrands',$productFromSameBrands)->with('productAttributeValue',$productAttr)->with('productPrice',$productPrice)->with('productPrices',$productPrices)->with('productAttValueArray',$productAttValueArray)->with('proReviews',$proReviews);
    }

    public function adminproductDetail(Request $request,$product,$id){

        $minDistance = 0;
        $maxDistance = 30;
        $data = $request->all();
        // $idArray = explode("_",$id);
        $vid =  9;
        $pid =  $id;

        $ipadd = $_SERVER["REMOTE_ADDR"];
        $mac = exec("getmac");
        $macAdd = strtok($mac, ' ');
        $where = [];
        // if(isset($data['colour'])){
        //     $where[] = ['colour','=',$data['colour']];
        // }
        // if(Session::get('pincode') == ''){
        //     $latitude = '22.56986';
        //     $longitude = '72.963773';
        // }else{
        //     $latitude = Session::get('latitude');
        //     $longitude = Session::get('longitude');
        // }
        // $sqlDistance = DB::raw('( 6371 * acos( cos( radians(' . $latitude . ') )
        // * cos( radians( vendor.latitude ) )
        // * cos( radians( vendor.longitude )
        // - radians(' . $longitude  . ') )
        // + sin( radians(' . $latitude . ') )
        // * sin( radians( vendor.latitude ) ) ) )');

        // if(Auth::user()){
        //     $id = Auth::user()->id;
        //     if($id != 0){
        //         $user_id = $id;
        //     }else{
        //         $user_id = 0;
        //     }
        // }else{
        //     $user_id = 0;
        // }
        // $proView = new ProductView();
        // $proView->user_id = $user_id;
        // $proView->product_id = $pid;
        // $proView->latitude = $latitude;
        // $proView->longitude = $longitude;
        // $proView->save();

        $products = Product::where('id',$pid)->get()->first();
        $proReviews = Product::with('reviews')->where('id',$pid)->get();

        // $productPrice = ProductPrice::where('vender_id',$vid)->where('product_id',$pid)->get()->first();
        // $productPrices = ProductPrice::select('product_prices.*')->join('vendor','vendor.id','=','product_prices.vender_id')->where('product_prices.vender_id','!=',$vid)->where('product_prices.product_id',$pid)->whereBetween($sqlDistance,[$minDistance,$maxDistance])->get();

        // $productAttributeElement = ProductAttribute::where('product_id',$pid)->select('product_attributes.attribute_name')->distinct('product_attributes.attribute_name')->get();
        // $storages = ProductAttributeValue::where('product_price_id',$productPrice->id)->select('product_attribute_values.storage')->where('product_attribute_values.storage','!=',null)->distinct('product_attribute_values.storage')->get();
        // $colours = ProductAttributeValue::where('product_price_id',$productPrice->id)->select('product_attribute_values.colour')->where('product_attribute_values.colour','!=',null)->groupby('product_attribute_values.colour')->get();
        // $measurements = ProductAttributeValue::where('product_price_id',$productPrice->id)->select('product_attribute_values.measurement')->where('product_attribute_values.measurement','!=',null)->distinct('product_attribute_values.measurement')->get();
        // $weights = ProductAttributeValue::where('product_price_id',$productPrice->id)->select('product_attribute_values.weight')->where('product_attribute_values.weight','!=',null)->distinct('product_attribute_values.weight')->get();
        // $sizes = ProductAttributeValue::where('product_price_id',$productPrice->id)->select('product_attribute_values.size')->where('product_attribute_values.size','!=',null)->distinct('product_attribute_values.size')->get();
        // $productAttValueArray = ProductAttributeValue::where('product_price_id',$productPrice->id)->select('product_attribute_values.*')->get();
        // $productAttr = [];
        // $productAttr['colour'] = $colours;
        // $productAttr['storage'] = $storages;
        // $productAttr['measurement'] = $measurements;
        // $productAttr['weight'] = $weights;
        // $productAttr['size'] = $sizes;
        // $productAttr['vendorId'] = $vid;
        $productSubcategoryId = $products->productsubcategory->subcategory_id;
        $productCategoryId = $products->productsubcategory->subcategory->category_id;
        $productbysubcategory = Subcategory::Join('product_subcategories', 'product_subcategories.subcategory_id', '=', 'subcategories.id')
        ->Join('products', 'products.id', '=', 'product_subcategories.product_id')
        ->Join('product_images', 'product_images.product_id', '=', 'products.id')
        ->Join('product_prices', 'product_prices.product_id', '=', 'products.id')
        ->Join('vendor', 'vendor.id', '=', 'product_prices.vender_id')
        ->where('product_subcategories.subcategory_id',$productSubcategoryId)
        ->groupBy('product_subcategories.product_id')
        ->select('products.*', 'subcategories.*', 'product_subcategories.*','product_images.name as productImage','product_prices.*','subcategories.name as subcategoryName','vendor.nickname','vendor.id as vendorID')->get();
        $customerBroughtProducts = Subcategory::Join('product_subcategories', 'product_subcategories.subcategory_id', '=', 'subcategories.id')
        ->Join('products', 'products.id', '=', 'product_subcategories.product_id')
        ->rightjoin('product_images', 'product_images.product_id', '=', 'products.id')
        ->Join('product_prices', 'product_prices.product_id', '=', 'products.id')
        ->Join('vendor', 'vendor.id', '=', 'product_prices.vender_id')
        ->groupBy('product_subcategories.product_id')
        ->orderby('products.created_at','DESC')
        ->select('products.*', 'subcategories.*', 'product_subcategories.*','product_images.name as productImage','product_prices.*','subcategories.name as subcategoryName','vendor.nickname','vendor.id as vendorID')->skip(0)->take(12)->get();
        $productFromSameBrands = Subcategory::Join('product_subcategories', 'product_subcategories.subcategory_id', '=', 'subcategories.id')
            ->Join('products', 'products.id', '=', 'product_subcategories.product_id')
            ->Join('product_images', 'product_images.product_id', '=', 'products.id')
            ->Join('product_prices', 'product_prices.product_id', '=', 'products.id')
            ->Join('vendor', 'vendor.id', '=', 'product_prices.vender_id')
            ->where('products.manufacturer',$products->manufacturer)
            ->groupBy('products.id')
            ->select('products.*', 'subcategories.*', 'product_subcategories.*','product_images.name as productImage','product_prices.*','subcategories.name as subcategoryName','vendor.nickname','vendor.id as vendorID')->skip(0)->take(3)->get();;

        // $proAtt = ProductAttributeValue::where('product_id',$id)->get();
        // print_r($productAttributeElement); exit;
        return view('product.admin_detail')->with('products',$products)->with('ipadd',$ipadd)->with('macAdd',$macAdd)->with('productbysubcategory',$productbysubcategory)->with('customerBroughtProducts',$customerBroughtProducts)->with('productFromSameBrands',$productFromSameBrands)->with('proReviews',$proReviews);
    }
}
