<?php

namespace App\Http\Controllers;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\PaginationServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\User;

use App\City;

use Maatwebsite\Excel\Facades\Excel;
use Session;
use DB;
use Auth;

class CityController extends Controller
{
    public function city(Request $request)
    {
        $search = $request->get('search');
        if($search != ''){
            $city =City::where('city_name','like',"%{$search}%")->paginate('20');
        }else{
            $city = City::paginate('20');
        }
        $data = [];
        $data['cities'] = $city;
        return view('city.list',$data);
    }
    public function add()
    {
        return view('city.add');
    }
    public function save_add(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'city_name' => 'required|unique:cities',
            'country_code' => 'required',
            'state_code' => 'required',
            'zipcode' => 'required',
            'latitude' => 'required',
            'longitude' => 'required'

        ]);
        if ($validator->fails()) {
            return redirect(route('city_add'))
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $add = new City();
        $add->city_name = $data['city_name'];
        $add->country_code = $data['country_code'];
        $add->state_code = $data['state_code'];
        $add->zipcode = $data['zipcode'];
        $add->latitude = $data['latitude'];
        $add->longitude = $data['longitude'];
        $add->save();
        return redirect('/backend/admin/city/list');
    }
    public function edit($id)
    {
        $edit = City::find($id);
        $data = [];
        $data['edit'] = $edit;
        return view('city.edit',$data);
    }

    public function city_edit(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'city_name' => 'required',
            'country_code' => 'required',
            'state_code' => 'required',
            'zipcode' => 'required',
            'latitude' => 'required',
            'longitude' => 'required'
        ]);
        
        if ($validator->fails()) {
            return redirect(route('city_edit_view',$data['id']))
                        ->withErrors($validator)
                        ->withInput();
        }

        $edit = City::find($data['id']);
        $edit->city_name = $data['city_name'];
        $edit->country_code = $data['country_code'];
        $edit->state_code = $data['state_code'];
        $edit->zipcode = $data['zipcode'];
        $edit->latitude = $data['latitude'];
        $edit->longitude = $data['longitude'];
        $edit->save();

        return redirect('/backend/admin/city/list');
    }
    public function city_delete($id)
    {
        $delete = City::find($id);
        $delete->delete();
        return redirect('/backend/admin/city/list');
    }

    


}
