<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Category;
use App\Product;
use App\Subcategory;
use App\User;
use App\Cart;
use App\Vendor;
use App\Subscribe;
use App\UserAddress;
use App\Order;
use App\SubOrders;
use App\OrderShippingDetail;
use App\City;
use Mail;
use Auth;
use DB;
use Session;
class FrontCheckOutController extends Controller
{
    public function checkout(){
        $user = Auth::user();
        if($user){
            $data = [];
            $user = User::with('address')->find($user->id);
            if(Session::get('pincode') != ''){
                $city = City::where('zipcode',Session::get('pincode'))->get();
                if(sizeof($city) > 0){
                    if( $city[0]->state_code == 'GU'){
                        $data['state'] = 'Gujarat';
                    }else{
                        $data['state'] = '';
                    }
                    $data['city'] = $city[0]->city_name;
                }
            }

            return view('front.checkout.checkout')->with('user',$user)->with('data',$data);
        }
        Session::put('signupfor','checkout');
        return redirect('/signup');

    }

    public function add_to_checkout(Request $request){
        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'addressline1' => 'required',
            'city' => 'required',
            'state' => 'required',
            'pincode' => 'required',

        ]);
        if ($validator->fails()) {
            return redirect(route('checkout'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $Numorder =Order::get();
        if(count($Numorder) > 0)
        {
            $id = Order::latest()->first()->id;
            $id = $id +1 ;
        }else{
            $id = 1;
        }
        $userAddress = UserAddress::where('user_id',$data['user_id'])->get();

        if(sizeof($userAddress) == 0){
            $user = User::find($data['user_id']);
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->pincode = $data['pincode'];
            $user->save();

            $address = new UserAddress();
            $address->user_id = $data['user_id'];
            $address->addressline1 = $data['addressline1'];
            $address->addressline2 = $data['addressline2'];
            $address->city = $data['city'];
            $address->state = $data['state'];
            $address->pincode = $data['pincode'];
            $address->save();


        }

        $date = date('Y-m-d H:i:s');
        $orderId = date("ymd").$data['user_id'].$id;
        $trackingNumber = $this->trackingcode();

            $order = new Order();
            $order->order_id = $orderId;
            $order->user_id = $data['user_id'];
            $order->tracking_number = $trackingNumber;
            $order->amount = $data['amount'];
            $order->total_amount = $data['amount'] ;
            $order->note = $data['note'];
            $order->status = 'pending_payment';
            $order->save();

            if($order->id > 0){
                for($i=1 ; $i<= (count($data['product_id'])) ; $i++)
                {
                    $deliveryArray = [];
                    $deliveryArray['km'] = $data['deliverykm'][$i];
                    $deliveryOption = explode("_",$data['deliveryvalue'][$i]);
                    if($deliveryOption[0] == 0){
                        $deliveryArray['type'] = "Pickup";
                    }else{
                        if($data['deliverykm'][$i] > $deliveryOption[0] ){
                            $deliveryArray['type'] = "Pickup";
                        }else{
                            $deliveryArray['type'] = "Delivery";
                            if($deliveryOption[1] == 1){
                                    if(date('H') < 14){
                                        $deliveryArray['day'] = 'today';
                                        $deliveryArray['date'] = $date;
                                    }else{
                                        $deliveryArray['day'] = 'tomorrow';
                                        $deliveryArray['date'] = $date;
                                    }
                            }else{
                                $deliveryArray['day'] = 'tomorrow';
                                $deliveryArray['date'] = $date;
                            }
                        }
                    }
                    $securityCode = mt_rand(1000,9999);
                    $sub = new SubOrders();
                    $sub->order_id = $order->id;
                    $sub->vendor_id = $data['vendor_id'][$i];
                    $sub->product_id = $data['product_id'][$i];
                    $sub->quantity = $data['quantity'][$i];
                    $sub->amount = $data['amounts'][$i] ;
                    $sub->product_attribute = $data['product_attribute'][$i];
                    $sub->product_attribute_value_id = $data['product_attribute_value_id'][$i];
                    $sub->product_price_id = $data['product_price_id'][$i];
                    $sub->security_code = $securityCode;
                    $sub->delivery_option = json_encode($deliveryArray);
                    $sub->status = 'pending';
                    $sub->save();
                }
             }

            if($order->id > 0){
                $add = new OrderShippingDetail();
                $add->order_id = $order->id;
                $add->name = $data['name'];
                $add->email = $data['email'];
                $add->phone = $data['phone'];
                $add->whatsapp_no = $data['whatsapp_no'];
                $add->address_line1 = $data['addressline1'];
                $add->address_line2 = $data['addressline2'];
                $add->city = $data['city'];
                $add->state = $data['state'];
                $add->pincode = $data['pincode'];
                $add->save();
            }

             //send email to customer
            //  $orders = Order::with('subOrders')
            //  ->Join('sub_orders', 'sub_orders.order_id', '=', 'orders.id')
            //  ->Join('products', 'products.id', '=', 'sub_orders.product_id')
            //  ->leftJoin('product_images', 'product_images.product_id', '=', 'products.id')
            //  ->where('orders.order_id',$orderId)
            //  ->select('orders.*' ,'orders.id as oId','sub_orders.id as sId','sub_orders.vendor_id','sub_orders.quantity','sub_orders.amount' ,'products.product_name' ,'product_images.name')
            //  ->orderBy('orders.id', 'DESC')
            //  ->groupBy('orders.id')->get();

            //  $user = Order::where('order_id',$orderId)->get()->first();
            //     // print_r($user->users); exit;
            //  if($user->users->email != ''){
            //     return view('front.mail.order_placed')->with('user',$user)->with('orders',$orders);
            //      Mail::send(['html' => 'front.mail.order_placed'], ['user'=> $user,'orders'=>$orders], function($message) use ($user) {
            //          $message->to($user->users->email,$user->users->name)->subject('Order from DASH Shop');
            //          $message->from('orders@dashshop.in','DASH Shop');
            //      });
            //  }
            //  exit;

            return redirect(route('payment',$orderId));
            // return redirect('payment',['orderId',$orderId]);
    }

    public function trackingcode(){
        $trackingNumber = mt_rand(10000000,99999999);
        $orders = Order::where('tracking_number', $trackingNumber)->get();
        if(sizeof($orders) > 0){
            trackingcode();
        }else{
            return $trackingNumber;
        }
    }

}
