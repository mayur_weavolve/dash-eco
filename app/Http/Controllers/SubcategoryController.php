<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Category;
use App\Subcategory;

class SubcategoryController extends Controller
{
    public function list(Request $request)
    {
        $search = $request->get('search');
        if($search != ''){
            $subcategory = Subcategory::with('category')->where('name','like',"%{$search}%")->paginate('20');
        }else{
            $subcategory = Subcategory::with('category')->paginate('20');
        }
        
        $data = [];
        $data['subcategories'] = $subcategory;
        return view('subcategory.list',$data);
    }
    public function add()
    {
        $catgory = Category::get();
        $data['categories'] = $catgory;
        return view('subcategory.add',$data);
    }
    public function save_add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'category_name' => 'required|int',
            'status' => 'required',

        ]);
        if ($validator->fails()) {
            return redirect(route('subcategory_add'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $add = new Subcategory();
        $add->name = $data['name'];
        $add->category_id = $data['category_name'];
        $add->status = $data['status'];
        $add->save();
        return redirect('/backend/admin/subcategory/list');
    }
    public function edit($id)
    {
        $edit = Subcategory::find($id);
        $catgory = Category::get();
        $data['categories'] = $catgory;
        $data['edit'] = $edit;
        return view('subcategory.edit',$data);
    }
    public function edit_save(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'category_name' => 'required|int',
            'status' => 'required',

        ]);
        if ($validator->fails()) {
            return redirect(route('subcategory_edit'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $edit = Subcategory::find($id);
        $edit->name = $data['name'];
        $edit->category_id = $data['category_name'];
        $edit->status = $data['status'];
        $edit->save();
        return redirect('/backend/admin/subcategory/list');
    }
    public function subcategory_delete($id)
    {
        $delete = Subcategory::find($id);
        $delete->status = 'inactive';
        $delete->save();
        return redirect('/backend/admin/subcategory/list');
    }

}
