<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\PaginationServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\Imports\ProductImport;
use App\Order;
use App\SubOrders;
use App\Refund;
use App\Payout;
use App\Payment;
use App\VendorPaymentAccount;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use PaytmWallet;
class OrderController extends Controller
{
    public function order(Request $request)
    {
        $data = $request->all();
        $search = $request->get('search');
        if($search != ''){
            $list = Order::where('tracking_number','like',"%{$search}%")
                            ->orWhere('order_id','like',"%{$search}%")
                            ->orWhere('status','like',"%{$search}%")
                            ->where('status','!=','pending_payment')
                            ->orderBy('id','DESC')
                            ->paginate('20');
        }else{
            $list = Order::where('status','!=','pending_payment')->orderBy('id','DESC')->paginate('20');
        }
        $data = [];
        $data['orders'] = $list;
        // $request->session()->put('my_name',$list);
        return view('order.list',$data)->with('search',$search);
    }
    public function detail(Request $request, $id)
    {
        $order = Order::with('subOrders')->with('users')->with('address')->where('orders.id',$id)->get()->first();

        $detail = Order::with('subOrders')->with('users')->with('address')
        ->Join('sub_orders', 'sub_orders.order_id', '=', 'orders.id')
        ->join('vendor','vendor.id','=','sub_orders.vendor_id')
        ->join('users','users.id','=','vendor.user_id')
        ->Join('products', 'products.id', '=', 'sub_orders.product_id')
        ->leftJoin('product_images', 'product_images.product_id', '=', 'products.id')
        ->leftJoin('product_prices', 'product_prices.product_id', '=', 'products.id')
        ->where('orders.id',$id)
        ->select('orders.*' ,'sub_orders.id as sId','sub_orders.vendor_id','sub_orders.status as subStatus','sub_orders.quantity','sub_orders.amount','users.*','products.product_name' ,'product_images.name','vendor.name as vName','vendor.id as vId','products.id as pId')
        ->orderBy('orders.id', 'DESC')
        ->groupBy('sub_orders.id')->get();

        $data = [];
        $data['order'] = $order;
        $data['infos'] = $detail;
        // $request->session()->put('my_name',$list);
        return view('order.detail',$data);
    }
    public function updateStatus(Request $request){
        $response = [];

        $response['status'] = 'error';
        $response['message'] = '';
        $data = $request->all();


        $orders = SubOrders::find($data['id']);
        $orders->status = $data['status'];
        $orders->save();

        $detail = SubOrders::with('payment')->where('id',$data['id'])->get()->first();

        if($data['status'] == 'cancelled'){
            $refund = new Refund();
            $refund->order_id = $detail->order_id;
            $refund->suborder_id = $detail->id;
            $refund->user_id = $detail->orders->user_id;
            $refund->product_id = $detail->product_id;
            $refund->price = $detail->amount;
            $refund->quantity = $detail->quantity;
            $refund->amount = ($detail->amount * $detail->quantity);
            $refund->is_refunded = '0';
            $refund->order_payment_id = $detail->orders->payment->id;
            // $refund->refund_transaction_id = $detail->payment->transaction_id;
            $refund->save();

        }

        if($data['status'] == 'delivered'){
            addToPayout($data['id']);
        }

        $response['status'] = 'success';
        $response['message'] = 'Status Updated Successfully Done';


        echo json_encode($response);

    }

    public function cancelByVendor(Request $request,$id){
        $subOrder= SubOrders::where('id',$id)->get()->first();
        $list = Order::with('users')->with('address')->where('id',$subOrder->order_id)->get();
        $info= SubOrders::with('product')->where('id',$id)->get()->first();

        $detail = SubOrders::leftJoin('product_prices', 'product_prices.product_id', '=', 'sub_orders.product_id')
        ->join('vendor','vendor.id','=','product_prices.vender_id')
        ->where('vendor.id','!=',$subOrder->vendor_id)
        ->where('sub_orders.id',$id)
        ->select('vendor.*')
        ->orderBy('vendor.id', 'DESC')
        ->groupBy('vendor.id')->get();


        return view('order.cancelByVendor')->with('sub',$subOrder)->with('suborder',$info)->with('list',$list)->with('details',$detail);
    }

    public function addSubOrder(Request $request){
        $data = $request->all();

        $securityCode = mt_rand(1000,9999);
        $subOrder = SubOrders::find($data['id']);
        $subOrder->status = 'transfer';
        $subOrder->save();

        $sub = new SubOrders();
        $sub->order_id = $data['order_id'];
        $sub->vendor_id = $data['vendor_id'];
        $sub->product_id = $data['product_id'];
        $sub->quantity = $data['quantity'];
        $sub->amount = $data['amounts'];
        $sub->product_attribute = $data['product_attribute'];
        $sub->security_code = $securityCode;
        $sub->status = 'pending';
        $sub->save();

        return redirect('/backend/admin/order/list');
    }

    public function refundList(Request $request){
        $refund = Refund::with('user')->with('product')->where('is_refunded',0)->paginate('20');
        return view('refund/list')->with('refunds',$refund);
    }

    public function refundPayView(Request $request ,$id){

        $refund = Refund::with('user')->with('product')->where('id',$id)->get()->first();
        return view('refund/view')->with('refund',$refund);
    }

    public function refundPayAdd(Request $request){
        $date = date("Y-m-d H:i:s");
        $data = $request->all();

        $payment = Payment::where('id',$data['transaction'])->get()->first();
        $refund = PaytmWallet::with('refund');
        $refund->prepare([
            'order' => $data['order_id'],
            'reference' => "refund-order-".$data['order_id'], // provide refund reference for your future reference (should be unique for each order)
            'amount' => $data['amount'], // refund amount
            'transaction' =>  $payment->transaction_id // provide paytm transaction id referring to this order
        ]);
        $refund->initiate();
        $response = $refund->response(); // To get raw response as array

        if($refund->isSuccessful()){
         // echo('Refund Successful');

          $refundData = Refund::find($data['refund_id']);
          $refundData->is_refunded = '1';
          $refundData->refund_transaction_id = $response['REFUNDID'];
          $refundData->refunded_at = $date;
          $refundData->amount = $data['amount'];
          $refundData->save();
          return redirect(route('refund_list'));
        }else if($refund->isFailed()){
          echo('Refund Failed');
          return redirect(route('refund_list'));
        }else if($refund->isOpen()){
          echo('Refund Open/Processing');
          return redirect(route('refund_list'));
        }else if($refund->isPending()){
          echo('Refund Pending');
          return redirect(route('refund_list'));
        }else{
            exit;
        }


    }

    public function payoutList(Request $request){
        $payout = Payout::with('user')->with('product')->where('is_paid','=','0')->paginate('20');
        return view('payout/list')->with('payouts',$payout);
    }

    public function payoutStatusAdd(Request $request){
        $data = $request->all();

        $payout = Payout::find($data['pay_id']);
        $payout->pay_status = $data['pay_status'];
        $payout->paid_at = date('Y-m-d H:i:s');
        $payout->save();

        return redirect('/backend/admin/payout/list');
    }
    public function payoutStatusView(Request $request ,$id){

        $payout = Payout::with('user')->with('product')->where('id',$id)->get()->first();
        return view('payout/view')->with('payout',$payout);
    }
    public function payoutPayView(Request $request ,$id){

        $payout = Payout::where('vendor_id',$id)->where('pay_status','1')->get();
        $paymentAccounts = VendorPaymentAccount::where('vendor_id',$id)->get();
        return view('payout/pay')->with('payouts',$payout)->with('paymentAccounts',$paymentAccounts);
    }

    public function payoutPayAdd(Request $request){
        $data = $request->all();

        $url = 'https://api.razorpay.com/v1/payouts';
        $ch = curl_init($url);
        $username = 'rzp_test_kDo8L2Su1nuZqW';
        $password = 'k73VtcnqxWDnI19B2GHgY4sH';

        $accontData = explode("?",$data['fa_account_number']);
        $fields = array();
        $fields["account_number"] = '2323230073245342';
        $fields["fund_account_id"] = $accontData[1];
        $fields["amount"] = $data['amount'] * 100;
        $fields["currency"] = 'INR';
        if($accontData[0] == 'bank_account'){
            $fields["mode"] = 'IMPS';
        }else{
            $fields["mode"] = 'UPI';
        }
        $fields["purpose"] = 'payout';
        $fields["queue_if_low_balance"] = true;
        $fields["reference_id"] = 'Acme Transaction ID 12345';
        $fields["narration"] = 'Acme Corp Fund Transfer';
        // $note = array(
        //     "notes_key_1" =>'Tea, Earl Grey, Hot',
        //     "notes_key_2"  => 'Tea, Earl Grey...'
        // );
        // $fields['notes'] = $note;
        print_r($fields);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type:application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);

        if ($result) {
            $dresult = json_decode($result, TRUE);
            foreach($data['payId'] as $payId){
                $new = Payout::find($payId);
                $new->transaction_id = $dresult['id'];
                $new->is_paid = 1 ;
                $new->save();
            }
        }
        return redirect(route('payout_list'));

    }

}
