<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'getproductattribute',
        'editproductattributevalue',
        'editvendorproductattributevalue',
        'productlist',
        'setpincode',
        '/user/search/ajax',
        '/update/image'
    ];
}
