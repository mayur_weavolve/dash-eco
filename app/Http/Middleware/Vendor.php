<?php
namespace App\Http\Middleware;
use Closure;
use Auth;
class Vendor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
	function handle($request, Closure $next)
	{
		if (Auth::check() && Auth::user()->type == 'user') {
			return redirect('/user/my-account');
		}
		elseif (Auth::check() && Auth::user()->type == 'vendor') {
            return $next($request);
		}
		elseif (Auth::check() && Auth::user()->type == 'admin') {
			return redirect('/home');
		}
		else {
            return redirect('/');
		}
	}
}
