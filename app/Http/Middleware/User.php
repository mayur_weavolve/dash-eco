<?php
namespace App\Http\Middleware;
use Closure;
use Auth;
class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
	function handle($request, Closure $next)
	{
		if (Auth::check() && Auth::user()->type == 'user') {
			return $next($request);
		}
		elseif (Auth::check() && Auth::user()->type == 'vendor') {
			return redirect('/vendor/home');
		}
		elseif (Auth::check() && Auth::user()->type == 'admin') {
			return redirect('/home');
		}
		else {
            return redirect('/');
		}
	}
}
