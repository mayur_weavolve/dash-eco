<?php
    use Illuminate\Support\Str;
    use URL as url;
    use App\SubOrders;
    use App\Payout;
    // function checkRole(){

    //     // $productPrices = App\ProductPrice::get();
    //     // foreach($productPrices as $price){
    //     //     $attributevalue = App\ProductAttributeValue::where('product_id',$price->product_id)->where('vendor_id',$price->vender_id)->get();
    //     //     foreach($attributevalue as $attrValue){
    //     //         $newPrice = App\ProductAttributeValue::find($attrValue->id);
    //     //         $newPrice->product_price_id = $price->id;
    //     //         $newPrice->save();
    //     //     }
    //     // }
    //    $url = url::current();
    //    $containAdmin = Str::contains($url, 'backend');
    //    $containVendor = Str::contains($url, 'vendor');
    //    $containUser = Str::contains($url, 'user');
    //    $user = Auth::user();
    // //    print_r($user); exit;

    //     if($user){

    //         if(Auth::user()->type == 'admin' && $containAdmin){
    //                return true;
    //         }else if(Auth::user()->type == 'vendor' && $containVendor){
    //             return true;
    //         }else if(Auth::user()->type == 'user' && $containUser){
    //             return true;
    //         }else if(Auth::user()->type == 'user' && ($containAdmin || $containVendor) ){
    //             $redirect = route('my_account');
    //             echo "<script>window.location.href='".$redirect."'</script>";
    //             return false;
    //         }else if(Auth::user()->type == 'vendor' && ($containAdmin || $containUser) ){
    //                 $redirect = route('vendor_home');
    //                 echo "<script>window.location.href='".$redirect."'</script>";
    //                 return false;
    //         }else if(Auth::user()->type == 'admin' && ($containVendor || $containUser) ){
    //             $redirect = route('welcome');
    //             echo "<script>window.location.href='".$redirect."'</script>";
    //             return false;
    //         }else{
    //             // $redirect = route('comingsoon');
    //             // echo "<script>window.location.href='".$redirect."'</script>";
    //             // return false;
    //         }

    //     }else{
    //         if($containAdmin || $containUser || $containVendor){
    //             $redirect = route('comingsoon');
    //             echo "<script>window.location.href='".$redirect."'</script>";
    //             return false;
    //         }else{
    //             return true;
    //         }
    //     }
    // }
    function usercart()
    {
        $dataDelete =DB::delete('delete from carts where created_at < (NOW() - INTERVAL 30 MINUTE)');
        $ipadd = $_SERVER["REMOTE_ADDR"];
        $macAdd =  substr(shell_exec('getmac'), 159,20);
        if(Auth::user()){
            $user = Auth::user();
            $userId = $user->id;
            $cartProducts = App\Cart::select('carts.*','carts.id as cartId','product_attribute_values.*','product_prices.*','carts.quantity as cartQauntity')
                                ->leftJoin('product_attribute_values', 'product_attribute_values.id', '=', 'carts.product_attribute_value_id')
                                ->leftJoin('product_prices', 'product_prices.id', '=', 'product_attribute_values.product_price_id')
                                ->where("carts.user_id", $userId)
                                ->orWhere('carts.ip_address',$ipadd)
                                ->orWhere('carts.mac_address',$macAdd)
                                ->orderBy('carts.id','desc')->get();
        }else{
            $userId = 0;
            $cartProducts = App\Cart::select('carts.*','carts.id as cartId','product_attribute_values.*','product_prices.*','carts.quantity as cartQauntity')
                ->leftJoin('product_attribute_values', 'product_attribute_values.id', '=', 'carts.product_attribute_value_id')
                ->leftJoin('product_prices', 'product_prices.id', '=', 'product_attribute_values.product_price_id')
                ->orWhere('carts.ip_address',$ipadd)
                ->orWhere('carts.mac_address',$macAdd)
                ->orderBy('carts.id','desc')->get();
        }

        if($cartProducts != null){
            return  $cartProducts;
        }else{
            return  [];
        }
        return [];
    }

    function  subtotal($userscartProducts){
        $total = 0;
        foreach($userscartProducts as $cartProduct){
            $itemPrice = $cartProduct->cartQauntity * $cartProduct->final_price;
            $total =  $total +  $itemPrice;
        }
        return $total;
    }

    function  getFilter($id = 0, $filter = 'category'){
        $pincode =   Session::get('pincode');
        if(Session::get('pincode') == ''){
            $latitude = '22.56986';
            $longitude = '72.963773';
        }else{
            $latitude = Session::get('latitude');
            $longitude = Session::get('longitude');
        }
        // echo $pincode.$latitude; exit;
        $minDistance = 0;
        $maxDistance = 30;
        $sqlDistance = DB::raw('( 6371 * acos( cos( radians(' . $latitude . ') )
            * cos( radians( vendor.latitude ) )
            * cos( radians( vendor.longitude )
            - radians(' . $longitude  . ') )
            + sin( radians(' . $latitude . ') )
            * sin( radians( vendor.latitude ) ) ) )');
        $products = App\Product::Join('product_attribute_values', 'product_attribute_values.product_id', '=', 'products.id')
            ->leftJoin('product_prices', 'product_prices.product_id', '=', 'products.id')
            ->leftJoin('product_subcategories', 'product_subcategories.product_id', '=', 'products.id')
            ->leftJoin('subcategories', 'subcategories.id', '=', 'product_subcategories.subcategory_id')
            ->Join('vendor', 'vendor.id', '=', 'product_prices.vender_id')
            ->where('product_attribute_values.quantity','>',0)
            // ->select("products.*")
            ->whereBetween($sqlDistance,[$minDistance,$maxDistance]);
            if($filter != '' && $id != 0){
                if($filter == 'category'){
                    $products->where('subcategories.category_id','=',$id);
                }
                if($filter == 'subcategory'){
                    $products->where('subcategories.id','=',$id);
                }
            }
            $colours = $products->select('product_attribute_values.colour')->where('product_attribute_values.colour','!=',null)->groupby('product_attribute_values.colour')->get();
            $storages = $products->select('product_attribute_values.storage')->where('product_attribute_values.storage','!=',null)->distinct('product_attribute_values.storage')->get();
            $measurements = $products->select('product_attribute_values.measurement')->where('product_attribute_values.measurement','!=',null)->distinct('product_attribute_values.measurement')->get();
            $weights = $products->select('product_attribute_values.weight')->where('product_attribute_values.weight','!=',null)->distinct('product_attribute_values.weight')->get();
            $sizes = $products->select('product_attribute_values.size')->where('product_attribute_values.size','!=',null)->distinct('product_attribute_values.size')->get();
            // $maximumPrice = $products->select('product_prices.final_price')->max('product_prices.final_price')->get();


           $data = [];
           $data['colours'] =  $colours;
           $data['storages'] =  $storages;
           $data['measurements'] =  $measurements;
           $data['weights'] =  $weights;
           $data['sizes'] =  $sizes;
        //    $data['maximumPrice'] =  $maximumPrice;
        //    print_r( $maximumPrice); exit;
           return $data;
        exit;
    }

    function  getSearchFilter($id = 0, $filter = 'category', $search){
        $pincode =   Session::get('pincode');
        if(Session::get('pincode') == ''){
            $latitude = '22.56986';
            $longitude = '72.963773';
        }else{
            $latitude = Session::get('latitude');
            $longitude = Session::get('longitude');
        }
        // echo $pincode.$latitude; exit;
        $minDistance = 0;
        $maxDistance = 30;
        $sqlDistance = DB::raw('( 6371 * acos( cos( radians(' . $latitude . ') )
            * cos( radians( vendor.latitude ) )
            * cos( radians( vendor.longitude )
            - radians(' . $longitude  . ') )
            + sin( radians(' . $latitude . ') )
            * sin( radians( vendor.latitude ) ) ) )');
        $products = App\Product::Join('product_attribute_values', 'product_attribute_values.product_id', '=', 'products.id')
            ->leftJoin('product_prices', 'product_prices.product_id', '=', 'products.id')
            ->leftJoin('product_subcategories', 'product_subcategories.product_id', '=', 'products.id')
            ->leftJoin('subcategories', 'subcategories.id', '=', 'product_subcategories.subcategory_id')
            ->leftJoin('product_hashtags', 'product_hashtags.product_id', '=', 'products.id')
            ->Join('vendor', 'vendor.id', '=', 'product_prices.vender_id')
            ->where('product_attribute_values.quantity','>',0)
            ->where('products.product_name','LIKE',"%".$search."%")
            ->where('product_hashtags.hashtag','LIKE',"%".$search."%")
            // ->select("products.*")
            ->whereBetween($sqlDistance,[$minDistance,$maxDistance]);
            if($filter != '' && $id != 0){
                if($filter == 'category'){
                    $products->where('subcategories.category_id','=',$id);
                }
                if($filter == 'subcategory'){
                    $products->where('subcategories.id','=',$id);
                }
            }
            $colours = $products->select('product_attribute_values.colour')->where('product_attribute_values.colour','!=',null)->groupby('product_attribute_values.colour')->get();
            $storages = $products->select('product_attribute_values.storage')->where('product_attribute_values.storage','!=',null)->distinct('product_attribute_values.storage')->get();
            $measurements = $products->select('product_attribute_values.measurement')->where('product_attribute_values.measurement','!=',null)->distinct('product_attribute_values.measurement')->get();
            $weights = $products->select('product_attribute_values.weight')->where('product_attribute_values.weight','!=',null)->distinct('product_attribute_values.weight')->get();
            $sizes = $products->select('product_attribute_values.size')->where('product_attribute_values.size','!=',null)->distinct('product_attribute_values.size')->get();
            // $maximumPrice = $products->select('product_prices.final_price')->max('product_prices.final_price')->get();


           $data = [];
           $data['colours'] =  $colours;
           $data['storages'] =  $storages;
           $data['measurements'] =  $measurements;
           $data['weights'] =  $weights;
           $data['sizes'] =  $sizes;
        //    $data['maximumPrice'] =  $maximumPrice;
        //    print_r( $maximumPrice); exit;
           return $data;
        exit;
    }

    function  getAvailableCategory(){
        $pincode =   Session::get('pincode');
        if(Session::get('pincode') == ''){
            $latitude = '22.56986';
            $longitude = '72.963773';
        }else{
            $latitude = Session::get('latitude');
            $longitude = Session::get('longitude');
        }
        $minDistance = 0;
        $maxDistance = 30;
        $sqlDistance = DB::raw('( 6371 * acos( cos( radians(' . $latitude . ') )
            * cos( radians( vendor.latitude ) )
            * cos( radians( vendor.longitude )
            - radians(' . $longitude  . ') )
            + sin( radians(' . $latitude . ') )
            * sin( radians( vendor.latitude ) ) ) )');
        $products = App\Product::Join('product_attribute_values', 'product_attribute_values.product_id', '=', 'products.id')
            ->Join('product_subcategories', 'product_subcategories.product_id', '=', 'products.id')
            ->Join('subcategories', 'subcategories.id', '=', 'product_subcategories.subcategory_id')
            ->Join('categories', 'categories.id', '=', 'subcategories.category_id')
            ->leftJoin('product_prices', 'product_prices.product_id', '=', 'products.id')
            ->Join('vendor', 'vendor.id', '=', 'product_prices.vender_id')
            ->where('product_attribute_values.quantity','>',0)
            ->whereBetween($sqlDistance,[$minDistance,$maxDistance])
            ->select('subcategories.name as subcategoryName','subcategories.id as subcategoryId','subcategories.category_id as subCategory_categoryId','categories.id as categoryId','categories.name as categoryName','categories.icon','categories.image')->get();

            $subcategories = [];
            $categories = [];
            $i = 0;
            $cat = [];
            $subcat = [];
            foreach($products as $product){
                if(!in_array($product->categoryId,$cat)){
                    array_push($cat,$product->categoryId);
                    $categories[$i]['id'] = $product->categoryId;
                    $categories[$i]['name'] = $product->categoryName;
                    $categories[$i]['icon'] = $product->icon;
                    $categories[$i]['image'] = $product->image;
                    $i++;
                }
            }
            $i = 0;
            foreach($products as $product){
                if(!in_array($product->subcategoryId,$subcat)){
                    array_push($subcat,$product->subcategoryId);
                    $subcategories[$product->categoryId][$i]['id'] = $product->subcategoryId;
                    $subcategories[$product->categoryId][$i]['name'] = $product->subcategoryName;
                    $i++;
                }
            }
           $data = [];
           $data['categories'] = $categories;
           $data['subcategories'] = $subcategories;
           return $data;
        exit;
    }


     function getDistance($vendor){

        $earthRadius = 6371000;
        // $earthRadius = 3259;
        $latitudeVendor = $vendor->latitude;
        $longitudeVendor = $vendor->longitude;

        $pincode = Session::get("pincode");
        $latitudeUser =  Session::get("latitude");
        $longitudeUser = Session::get("longitude");
        if($pincode == ''){
            $pincode = '388001';
        }
        if($latitudeUser == ''){
              $city = App\City::where('zipcode', $pincode)->get()->first();
              $latitudeUser = $city->latitude;
              $longitudeUser = $city->longitude;
        }

      // convert from degrees to radians
        $latFrom = deg2rad($latitudeVendor);
        $lonFrom = deg2rad($longitudeVendor);
        $latTo = deg2rad($latitudeUser);
        $lonTo = deg2rad($longitudeUser);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
            cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        $distanceInMeter = $angle * $earthRadius;
        return  round($distanceInMeter / 1000);

        exit;
    }

    function productHashtag(){
        // $pincode =   Session::get('pincode');
        // if(Session::get('pincode') == ''){
        //     $latitude = '22.56986';
        //     $longitude = '72.963773';
        // }else{
        //     $latitude = Session::get('latitude');
        //     $longitude = Session::get('longitude');
        // }
        // $minDistance = 0;
        // $maxDistance = 30;
        // $sqlDistance = DB::raw('( 6371 * acos( cos( radians(' . $latitude . ') )
        //     * cos( radians( vendor.latitude ) )
        //     * cos( radians( vendor.longitude )
        //     - radians(' . $longitude  . ') )
        //     + sin( radians(' . $latitude . ') )
        //     * sin( radians( vendor.latitude ) ) ) )');

            $pincode =   Session::get('pincode');
            if(Session::get('pincode') == ''){
                $latitude = '22.56986';
                $longitude = '72.963773';
            }else{
                $latitude = Session::get('latitude');
                $longitude = Session::get('longitude');
            }
            // echo $pincode.$latitude; exit;
            $minDistance = 0;
            $maxDistance = 30;
            $sqlDistance = DB::raw('( 6371 * acos( cos( radians(' . $latitude . ') )
                * cos( radians( vendor.latitude ) )
                * cos( radians( vendor.longitude )
                - radians(' . $longitude  . ') )
                + sin( radians(' . $latitude . ') )
                * sin( radians( vendor.latitude ) ) ) )');
            $hastags = App\Product::Join('product_attribute_values', 'product_attribute_values.product_id', '=', 'products.id')
                ->leftJoin('product_prices', 'product_prices.product_id', '=', 'products.id')
                ->leftJoin('product_subcategories', 'product_subcategories.product_id', '=', 'products.id')
                ->leftJoin('subcategories', 'subcategories.id', '=', 'product_subcategories.subcategory_id')
                ->leftJoin('product_hashtags', 'product_hashtags.product_id', '=', 'products.id')
                ->Join('vendor', 'vendor.id', '=', 'product_prices.vender_id')
                ->where('product_attribute_values.quantity','>',0)
                // ->where('products.product_name','LIKE',"%".$search."%")
                // ->where('product_hashtags.hashtag','LIKE',"%".$search."%")
                // ->select("products.*")
                ->whereBetween($sqlDistance,[$minDistance,$maxDistance])
                ->select('product_hashtags.hashtag')
                ->groupBy('product_hashtags.hashtag')
                ->inRandomOrder()
                ->skip(0)->take(7)->get();

    //    $hastags = App\ProductHashtag::Join('products', 'products.id', '=', 'product_hashtags.product_id')
    //                 ->leftJoin('product_prices', 'product_prices.product_id', '=', 'products.id')
    //                 ->rightJoin('product_attribute_values', 'product_attribute_values.product_price_id', '=', 'product_prices.id')
    //                 ->rightJoin('vendor', 'vendor.id', '=', 'product_prices.vender_id')
    //                 ->whereBetween($sqlDistance,[$minDistance,$maxDistance])
    //                 ->where('product_attribute_values.quantity','>','0')
    //                 ->select('product_hashtags.hashtag')
    //                 ->groupBy('product_hashtags.hashtag')
    //                 ->inRandomOrder()
    //                 ->skip(0)->take(7)->get();
        return  $hastags;
    }
    function addToPayout($id)
    {

        $detail = SubOrders::with('payment')->where('id',$id)->get()->first();

        $payout = new Payout();
        $payout->order_id = $detail->order_id;
        $payout->suborder_id = $detail->id;
        $payout->user_id = $detail->orders->user_id;
        $payout->vendor_id = $detail->vendor_id;
        $payout->product_id = $detail->product_id;
        $payout->price = $detail->amount;
        $payout->quantity = $detail->quantity;
        $payout->amount = ($detail->amount * $detail->quantity);
        $payout->is_paid = '0';
        // $payout->transaction_id = $detail->payment->transaction_id;
        $payout->save();


    }
