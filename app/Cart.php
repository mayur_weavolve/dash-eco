<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Auth;
class Cart extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'vendor_id', 'mac_address','ip_address','product_id','quantity'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    public function product()
    {
        return $this->hasOne('App\Product','id','product_id');
    }

    public function productPrice()
    {
        return $this->hasMany('App\ProductPrice','product_id','product_id');
    }

    public function productAttributeValue(){
        return $this->hasOne('App\ProductAttributeValue','id','product_attribute_value_id');
    }

    public function attribute(){
        return $this->hasmany('App\ProductAttribute','product_id','product_id');
    }

    public function vendor()
    {
        return $this->hasOne('App\Vendor','id','vendor_id');
    }
    public function getDistance($vendor){
        $earthRadius = 6371000;
        $latitudeVendor = $vendor->latitude;
        $longitudeVendor = $vendor->longitude;
        if(Auth::user()){
           $user = Auth::user();
           $latitudeUser = $user->latitude;
           $longitudeUser = $user->longitude;
        }else{
            $pincode = session("pincode");
            if($pincode == ''){
                $pincode = '388001';
            }
            $city = City::where('zipcode', $pincode)->get()->first();
            $latitudeUser = $city->latitude;
            $longitudeUser = $city->longitude;
        }
       // convert from degrees to radians
        $latFrom = deg2rad($latitudeVendor);
        $lonFrom = deg2rad($longitudeVendor);
        $latTo = deg2rad($latitudeUser);
        $lonTo = deg2rad($longitudeUser);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
            cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        $distanceInMeter = $angle * $earthRadius;
        // echo  round($distanceInMeter / 1000);
        return  round($distanceInMeter / 1000);

        exit;
    }



}
