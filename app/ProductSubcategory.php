<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSubcategory extends Model
{
    protected $fillable = [
        'product_id','subcategory_id'
    ];

    public $table = "product_subcategories";

    public function subcategory()
    {
        return $this->hasOne('App\Subcategory','id','subcategory_id');
    }

}
